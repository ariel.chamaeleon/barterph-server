﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AppStandardLibrary;
using AppStandardLibrary.Repository;
using AppStandardLibrary.Services;
using AppStandardLibrary.Services.Interface;
using MobileNetStandardLibrary.Commands;
using MobileNetStandardLibrary.DTO;

namespace MobileNetStandardLibrary.Controller
{
    public class BaseViewPresenter
    {
        public IAccountService accountService;
        public IUser userService;
        public IAccountDatabase database;

        public BaseViewPresenter(IAccountService accountService, IUser userService, IAccountDatabase accountDatabase)
        {
            this.userService = userService;
            this.accountService = accountService;
            this.database = accountDatabase;
        }

        public async Task<Response<ItemDTO>> EditBarter(CreateBartersCommand command) {
            var response = await accountService.EditBarter(command);
            return response;
        }

        public async Task<Response<bool>> DeleteComment(long id) {
            var response = await accountService.DeleteComment(id);
            return response;
        }

        public async Task<Response<bool>> RepostBarter(long barterId)
        {
            var response = await accountService.RepostBarter(barterId);
            return response;
        }

        public async Task<Response<bool>> UpdateReminderSettings(long barterId, int days) {
            var response = await accountService.UpdateReminderSettings(barterId, days);
            return response;
        }

        public async Task<Response<bool>> CancelBarter(long itemId) {
            var response = await accountService.CancelBarter(itemId);
            return response;
        }

        public async Task<Response<NotificationList>> GetNotifications(NotificationCommand command)
        {
            var response = await accountService.GetNotifications(command);
            return response;
        }

        public async Task<Response<ForgotPasswordDTO>> ResetPasswordInit(string mobileNumber) {
            var response = await accountService.ResetPasswordInit(mobileNumber);
            return response;
        }

        public async Task<Response<UserDTO>> ResetPasswordFinish(ChangePasswordCommand mobileNumber) {
            var response = await accountService.ResetPasswordFinish(mobileNumber);
            return response;
        }


        public async Task<Response<bool>> BarterComplete(long barterId)
        {
            var response = await accountService.BarterComplete(barterId);
            return response;
        }

        public async Task<Response<CommentData>> PostComment(CommentCommand command)
        {
            var response = await accountService.PostComment(command);
            return response;
        }

        public async Task<Response<CommentDTO>> GetComments(GetCommentCommand command)
        {
            var response = await accountService.GetComments(command);
            return response;
        }


        public async Task<Response<CommentData>> GetCommentsSub(GetCommentCommand command, long parentId) {
            var response = await accountService.GetCommentsSub(command, parentId);
            return response;
        }

        public async Task<Response<CommentData>> PostOffer(OfferCommand command)
        {
            var response = await accountService.PostOffer(command);
            return response;
        }

        public async Task<Response<CommentDTO>> GetOffers(GetCommentCommand command)
        {
            var response = await accountService.GetOffers(command);
            return response;
        }


        public async Task<Response<CommentData>> GetOffersSub(GetCommentCommand command, long parentId)
        {
            var response = await accountService.GetOffersSub(command, parentId);
            return response;
        }

        public async Task<Response<string>> UploadPhoto(byte[] image)
        {
            var response = await accountService.UploadPhoto(image);
            return response;
        }

        public async Task<Response<ItemDTO>> GetItem(long id) {
            var response = await accountService.GetItem(id);
            return response;
        }

        public async Task<Response<ItemDTO>> PostItem(CreateBartersCommand barter)
        {
            var response = await accountService.CreateBarters(barter);
            return response;
        }

        public async Task<Response<UserDTO>> Authenticate(LoginRegistrationCommand command)
        {
            var response = await accountService.Authenticate(command);
            return response;
        }

        public async Task<Response<UserDTO>> FacebookAuth(string accessToken)
        {
            var response = await accountService.FacebookAuth(accessToken);
            return response;
        }

        public async Task<Response<UserDTO>> GoogleAuth(string accessToken)
        {
            var response = await accountService.GoogleAuth(accessToken);
            return response;
        }

        public async Task<Response<ItemList>> GetItems(SearchCommand command)
        {
            var response = await accountService.GetItems(command);
            return response;
        }

        public async Task<Response<UserDTO>> RegisterUser(LoginRegistrationCommand command)
        {
            var response = await accountService.RegisterUser(command);
            return response;
        }

        public async Task<Response<UserDTO>> ActivateUser(ActivateAccountCommand command)
        {
            var response = await accountService.ActivateRegistration(command);
            return response;
        }

        public async Task<Response<UserDTO>> UpdateProfile(UpdateUserCommand command)
        {
            var response = await accountService.UpdateProfile(command);
            return response;
        }

        public async Task<Response<UserDTO>> ChangePassword(string currentPassword, string newPassword)
        {
            var response = await accountService.ChangePassword(currentPassword, newPassword);
            return response;
        }
    }
}
