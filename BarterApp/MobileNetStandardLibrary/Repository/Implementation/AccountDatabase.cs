﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using SQLite;
using AppStandardLibrary.Model;
using AppStandardLibrary;

namespace AppStandardLibrary.Repository
{
    public class AccountDatabase : IAccountDatabase
    {
        readonly SQLiteAsyncConnection database;

        public AccountDatabase(IFileHelper fileHelper)
        {
            database = new SQLiteAsyncConnection(fileHelper.GetLocalFilePath("barter.db3"));
        }

        public async Task<bool> DeleteUser(User user)
        {
            return await database.DeleteAsync(user) > 0;
        }

        public async Task<User> GetUserAsync()
		{
            await database.CreateTableAsync<User>();
			return await database.Table<User>().FirstOrDefaultAsync();
		}

		public async Task<int> SaveUserAsync(User item)
		{
            return await database.InsertOrReplaceAsync(item);
		}

    }
}
