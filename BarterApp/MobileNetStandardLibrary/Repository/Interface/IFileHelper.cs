﻿using System;
namespace AppStandardLibrary
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string filename);
    }
}
