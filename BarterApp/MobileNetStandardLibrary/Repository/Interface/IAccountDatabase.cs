﻿using System;
using System.Threading.Tasks;
using AppStandardLibrary.Model;

namespace AppStandardLibrary.Repository
{
    public interface IAccountDatabase
    {
        Task<int> SaveUserAsync(User item);

        Task<User> GetUserAsync();

        Task<bool> DeleteUser(User user);
    }
}
