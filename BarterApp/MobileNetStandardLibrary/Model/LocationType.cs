﻿using System;
namespace MobileNetStandardLibrary.Model
{
    public enum LocationType
    {
        STRICT, OPEN
    }

    public enum DeliveryOption
    {
        MEETUP, COURIER
    }
}
