﻿using System;
namespace MobileNetStandardLibrary.Model
{
    public class FilterModel
    {
        public string FilterName { get; set; }
        public bool IsChecked { get; set; }
    }
}
