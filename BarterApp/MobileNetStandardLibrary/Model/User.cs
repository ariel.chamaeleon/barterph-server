﻿using System;
using AppStandardLibrary.Services.Interface;
using Newtonsoft.Json;
using SQLite;

namespace AppStandardLibrary.Model
{
    public class User
    {
        [JsonIgnore]
        [PrimaryKey]
        public long id { get; set; }
        //public string Password { get; set; }
        public string token { get; set; }
        public string imageUrl { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string mobileNo { get; set; }
        public string activated { get; set; }
        public DateTime? createdDate { get; set; }
        public DateTime? lastModifiedDate { get; set; }

        public bool IsFacebook { get; set; }
        public bool IsGoogle { get; set; }

        public long locationId { get; set; }
        public string location { get; set; }
        public string country { get; set; }
        public string province { get; set; }
        public string city { get; set; }
        public string barangay { get; set; }
    }

    public class Location{
        public long id { get; set; }
        public string location { get; set; }
        public string country { get; set; }
        public string province { get; set; }
        public string city { get; set; }
        public string barangay { get; set; }
    }


    public class Response {

        [JsonProperty("success")]
        public string Success { get; set; }
        [JsonProperty("error")]
        public string Error { get; set; }

    }

}
