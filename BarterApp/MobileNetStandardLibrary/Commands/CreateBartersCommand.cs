﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace MobileNetStandardLibrary.Commands
{
    public class CreateBartersCommand
    {
        public long? id;
        public string item;
        public string locationType;
        [JsonProperty("location.location")]
        public string location;
        [JsonProperty("location.city")]
        public string city;
        [JsonProperty("location.province")]
        public string province;
        [JsonProperty("location.barangay")]
        public string barangay;
        [JsonProperty("location.houseNumber")]
        public string houseNumber;
        [JsonProperty("location.street")]
        public string street;

        public string courier;

        public string deliveryType;
        [JsonProperty("deliveryLocation.location")]
        public string deliveryLocation;
        [JsonProperty("deliveryLocation.city")]
        public string deliveryCity;
        [JsonProperty("deliveryLocation.province")]
        public string deliveryProvince;

        public string deliveryCountry;
        public string deliveryBarangay;
        public string deliveryHouseNumber;
        public string deliveryStreet;

        public string reason;
        public string preferences;
        public string contactNumber;

        [JsonIgnore]
        public List<string> photos;
    }
}
