﻿using System;
namespace MobileNetStandardLibrary.Commands
{
    public class GetCommentCommand
    {
        public int page { get; set; }
        public int pageSize { get; set; }
        public int sort { get; set; }
        public long barterId { get; set; }
    }
}
