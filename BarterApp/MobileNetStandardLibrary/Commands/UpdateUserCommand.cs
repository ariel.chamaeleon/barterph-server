﻿using System;
using Newtonsoft.Json;

namespace MobileNetStandardLibrary.Commands
{
    public class UpdateUserCommand
    {
        public string firstName;
        public string lastName;
        [JsonProperty("location.city")]
        public string city;
        [JsonProperty("location.country")]
        public string country;
        [JsonProperty("location.location")]
        public string location;
        [JsonProperty("location.province")]
        public string province;
        [JsonProperty("location.barangay")]
        public string barangay;
        public string imageUrl;
    }
}
