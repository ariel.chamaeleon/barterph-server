﻿using System;
namespace MobileNetStandardLibrary.Commands
{
    public class PageCommand
    {
        public int page { get; set; }
        public int pageSize { get; set; }
        public int sort { get; set; }
        public string key { get; set; }
    }

    public class SearchCommand : PageCommand
    {
        public int? withComments { get; set; }
        public int? withOffers { get; set; }
        public string status { get; set; }
        public long? userId { get; set; }
    }

    public enum ItemStatus {
        ACTIVE, COMPLETED, CANCELLED
    }

    public class NotificationCommand : PageCommand
    {
        public long? userId { get; set; }
        public long? receiverId { get; set; }
    }
}
