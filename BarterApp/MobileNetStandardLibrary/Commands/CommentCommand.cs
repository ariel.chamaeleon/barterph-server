﻿using System;
using System.Collections.Generic;

namespace MobileNetStandardLibrary.Commands
{
    public class CommentCommand
    {
        public long barterId { get; set; }
        public long? parentCommentId { get; set; }
        public long? offerId { get; set; }
        public string comment { get; set; }
    }

    public class OfferCommand
    {
        public long barterId { get; set; }
        public string note { get; set; }
        public long offerId { get; set; }
        public string comment { get; set; }
        public string item { get; set; }
        public string description { get; set; }
        public List<string> photos { get; set; }
    }

}
