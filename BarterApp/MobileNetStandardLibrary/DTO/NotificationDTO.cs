﻿using System;
using System.Collections.Generic;

namespace MobileNetStandardLibrary.DTO
{
    public class NotificationList
    {
        public int page;
        public int pageSize;
        public int totalPage;
        public int totalCount;
        public List<NotificationDTO> list;
    }

    public class NotificationDTO
    {
        public int id { get; set; }
        public string title { get; set; }
        public string subTitle { get; set; }
        public string type { get; set; }
        public DateTime? datePosted { get; set; }
        public UserDTO receiver { get; set; }
        public UserDTO sender { get; set; }
        public ItemDTO barter { get; set; }
        public CommentData comment { get; set; }
        public CommentData offer { get; set; }
        public bool read { get; set; }
    }
}
