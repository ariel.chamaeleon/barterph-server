﻿using System;
using System.Collections.Generic;
using AppStandardLibrary.Model;
using MobileNetStandardLibrary.Model;

namespace MobileNetStandardLibrary.DTO
{
    public class ItemList {
        public int page;
        public int pageSize;
        public int totalPage;
        public int totalCount;
        public List<ItemDTO> list;
    }

    public class ItemDTO
    {
        public long id;
        public UserDTO user;
        public string Item;
        public LocationType? locationType;
        public DeliveryOption? deliveryType;
        public Location location;
        public string reason;
        public string courier;
        public string preferences;
        public string contactNumber;
        public int offerCount;
        public int commentCount;
        public DateTime? datePosted;
        public string status;
        public DateTime? lastUpdateDate;
        public List<string> photos;
    }
}
