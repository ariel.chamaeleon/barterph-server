﻿using System;
using AppStandardLibrary;
using AppStandardLibrary.Model;

namespace MobileNetStandardLibrary.DTO
{
    public class UserDTO
    {
        public long id { get; set; }
        //public string Password { get; set; }
        public string token { get; set; }
        public string imageUrl { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string mobileNo { get; set; }
        public string activated { get; set; }
        public DateTime? createdDate { get; set; }
        public DateTime? lastModifiedDate { get; set; }
        public Location location { get; set; }
        //private string photoLink;
        //public string UserPhotoLink { get { return string.IsNullOrEmpty(photoLink) ? string.Empty : photoLink.Trim().StartsWith("http") ? photoLink.Replace("-thumbnail", "").Replace("-mini", "").Replace(".jpg", "-thumbnail.jpg") : URLPath.BaseURL + "" + photoLink.Replace("-thumbnail", "").Replace("-mini", "").Replace(".jpg", "-thumbnail.jpg"); } set { photoLink = value; } }
        //public string UserPhotoMini { get { return string.IsNullOrEmpty(photoLink) ? string.Empty : photoLink.Trim().StartsWith("http") ? photoLink.Replace("-thumbnail", "").Replace("-mini", "").Replace(".jpg", "-mini.jpg") : URLPath.BaseURL + "" + photoLink.Replace("-thumbnail", "").Replace("-mini", "").Replace(".jpg", "-mini.jpg"); } set { photoLink = value; } }
        //public string MobileNumber { get; set; }
        //public string FirstName { get; set; }
        //public string LastName { get; set; }
    }
}
