﻿using System;
using System.Collections.Generic;

namespace MobileNetStandardLibrary.DTO
{
    public class CommentDTO
    {
        public List<CommentData> list;
        public int currentPage;
        public int pageSize;
        public int totalPages;
        public int totalCount;
    }

    public class CommentData
    {
        public long id;
        public string comment;
        public string description;
        public string item;
        public List<string> photos;
        public UserDTO user;
        public string parentCommentId;
        public int repliesCount;
        public List<CommentData> replies;
        public DateTime? datePosted;
    }

}
