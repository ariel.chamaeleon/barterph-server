﻿using System;
using AppStandardLibrary.Model;
using MobileNetStandardLibrary.DTO;

namespace AppStandardLibrary
{
    public static class ModelMapper
    {
        public static User MapUserDTO(UserDTO dto, User currentUser)
        {
            User user = new User();
            user.id = dto.id;
            user.token = dto.token;
            user.mobileNo = dto.mobileNo;
            user.createdDate = dto.createdDate;
            user.lastModifiedDate = dto.lastModifiedDate;
            user.activated = dto.activated;
            user.firstName = dto.firstName;
            user.lastName = dto.lastName;
            user.imageUrl = dto.imageUrl;
            if (dto.location != null)
            {
                user.locationId = dto.location.id;
                user.location = dto.location.location;
                user.city = dto.location.city;
                user.country = dto.location.country;
                user.province = dto.location.province;
                user.barangay = dto.location.barangay;
            }
            if (currentUser != null) {
                user.IsFacebook = currentUser.IsFacebook;
                user.IsGoogle = currentUser.IsGoogle;
            }
            return user;
        }
    }
}
