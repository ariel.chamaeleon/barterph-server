﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AppStandardLibrary.Model;
using AppStandardLibrary.Services;
using MobileNetStandardLibrary.Commands;
using MobileNetStandardLibrary.DTO;

namespace AppStandardLibrary
{
	public interface IAccountService
	{
        Task<bool> CheckIfUserExist();
        Task<Response<UserDTO>> RegisterUser(LoginRegistrationCommand command);
        Task<Response<ForgotPasswordDTO>> ResetPasswordInit(string mobileNumber);
        Task<Response<bool>> CancelBarter(long itemId);
        Task<Response<UserDTO>> ResetPasswordFinish(ChangePasswordCommand mobileNumber);
        Task<Response<UserDTO>> ChangePassword(string currentPassword, string newPassword);
        Task<Response<UserDTO>> Authenticate(LoginRegistrationCommand command);
        Task<Response<UserDTO>> ActivateRegistration(ActivateAccountCommand command);
        Task<Response<UserDTO>> UpdateProfile(UpdateUserCommand command);
        Task<Response<UserDTO>> FacebookAuth(string accessToken);
        Task<Response<UserDTO>> GoogleAuth(string accessToken);
        Task<Response<string>> UploadPhoto(byte[] image);
        Task<Response<ItemDTO>> CreateBarters(CreateBartersCommand command);
        Task<Response<ItemDTO>> EditBarter(CreateBartersCommand command);

        Task<Response<bool>> RepostBarter(long barterId);
        Task<Response<bool>> UpdateReminderSettings(long barterId, int days);

        Task<Response<bool>> UpdateFCMToken(string token);

        Task<Response<bool>> DeleteComment(long id);
        Task<Response<CommentData>> PostComment(CommentCommand command);
        Task<Response<CommentDTO>> GetComments(GetCommentCommand comment);
        Task<Response<CommentData>> GetCommentsSub(GetCommentCommand command, long parentId);

        Task<Response<CommentData>> PostOffer(OfferCommand command);
        Task<Response<CommentDTO>> GetOffers(GetCommentCommand comment);
        Task<Response<CommentData>> GetOffersSub(GetCommentCommand command, long parentId);
        Task<Response<bool>> BarterComplete(long barterId);

        Task<Response<ItemDTO>> GetItem(long id);
        Task<Response<ItemList>> GetItems(SearchCommand command);

        Task<Response<NotificationList>> GetNotifications(NotificationCommand command);
    }
}
