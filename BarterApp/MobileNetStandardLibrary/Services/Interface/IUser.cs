﻿using System;
using AppStandardLibrary.Model;

namespace AppStandardLibrary.Services.Interface
{
    public interface IUser
    {
        User CurrentUser { get; set; }
    }
}
