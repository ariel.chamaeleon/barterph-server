﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace AppStandardLibrary
{
	public interface IRestService
	{
		HttpClient client { get; }

		string EncodePostData(string data);

		string EncodeUrlData(object value);

        string CreateGetURL(string url, object command);

		StringContent GetPostContent(object data);

	}
}
