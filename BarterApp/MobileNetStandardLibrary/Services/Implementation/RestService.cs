﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AppStandardLibrary
{
	public class RestService : IRestService
	{
		HttpClient client;

		public RestService()
		{
			client = new HttpClient();
			// client.DefaultRequestHeaders.Add("Accept", "application/vnd.planetreviews.api.v1.0+json");
			client.DefaultRequestHeaders.Add("DeviceId", MobileNetStandardLibrary.Model.Device.DeviceId);
			client.Timeout = TimeSpan.FromSeconds(60);
		}

		HttpClient IRestService.client
		{
			get
			{
				return client;
			}
		}

		public string EncodePostData(string data)
		{
			if (!string.IsNullOrEmpty(data))
			{
				if (((data.StartsWith("[") && data.EndsWith("]"))
					|| (data.StartsWith("{") && data.EndsWith("}"))))
					return data;
				return Uri.EscapeDataString(data);
			}
			return data;
		}


        public string CreateGetURL(string url, object command){
            
            if (!url.Contains("?"))
                url = url + "?";

            Dictionary<string, object> properties;

            properties = new Dictionary<string, object>();
            Type myType = command.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetRuntimeProperties());
            foreach (PropertyInfo p in props)
            {
                if(p.GetValue(command, null) != null)
                {
                    properties.Add(p.Name, p.GetValue(command, null));    
                }
            }

            //foreach (var prop in ReflectionHelper.GetPropertyValues(command))
            //{
            //    if (prop.CurrentValue != null)
            //        properties.Add(prop.Name, prop.CurrentValue);
            //}

            foreach (var prop in properties)
            {
                url += string.Format("&{0}={1}", System.Uri.EscapeDataString(prop.Key), System.Uri.EscapeDataString(EncodeUrlData(prop.Value)));
            }

            System.Diagnostics.Debug.WriteLine("GET:"+ url);

            return url;
        }

		public string EncodeUrlData(object value)
		{
			if (value == null) return string.Empty;
			DateTime? dtValue = null;
			if (value is DateTime)
				dtValue = (DateTime)value;
			if (!dtValue.HasValue)
			{
				var ntype = Nullable.GetUnderlyingType(value.GetType());
				if (ntype != null && ntype.FullName == "System.DateTime")
					dtValue = (DateTime?)value;
			}
			if (dtValue.HasValue)
				return dtValue.Value.ToString("yyyy-MM-dd HH:mm:ss");
			return value.ToString();
		}

		public StringContent GetPostContent(object data)
		{
            var lcPostData = JsonConvert.SerializeObject(data);
            System.Diagnostics.Debug.WriteLine(lcPostData);
			return new StringContent(lcPostData, Encoding.UTF8, "application/json");
		}


	}
}
