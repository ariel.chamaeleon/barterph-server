﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using AppStandardLibrary.Repository;
using AppStandardLibrary.Services;
using AppStandardLibrary.Services.Implementation;
using AppStandardLibrary.Services.Interface;
using MobileNetStandardLibrary.DTO;
using MobileNetStandardLibrary;
using MobileNetStandardLibrary.Commands;

namespace AppStandardLibrary
{
    public class AccountService : BaseService, IAccountService
    {
        public AccountService(IRestService restService, IAccountDatabase database, IUser userService) : base(restService, database, userService)
        {
        }

        public async Task<bool> CheckIfUserExist()
        {
            userService.CurrentUser = await accountDatabase.GetUserAsync();
            return (userService.CurrentUser != null);
        }

        public async Task<Response<bool>> DeleteComment(long id) {
            Response<bool> responseData = new Response<bool>();
            try
            {
                var uri = new Uri(URLPath.GetComments + "?id=" + id);

                var request = new HttpRequestMessage(HttpMethod.Delete, uri);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    responseData.Data = true;
                    responseData.Success = true;
                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"{HttpMethod.Get.ToString()}:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception ex)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<ForgotPasswordDTO>> ResetPasswordInit(string mobileNumber) {
            Response<ForgotPasswordDTO> responseData = new Response<ForgotPasswordDTO>();
            try
            {
                var uri = new Uri(URLPath.ResetPasswordInit);
                var dictionary = new { mobileNo = mobileNumber }.ToKeyValue();

                var request = new HttpRequestMessage(HttpMethod.Post, uri);
                request.Content = new FormUrlEncodedContent(dictionary);

                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    var jo = JObject.Parse(stringContent);
                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");
                    var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ForgotPasswordDTO>(stringContent);
                    if (parsedResponse != null)
                    {
                        responseData.Data = parsedResponse;
                        responseData.Success = true;
                    }
                    else
                    {
                        responseData.Success = false;
                    }
                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"{HttpMethod.Get.ToString()}:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception ex)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<UserDTO>> ResetPasswordFinish(ChangePasswordCommand mobileNumber) {
            Response<UserDTO> responseData = new Response<UserDTO>();
            try
            {
                var uri = new Uri(URLPath.ResetPasswordFinish);
                var dictionary = mobileNumber.ToKeyValue();

                var request = new HttpRequestMessage(HttpMethod.Post, uri);
                request.Content = new FormUrlEncodedContent(dictionary);

                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    var jo = JObject.Parse(stringContent);
                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");
                    var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<UserDTO>(stringContent);
                    if (parsedResponse != null)
                    {
                        responseData.Data = parsedResponse;
                        responseData.Success = true;
                        userService.CurrentUser = ModelMapper.MapUserDTO(responseData.Data, userService.CurrentUser);
                        await accountDatabase.SaveUserAsync(userService.CurrentUser);
                    }
                    else
                    {
                        responseData.Success = false;
                    }
                   
                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"{HttpMethod.Get.ToString()}:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception ex)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<ItemDTO>> GetItem(long id)
        {
            Response<ItemDTO> responseData = new Response<ItemDTO>();
            try
            {
                var uri = new Uri(URLPath.CreateBarters + "/" + id);

                var request = new HttpRequestMessage(HttpMethod.Get, uri);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userService.CurrentUser.token);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");
                    var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ItemDTO>(stringContent);
                    if (parsedResponse != null)
                    {
                        responseData.Data = parsedResponse;
                        responseData.Success = true;
                    }
                    else
                    {
                        responseData.Success = false;
                    }
                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"URI:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception ex)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<ItemDTO>> EditBarter(CreateBartersCommand command)
        {
            Response<ItemDTO> responseData = new Response<ItemDTO>();
            try
            {
                var uri = new Uri(URLPath.EditBarter);
                var dictionary = command.ToKeyValue();

                if (command.photos != null)
                {
                    for (int x = 0; x < command.photos.Count; x++)
                    {
                        dictionary.Add(new KeyValuePair<string, string>($"Photos[{x}]", $"{command.photos[x]}"));
                    }
                }
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userService.CurrentUser.token);
                var request = new HttpRequestMessage(HttpMethod.Post, uri);
                request.Content = new FormUrlEncodedContent(dictionary);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");
                    responseData.Data = null;
                    responseData.Success = true;
                    //var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ItemDTO>(stringContent);
                    //if (parsedResponse != null)
                    //{
                    //    responseData.Data = parsedResponse;
                    //    responseData.Success = true;
                    //}
                    //else
                    //{
                    //    responseData.Success = false;
                    //}
                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"URI:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception ex)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<ItemDTO>> CreateBarters(CreateBartersCommand command) {
            Response<ItemDTO> responseData = new Response<ItemDTO>();
            try
            {
                var uri = new Uri(URLPath.CreateBarters);
                var dictionary = command.ToKeyValue();

                if (command.photos != null)
                {
                    for (int x = 0; x < command.photos.Count; x++)
                    {
                        dictionary.Add(new KeyValuePair<string, string>($"Photos[{x}]", $"{command.photos[x]}"));
                    }
                }
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userService.CurrentUser.token);
                var request = new HttpRequestMessage(HttpMethod.Post, uri);
                request.Content = new FormUrlEncodedContent(dictionary);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");
                    var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ItemDTO>(stringContent);
                    if (parsedResponse != null)
                    {
                        responseData.Data = parsedResponse;
                        responseData.Success = true;
                    }
                    else
                    {
                        responseData.Success = false;
                    }
                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"URI:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception ex)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<CommentDTO>> GetComments(GetCommentCommand command)
        {
            Response<CommentDTO> responseData = new Response<CommentDTO>();
            try
            {
                var url = string.Format(restService.CreateGetURL(URLPath.GetComments, command));
                var uri = new Uri(url);
                var request = new HttpRequestMessage(HttpMethod.Get, uri);
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", userService.CurrentUser.token);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);
                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    var jo = JObject.Parse(stringContent);
                    if (jo != null)
                    {
                        var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<CommentDTO>(jo.ToString());
                        if (parsedResponse != null)
                        {
                            responseData.Data = parsedResponse;
                            responseData.Success = true;
                        }
                        else
                        {
                            responseData.ErrorMessage = jo["error"]?.ToString();
                            responseData.Success = false;
                        }
                    }
                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");

                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"{HttpMethod.Get.ToString()}:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<CommentData>> GetCommentsSub(GetCommentCommand command, long parentId)
        {
            Response<CommentData> responseData = new Response<CommentData>();
            try
            {
                var url = URLPath.GetComments + "/" + parentId;
                var uri = new Uri(url);
                var request = new HttpRequestMessage(HttpMethod.Get, uri);
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", userService.CurrentUser.token);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);
                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    var jo = JObject.Parse(stringContent);
                    if (jo != null)
                    {
                        var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<CommentData>(stringContent);
                        if (parsedResponse != null)
                        {
                            responseData.Data = parsedResponse;
                            responseData.Success = true;
                        }
                        else
                        {
                            responseData.ErrorMessage = jo["error"]?.ToString();
                            responseData.Success = false;
                        }
                    }
                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");

                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"{HttpMethod.Get.ToString()}:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }


        public async Task<Response<CommentData>> PostComment(CommentCommand command)
        {
            Response<CommentData> responseData = new Response<CommentData>();
            try
            {
                var uri = new Uri(URLPath.PostComment);
                var dictionary = command.ToKeyValue();

                var request = new HttpRequestMessage(HttpMethod.Post, uri);
                foreach (var value in dictionary.Values)
                {
                    System.Diagnostics.Debug.WriteLine($"CONTENT: {value}");
                }

                request.Content = new FormUrlEncodedContent(dictionary);
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", userService.CurrentUser.token);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");
                    var jo = JObject.Parse(stringContent);
                    var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<CommentData>(stringContent);
                    responseData.Data = parsedResponse;
                    responseData.Success = true;
                 
                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"{HttpMethod.Get.ToString()}:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }


        public async Task<Response<CommentData>> PostOffer(OfferCommand command)
        {
            Response<CommentData> responseData = new Response<CommentData>();
            try
            {
                var uri = new Uri(URLPath.PostOffer);
                var dictionary = command.ToKeyValue();

                var request = new HttpRequestMessage(HttpMethod.Post, uri);
                foreach (var value in dictionary.Values)
                {
                    System.Diagnostics.Debug.WriteLine($"CONTENT: {value}");
                }
                if (command.photos != null)
                {
                    for (int x = 0; x < command.photos.Count; x++)
                    {
                        dictionary.Add(new KeyValuePair<string, string>($"Photos[{x}]", $"{command.photos[x]}"));
                    }
                }
                request.Content = new FormUrlEncodedContent(dictionary);
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", userService.CurrentUser.token);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");
                    var jo = JObject.Parse(stringContent);
                    var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<CommentData>(stringContent);
                    responseData.Data = parsedResponse;
                    responseData.Success = true;
                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"{HttpMethod.Get.ToString()}:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<NotificationList>> GetNotifications(NotificationCommand command)
        {
            Response<NotificationList> responseData = new Response<NotificationList>();
            try
            {
                var url = string.Format(restService.CreateGetURL(URLPath.GetNotifications, command));
                var uri = new Uri(url);
                var request = new HttpRequestMessage(HttpMethod.Get, uri);
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", userService.CurrentUser.token);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);
                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    var jo = JObject.Parse(stringContent);
                    if (jo != null)
                    {
                        var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<NotificationList>(jo.ToString());
                        if (parsedResponse != null)
                        {
                            responseData.Data = parsedResponse;
                            responseData.Success = true;
                        }
                        else
                        {
                            responseData.ErrorMessage = jo["error"]?.ToString();
                            responseData.Success = false;
                        }
                    }
                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");

                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"{HttpMethod.Get.ToString()}:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<CommentDTO>> GetOffers(GetCommentCommand comment)
        {
            Response<CommentDTO> responseData = new Response<CommentDTO>();
            try
            {
                var url = string.Format(restService.CreateGetURL(URLPath.GetOffers, comment));
                var uri = new Uri(url);
                var request = new HttpRequestMessage(HttpMethod.Get, uri);
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", userService.CurrentUser.token);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);
                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    var jo = JObject.Parse(stringContent);
                    if (jo != null)
                    {
                        var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<CommentDTO>(jo.ToString());
                        if (parsedResponse != null)
                        {
                            responseData.Data = parsedResponse;
                            responseData.Success = true;
                        }
                        else
                        {
                            responseData.ErrorMessage = jo["error"]?.ToString();
                            responseData.Success = false;
                        }
                    }
                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");

                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"{HttpMethod.Get.ToString()}:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<CommentData>> GetOffersSub(GetCommentCommand command, long parentId)
        {
            Response<CommentData> responseData = new Response<CommentData>();
            try
            {
                var url = URLPath.GetOffers + "/" + parentId;
                var uri = new Uri(url);
                var request = new HttpRequestMessage(HttpMethod.Get, uri);
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", userService.CurrentUser.token);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);
                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    var jo = JObject.Parse(stringContent);
                    if (jo != null)
                    {
                        var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<CommentData>(stringContent);
                        if (parsedResponse != null)
                        {
                            responseData.Data = parsedResponse;
                            responseData.Success = true;
                        }
                        else
                        {
                            responseData.ErrorMessage = jo["error"]?.ToString();
                            responseData.Success = false;
                        }
                    }
                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");

                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"{HttpMethod.Get.ToString()}:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<string>> UploadPhoto(byte[] image)
        {
            Response<string> response = new Response<string>();
            string Url = $"{URLPath.UploadPhoto}";
            try
            {
                Stream stream = new MemoryStream(image);
                var content = new MultipartFormDataContent();
                content.Headers.ContentType.MediaType = "multipart/form-data";
                content.Add(new StreamContent(stream),
                  "file",
                   DateTime.Now.Millisecond.ToString() + ".jpg");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userService.CurrentUser.token);
                var message = await client.PostAsync(Url, content);

                if (message.IsSuccessStatusCode)
                {
                    var stringContent = await message.Content.ReadAsStringAsync();
                    response.Success = true;
                    var jo = JObject.Parse(stringContent);
                    if (jo != null)
                    {
                        if (jo["file"] != null)
                        {
                            response.Data = jo["file"].ToString();
                            response.Success = true;
                        }
                        else
                        {
                            response.ErrorMessage = jo["error"]?.ToString();
                            response.Success = false;
                        }
                    }
                }
                else
                {
                    response.Success = false;
                }

            }
            catch (Exception)
            {

            }
            return response;
        }

        public async Task<Response<ItemList>> GetItems(SearchCommand command)
        {
            Response<ItemList> responseData = new Response<ItemList>();
            try
            {
                var uri = new Uri(restService.CreateGetURL(URLPath.CreateBarters, command));
                var request = new HttpRequestMessage(HttpMethod.Get, uri);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userService.CurrentUser.token);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");
                    var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ItemList>(stringContent);
                    if (parsedResponse != null)
                    {
                        responseData.Data = parsedResponse;
                        responseData.Success = true;
                    }
                    else
                    {
                        responseData.Success = false;
                    }
                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"URI:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception ex)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<bool>> CancelBarter(long itemId) {
            Response<bool> responseData = new Response<bool>();
            try
            {
                var uri = new Uri(URLPath.CancelBarter);
                var dictionary = new { id = itemId }.ToKeyValue();

                var request = new HttpRequestMessage(HttpMethod.Post, uri);
                request.Content = new FormUrlEncodedContent(dictionary);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userService.CurrentUser.token);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();

                    responseData.Data = true;
                    responseData.Success = true;

                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");

                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"URI:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception ex)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<UserDTO>> ChangePassword(string currentPassword, string newPassword) {
            Response<UserDTO> responseData = new Response<UserDTO>();
            try
            {
                var uri = new Uri(URLPath.ChangePassword);
                var dictionary = new { currentPassword = currentPassword, newPassword = newPassword}.ToKeyValue();

                var request = new HttpRequestMessage(HttpMethod.Post, uri);
                request.Content = new FormUrlEncodedContent(dictionary);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userService.CurrentUser.token);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                  
                        responseData.Data = null;
                        responseData.Success = true;

                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");

                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"URI:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception ex)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<UserDTO>> Authenticate(LoginRegistrationCommand command) {
            Response<UserDTO> responseData = new Response<UserDTO>();
            try
            {
                var uri = new Uri(URLPath.Authenticate);
                var dictionary = command.ToKeyValue();

                var request = new HttpRequestMessage(HttpMethod.Post, uri);
                request.Content = new FormUrlEncodedContent(dictionary);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<UserDTO>(stringContent);
                    if (parsedResponse != null)
                    {
                        responseData.Data = parsedResponse;
                        responseData.Success = true;
                        userService.CurrentUser = ModelMapper.MapUserDTO(responseData.Data, userService.CurrentUser);
                        await accountDatabase.SaveUserAsync(userService.CurrentUser);
                    }
                    else
                    {
                        responseData.Success = false;
                    }

                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");

                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"URI:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception ex)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<UserDTO>> GoogleAuth(string accessToken)
        {
            Response<UserDTO> responseData = new Response<UserDTO>();
            try
            {
                var uri = new Uri(URLPath.GoogleAuth);
                var dictionary = new { accessToken = accessToken }.ToKeyValue();

                var request = new HttpRequestMessage(HttpMethod.Post, uri);
                request.Content = new FormUrlEncodedContent(dictionary);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    //var jo = JObject.Parse(stringContent);
                    var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<UserDTO>(stringContent);
                    if (parsedResponse != null)
                    {
                        responseData.Data = parsedResponse;
                        responseData.Success = true;
                        userService.CurrentUser = new Model.User();
                        userService.CurrentUser.IsGoogle = true;
                        userService.CurrentUser = ModelMapper.MapUserDTO(responseData.Data, userService.CurrentUser);
                        await accountDatabase.SaveUserAsync(userService.CurrentUser);
                    }
                    else
                    {
                        responseData.Success = false;
                    }

                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");

                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"URI:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception ex)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<UserDTO>> FacebookAuth(string accessToken) {
            Response<UserDTO> responseData = new Response<UserDTO>();
            try
            {
                var uri = new Uri(URLPath.FacebookAuth);
                var dictionary = new { accessToken = accessToken }.ToKeyValue();

                var request = new HttpRequestMessage(HttpMethod.Post, uri);
                request.Content = new FormUrlEncodedContent(dictionary);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    //var jo = JObject.Parse(stringContent);
                    var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<UserDTO>(stringContent);
                    if (parsedResponse != null)
                    {
                        responseData.Data = parsedResponse;
                        responseData.Success = true;
                        userService.CurrentUser = new Model.User();
                        userService.CurrentUser.IsFacebook = true;
                        userService.CurrentUser = ModelMapper.MapUserDTO(responseData.Data, userService.CurrentUser);
                        await accountDatabase.SaveUserAsync(userService.CurrentUser);
                    }
                    else
                    {
                        responseData.Success = false;
                    }

                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");

                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"URI:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception ex)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<bool>> UpdateFCMToken(string token) {
            Response<bool> responseData = new Response<bool>();
            try
            {
                var uri = new Uri(URLPath.UpdateFCMToken);
                var dictionary = new { token = token}.ToKeyValue();

                var request = new HttpRequestMessage(HttpMethod.Post, uri);
                request.Content = new FormUrlEncodedContent(dictionary);
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", userService.CurrentUser.token);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    //var jo = JObject.Parse(stringContent);
                    responseData.Data = true;
                    responseData.Success = true;
                    
                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");

                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"URI:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception ex)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<UserDTO>> UpdateProfile(UpdateUserCommand command) {
            Response<UserDTO> responseData = new Response<UserDTO>();
            try
            {
                var uri = new Uri(URLPath.Account);
                var dictionary = command.ToKeyValue();

                var request = new HttpRequestMessage(HttpMethod.Post, uri);
                request.Content = new FormUrlEncodedContent(dictionary);
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", userService.CurrentUser.token);
                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    //var jo = JObject.Parse(stringContent);
                    var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<UserDTO>(stringContent);
                    if (parsedResponse != null)
                    {
                        responseData.Data = parsedResponse;
                        responseData.Success = true;
                        var token = userService.CurrentUser.token;
                        userService.CurrentUser = ModelMapper.MapUserDTO(responseData.Data, userService.CurrentUser);
                        userService.CurrentUser.token = token;
                        await accountDatabase.SaveUserAsync(userService.CurrentUser);
                    }
                    else
                    {
                        responseData.Success = false;
                    }

                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");

                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"URI:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception ex)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<UserDTO>> ActivateRegistration(ActivateAccountCommand command) {
            Response<UserDTO> responseData = new Response<UserDTO>();
            try
            {
                var uri = new Uri(URLPath.ActivateRegistration);
                var dictionary = command.ToKeyValue();

                var request = new HttpRequestMessage(HttpMethod.Post, uri);
                request.Content = new FormUrlEncodedContent(dictionary);

                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<UserDTO>(stringContent);
                    if (parsedResponse != null)
                    {
                        responseData.Data = parsedResponse;
                        responseData.Success = true;
                        userService.CurrentUser = ModelMapper.MapUserDTO(responseData.Data, userService.CurrentUser);
                        await accountDatabase.SaveUserAsync(userService.CurrentUser);
                    }
                    else
                    {
                        responseData.Success = false;
                    }

                    System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");

                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"{HttpMethod.Get.ToString()}:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception ex)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<UserDTO>> RegisterUser(LoginRegistrationCommand command)
        {
            Response<UserDTO> responseData = new Response<UserDTO>();
            try
            {
                var uri = new Uri(URLPath.APIRegister);
                var dictionary = command.ToKeyValue();

                var request = new HttpRequestMessage(HttpMethod.Post, uri);
                request.Content = new FormUrlEncodedContent(dictionary);

                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    responseData.Data = null;
                    responseData.Success = true;
                    //var stringContent = await response.Content.ReadAsStringAsync();
                    //var jo = JObject.Parse(stringContent);
                    //var parsedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<UserDTO>(stringContent);
                    //if (parsedResponse != null)
                    //{
                    //    responseData.Data = null;
                    //    responseData.Success = true;
                    //    userService.CurrentUser = ModelMapper.MapUserDTO(responseData.Data, userService.CurrentUser);
                    //    await accountDatabase.SaveUserAsync(userService.CurrentUser);
                    //}
                    //else
                    //{
                    //    responseData.Success = false;
                    //}
                    //System.Diagnostics.Debug.WriteLine($"CONTENT: {stringContent}");
                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"{HttpMethod.Get.ToString()}:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception ex)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<bool>> RepostBarter(long barterId)
        {
            Response<bool> responseData = new Response<bool>();
            try
            {
                var uri = new Uri(URLPath.RepostBarter);
                var dictionary = new { id = barterId }.ToKeyValue();

                var request = new HttpRequestMessage(HttpMethod.Post, uri);
                request.Content = new FormUrlEncodedContent(dictionary);

                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    responseData.Success = true;
                    responseData.Data = true;
                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"{HttpMethod.Get.ToString()}:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception ex)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<bool>> UpdateReminderSettings(long barterId, int days)
        {
            Response<bool> responseData = new Response<bool>();
            try
            {
                var uri = new Uri(URLPath.UpdateRemindSettings);
                var dictionary = new { id = barterId, remindInDays = days }.ToKeyValue();

                var request = new HttpRequestMessage(HttpMethod.Post, uri);
                request.Content = new FormUrlEncodedContent(dictionary);

                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    responseData.Success = true;
                    responseData.Data = true;
                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"{HttpMethod.Get.ToString()}:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception ex)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }

        public async Task<Response<bool>> BarterComplete(long barterId)
        {
            Response<bool> responseData = new Response<bool>();
            try
            {
                var uri = new Uri(URLPath.BarterComplete);
                var dictionary = new { id = barterId, tradedTo = 1 }.ToKeyValue();

                var request = new HttpRequestMessage(HttpMethod.Post, uri);
                request.Content = new FormUrlEncodedContent(dictionary);

                var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);

                responseData.Status = (int)response.StatusCode;
                if (response.IsSuccessStatusCode)
                {
                    responseData.Success = true;
                    responseData.Data = true;
                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = await HandleErrorResponse(response);
                }

                System.Diagnostics.Debug.WriteLine($"{HttpMethod.Get.ToString()}:{uri?.ToString()}");
                System.Diagnostics.Debug.WriteLine($"RESPONSE: {responseData?.Success} ");
                System.Diagnostics.Debug.WriteLine($"ERROR: {responseData?.ErrorMessage}");
            }
            catch (Exception ex)
            {
                responseData.ErrorMessage = "Please check your network connection and try again.";
            }
            return responseData;
        }
    }
}