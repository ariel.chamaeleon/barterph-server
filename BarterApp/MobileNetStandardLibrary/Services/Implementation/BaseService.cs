﻿using System;
using System.Net.Http;
using AppStandardLibrary.Model;
using AppStandardLibrary.Repository;
using AppStandardLibrary.Services.Interface;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace AppStandardLibrary.Services.Implementation
{
    public abstract class BaseService
    {
        public const int DefaultBufferSize = 8192;
        public HttpClient client;
        public IRestService restService;
        public IAccountDatabase accountDatabase;
        public IUser userService;

        public BaseService(IRestService restService, IAccountDatabase database, IUser userService)
        {
            this.accountDatabase = database;
            this.userService = userService;
            this.restService = restService;
            this.client = restService.client;
        }

        public async Task<string> HandleErrorResponse(HttpResponseMessage response)
        {
            try
            {
                /**/
                if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    if (!string.IsNullOrWhiteSpace(stringContent))
                    {
                        var jo = JObject.Parse(stringContent);
                        var error = jo["message"]?.ToString();
                        if (!string.IsNullOrEmpty(error))
                        {
                            return error;
                        }
                        else
                        {
                            return "Server is down as of this moment. Please try again later.";
                        }
                    }
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    if (!string.IsNullOrWhiteSpace(stringContent))
                    {
                        var jo = JObject.Parse(stringContent);
                        var error = jo["message"]?.ToString();
                        if (!string.IsNullOrEmpty(error))
                        {
                            return error;
                        }
                        else
                        {
                            return "Server is down as of this moment. Please try again later.";
                        }
                    }
                }
                else
                {
                    var stringContent = await response.Content.ReadAsStringAsync();
                    if (!string.IsNullOrWhiteSpace(stringContent))
                    {
                        var jo = JObject.Parse(stringContent);
                        var error = jo["message"]?.ToString();
                        if (!string.IsNullOrEmpty(error))
                        {
                            return error;
                        }
                        else
                        {
                            return "Server is down as of this moment. Please try again later.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return "Server is down as of this moment. Please try again later.";
        }
    }
}
