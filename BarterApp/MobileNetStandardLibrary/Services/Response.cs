﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppStandardLibrary.Services
{
    public class Response
    {
        public Error error;
        /// <summary>
        /// Whether the operation completed successfully.
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// HTTP response code (4xx/5xx) if applicable.
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Error message.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Resource key of error message.
        /// </summary>
        public string ErrorKey { get; set; }

        /// <summary>
        /// Checks if response has errors.
        /// </summary>
        public bool HasError { get { return !string.IsNullOrEmpty(ErrorKey) || !string.IsNullOrEmpty(ErrorMessage); } }
    }

    public class Result<T>
    {
        public Result()
        {
            Data = new List<T>();
        }

        /// <summary>
        /// The outout if the operation
        /// </summary>
        public List<T> Data { get; set; }
        /// <summary>
        /// Total count of data
        /// </summary>
        public long Count { get; set; }
    }

    public class Error
    {
        public int Code { get; set; }

        public string Text { get; set; }
    }

    public class Response<T> : Response
    {


        /// <summary>
        /// The output of the operation.
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Copies properties from response.
        /// </summary>
        /// <param name="response"></param>
        /// <param name="property"></param>
    }
}
