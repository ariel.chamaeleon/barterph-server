﻿using System;

namespace AppStandardLibrary
{
    public class URLPath
    {
        public static string PrivacyPolicy = BaseURL + "https://barterph.app/dataprivacy";
        public static string TermsAndConditions = BaseURL + "https://barterph.app/terms";        
        public static bool IsLive = true;
        public static string BaseURL = IsLive ? "https://api.barterph.app/api/" : "http://34.87.133.204:8080/api/";
        public static string APIRegister = BaseURL + "register";
        public static string ActivateRegistration = BaseURL + "activate";
        public static string Account = BaseURL + "account";
        public static string ResetPasswordInit = BaseURL + "account/reset-password/init";
        public static string ResetPasswordFinish = BaseURL + "account/reset-password/finish";
        public static string ChangePassword = BaseURL + "account/change-password";
        public static string CancelBarter = BaseURL + "barters/cancel";
        public static string FacebookAuth = BaseURL + "fbauth";
        public static string GoogleAuth = BaseURL + "googleauth";
        public static string Authenticate = BaseURL + "authenticate";
        public static string UploadPhoto = BaseURL + "photo/upload";
        public static string CreateBarters = BaseURL + "barters";
        public static string EditBarter = BaseURL + "barters/update";
        public static string GetComments = BaseURL + "comments";
        public static string PostComment = BaseURL + "comments/add";
        public static string UpdateFCMToken = BaseURL + "fcm-token";
        public static string RepostBarter = BaseURL + "barters/repost";
        public static string UpdateRemindSettings = BaseURL + "barters/remind-settings";

        public static string GetOffers = BaseURL + "offers";
        public static string PostOffer = BaseURL + "offers/add";
        public static string GetNotifications = BaseURL + "notifications";
        public static string BarterComplete = BaseURL + "barters/close";
    }
}