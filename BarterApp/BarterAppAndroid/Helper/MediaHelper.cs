﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.Provider;
using Java.IO;

namespace BarterApp.Droid
{
	public static class MediaHelper
	{
		public static File _file;
		public static File _dir;
		public static Bitmap bitmap;

		public static Bitmap LoadAndResizeBitmap(this string fileName, int width, int height)
		{
			// First we get the the dimensions of the file on disk
			BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
			BitmapFactory.DecodeFile(fileName, options);

			// Next we calculate the ratio that we need to resize the image by
			// in order to fit the requested dimensions.
			int outHeight = options.OutHeight;
			int outWidth = options.OutWidth;
			int inSampleSize = 3;

			if (outHeight > height || outWidth > width)
			{
				inSampleSize = outWidth > outHeight
								   ? outHeight / height
								   : outWidth / width;
			}

			// Now we will load the image and have BitmapFactory resize it for us.
			options.InSampleSize = inSampleSize;
			options.InJustDecodeBounds = false;
			try
			{
				return BitmapFactory.DecodeFile(fileName, options); ;
			}
			catch (System.Exception ex)
			{
				//Crashes.TrackError(ex);
				return null;
			}
		}

		public static Bitmap ResizeBitmap(Bitmap originalImage, int widthToScae, int heightToScale)
		{
			Bitmap resizedBitmap = Bitmap.CreateBitmap(widthToScae, heightToScale, Bitmap.Config.Argb8888);

			float originalWidth = originalImage.Width;
			float originalHeight = originalImage.Height;

			Canvas canvas = new Canvas(resizedBitmap);

			float scale = 320 / originalWidth;

			float xTranslation = 0.0f;
			float yTranslation = (320 - originalHeight * scale) / 2.0f;

			Matrix transformation = new Matrix();
			transformation.PostTranslate(xTranslation, yTranslation);
			transformation.PreScale(scale, scale);

			Paint paint = new Paint();
			paint.FilterBitmap = true;

			canvas.DrawBitmap(originalImage, transformation, paint);

			return resizedBitmap;
		}


	}
}

