﻿using System;
using System.Collections.Generic;
using Acr.UserDialogs;
using Android.Content;
using Android.Graphics;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using AppStandardLibrary;
using BarterAppAndroid.Activity;
using BarterAppAndroid.Droid;
using MobileNetStandardLibrary.Controller;
using MobileNetStandardLibrary.DTO;
using Square.Picasso;
using Unity;
using static BarterAppAndroid.MainActivity;

namespace BarterAppAndroid.Adapter
{
    public class CommentAdapterRecycler : RecyclerView.Adapter
    {
        public event EventHandler<HomeAdapterClickEventArgs> ItemClick;
        public event EventHandler<HomeAdapterClickEventArgs> ItemLongClick;
        public List<CommentData> items;
        Context context;
        bool withOutReply;
        public long barterOwnerUserId;
        public long viewerId;
        public long userId;
        public bool barterOwner { get; set; }
        BaseViewPresenter controller;

        public CommentAdapterRecycler(List<CommentData> items, bool withOutReply, Context context, long userId)
        {
            this.items = items;
            this.context = context;
            this.withOutReply = withOutReply;
            this.userId = userId;
            controller = Resolver.Container.Resolve(typeof(BaseViewPresenter)) as BaseViewPresenter;
        }

        public override RecyclerView.ViewHolder
            OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).
                        Inflate(Resource.Layout.CommentAdapter, parent, false);
            ReviewViewHolder vh = new ReviewViewHolder(itemView, OnClick, OnLongClick);


            return vh;
        }

        public override void
            OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var item = items[position];
            ReviewViewHolder vh = holder as ReviewViewHolder;
            Picasso.With(context).Load(URLPath.BaseURL + item.user.imageUrl).Placeholder(Resource.Mipmap.user_placeholder).MemoryPolicy(MemoryPolicy.NoCache).Into(vh.ivProfilePic);
            vh.tvReply.Text = item.repliesCount > 0 ? $"View Replies" : "Reply";

            vh.tvName.Text = item.user.firstName;
            vh.llReplyContainer.Visibility = withOutReply ? ViewStates.Gone : ViewStates.Visible;
            vh.ivMore.Tag = position;
            vh.ivMore.Click += IvMore_Click;
            if (!string.IsNullOrEmpty(item.comment))
            {
                vh.ivMore.Visibility = item.user.id == controller.userService.CurrentUser.id ? ViewStates.Visible : ViewStates.Gone;
            }
            else
            {
                vh.ivMore.Visibility = ViewStates.Gone;
            }

            vh.tvDate.Text = CommonAndroidHelper.ConvertDateTimeToText(item.datePosted.GetValueOrDefault());
            vh.tvComment.SetTextColor(Android.Graphics.Color.Black);
            vh.tvComment.SetTypeface(null, TypefaceStyle.Normal);
            vh.tvComment.SetBackgroundResource(0);
            vh.tvComment.Gravity = GravityFlags.CenterVertical;
            if (!string.IsNullOrEmpty(item.item))
            {
                if (item.user.id != userId && !barterOwner)
                {
                    item.description = "Made an Offer";
                    vh.tvComment.SetTextColor(Android.Graphics.Color.White);
                    vh.tvComment.SetTypeface(null, TypefaceStyle.Italic);
                    vh.tvComment.SetBackgroundResource(Resource.Drawable.made_an_offer);
                    vh.tvComment.Gravity = GravityFlags.Center;
                    vh.llReplyContainer.Visibility = ViewStates.Gone;
                    vh.tvComment.Text = item.description;
                    vh.tvItemName.Visibility = ViewStates.Gone;
                    vh.llPhotos.RemoveAllViews();
                }
                else
                {
                    vh.tvComment.Text = item.description;
                    vh.tvItemName.Visibility = ViewStates.Visible;
                    vh.tvItemName.Text = item.item;
                    if (item.photos?.Count > 0) {
                        vh.llPhotos.RemoveAllViews();
                        foreach (var photoURL in item.photos) {
                            ImageView image = new ImageView(context);
                            var paramsImage = new LinearLayout.LayoutParams(150, 150);
                            paramsImage.LeftMargin = 20;
                            image.LayoutParameters = paramsImage;
                            image.SetScaleType(scaleType: ImageView.ScaleType.CenterCrop);
                            image.SetImageResource(Resource.Mipmap.product_placeholder);
                            Picasso.Get().Load(URLPath.BaseURL + photoURL).Placeholder(Resource.Mipmap.product_placeholder).MemoryPolicy(MemoryPolicy.NoCache).Into(image);
                            if (!image.HasOnClickListeners) {
                                image.Click += (s, e) => {
                                    GalleryActivity.ListOfImages = item.photos;
                                    GalleryActivity.ListOfImagesIndex = item.photos.IndexOf(photoURL);
                                    context.StartActivity(new Intent(context, typeof(GalleryActivity)));
                                };
                            }
                            vh.llPhotos.AddView(image);
                        }
                    }

                }
            }
            else {
                vh.tvItemName.Visibility = ViewStates.Gone;
                vh.tvComment.Text = item.comment;
            }
        }


        private async void IvMore_Click(object sender, EventArgs e)
        {
            var response = await UserDialogs.Instance.ActionSheetAsync("Choose", "Cancel", "", null, new string[] { "Delete Comment" });
            if (response == "Delete Comment")
            {
                var tag = (sender as ImageView).Tag;

                var page = context as BarterItemDetailActivity;
                if (page != null)
                {
                    page.DeleteComment(items[(int)tag].id);
                }

                items.RemoveAt((int)tag);
                NotifyDataSetChanged();
            }
        }

        public override int ItemCount
        {
            get { return items.Count; }
        }

        void OnClick(HomeAdapterClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(HomeAdapterClickEventArgs args) => ItemLongClick?.Invoke(this, args);
    }

    public class ReviewViewHolder : RecyclerView.ViewHolder
    {
        public ImageView ivMore { get; private set; }
        public ImageView ivProfilePic { get; private set; }
        public TextView tvName { get; private set; }
        public TextView tvReply { get; private set; }
        public TextView tvComment { get; private set; }
        public TextView tvItemName { get; private set; }
        public LinearLayout llPhotos { get; private set; }
        public TextView tvDate { get; private set; }
        public LinearLayout llReplyContainer { get; private set; }

        public ReviewViewHolder(View itemView, Action<HomeAdapterClickEventArgs> clickListener,
                        Action<HomeAdapterClickEventArgs> longClickListener) : base(itemView)
        {
            // Locate and cache view references:
            tvItemName = itemView.FindViewById<TextView>(Resource.Id.tvItemName);
            llPhotos = itemView.FindViewById<LinearLayout>(Resource.Id.llPhotos);
            tvDate = itemView.FindViewById<TextView>(Resource.Id.tvDate);
            tvComment = itemView.FindViewById<TextView>(Resource.Id.tvComment);
            ivMore = itemView.FindViewById<ImageView>(Resource.Id.ivMore);
            ivProfilePic = itemView.FindViewById<ImageView>(Resource.Id.ivProfilePic);
            tvName = itemView.FindViewById<TextView>(Resource.Id.tvName);
            tvReply = itemView.FindViewById<TextView>(Resource.Id.tvReply);
            llReplyContainer = itemView.FindViewById<LinearLayout>(Resource.Id.llReplyContainer);

            itemView.Click += (sender, e) => clickListener(new HomeAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new HomeAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
        }
    }

    public class HomeAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}