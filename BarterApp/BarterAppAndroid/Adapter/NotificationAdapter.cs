﻿using System;
using System.Collections.Generic;
using Android.Graphics;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Text;
using Android.Text.Style;
using Android.Views;
using Android.Widget;
using AppStandardLibrary;
using BarterAppAndroid.Droid;
using MobileNetStandardLibrary.DTO;
using Square.Picasso;

namespace BarterAppAndroid.Adapter
{
    public class NotificationAdapter : BaseAdapter<NotificationDTO>
    {
        List<NotificationDTO> items;
        AppCompatActivity context;

        ViewHolder vh;

        public NotificationAdapter(AppCompatActivity context, List<NotificationDTO> items) : base()
        {
            this.context = context;
            this.items = items;
        }
        public override long GetItemId(int position)
        {
            return position;
        }
        public override NotificationDTO this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];
            View view = convertView;
            if (view == null)
            {
                vh = new ViewHolder();
                view = context.LayoutInflater.Inflate(Resource.Layout.NotificationAdapter, null);
                vh.tvDate = view.FindViewById<TextView>(Resource.Id.tvDate);
                vh.tvDetail = view.FindViewById<TextView>(Resource.Id.tvDetail);
                vh.ivImage = view.FindViewById<ImageView>(Resource.Id.ivImage);
                //vh.btnRemindMeAgain = view.FindViewById<Button>(Resource.Id.btnRemindMeAgain);
                //vh.btnSetAsCompleted = view.FindViewById<Button>(Resource.Id.btnSetCompleted);
                view.Tag = vh;
            }
            else
            {
                vh = (ViewHolder)view.Tag;
            } // no view to re-use, create new

            //if (item.type == "BARTER_REMINDER")
            //{
            //    vh.btnRemindMeAgain.Visibility = ViewStates.Visible;
            //    vh.btnSetAsCompleted.Visibility = ViewStates.Visible;
            //    if (!vh.btnRemindMeAgain.HasOnClickListeners)
            //    {
            //        vh.btnRemindMeAgain.Click += (s, e) =>
            //        {
            //            BottomSheetDialog dialog = new BottomSheetDialog(context);
            //            dialog.SetContentView(Resource.Layout.remind_me_again_bottom);
            //            dialog.Show();
            //        };
            //    }
            //}
            //else {
            //    vh.btnRemindMeAgain.Visibility = ViewStates.Gone;
            //    vh.btnSetAsCompleted.Visibility = ViewStates.Gone;
            //}


            ////var tvTitle = view.FindViewById<TextView>(Resource.Id.tvTitle);
            //var tvDetail = view.FindViewById<TextView>(Resource.Id.tvDetail);
            //var tvDate = view.FindViewById<TextView>(Resource.Id.tvDate);
            //var ivImage = view.FindViewById<ImageView>(Resource.Id.ivImage);
            //var llAcceptDeny = view.FindViewById<LinearLayout>(Resource.Id.llAcceptDeny);

            StyleSpan bss = new StyleSpan(TypefaceStyle.Bold);
            StyleSpan bss2 = new StyleSpan(TypefaceStyle.Bold);
            SpannableStringBuilder ssb = new SpannableStringBuilder($"{item.subTitle}");
            vh.ivImage.SetImageResource(Resource.Mipmap.user_placeholder);
            try
            {
                if (item.sender != null)
                {
                    ssb.SetSpan(bss, item.subTitle.IndexOf($"{item.sender.firstName + " " + item.sender.lastName}"), item.subTitle.IndexOf($"{item.sender.firstName + " " + item.sender.lastName}") + (item.sender.firstName + " " + item.sender.lastName).Length, SpanTypes.InclusiveExclusive);
                    if (!string.IsNullOrEmpty(item.sender.imageUrl))
                        Picasso.With(context).Load(URLPath.BaseURL + item.sender.imageUrl).Placeholder(Resource.Mipmap.user_placeholder).MemoryPolicy(MemoryPolicy.NoCache).Into(vh.ivImage);
                    else
                    {
                        vh.ivImage.SetImageResource(Resource.Mipmap.user_placeholder);
                    }
                }
                else if (item.receiver != null) {
                    ssb.SetSpan(bss, item.subTitle.IndexOf($"{item.receiver.firstName + " " + item.receiver.lastName}"), item.subTitle.IndexOf($"{item.receiver.firstName + " " + item.receiver.lastName}") + (item.receiver.firstName + " " + item.receiver.lastName).Length, SpanTypes.InclusiveExclusive);
                    if (!string.IsNullOrEmpty(item.receiver.imageUrl))
                        Picasso.With(context).Load(URLPath.BaseURL + item.receiver.imageUrl).Placeholder(Resource.Mipmap.user_placeholder).MemoryPolicy(MemoryPolicy.NoCache).Into(vh.ivImage);
                    else
                    {
                        vh.ivImage.SetImageResource(Resource.Mipmap.user_placeholder);
                    }
                }
                if (!string.IsNullOrEmpty(item.barter.Item)) {
                    ssb.SetSpan(bss2, item.subTitle.IndexOf($"{item.barter.Item}"), item.subTitle.IndexOf($"{item.barter.Item}") + item.barter.Item.Length, SpanTypes.InclusiveExclusive);
                    ssb.SetSpan(new ForegroundColorSpan(Color.DarkGreen), item.subTitle.IndexOf($"{item.barter.Item}"), item.subTitle.IndexOf($"{item.barter.Item}") + item.barter.Item.Length, SpanTypes.InclusiveExclusive);

                }
            }
            catch (Exception ex)
            {
                //Crashes.TrackError(ex);
            }

            vh.tvDetail.SetText(ssb, TextView.BufferType.Spannable);
            vh.tvDate.Text = CommonAndroidHelper.ConvertDateTimeToText(item.datePosted.GetValueOrDefault());

            //if (item.read)
            //{
            //    view.SetBackgroundColor(Android.Graphics.Color.ParseColor("a9a9a9"));
            //}
            //else {
            //    view.SetBackgroundColor(Android.Graphics.Color.White);
            //}

            //if (item.type == RecentActivityType.friendrequest.ToString())
            //{
            //    vh.llAcceptDeny.Visibility = ViewStates.Visible;
            //}
            //else {
            //    vh.llAcceptDeny.Visibility = ViewStates.Gone;
            //}

            return view;
        }

        private class ViewHolder : Java.Lang.Object
        {
            public TextView tvDetail { get; set; }
            public TextView tvDate { get; set; }
            public ImageView ivImage { get; set; }
            public LinearLayout llAcceptDeny { get; set; }
            public Button btnSetAsCompleted { get; set; }
            public Button btnRemindMeAgain { get; set; }
        }
    }


}