﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Runtime;
using Android.Support.V4.View;
using Android.Views;
using Android.Widget;
using AppStandardLibrary;
using BarterAppAndroid.Activity;
using Square.Picasso;

namespace BarterAppAndroid.Adapter
{
    public class ImagePagerAdapter : PagerAdapter
    {
        Context context;

        // Underlying data (tree catalog):
        public List<string> ImageUrls;

        // Load the adapter with the tree catalog at construction time:
        public ImagePagerAdapter(Context context, List<string> imageUrls)
        {
            this.context = context;
            this.ImageUrls = imageUrls;
        }

        // Return the number of trees in the catalog:
        public override int Count
        {
            get { return ImageUrls.Count; }
        }

        // Create the tree page for the given position:
        public override Java.Lang.Object InstantiateItem(View container, int position)
        {
            // Instantiate the ImageView and give it an image:
            var imageView = new ImageView(context);
            Picasso.With(this.context).Load(URLPath.BaseURL + ImageUrls[position]).Into(imageView);
            // Add the image to the ViewPager:

            imageView.Click += (object sender, EventArgs e) =>
            {
                GalleryActivity.ListOfImages = ImageUrls;
                context.StartActivity(new Intent(context, typeof(GalleryActivity)));
            };

            var viewPager = container.JavaCast<ViewPager>();
            viewPager.AddView(imageView);
            return imageView;
        }

        // Remove a tree page from the given position.
        public override void DestroyItem(View container, int position, Java.Lang.Object view)
        {
            var viewPager = container.JavaCast<ViewPager>();
            viewPager.RemoveView(view as View);
        }

        // Determine whether a page View is associated with the specific key object
        // returned from InstantiateItem (in this case, they are one in the same):
        public override bool IsViewFromObject(View view, Java.Lang.Object obj)
        {
            return view == obj;
        }

        // Display a caption for each Tree page in the PagerTitleStrip:
        public override Java.Lang.ICharSequence GetPageTitleFormatted(int position)
        {
            return new Java.Lang.String(string.Empty);
        }
    }
}