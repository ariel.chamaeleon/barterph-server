﻿using System;
using System.Collections.Generic;
using Acr.UserDialogs;
using Android.Content;
using Android.Support.V7.App;
using Android.Text;
using Android.Text.Style;
using Android.Views;
using Android.Widget;
using AppStandardLibrary;
using BarterAppAndroid.Activity;
using BarterAppAndroid.Droid;
using MobileNetStandardLibrary.Controller;
using MobileNetStandardLibrary.DTO;
using Square.Picasso;
using Unity;
using static BarterAppAndroid.MainActivity;

namespace BarterAppAndroid.Adapter
{
	public class CommentAdapter : BaseAdapter<CommentData>
	{
		List<CommentData> items;
		AppCompatActivity context;
		bool withOutReply;
		BaseViewPresenter controller;

		public CommentAdapter(AppCompatActivity context, List<CommentData> items, bool withOutReply) : base()
		{
			this.context = context;
			this.items = items;
			this.withOutReply = withOutReply;
			controller = Resolver.Container.Resolve(typeof(BaseViewPresenter)) as BaseViewPresenter;
		}
		public override long GetItemId(int position)
		{
			return position;
		}
		public override CommentData this[int position]
		{
			get { return items[position]; }
		}
		public override int Count
		{
			get { return items.Count; }
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			var item = items[position];
			View view = convertView;
			if (view == null) // no view to re-use, create new
				view = context.LayoutInflater.Inflate(Resource.Layout.CommentAdapter, null);
			var ivMore = view.FindViewById<ImageView>(Resource.Id.ivMore);
			var ivProfilePic = view.FindViewById<ImageView>(Resource.Id.ivProfilePic);
			var tvName = view.FindViewById<TextView>(Resource.Id.tvName);
			var tvReply = view.FindViewById<TextView>(Resource.Id.tvReply);
			var tvItemName = view.FindViewById<TextView>(Resource.Id.tvItemName);

			try
			{
				Picasso.Get().Load(URLPath.BaseURL + item.user.imageUrl).Placeholder(Resource.Mipmap.user_placeholder).MemoryPolicy(MemoryPolicy.NoCache).Into(ivProfilePic);
				//if (!tvReply.HasOnClickListeners)
				//{
				//	tvReply.Tag = item.id;
				//	tvReply.Click += TvReply_Click;
				//}
				//tvReply.Text = item.repliesCount > 0 ? $"View Replies" : "Reply";
				tvItemName.Visibility = ViewStates.Gone;
				tvName.Text = item.user.firstName + " " + item.user.lastName;

				var llReplyContainer = view.FindViewById<LinearLayout>(Resource.Id.llReplyContainer);
				llReplyContainer.Visibility = withOutReply ? ViewStates.Gone : ViewStates.Visible;

				ivMore.Tag = position;
				if (!ivMore.HasOnClickListeners) {
					ivMore.Click += IvMore_Click;
				}

				if (!string.IsNullOrEmpty(item.comment))
				{
					ivMore.Visibility = item.user.id == controller.userService.CurrentUser.id ? ViewStates.Visible : ViewStates.Gone;
				}
				else
				{
					ivMore.Visibility = ViewStates.Gone;
				}
				var tvComment = view.FindViewById<TextView>(Resource.Id.tvComment);
				tvComment.Text = item.comment.Length > 500 ? item.comment.Substring(0, 500).ToString() : item.comment;
				var tvDate = view.FindViewById<TextView>(Resource.Id.tvDate);
				tvDate.Text = CommonAndroidHelper.ConvertDateTimeToText(item.datePosted.GetValueOrDefault());
			}
			catch (Exception ex)
			{
			}
			return view;
		}

		private void TvReply_Click(object sender, EventArgs e)
		{
			CommonAndroidHelper.DisableButtonTimer(sender as View);
			var textView = sender as TextView;
			if (textView != null)
			{
				Intent intent = new Intent(context, typeof(CommentsSecondActivity));
				intent.PutExtra("Id", textView.Tag.ToString());
				intent.PutExtra("IsComment", true);
				context.StartActivity(intent);
			}
		}

		private async void IvMore_Click(object sender, EventArgs e)
		{
			var response = await UserDialogs.Instance.ActionSheetAsync("Choose", "Cancel", "", null, new string[] { "Delete Comment" });
			if (response == "Delete Comment")
			{
				var tag = (sender as ImageView).Tag;

				var page = context as CommentsSecondActivity;
				if (page != null)
				{
					page.DeleteComment(items[(int)tag].id);
				}

				items.RemoveAt((int)tag);
				NotifyDataSetChanged();
			}
		}

	}
}