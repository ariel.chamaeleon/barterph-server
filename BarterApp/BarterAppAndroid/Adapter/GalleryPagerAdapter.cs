﻿using System;
using System.Collections.Generic;
using System.Net;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Support.V4.View;
using Android.Views;
using AppStandardLibrary;
using ImageViews.Photo;
using Square.Picasso;

namespace BarterAppAndroid.Adapter
{
	public class GalleryPagerAdapter : PagerAdapter
	{
		Context context;

		// Underlying data (tree catalog):
		public List<string> ImageUrls;

		// Load the adapter with the tree catalog at construction time:
		public GalleryPagerAdapter(Context context, List<string> imageUrls)
		{
			this.context = context;
			this.ImageUrls = imageUrls;
		}

		// Return the number of trees in the catalog:
		public override int Count
		{
			get { return ImageUrls.Count; }
		}

		// Create the tree page for the given position:
		public override Java.Lang.Object InstantiateItem(View container, int position)
		{
			// Instantiate the ImageView and give it an image:
			var imageView = new PhotoView(context);
			Picasso.With(this.context).Load(URLPath.BaseURL + ImageUrls[position]).MemoryPolicy(MemoryPolicy.NoCache).Into(imageView);
			// Add the image to the ViewPager:
			//var bitmap = GetImageBitmapFromUrl(ImageUrls[position]);
			//imageView.SetScaleType(ImageView.ScaleType.FitCenter);
			//imageView.SetImageBitmap(bitmap);
			var viewPager = container.JavaCast<ViewPager>();
			viewPager.AddView(imageView);
			return imageView;
		}

		private Bitmap GetImageBitmapFromUrl(string url)
		{
			Bitmap imageBitmap = null;

			using (var webClient = new WebClient())
			{
				var imageBytes = webClient.DownloadData(url);
				if (imageBytes != null && imageBytes.Length > 0)
				{
					imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
				}
			}

			return imageBitmap;
		}

		// Remove a tree page from the given position.
		public override void DestroyItem(View container, int position, Java.Lang.Object view)
		{
			var viewPager = container.JavaCast<ViewPager>();
			viewPager.RemoveView(view as View);
		}

		// Determine whether a page View is associated with the specific key object
		// returned from InstantiateItem (in this case, they are one in the same):
		public override bool IsViewFromObject(View view, Java.Lang.Object obj)
		{
			return view == obj;
		}

		// Display a caption for each Tree page in the PagerTitleStrip:
		public override Java.Lang.ICharSequence GetPageTitleFormatted(int position)
		{
			return new Java.Lang.String(string.Empty);
		}
	}
}