﻿using System;
using System;
using System.Collections.Generic;
using Android.Content;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using AppStandardLibrary;
using BarterAppAndroid.Activity;
using MobileNetStandardLibrary.Commands;
using MobileNetStandardLibrary.DTO;
using Square.Picasso;

namespace BarterAppAndroid.Adapter
{
    public class ImageAdapter : BaseAdapter<ItemDTO>
    {
        AppCompatActivity context;
        List<ItemDTO> items;

        public ImageAdapter(AppCompatActivity c, List<ItemDTO> items)
        {
            context = c;
            this.items = items;
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override ItemDTO this[int position]
        {
            get { return items[position]; }
        }

        public override long GetItemId(int position)
        {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];

            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.ItemWhiteCard, null);

            var profilePic = view.FindViewById<ImageView>(Resource.Id.ivProfilePic);
            var ivImage = view.FindViewById<ImageView>(Resource.Id.ivImage);
            var ivGrayScale = view.FindViewById<ImageView>(Resource.Id.ivGrayScale);
            var tvItemName = view.FindViewById<TextView>(Resource.Id.tvItemName);
            var tvLocation = view.FindViewById<TextView>(Resource.Id.tvLocation);
            var tvDate = view.FindViewById<TextView>(Resource.Id.tvDateCreated);
            var tvOffers = view.FindViewById<TextView>(Resource.Id.tvOffers);

            var btnStatus = view.FindViewById<Button>(Resource.Id.btnStatus);

            if (item.status == ItemStatus.COMPLETED.ToString())
            {
                btnStatus.Visibility = ViewStates.Visible;
                btnStatus.Text = "Completed";
                btnStatus.SetTextColor(Android.Graphics.Color.ParseColor("#27AE60"));
                ivGrayScale.Visibility = ViewStates.Gone;
            }
            else if (item.status == ItemStatus.CANCELLED.ToString())
            {
                btnStatus.Visibility = ViewStates.Visible;
                btnStatus.Text = "Deleted";
                btnStatus.SetTextColor(Android.Graphics.Color.Red);
                ivGrayScale.Visibility = ViewStates.Visible;
            }
            else {
                btnStatus.Visibility = ViewStates.Gone;
                ivGrayScale.Visibility = ViewStates.Gone;
            }

            Picasso.Get().Load(URLPath.BaseURL + item.photos[0]).Placeholder(Resource.Mipmap.product_placeholder).MemoryPolicy(MemoryPolicy.NoCache).Into(ivImage);
            Picasso.Get().Load(URLPath.BaseURL + item.user.imageUrl).Placeholder(Resource.Mipmap.product_placeholder).MemoryPolicy(MemoryPolicy.NoCache).Into(profilePic);
            tvItemName.Text = item.Item;
            tvOffers.Text = item.offerCount == 1 ? $"{item.offerCount} offer" : $"{item.offerCount} offers";
            tvLocation.Text = item.location.city + " " + item.location.province;
            tvDate.Text = item.lastUpdateDate.GetValueOrDefault().ToLocalTime().ToString("MMM dd, yyyy");
            view.Tag = item.id;
            if (!view.HasOnClickListeners)
            {
                view.Click += View_Click;
            }

            return view;
        }

        private void View_Click(object sender, EventArgs e)
        {
            try
            {
                var senderView = sender as View;
                var id = (long)senderView.Tag;
                Intent intent = new Intent(context, typeof(BarterItemDetailActivity));
                intent.PutExtra("Id", id);
                context.StartActivity(intent);
            }
            catch (Exception ex)
            {

            }
        }
        // references to our images
    }
}
