﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using AppStandardLibrary;
using BarterAppAndroid.Activity;
using MobileNetStandardLibrary.DTO;
using MobileNetStandardLibrary.Model;
using Square.Picasso;

namespace BarterAppAndroid.Adapter
{
    public class FilterAdapter : BaseAdapter<FilterModel>
    {
        AppCompatActivity context;
        List<FilterModel> items;

        public FilterAdapter(AppCompatActivity c, List<FilterModel> items)
        {
            context = c;
            this.items = items;
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override FilterModel this[int position]
        {
            get { return items[position]; }
        }

        public override long GetItemId(int position)
        {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];

            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.filter_adapter, null);

            var ivImage = view.FindViewById<ImageView>(Resource.Id.ivCheck);
            var tvName = view.FindViewById<TextView>(Resource.Id.tvName);
            tvName.Text = item.FilterName;
            if (item.IsChecked)
            {
                tvName.SetTextColor(Android.Graphics.Color.ParseColor("#27AE60"));
                ivImage.Visibility = ViewStates.Visible;
            }
            else {
                tvName.SetTextColor(Android.Graphics.Color.Black);
                ivImage.Visibility = ViewStates.Gone;
            }


            return view;
        }

        private void View_Click(object sender, EventArgs e)
        {
            try
            {
                var senderView = sender as View;
                var id = (long)senderView.Tag;
                Intent intent = new Intent(context, typeof(BarterItemDetailActivity));
                intent.PutExtra("Id", id);
                context.StartActivity(intent);
            }
            catch (Exception ex)
            {

            }
        }
        // references to our images
    }
}
