﻿using System;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using BarterAppAndroid;
using BarterAppAndroid.Activity;
using Firebase.Messaging;
using Microsoft.AppCenter.Crashes;

namespace FCMClient
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class MyFirebaseMessagingService : FirebaseMessagingService
    {
        const string TAG = "MyFirebaseMsgService";
        public override void OnMessageReceived(RemoteMessage message)
        {
            try
            {
                var intent = new Intent(this, typeof(MainActivity));

                string barterId = string.Empty;
                string commentId = string.Empty;
                string offerId = string.Empty;
                string badgeCount = "0";

                message.Data.TryGetValue("barterId", out barterId);
                message.Data.TryGetValue("barterCommentId", out commentId);
                message.Data.TryGetValue("barterOfferId", out offerId);
                message.Data.TryGetValue("badge", out badgeCount);

                if (MainActivity.IsAppActive)
                {
                    //UpdateBadge
                    if (DashboardActivity.Dashboard != null)
                    {
                        if (!string.IsNullOrEmpty(badgeCount))
                            DashboardActivity.Dashboard.UpdateCount(int.Parse(badgeCount));
                    }
                    return;
                }

                intent.AddFlags(ActivityFlags.ClearTop);
                intent.PutExtra("barterId", barterId);
                intent.PutExtra("commentId", commentId);
                intent.PutExtra("offerId", offerId);
                intent.PutExtra("badge", badgeCount);

                var pendingIntent = PendingIntent.GetActivity(this, 0, intent, PendingIntentFlags.OneShot);

                var notificationBuilder = new Notification.Builder(this)
                                                          .SetSmallIcon(Resource.Mipmap.appicon)
                    .SetContentTitle(message.GetNotification().Title)
                    .SetContentText(message.GetNotification().Body)
                    .SetAutoCancel(true)
                    .SetShowWhen(true)
                    .SetWhen(DateTimeOffset.Now.ToLocalTime().ToUnixTimeMilliseconds())
                    .SetContentIntent(pendingIntent);
                notificationBuilder.SetDefaults(NotificationDefaults.All);
                var notificationManager = (NotificationManager)GetSystemService(Context.NotificationService);

                if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
                {
                    var chan1 = new NotificationChannel("default",
                    "Primary Channel", NotificationImportance.High);
                    chan1.LightColor = Color.Green;
                    chan1.LockscreenVisibility = NotificationVisibility.Public;
                    notificationManager.CreateNotificationChannel(chan1);
                    notificationBuilder.SetChannelId("default");
                }
                /**/
                if (!string.IsNullOrWhiteSpace(message.GetNotification().Body))
                    notificationManager.Notify(1, notificationBuilder.Build());
            }
            catch (System.Exception ex)
            {
                Crashes.TrackError(ex);
            }
        }
    }
}