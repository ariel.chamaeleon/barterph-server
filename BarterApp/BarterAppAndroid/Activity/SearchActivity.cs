﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Acr.UserDialogs.Fragments;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using BarterAppAndroid.Adapter;
using MobileNetStandardLibrary.DTO;
using MobileNetStandardLibrary.Model;

namespace BarterAppAndroid.Activity
{
    [Activity(Label = "SearchActivity", Theme = "@style/AppTheme.NoActionBar")]
    public class SearchActivity : BaseActivity
    {
        private GridView gridView;
        private ImageAdapter adapter;
        private List<ItemDTO> listOfItems;
        private List<FilterModel> listOfFilterModel;
        private FilterAdapter filterAdapter;
        private EditText etSearch;
        private TextView tvEmptyText;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.SearchGridLayout);
            gridView = FindViewById<GridView>(Resource.Id.gridview);
            listOfItems = new List<ItemDTO>();
            tvEmptyText = FindViewById<TextView>(Resource.Id.tvEmptyText);

            etSearch = FindViewById<EditText>(Resource.Id.etSearch);
            etSearch.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextVariationEmailAddress;
            etSearch.ImeOptions = Android.Views.InputMethods.ImeAction.Search;
            etSearch.TextChanged += EtSearch_TextChanged;
            etSearch.EditorAction += EtSearch_EditorAction;

            adapter = new ImageAdapter(this, listOfItems);
            gridView.Adapter = adapter;
            page = 1;
            status = "ACTIVE,COMPLETED";
            GetItems();

            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            var actionBar = SupportActionBar;
            actionBar.SetHomeButtonEnabled(true);
            actionBar.SetDisplayHomeAsUpEnabled(true);

            var ivSearch = toolbar.FindViewById<ImageView>(Resource.Id.ivCheck);
            ivSearch.SetImageResource(Resource.Mipmap.filter);
            ivSearch.Visibility = ViewStates.Visible;
            ivSearch.Click += IvSearch_Click;
            listOfFilterModel = new List<FilterModel>();
            listOfFilterModel.Add(new FilterModel { FilterName = "All Post", IsChecked = true });
            listOfFilterModel.Add(new FilterModel { FilterName = "With Comments", IsChecked = false });
            listOfFilterModel.Add(new FilterModel { FilterName = "With Offers", IsChecked = false });
            listOfFilterModel.Add(new FilterModel { FilterName = "Open", IsChecked = false });
            listOfFilterModel.Add(new FilterModel { FilterName = "Completed", IsChecked = false });
            if (Intent.HasExtra("MyList"))
            {
                listOfFilterModel.Add(new FilterModel { FilterName = "Deleted", IsChecked = false });
            }
            filterAdapter = new FilterAdapter(this, listOfFilterModel);

            if (Intent.HasExtra("MyList"))
            {
                actionBar.Title = "My Items";
            }
            else
            {
                actionBar.Title = "All Post";
            }
        }
        string status;
        int? withOffers;
        int? withComments;

        bool IsLoading;

        private async void EtSearch_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            try
            {
                if (IsLoading)
                    return;
                tvEmptyText.Visibility = ViewStates.Visible;
                tvEmptyText.Text = GetText(Resource.String.Searching);
                listOfItems.Clear();
                adapter.NotifyDataSetChanged();

                IsLoading = true;
                await Task.Delay(1000);

                listOfItems.Clear();
                MobileNetStandardLibrary.Commands.SearchCommand command = null;
                if (Intent.HasExtra("MyList"))
                {
                    command = new MobileNetStandardLibrary.Commands.SearchCommand { page = page, pageSize = 120, sort = 1, withComments = withComments, withOffers = withOffers, status = status, userId = presenter.userService.CurrentUser.id, key = etSearch.Text.Trim() };
                }
                else
                {
                    command = new MobileNetStandardLibrary.Commands.SearchCommand { page = page, pageSize = 120, sort = 1, withComments = withComments, withOffers = withOffers, status = status, userId = null, key = etSearch.Text.Trim() };
                }

                var response = await presenter.GetItems(command);
                if (response.Success)
                {
                    listOfItems.AddRange(response.Data.list);
                    adapter.NotifyDataSetChanged();
                }
                if (listOfItems.Count > 0)
                {
                    tvEmptyText.Visibility = ViewStates.Gone;
                }
                else
                {
                    tvEmptyText.Visibility = ViewStates.Visible;
                    tvEmptyText.Text = "No Results Found!";
                }
                adapter.NotifyDataSetChanged();
                IsLoading = false;
            }
            catch (System.Exception ex)
            {
                // Crashes.TrackError(ex);
            }

        }

        private async void EtSearch_EditorAction(object sender, TextView.EditorActionEventArgs e)
        {
            //var dlg = new BottomSheetDialog(this);
            //dlg.SetContentView(Resource.Layout.filterDialog);
            //dlg.Show();
            //return;

            if (e != null)
            {
                if (e.ActionId == Android.Views.InputMethods.ImeAction.Search)
                {
                    if (string.IsNullOrWhiteSpace(etSearch.Text))
                        return;

                    try
                    {
                        if (IsLoading)
                            return;
                        tvEmptyText.Visibility = ViewStates.Visible;
                        tvEmptyText.Text = GetText(Resource.String.Searching);
                        listOfItems.Clear();
                        adapter.NotifyDataSetChanged();

                        IsLoading = true;
                        await Task.Delay(1000);

                        listOfItems.Clear();
                        MobileNetStandardLibrary.Commands.SearchCommand command = null;
                        if (Intent.HasExtra("MyList"))
                        {
                            command = new MobileNetStandardLibrary.Commands.SearchCommand { page = page, pageSize = 120, sort = 1, withComments = withComments, withOffers = withOffers, status = status, userId = presenter.userService.CurrentUser.id, key = etSearch.Text.Trim() };
                        }
                        else
                        {
                            command = new MobileNetStandardLibrary.Commands.SearchCommand { page = page, pageSize = 120, sort = 1, withComments = withComments, withOffers = withOffers, status = status, userId = null, key = etSearch.Text.Trim() };
                        }
                        var response = await presenter.GetItems(command);
                        if (response.Success)
                        {
                            listOfItems.AddRange(response.Data.list);
                            adapter.NotifyDataSetChanged();

                        }



                        if (listOfItems.Count > 0)
                        {
                            tvEmptyText.Visibility = ViewStates.Gone;
                        }
                        else
                        {
                            tvEmptyText.Visibility = ViewStates.Visible;
                            tvEmptyText.Text = "No Results Found!";
                        }
                        adapter.NotifyDataSetChanged();
                        IsLoading = false;
                    }
                    catch (System.Exception ex)
                    {
                        // Crashes.TrackError(ex);
                    }
                }
            }
        }

        Dialog filterDialog;
        ListView listViewFilter;

        private void IvSearch_Click(object sender, EventArgs e)
        {
            if (filterDialog == null)
            {
                filterDialog = new Dialog(this);
                filterDialog.SetContentView(Resource.Layout.filterDialog);
                filterDialog.SetTitle("Filter By:");

                listViewFilter = filterDialog.FindViewById<ListView>(Resource.Id.lvList);
                listViewFilter.Adapter = filterAdapter;
                listViewFilter.ItemClick += ListView_ItemClick;
            }
            else
            {

            }
            filterDialog.Show();
        }

        private void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            foreach (var filter in listOfFilterModel)
            {
                filter.IsChecked = false;
            }

            var item = listOfFilterModel[e.Position];
            item.IsChecked = true;
            filterAdapter.NotifyDataSetChanged();
            filterDialog.Hide();
            page = 1;
            if (Intent.HasExtra("MyList"))
            {
                SupportActionBar.Title = listOfFilterModel[e.Position].FilterName == "All" ? "My List" : listOfFilterModel[e.Position].FilterName;
            }
            else
            {
                SupportActionBar.Title = listOfFilterModel[e.Position].FilterName == "All" ? "All Items for Barter" : listOfFilterModel[e.Position].FilterName;
            }

            if (listOfFilterModel[e.Position].FilterName == "All Post")
            {
                withComments = null;
                withOffers = null;
                status = "ACTIVE,COMPLETED";
            }

            if (listOfFilterModel[e.Position].FilterName == "With Comments")
            {
                withComments = 1;
                withOffers = null;
                status = "ACTIVE,COMPLETED";
            }
            else if (listOfFilterModel[e.Position].FilterName == "With Offers")
            {
                withComments = null;
                withOffers = 1;
                status = "ACTIVE,COMPLETED";
            }
            else if (listOfFilterModel[e.Position].FilterName == "Open")
            {
                withComments = null;
                withOffers = null;
                status = "ACTIVE";
            }
            else if (listOfFilterModel[e.Position].FilterName == "Completed")
            {
                withComments = null;
                withOffers = null;
                status = "COMPLETED";
            }
            else if (listOfFilterModel[e.Position].FilterName == "Deleted")
            {
                withComments = null;
                withOffers = null;
                status = "CANCELLED";
            }

            GetItems();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        int page;
        long userId;

        public async Task GetItems()
        {
            UserDialogs.Instance.ShowLoading();
            if (page == 1)
            {
                listOfItems.Clear();
            }
            MobileNetStandardLibrary.Commands.SearchCommand command = null;
            if (Intent.HasExtra("MyList"))
            {
                command = new MobileNetStandardLibrary.Commands.SearchCommand { page = page, pageSize = 120, sort = 1, withComments = withComments, withOffers = withOffers, status = status, userId = presenter.userService.CurrentUser.id, key = etSearch.Text.Trim() };
            }
            else
            {
                command = new MobileNetStandardLibrary.Commands.SearchCommand { page = page, pageSize = 120, sort = 1, withComments = withComments, withOffers = withOffers, status = status, userId = null, key = etSearch.Text.Trim() };
            }

            var response = await presenter.GetItems(command);
            if (response.Success)
            {
                listOfItems.AddRange(response.Data.list);
                adapter.NotifyDataSetChanged();
            }
            if (listOfItems.Count <= 0)
            {
                tvEmptyText.Text = "No Result Found!";
                tvEmptyText.Visibility = ViewStates.Visible;
            }
            else
            {
                tvEmptyText.Visibility = ViewStates.Gone;
            }

            UserDialogs.Instance.HideLoading();
        }
    }
}
