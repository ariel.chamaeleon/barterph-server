﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace BarterAppAndroid.Activity
{
    [Activity(Label = "UserSettingsActivity", Theme = "@style/AppTheme.NoActionBar", ScreenOrientation = ScreenOrientation.Portrait)]
    public class UserSettingsActivity : BaseActivity
    {
        LinearLayout llSocialMedia;
        LinearLayout llMobileNumber;
        EditText etMobileNumber;
        TextView tvUserName;
        TextView tvSocialAccount;
        Button btnChangePassword;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.user_settings_activity);
            btnChangePassword = FindViewById<Button>(Resource.Id.btnChangePassword);
            llSocialMedia = FindViewById<LinearLayout>(Resource.Id.llSocialMedia);
            llMobileNumber = FindViewById<LinearLayout>(Resource.Id.llMobileNumber);
            etMobileNumber = FindViewById<EditText>(Resource.Id.etMobileNumber);
            tvUserName = FindViewById<TextView>(Resource.Id.tvUserName);
            tvSocialAccount = FindViewById<TextView>(Resource.Id.tvSocialAccount);
            btnChangePassword = FindViewById<Button>(Resource.Id.btnChangePassword);

            if (presenter.userService.CurrentUser.IsFacebook)
            {
                llSocialMedia.Visibility = ViewStates.Visible;
                llMobileNumber.Visibility = ViewStates.Gone;
                tvUserName.Text = presenter.userService.CurrentUser.firstName + " " + presenter.userService.CurrentUser.lastName;
                tvSocialAccount.Text = "Facebook Account";
            }
            else if (presenter.userService.CurrentUser.IsGoogle)
            {
                llSocialMedia.Visibility = ViewStates.Visible;
                llMobileNumber.Visibility = ViewStates.Gone;
                tvUserName.Text = presenter.userService.CurrentUser.firstName + " " + presenter.userService.CurrentUser.lastName;
                tvSocialAccount.Text = "Google Account";
            }
            else {
                llSocialMedia.Visibility = ViewStates.Gone;
                llMobileNumber.Visibility = ViewStates.Visible;
                etMobileNumber.Text = presenter.userService.CurrentUser.mobileNo;
                btnChangePassword.Click += BtnChangePassword_Click;
            }

            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            var actionBar = SupportActionBar;
            actionBar.SetHomeButtonEnabled(true);
            actionBar.SetDisplayHomeAsUpEnabled(true);
            actionBar.Title = "User Settings";
            // Create your application here
        }

        private async void BtnChangePassword_Click(object sender, EventArgs e)
        {
            var response = await presenter.ResetPasswordInit(presenter.userService.CurrentUser.mobileNo);
            if (response.Success) {
                Intent intent = new Intent(this, typeof(ChangePasswordActivity));
                intent.PutExtra("ChangePassword", true);
                StartActivity(intent);
            }
        }
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}
