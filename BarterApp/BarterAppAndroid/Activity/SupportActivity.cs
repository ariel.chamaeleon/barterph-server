﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace BarterAppAndroid.Activity
{
    [Activity(Label = "SupportActivity", Theme = "@style/AppTheme.NoActionBar", ScreenOrientation = ScreenOrientation.Portrait)]
    public class SupportActivity : BaseActivity
    {
        private TextView tvFacebookPage;
        private TextView tvEmailText;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.support_activity);
            tvEmailText = FindViewById<TextView>(Resource.Id.tvEmailText);
            tvFacebookPage = FindViewById<TextView>(Resource.Id.tvFacebookPage);

            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            var actionBar = SupportActionBar;
            actionBar.SetHomeButtonEnabled(true);
            actionBar.SetDisplayHomeAsUpEnabled(true);
            actionBar.Title = "Support";

            tvFacebookPage.Click += TvFacebookPage_Click;
            tvEmailText.Click += TvEmailText_Click;
        }

        private void TvEmailText_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(Intent.ActionSendto);
            intent.SetData(Android.Net.Uri.Parse("mailto:")); // only email apps should handle this
            intent.PutExtra(Intent.ExtraEmail, new String[] { tvEmailText.Text.Trim() });
            StartActivity(intent);
        }

        private void TvFacebookPage_Click(object sender, EventArgs e)
        {
            Intent browserIntent = new Intent(Intent.ActionView);
            browserIntent.SetData(Android.Net.Uri.Parse(tvFacebookPage.Text.Trim()));
            StartActivity(browserIntent);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}
