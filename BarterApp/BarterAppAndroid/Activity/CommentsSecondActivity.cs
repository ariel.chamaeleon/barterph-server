﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AppStandardLibrary;
using BarterAppAndroid.Adapter;
using BarterAppAndroid.Droid;
using MobileNetStandardLibrary.Commands;
using MobileNetStandardLibrary.DTO;
using Square.Picasso;

namespace BarterAppAndroid.Activity
{
    [Activity(Label = "NotificationActivity", Theme = "@style/AppTheme.NoActionBar", ScreenOrientation = ScreenOrientation.Portrait)]
    public class CommentsSecondActivity : BaseActivity
    {
        private ImageView ivBack;

        private ListView lvComments;
        private ImageView btnComment;
        private CommentAdapter adapter;
        private List<CommentData> listOfComments;
        private EditText etComment;
        private TextView tvTitleBar;

        private ImageView ivProfilePic;
        private ImageView ivLevelGem;
        private TextView tvName;
        private TextView tvComment;
        private TextView tvItemName;
        private LinearLayout llPhotos;

        private TextView tvDate;

        string reviewId;
        string parentId;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.CommentAndQuestionSecondLevel);
            tvItemName = FindViewById<TextView>(Resource.Id.tvItemName);
            llPhotos = FindViewById<LinearLayout>(Resource.Id.llPhotos);
            lvComments = FindViewById<ListView>(Resource.Id.lvComments);
            btnComment = FindViewById<ImageView>(Resource.Id.btnComment);
            etComment = FindViewById<EditText>(Resource.Id.etComment);
            tvTitleBar = FindViewById<TextView>(Resource.Id.tvTitleBar);

            ivProfilePic = FindViewById<ImageView>(Resource.Id.ivProfilePic);
            tvName = FindViewById<TextView>(Resource.Id.tvName);
            tvComment = FindViewById<TextView>(Resource.Id.tvComment);
            tvDate = FindViewById<TextView>(Resource.Id.tvDate);

            tvTitleBar.Text = Intent.GetBooleanExtra("IsComment", true) ? "Comment" : "Offer";
            listOfComments = new List<CommentData>();

            btnComment.Click += BtnComment_Click;

            reviewId = Intent.GetStringExtra("BarterId");
            parentId = Intent.GetStringExtra("Id");

            ivBack = FindViewById<ImageView>(Resource.Id.ivBack);
            ivBack.Click += IvBack_Click;

            adapter = new CommentAdapter(this, listOfComments, true);
            lvComments.Adapter = adapter;
            if (Intent.GetBooleanExtra("IsComment", true))
            {
                GetComments();
            }
            else
            {
                GetOffers();
            }
            // Create your application here
        }

        public void DeleteComment(long id)
        {
            presenter.DeleteComment(id);
        }

        protected override void OnStop()
        {
            CommonAndroidHelper.ShowKeyboard(this, etComment);
            base.OnStop();
        }

        private async void BtnComment_Click(object sender, EventArgs e)
        {


            CommonAndroidHelper.DisableButtonTimer(sender as View);
            if (string.IsNullOrEmpty(etComment.Text.Trim()))
                return;

            if (etComment.Text.Trim().Length > 500)
            {
                UserDialogs.Instance.Alert("Comment should not exceed more than 500 characters.");
                return;
            }

            btnComment.Enabled = false;
            CommentCommand paramsCommand = null;
            if (Intent.GetBooleanExtra("IsComment", true))
            {
                paramsCommand = new MobileNetStandardLibrary.Commands.CommentCommand
                {
                    comment = etComment.Text.Trim(),
                    parentCommentId = long.Parse(Intent.GetStringExtra("Id")),
                    barterId = long.Parse(Intent.GetStringExtra("BarterId")),
                    offerId = null,
                };
            }
            else
            {
                paramsCommand = new MobileNetStandardLibrary.Commands.CommentCommand
                {
                    comment = etComment.Text.Trim(),
                    offerId = long.Parse(Intent.GetStringExtra("Id")),
                    barterId = long.Parse(Intent.GetStringExtra("BarterId")),
                    parentCommentId = null
                };
            }

            var response = await presenter.PostComment(paramsCommand);
            etComment.Text = string.Empty;
            if (response.Success)
            {
                listOfComments.Insert(listOfComments.Count, response.Data);
            }

            adapter.NotifyDataSetChanged();

            btnComment.Enabled = true;
        }

        public int currentPageComments;
        public int pageSizeComments;

        public async Task GetComments()
        {
            try
            {
                /**/
                currentPageComments = 1;
                pageSizeComments = 10000;
                listOfComments.Clear();
                var commentsList = await presenter.GetCommentsSub(new GetCommentCommand { page = currentPageComments, pageSize = pageSizeComments, barterId = long.Parse(reviewId) }, long.Parse(parentId));
                if (commentsList.Success)
                {
                    tvName.Text = commentsList.Data.user.firstName + " " + commentsList.Data.user.lastName; ;
                    tvComment.Text = commentsList.Data.comment;
                    tvItemName.Visibility = ViewStates.Gone;
                    tvDate.Text = CommonAndroidHelper.ConvertDateTimeToText(commentsList.Data.datePosted.GetValueOrDefault());

                    Picasso.Get().Load(URLPath.BaseURL + commentsList.Data.user.imageUrl).Placeholder(Resource.Mipmap.user_placeholder).MemoryPolicy(MemoryPolicy.NoCache).Into(ivProfilePic);

                    if (commentsList.Data.repliesCount > 0)
                    {
                        listOfComments.InsertRange(listOfComments.Count, commentsList.Data.replies);
                    }
                    adapter.NotifyDataSetChanged();
                }
            }
            catch (Exception ex)
            {
                //Crashes.TrackError(ex);
            }
        }

        public async Task GetOffers()
        {
            try
            {
                /**/
                currentPageComments = 1;
                pageSizeComments = 10000;
                listOfComments.Clear();
                var commentsList = await presenter.GetOffersSub(new GetCommentCommand { page = currentPageComments, pageSize = pageSizeComments, barterId = long.Parse(reviewId) }, long.Parse(parentId));
                if (commentsList.Success)
                {
                    tvName.Text = commentsList.Data.user.firstName + " " + commentsList.Data.user.lastName;
                    tvComment.Text = commentsList.Data.description;
                        tvItemName.Text = commentsList.Data.item;

                    if (commentsList.Data.photos?.Count > 0)
                    {

                        foreach (var photoURL in commentsList.Data.photos)
                        {
                            ImageView image = new ImageView(this);
                            var paramsImage = new LinearLayout.LayoutParams(150, 150);
                            paramsImage.LeftMargin = 20;
                            image.LayoutParameters = paramsImage;
                            image.SetScaleType(scaleType: ImageView.ScaleType.CenterCrop);
                            Picasso.Get().Load(URLPath.BaseURL + photoURL).Placeholder(Resource.Mipmap.user_placeholder).MemoryPolicy(MemoryPolicy.NoCache).Into(image);
                            if (!image.HasOnClickListeners)
                            {
                                image.Click += (s, e) => {
                                    GalleryActivity.ListOfImages = commentsList.Data.photos;
                                    GalleryActivity.ListOfImagesIndex = commentsList.Data.photos.IndexOf(photoURL);
                                    StartActivity(new Intent(this, typeof(GalleryActivity)));
                                };
                            }
                            llPhotos.AddView(image);
                        }
                    }
                    tvDate.Text = CommonAndroidHelper.ConvertDateTimeToText(commentsList.Data.datePosted.GetValueOrDefault());
                    Picasso.Get().Load(URLPath.BaseURL + commentsList.Data.user.imageUrl).Placeholder(Resource.Mipmap.user_placeholder).MemoryPolicy(MemoryPolicy.NoCache).Into(ivProfilePic);

                    if (commentsList.Data.repliesCount > 0)
                    {
                        listOfComments.InsertRange(listOfComments.Count, commentsList.Data.replies);
                    }
                    adapter.NotifyDataSetChanged();
                }
            }
            catch (Exception ex)
            {
                //Crashes.TrackError(ex);
            }
        }

        private void IvBack_Click(object sender, EventArgs e)
        {
            CommonAndroidHelper.DisableButtonTimer(sender as View);
            Finish();
        }
    }
}



