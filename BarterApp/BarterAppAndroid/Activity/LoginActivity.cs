﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.Gms.Auth.Api;
using Android.Gms.Auth.Api.SignIn;
using Android.Gms.Common;
using Android.Gms.Common.Apis;
using Android.OS;
using Android.Preferences;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Util;
using Android.Views;
using Android.Widget;
using BarterAppAndroid.Controls;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Xamarin.Facebook.Login.Widget;

namespace BarterAppAndroid.Activity
{
    [Activity(Label = "LoginActivity", Theme = "@style/AppTheme.NoActionBar")]
    public class LoginActivity : BaseActivity, IFacebookCallback, GoogleApiClient.IConnectionCallbacks, GoogleApiClient.IOnConnectionFailedListener
    {
        LoginButton BtnFBLogin;
        private ICallbackManager mFBCallManager;
        private MyProfileTracker mprofileTracker;

        private ImageView ivBack;
        private Button btnLogin;
        private TextView tvNoAccount;
        private MobileNumberText etMobileNumber;
        private PasswordEditText etPassword;

        private RelativeLayout rlGoogleLogin;
        GoogleApiClient mGoogleApiClient;
        GoogleSignInAccount acct;

        TextView tvForgotPassword;

        public void OnCancel()
        {
        }

        public void OnError(FacebookException error)
        {
        }

        public void OnSuccess(Java.Lang.Object result)
        {
            var accessToken = AccessToken.CurrentAccessToken?.Token;
            Log.Debug("FacebookAccess Token", accessToken);
            if (!string.IsNullOrEmpty(accessToken))
            {
                FacebookAuth(accessToken);
            }
        }

        private async void HandleSignInResult(GoogleSignInResult result)
        {
            try
            {
                /**/
                if (result.IsSuccess)
                {
                    // Signed in successfully, show authenticated UI.
                    acct = result.SignInAccount;
                    if (acct != null)
                    {
                        UserDialogs.Instance.ShowLoading();
                        var response = await presenter.GoogleAuth(acct.IdToken);
                        UserDialogs.Instance.HideLoading();
                        if (response.Success)
                        {
                            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(ApplicationContext);
                            var token = prefs.GetString("RegistrationToken", "");
                            if (!string.IsNullOrEmpty(token))
                            {
                                presenter.accountService.UpdateFCMToken(token);
                            }
                            if (string.IsNullOrEmpty(presenter.userService.CurrentUser.city))
                            {

                                Intent intent = new Intent(this, typeof(ProfileActivity));
                                intent.PutExtra("FromRegistration", true);
                                StartActivity(intent);
                            }
                            else
                            {
                                Intent intent = new Intent(this, typeof(DashboardActivity));
                                intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.ClearTask | ActivityFlags.NewTask);
                                StartActivity(intent);
                            }
                        }
                        else
                        {
                            UserDialogs.Instance.Alert(response.ErrorMessage);
                        }
                    }
                    //UpdateUI(true);
                }
                else
                {
                    // Signed out, show unauthenticated UI.
                    //UpdateUI(false);
                }
            }
            catch (Exception ex)
            {
                //Crashes.TrackError(ex);
            }
        }

        async Task FacebookAuth(string accessToken)
        {
            UserDialogs.Instance.ShowLoading();
            var response = await presenter.FacebookAuth(accessToken);
            UserDialogs.Instance.HideLoading();
            if (response.Success)
            {
                ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(ApplicationContext);
                var token = prefs.GetString("RegistrationToken", "");
                if (!string.IsNullOrEmpty(token))
                {
                    presenter.accountService.UpdateFCMToken(token);
                }
                if (string.IsNullOrEmpty(presenter.userService.CurrentUser.city))
                {
                    Intent intent = new Intent(this, typeof(ProfileActivity));
                    intent.PutExtra("FromRegistration", true);
                    StartActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(this, typeof(DashboardActivity));
                    intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.ClearTask | ActivityFlags.NewTask);
                    StartActivity(intent);
                }

                LoginManager.Instance.LogOut();
            }
            else
            {
                UserDialogs.Instance.Alert(response.ErrorMessage);
            }
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Android.Content.Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            mFBCallManager.OnActivityResult(requestCode, (int)resultCode, data);

            if (requestCode == RC_SIGN_IN)
            {
                GoogleSignInResult result = Auth.GoogleSignInApi.GetSignInResultFromIntent(data);
                HandleSignInResult(result);
            }
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.LoginActivity);
            tvForgotPassword = FindViewById<TextView>(Resource.Id.tvForgotPassword);
            tvForgotPassword.Click += TvForgotPassword_Click;
            rlGoogleLogin = FindViewById<RelativeLayout>(Resource.Id.rlGoogleLogin);
            etMobileNumber = FindViewById<MobileNumberText>(Resource.Id.tvMobileNumber);
            etPassword = FindViewById<PasswordEditText>(Resource.Id.tvPassword);
            tvNoAccount = FindViewById<TextView>(Resource.Id.tvNoAccount);
            ivBack = FindViewById<ImageView>(Resource.Id.ivBack);
            ivBack.Click += IvBack_Click;

            btnLogin = FindViewById<Button>(Resource.Id.btnLogin);
            btnLogin.Click += BtnLogin_Click;

            mprofileTracker = new MyProfileTracker();
            mprofileTracker.mOnProfileChanged += MprofileTracker_mOnProfileChanged; ;
            mprofileTracker.StartTracking();

            BtnFBLogin = FindViewById<LoginButton>(Resource.Id.login_button);
            // Create your application here
            BtnFBLogin.SetReadPermissions(new List<string> {
                "public_profile"
            });
            mFBCallManager = CallbackManagerFactory.Create();
            BtnFBLogin.RegisterCallback(mFBCallManager, this);

            tvNoAccount.Click += TvNoAccount_Click;

            rlGoogleLogin.Click += RlGoogleLogin_Click;
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DefaultSignIn)
             .RequestIdToken("334305398500-6iefprapuhfin227mq1fpbjb75bn11c2.apps.googleusercontent.com")
             .RequestEmail()
             .Build();

            mGoogleApiClient = new GoogleApiClient.Builder(this)
                .EnableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .AddApi(Android.Gms.Auth.Api.Auth.GOOGLE_SIGN_IN_API, gso)
                .Build();
        }

        private async void TvForgotPassword_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrWhiteSpace(etMobileNumber.Field.Text))
            {
                UserDialogs.Instance.Alert("Please put your mobile number");
                return;
            }
            UserDialogs.Instance.ShowLoading();
            var response = await presenter.ResetPasswordInit("63" + etMobileNumber.Field.Text);
            UserDialogs.Instance.HideLoading();
            if (response.Success)
            {
                Intent intent = new Intent(this, typeof(EnterOTPActivity));
                intent.PutExtra("ResetPassword", true);
                intent.PutExtra("MobileNumber", "63" + etMobileNumber.Field.Text);
                intent.PutExtra("user", "63" + etMobileNumber.Field.Text);
                intent.PutExtra("otp", response.Data.key);
                StartActivity(intent);
            }
        }

        const int RC_SIGN_IN = 9001;

        private void RlGoogleLogin_Click(object sender, EventArgs e)
        {
            Intent signInIntent = Auth.GoogleSignInApi.GetSignInIntent(mGoogleApiClient);
            StartActivityForResult(signInIntent, RC_SIGN_IN);
            UserDialogs.Instance.HideLoading();
        }

        public string TAG = "GoogleSignIn";
        public void OnConnected(Bundle connectionHint)
        {
            Log.Debug(TAG, "onConnected:" + connectionHint);
            //UpdateUI(true);
        }

        public void OnConnectionSuspended(int cause)
        {
            Log.Warn(TAG, "onConnectionSuspended:" + cause);
        }



        private void TvNoAccount_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(RegisterActivity));
            StartActivity(intent);
        }

        private async void BtnLogin_Click(object sender, EventArgs e)
        {
            UserDialogs.Instance.ShowLoading();

            var response = await presenter.Authenticate(new AppStandardLibrary.LoginRegistrationCommand { mobileNo = "63" + etMobileNumber.Field.Text.Trim(), password = etPassword.Field.Text });
            if (response.Success)
            {
                ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(ApplicationContext);
                var token = prefs.GetString("RegistrationToken", "");
                if (!string.IsNullOrEmpty(token))
                {
                    presenter.accountService.UpdateFCMToken(token);
                }
                Intent intent = new Intent(this, typeof(DashboardActivity));
                intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.ClearTask | ActivityFlags.NewTask);
                StartActivity(intent);
            }
            else
            {
                UserDialogs.Instance.Alert(response.ErrorMessage);
            }
            UserDialogs.Instance.HideLoading();
        }

        private void IvBack_Click(object sender, EventArgs e)
        {
            Finish();
        }

        private void MprofileTracker_mOnProfileChanged(object sender, OnProfileChangedEventArgs e)
        {
            if (e.mProfile != null)
            {
                try
                {
                    var firstName = e.mProfile.FirstName;
                    var lastName = e.mProfile.LastName;
                    var name = e.mProfile.Name;
                    var fbId = e.mProfile.Id;
                }
                catch (Java.Lang.Exception ex) { }
            }
        }

        public void OnConnectionFailed(ConnectionResult result)
        {
        }

        public class MyProfileTracker : ProfileTracker
        {
            public event EventHandler<OnProfileChangedEventArgs> mOnProfileChanged;
            protected override void OnCurrentProfileChanged(Profile oldProfile, Profile newProfile)
            {
                if (mOnProfileChanged != null)
                {
                    mOnProfileChanged.Invoke(this, new OnProfileChangedEventArgs(newProfile));
                }
            }
        }
        public class OnProfileChangedEventArgs : EventArgs
        {
            public Profile mProfile;
            public OnProfileChangedEventArgs(Profile profile)
            {
                mProfile = profile;

            }
        }
    }
}
