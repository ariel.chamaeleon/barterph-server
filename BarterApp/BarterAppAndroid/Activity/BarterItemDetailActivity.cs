﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.Gms.Ads;
using Android.Graphics;
using Android.Media;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using AppStandardLibrary;
using BarterApp.Droid;
using BarterAppAndroid.Adapter;
using BarterAppAndroid.Droid;
using MobileNetStandardLibrary.Commands;
using MobileNetStandardLibrary.DTO;
using Square.Picasso;

namespace BarterAppAndroid.Activity
{
    [Activity(Label = "BarterItemDetailActivity", Theme = "@style/AppTheme.NoActionBar", WindowSoftInputMode = SoftInput.StateHidden)]
    public class BarterItemDetailActivity : BaseActivity, IDialogInterfaceOnShowListener
    {
        private Button btnLogin;
        private Button btnRegister;

        private ViewPager ivImage;
        private ImageView ivProfilePic;
        private TextView tvItemName;
        private TextView tvLocationType;
        private TextView tvLocation;
        private TextView tvReason;
        private TextView tvName;
        private TextView tvPostedDate;
        private TextView tvDeliveryOption;
        private TextView tvContactNumber;
        private TextView tvEmptyText;
        private TextView tvPreferences;

        private TextView tvCommentText;
        private TextView tvOfferText;

        private Button btnBarterComplete;
        private Button btnMakeAnOffer;
        private Button btnStatusCompleted;

        private RelativeLayout rlComment;

        private ImageView btnComment;
        private EditText etComment;
        private List<CommentData> listOfComments;
        private List<CommentData> listOfOffers;
        private CommentAdapterRecycler adapter;

        private RecyclerView lvCommentList;
        private RecyclerView.LayoutManager mLayoutManager;

        private NestedScrollView scrollView;

        private RelativeLayout rlCommentSectionList;
        private ImageView ivLeft;
        private ImageView ivRight;

        private ImageView ivSearch;
        private Button btnScrollUp;

        public static List<string> Photos;
        public static List<string> LocalPhotos;
        public static int UploadCount;
        public static int InvalidPhotos;

        public string LoadingText { get; set; }

        protected InterstitialAd mInterstitialAd;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.BarterItemDetailActivity);

            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.AdUnitId = GetString(Resource.String.test_interstitial_ad_unit_id);
            mInterstitialAd.AdListener = new AdListener(this);
            var adRequest = new AdRequest.Builder().Build();
            mInterstitialAd.LoadAd(adRequest);

            btnScrollUp = FindViewById<Button>(Resource.Id.btnScrollUp);
            ivLeft = FindViewById<ImageView>(Resource.Id.pageLeft);
            ivRight = FindViewById<ImageView>(Resource.Id.pageRight);
            btnMakeAnOffer = FindViewById<Button>(Resource.Id.btnMakeAnOffer);
            ivLeft.Visibility = ViewStates.Gone;
            ivRight.Visibility = ViewStates.Gone;
            ivLeft.Click += IvLeft_Click;
            ivRight.Click += IvRight_Click;

            scrollView = FindViewById<NestedScrollView>(Resource.Id.scrollView);
            scrollView.ScrollChange += ScrollView_ScrollChange;
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            var actionBar = SupportActionBar;
            actionBar.SetHomeButtonEnabled(true);
            actionBar.SetDisplayHomeAsUpEnabled(true);
            actionBar.Title = "Item Details";
            lvCommentList = FindViewById<RecyclerView>(Resource.Id.lvCommentList);
            listOfComments = new List<CommentData>();
            listOfOffers = new List<CommentData>();
            adapter = new CommentAdapterRecycler(listOfComments, false, this, presenter.userService.CurrentUser.id);
            mLayoutManager = new LinearLayoutManager(this);
            lvCommentList.SetLayoutManager(mLayoutManager);
            lvCommentList.SetAdapter(adapter);
            btnStatusCompleted = FindViewById<Button>(Resource.Id.btnStatusCompleted);
            btnComment = FindViewById<ImageView>(Resource.Id.btnComment);
            etComment = FindViewById<EditText>(Resource.Id.etComment);
            tvCommentText = FindViewById<TextView>(Resource.Id.tvCommentText);
            tvEmptyText = FindViewById<TextView>(Resource.Id.tvEmptyText);
            rlComment = FindViewById<RelativeLayout>(Resource.Id.rlComment);
            rlCommentSectionList = FindViewById<RelativeLayout>(Resource.Id.rlCommentSectionList);
            tvPreferences = FindViewById<TextView>(Resource.Id.tvPreferences);

            tvOfferText = FindViewById<TextView>(Resource.Id.tvOffersText);
            tvReason = FindViewById<TextView>(Resource.Id.tvReason);
            ivImage = FindViewById<ViewPager>(Resource.Id.ivImage);
            ivProfilePic = FindViewById<ImageView>(Resource.Id.ivProfilePic);
            tvItemName = FindViewById<TextView>(Resource.Id.tvItemName);
            tvLocation = FindViewById<TextView>(Resource.Id.tvLocation);
            tvLocationType = FindViewById<TextView>(Resource.Id.tvLocationType);
            tvName = FindViewById<TextView>(Resource.Id.tvName);
            tvPostedDate = FindViewById<TextView>(Resource.Id.tvPostedDate);
            tvDeliveryOption = FindViewById<TextView>(Resource.Id.tvDeliveryOption);
            tvContactNumber = FindViewById<TextView>(Resource.Id.tvContactNumber);
            btnBarterComplete = FindViewById<Button>(Resource.Id.btnBarterComplete);
            btnBarterComplete.Visibility = ViewStates.Gone;
            ivImage.PageSelected += IvImage_PageSelected;
            tvCommentText.SetTextColor(Android.Graphics.Color.ParseColor("#27AE60"));
            tvOfferText.SetTextColor(Android.Graphics.Color.Black);
            btnMakeAnOffer.Click += BtnMakeAnOffer_Click;
            btnBarterComplete.Click += BtnBarterComplete_Click;
            btnComment.Click += BtnComment_Click;
            tvCommentText.Click += TvCommentText_Click;
            tvOfferText.Click += TvOfferText_Click;
            adapter.ItemClick += Adapter_ItemClick;
            GetItem();

            ivSearch = toolbar.FindViewById<ImageView>(Resource.Id.ivCheck);
            ivSearch.SetImageResource(Resource.Mipmap.option);
            ivSearch.Visibility = ViewStates.Gone;
            ivSearch.Click += IvSearch_Click;

            btnScrollUp.Click += BtnScrollUp_Click;

        }



        private void ScrollView_ScrollChange(object sender, NestedScrollView.ScrollChangeEventArgs e)
        {
            if (e.ScrollY > 1100)
            {
                btnScrollUp.Visibility = ViewStates.Visible;
            }
            else {
                btnScrollUp.Visibility = ViewStates.Gone;
            }
        }

        private void BtnScrollUp_Click(object sender, EventArgs e)
        {
            scrollView.SmoothScrollTo(0, 0);
        }

        public async void DeleteComment(long id)
        {
            var response = await UserDialogs.Instance.ConfirmAsync("Do you want to delete this comment?", null, "Yes", "No");
            if (response)
            {
                presenter.DeleteComment(id);
                var size = int.Parse(tvCommentText.Text.Split(" ")[0]);
                tvCommentText.Text = $"{size - 1} Comments";
            }
        }

        LinearLayout llPhotos;

        private async void BtnMakeAnOffer_Click(object sender, EventArgs e)
        {
            if (!isOfferTab) {
                isOfferTab = true;
                RecheckEmptyText();

                lvCommentList.Visibility = ViewStates.Visible;
                tvCommentText.SetTextColor(Android.Graphics.Color.Black);
                tvOfferText.SetTextColor(Android.Graphics.Color.ParseColor("#27AE60"));

                if (DTO.status == "ACTIVE")
                {
                    btnMakeAnOffer.Visibility = DTO.user.id == presenter.userService.CurrentUser.id ? ViewStates.Gone : ViewStates.Visible;
                    btnStatusCompleted.Visibility = ViewStates.Gone;
                    rlComment.Visibility = ViewStates.Visible;
                }
                else
                {
                    btnMakeAnOffer.Visibility = ViewStates.Gone;
                    btnStatusCompleted.Visibility = ViewStates.Visible;
                    rlComment.Visibility = ViewStates.Gone;
                }
                rlComment.Visibility = ViewStates.Gone;
                GetOffers();
            }

            BottomSheetDialog dialog = new BottomSheetDialog(this, Resource.Style.DialogStyle);
            dialog.SetContentView(Resource.Layout.makeOfferDialog);
            dialog.SetOnShowListener(this);
            llPhotos = dialog.FindViewById<LinearLayout>(Resource.Id.llPhotos);
            var tvPhotos = dialog.FindViewById<TextView>(Resource.Id.tvPhotos);
            tvPhotos.Click += (s,e)=> {
                AllowMultiplePhotos = true;
                ShowCameraPicker();
            };
            var etNotes = dialog.FindViewById<EditText>(Resource.Id.etNotes);
            var etItem = dialog.FindViewById<EditText>(Resource.Id.etItem);
            var tvItemName = dialog.FindViewById<TextView>(Resource.Id.tvItemName);
            var tvUserText = dialog.FindViewById<TextView>(Resource.Id.tvUserText);
            var btnMakeOffer = dialog.FindViewById<Button>(Resource.Id.btnMakeOffer);
            tvItemName.Text = $"For: {DTO.Item}";
            tvUserText.Text = $"You are making an offer to {DTO.user.firstName} {DTO.user.lastName}";
            btnMakeOffer.Click += async (s, e) =>
            {
                if (etNotes.Text.Trim().Count() == 0) {
                    UserDialogs.Instance.Alert("Description is required");
                    return;
                }

                if (etItem.Text.Trim().Count() == 0) {
                    UserDialogs.Instance.Alert("Item name is required");
                    return;
                }

                if (etNotes.Text.Trim().Length > 500) {
                    UserDialogs.Instance.Alert("Comment should not exceed more than 500 characters.");
                    return;
                }

                if (etItem.Text.Trim().Length > 500)
                {
                    UserDialogs.Instance.Alert("Comment should not exceed more than 500 characters.");
                    return;
                }

                UserDialogs.Instance.ShowLoading();
                btnMakeOffer.Enabled = false;
                var response = await presenter.PostOffer(new MobileNetStandardLibrary.Commands.OfferCommand
                {
                    barterId = DTO.id,
                    //note = etNotes.Text.Trim(),
                    description = etNotes.Text.Trim(),
                    item = etItem.Text.Trim(),
                    photos = Photos
                });

                btnMakeOffer.Enabled = true;
                if (response.Success)
                {
                    await GetOffers();
                    //listOfComments.Insert(listOfComments.Count, response.Data);
                    totalOffers += 1;
                    tvOfferText.Text = $"{totalOffers} Offers";
                }
                else
                {
                    if (!string.IsNullOrEmpty(response.ErrorMessage))
                        UserDialogs.Instance.Alert(response.ErrorMessage);
                }
                RecheckEmptyText();
                etComment.Text = string.Empty;
                adapter.NotifyDataSetChanged();
                await Task.Delay(800);
                UserDialogs.Instance.HideLoading();
                scrollView.SmoothScrollTo(0, int.MaxValue);
                dialog.Dismiss();
            };
            Photos?.Clear();
            LocalPhotos?.Clear();
            dialog.Show();
        }

        private async void IvSearch_Click(object sender, EventArgs e)
        {
            try
            {
                var confirm = await UserDialogs.Instance.ActionSheetAsync("Options", "Cancel", null, null, new string[] { "Edit this barter post?", "Delete this barter post?" });
                if (confirm == "Delete this barter post?")
                {
                    if (DTO.status == ItemStatus.ACTIVE.ToString())
                    {
                        var response = await presenter.CancelBarter(DTO.id);
                        if (response.Success)
                        {
                            btnStatusCompleted.Visibility = ViewStates.Visible;
                            btnStatusCompleted.Text = "Deleted";
                            btnStatusCompleted.SetTextColor(Android.Graphics.Color.Red);
                            DTO.status = ItemStatus.CANCELLED.ToString();
                            btnStatusCompleted.Visibility = ViewStates.Visible;
                            btnBarterComplete.Visibility = ViewStates.Gone;
                            rlComment.Visibility = ViewStates.Gone;
                            if (DashboardActivity.Dashboard != null)
                            {
                                DashboardActivity.Dashboard.GenerateMostViewedPost();
                                DashboardActivity.Dashboard.GenerateRecentPost();
                            }
                        }
                    }
                }
                else if (confirm == "Edit this barter post?") {
                    if (DTO.status == ItemStatus.ACTIVE.ToString())
                    {
                        Intent intent = new Intent(this, typeof(EditBarterItemActivity));
                        EditBarterItemActivity.DTO = DTO;
                        StartActivityForResult(intent, 102);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void IvRight_Click(object sender, EventArgs e)
        {
            ivImage.SetCurrentItem(ivImage.CurrentItem + 1, true);
        }

        private void IvLeft_Click(object sender, EventArgs e)
        {
            ivImage.SetCurrentItem(ivImage.CurrentItem - 1, true);
        }

        private void IvImage_PageSelected(object sender, ViewPager.PageSelectedEventArgs e)
        {
            if (e.Position == listOfImages.Count - 1)
            {
                ivRight.Visibility = ViewStates.Gone;
                ivLeft.Visibility = listOfImages.Count > 1 ? ViewStates.Visible : ViewStates.Gone;
            }
            else if (e.Position == 0)
            {
                ivLeft.Visibility = ViewStates.Gone;
                ivRight.Visibility = listOfImages.Count > 1 ? ViewStates.Visible : ViewStates.Gone;
            }
            else {
                ivLeft.Visibility = ViewStates.Visible;
                ivRight.Visibility = ViewStates.Visible;
            }
        }

        private void Adapter_ItemClick(object sender, HomeAdapterClickEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(listOfComments[e.Position].comment))
            {
                if (DTO.user.id != presenter.userService.CurrentUser.id && listOfComments[e.Position].user.id != presenter.userService.CurrentUser.id)
                {
                    return;
                }
            }

            Intent intent = new Intent(this, typeof(CommentsSecondActivity));
            intent.PutExtra("BarterId", DTO.id.ToString());
            intent.PutExtra("Id", listOfComments[e.Position].id.ToString());
            intent.PutExtra("IsComment", !string.IsNullOrEmpty(listOfComments[e.Position].comment));
            StartActivity(intent);
        }

        private async void BtnComment_Click(object sender, EventArgs e)
        {


            if (etComment.Text.Trim().Count() == 0)
                return;

            if (etComment.Text.Trim().Length > 500)
            {
                UserDialogs.Instance.Alert("Comment should not exceed more than 500 characters.");
                return;
            }

            UserDialogs.Instance.ShowLoading();
            btnComment.Enabled = false;
            var response = await presenter.PostComment(new MobileNetStandardLibrary.Commands.CommentCommand
            {
                barterId = DTO.id,
                comment = etComment.Text.Trim(),
                offerId = null,
                parentCommentId = null
            });
    
            btnComment.Enabled = true;
            if (response.Success)
            {
                listOfComments.Insert(listOfComments.Count, response.Data);
                totalComments += 1;
                tvCommentText.Text = $"{totalComments} Comments";
            }
            else
            {
                if (!string.IsNullOrEmpty(response.ErrorMessage))
                    UserDialogs.Instance.Alert(response.ErrorMessage);
            }
            RecheckEmptyText();
            etComment.Text = string.Empty;
            adapter.NotifyDataSetChanged();
            await Task.Delay(1000);
            UserDialogs.Instance.HideLoading();
            scrollView.SmoothScrollTo(0, int.MaxValue);
        }

        void RecheckEmptyText() {
            if (isOfferTab)
            {
                if (listOfComments?.Count <= 0)
                {
                    tvEmptyText.Visibility = ViewStates.Visible;
                    tvEmptyText.Text = "No offers found!";
                }
                else
                {
                    tvEmptyText.Visibility = ViewStates.Gone;
                }
            }
            else
            {
                if (listOfComments?.Count <= 0)
                {
                    tvEmptyText.Visibility = ViewStates.Visible;
                    tvEmptyText.Text = "No comments found!";
                }
                else
                {
                    tvEmptyText.Visibility = ViewStates.Gone;
                }
            }
        }

        async Task GetOffers(bool scrollToBottom = false)
        {
            if (DTO != null)
            {
                UserDialogs.Instance.ShowLoading();
                listOfComments.Clear();
                tvEmptyText.Text = "Loading...";
                var commentsList = await presenter.GetOffers(new GetCommentCommand { page = 1, pageSize = 1000, barterId = DTO.id, sort = 1 });
                if (commentsList.Success)
                {
                    if (commentsList.Data.list?.Count > 0)
                    {
                        listOfComments.InsertRange(listOfComments.Count, commentsList.Data.list);
                    }
                    adapter.NotifyDataSetChanged();

                    tvOfferText.Text = $"{commentsList.Data.totalCount} Offers";

                    RecheckEmptyText();
                }
                RecheckEmptyText();
                UserDialogs.Instance.HideLoading();
            }
            if (scrollToBottom)
            {
                await Task.Delay(500);
                scrollView.SmoothScrollTo(0, int.MaxValue);
            }
        }

        int totalComments;
        int totalOffers;
        async Task GetComments(bool scrollToBottom = false) {
            if (DTO != null) {
                UserDialogs.Instance.ShowLoading();
                listOfComments.Clear();
                tvEmptyText.Text = "Loading...";
                var commentsList = await presenter.GetComments(new GetCommentCommand { page = 1, pageSize = 1000, barterId = DTO.id, sort = 1 });
                if (commentsList.Success)
                {
                    if (commentsList.Data.list?.Count > 0)
                    {
                        listOfComments.InsertRange(listOfComments.Count, commentsList.Data.list);
                    }
                    adapter.NotifyDataSetChanged();

                    tvCommentText.Text = $"{commentsList.Data.totalCount} Comments";
                    totalComments = commentsList.Data.totalCount;
                    RecheckEmptyText();
                }
                RecheckEmptyText();
                UserDialogs.Instance.HideLoading();
            }
            if (scrollToBottom) {
                await Task.Delay(500);
                scrollView.SmoothScrollTo(0, int.MaxValue);
            }
        }

        private async void BtnBarterComplete_Click(object sender, EventArgs e)
        {
            try
            {
                var resp = await UserDialogs.Instance.ConfirmAsync("Set this barter as completed?", "Confirmation", "Yes", "Cancel");
                if (resp)
                {
                    UserDialogs.Instance.ShowLoading();
                    var response = await presenter.BarterComplete(DTO.id);
                    UserDialogs.Instance.HideLoading();
                    if (response.Success)
                    {
                        btnBarterComplete.Visibility = ViewStates.Gone;
                        btnStatusCompleted.Visibility = ViewStates.Visible;
                        DTO.status = "COMPLETED";
                        btnStatusCompleted.Visibility = ViewStates.Visible;
                        btnBarterComplete.Visibility = ViewStates.Gone;
                        rlComment.Visibility = ViewStates.Gone;
                        if (DashboardActivity.Dashboard != null)
                        {
                            DashboardActivity.Dashboard.GenerateMostViewedPost();
                            DashboardActivity.Dashboard.GenerateRecentPost();
                        }

                        if (mInterstitialAd.IsLoaded)
                        {
                            mInterstitialAd.Show();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected override async void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent intent)
        {
            try
            {
                if (requestCode == 102)
                {
                    if (resultCode == Result.Ok)
                    {
                        GetItem();
                    }
                }

                if (resultCode == Result.Ok)
                {
                    if (requestCode == 101)
                    {
                        if ((resultCode == Result.Ok))
                        {

                            Bitmap bitmap = LoadAndResizeBitmap(outputDir.AbsolutePath);
                            if (bitmap != null)
                            {
                                byte[] bitmapData;
                                var stream = new MemoryStream();
                                bitmap.Compress(Bitmap.CompressFormat.Jpeg, 90, stream);
                                bitmapData = stream.ToArray();

                                UserDialogs.Instance.ShowLoading("Uploading...");
                                var response = await presenter.UploadPhoto(bitmapData);


                                if (response.Success)
                                {
                                    if (Photos == null)
                                        Photos = new List<string>();
                                    Photos.Add(response.Data);

                                    ImageView image = new ImageView(this);
                                    var paramsImage = new LinearLayout.LayoutParams(150, 150);
                                    paramsImage.LeftMargin = 20;
                                    image.LayoutParameters = paramsImage;
                                    image.SetScaleType(scaleType: ImageView.ScaleType.CenterCrop);
                                    image.SetImageBitmap(bitmap);
                                    image.Tag = outputDir.AbsolutePath;
                                    image.Click += Image_Click;
                                    llPhotos.AddView(image);

                                    if (LocalPhotos == null)
                                        LocalPhotos = new List<string>();
                                    LocalPhotos.Add(outputDir.AbsolutePath);
                                }
                                UserDialogs.Instance.HideLoading();

                                GC.Collect();
                            }
                        }
                    }
                    if (requestCode == 102)
                    {
                        if (intent.Data == null)
                        {
                            if (intent.ClipData != null)
                            {
                                if (Photos == null)
                                    Photos = new List<string>();

                                if (LocalPhotos == null)
                                    LocalPhotos = new List<string>();

                                if (intent.ClipData.ItemCount + Photos.Count > PhotosAllowed)
                                {
                                    await Task.Delay(1000);
                                    UserDialogs.Instance.Alert(GetText(Resource.String.SorryYouHaveExceededMaxPhoto));
                                    return;
                                }
                                LoadingText = $"{GetText(Resource.String.UploadingPhotos)} ({1} out of {intent.ClipData.ItemCount}).";
                                var progress = UserDialogs.Instance.Progress(LoadingText, null, null, true);

                                for (int y = 0; y < intent.ClipData.ItemCount; y++)
                                {
                                    int counts = intent.ClipData.ItemCount > (PhotosAllowed - Photos.Count) ? PhotosAllowed - Photos.Count : intent.ClipData.ItemCount;
                                    LoadingText = $"{GetText(Resource.String.UploadingPhotos)} ({y + 1} out of {intent.ClipData.ItemCount}).";
                                    progress.Title = LoadingText;
                                    double percentage = ((y + 1.0) / counts);
                                    percentage = percentage * 100d;
                                    progress.PercentComplete = (int)percentage;
                                    ClipData.Item item = intent.ClipData.GetItemAt(y);
                                    var uri = item.Uri;
                                    if (uri == null)
                                    {
                                        InvalidPhotos++;
                                        continue;
                                    }
                                    ExifInterface exif = null;
                                    try
                                    {
                                        exif = new ExifInterface(GetPathToImage(uri));
                                    }
                                    catch (System.Exception ex)
                                    {
                                        //Crashes.TrackError(ex);
                                    }

                                    int orientation = exif != null ? exif.GetAttributeInt(ExifInterface.TagOrientation, 0) : 0;
                                    Bitmap bitmap = null;

                                    try
                                    {
                                        bitmap = MediaStore.Images.Media.GetBitmap(this.ContentResolver, uri);
                                    }
                                    catch (Exception ex)
                                    {
                                        //Crashes.TrackError(ex);
                                    }

                                    if (bitmap == null)
                                    {
                                        InvalidPhotos++;
                                        continue;
                                    }

                                    var path = Save(MediaHelper._dir.AbsolutePath, bitmap);

                                    MediaHelper.bitmap = path.LoadAndResizeBitmap(750, 750);

                                    MediaHelper.bitmap = RotateBitmap(MediaHelper.bitmap, orientation);

                                    if (MediaHelper.bitmap != null)
                                    {
                                        byte[] bitmapData;
                                        var stream = new MemoryStream();
                                        MediaHelper.bitmap.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
                                        bitmapData = stream.ToArray();
                                        //UserDialogs.Instance.ShowLoading("Uploading Photo...");


                                        var response = await presenter.UploadPhoto(bitmapData);
                                        if (response.Success)
                                        {
                                            if (Photos == null)
                                                Photos = new List<string>();
                                            Photos.Add(response.Data);

                                            ImageView image = new ImageView(this);
                                            var paramsImage = new LinearLayout.LayoutParams(150, 150);
                                            paramsImage.LeftMargin = 20;
                                            image.LayoutParameters = paramsImage;
                                            image.SetScaleType(scaleType: ImageView.ScaleType.CenterCrop);
                                            image.SetImageBitmap(MediaHelper.bitmap);
                                            image.Tag = path;
                                            image.Click += Image_Click;
                                            llPhotos.AddView(image);

                                            if (LocalPhotos == null)
                                                LocalPhotos = new List<string>();
                                            LocalPhotos.Add(path);
                                        }
                                        MediaHelper.bitmap.Dispose();
                                        MediaHelper.bitmap = null;
                                    }
                                    // Dispose of the Java side bitmap.
                                    GC.Collect();
                                }
                                progress.Hide();

                                if (InvalidPhotos > 0)
                                {
                                    UserDialogs.Instance.Alert($"We detected {InvalidPhotos} invalid/ private photo. Please use other photo as replacement.");
                                    InvalidPhotos = 0;
                                }
                                InvalidPhotos = 0;
                                //UserDialogs.Instance.Progress(LoadingText, null, null, true);
                            }
                        }
                        else
                        {
                            ExifInterface exif = null;
                            try
                            {
                                exif = new ExifInterface(GetPathToImage(intent.Data));

                            }
                            catch (System.Exception ex)
                            {
                                //Crashes.TrackError(ex);
                            }

                            int orientation = exif != null ? exif.GetAttributeInt(ExifInterface.TagOrientation, 0) : 0;
                            Bitmap bitmap = MediaStore.Images.Media.GetBitmap(this.ContentResolver, intent.Data);

                            var path = Save(MediaHelper._dir.AbsolutePath, bitmap);

                            MediaHelper.bitmap = path.LoadAndResizeBitmap(750, 750);

                            MediaHelper.bitmap = RotateBitmap(MediaHelper.bitmap, orientation);

                            if (MediaHelper.bitmap != null)
                            {
                                byte[] bitmapData;
                                var stream = new MemoryStream();
                                MediaHelper.bitmap.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
                                bitmapData = stream.ToArray();
                                UserDialogs.Instance.ShowLoading("Uploading Photo...");

                                var response = await presenter.UploadPhoto(bitmapData);
                                UserDialogs.Instance.HideLoading();
                                if (response.Success)
                                {
                                    if (Photos == null)
                                        Photos = new List<string>();
                                    Photos.Add(response.Data);

                                    ImageView image = new ImageView(this);
                                    var paramsImage = new LinearLayout.LayoutParams(150, 150);
                                    paramsImage.LeftMargin = 20;
                                    image.LayoutParameters = paramsImage;
                                    image.SetScaleType(scaleType: ImageView.ScaleType.CenterCrop);
                                    image.SetImageBitmap(bitmap);
                                    image.Tag = path;
                                    image.Click += Image_Click;
                                    llPhotos.AddView(image);


                                    if (LocalPhotos == null)
                                        LocalPhotos = new List<string>();
                                    LocalPhotos.Add(path);
                                }
                                MediaHelper.bitmap = null;
                            }
                            // Dispose of the Java side bitmap.
                            GC.Collect();
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            base.OnActivityResult(requestCode, resultCode, intent);
        }

        private async void Image_Click(object sender, EventArgs e)
        {
            var response = await UserDialogs.Instance.ConfirmAsync(GetText(Resource.String.DoYouWantToRemoveThisPhoto), "", GetText(Resource.String.Yes), GetText(Resource.String.Cancel));
            if (response)
            {
                var imageView = sender as ImageView;
                llPhotos.RemoveView(imageView);
                var index = LocalPhotos.IndexOf(imageView.Tag.ToString());
                Photos.RemoveAt(index);
                LocalPhotos.RemoveAt(index);
            }
        }

        bool isOfferTab = false;

        private async void TvOfferText_Click(object sender, EventArgs e)
        {
         
            isOfferTab = true;
            RecheckEmptyText();
            
            lvCommentList.Visibility = ViewStates.Visible;
            tvCommentText.SetTextColor(Android.Graphics.Color.Black);
            tvOfferText.SetTextColor(Android.Graphics.Color.ParseColor("#27AE60"));
     
            if (DTO.status == "ACTIVE")
            {
                btnMakeAnOffer.Visibility = DTO.user.id == presenter.userService.CurrentUser.id ? ViewStates.Gone : ViewStates.Visible;
                btnStatusCompleted.Visibility = ViewStates.Gone;
                rlComment.Visibility = ViewStates.Visible;
            }
            else
            {
                btnMakeAnOffer.Visibility = ViewStates.Gone;
                btnStatusCompleted.Visibility = ViewStates.Visible;
                rlComment.Visibility = ViewStates.Gone;
            }
            rlComment.Visibility = ViewStates.Gone;
            GetOffers(true);
     
        }

        private void TvCommentText_Click(object sender, EventArgs e)
        {
            isOfferTab = false;
            RecheckEmptyText();
            rlComment.Visibility = ViewStates.Visible;
            lvCommentList.Visibility = ViewStates.Visible;
            tvCommentText.SetTextColor(Android.Graphics.Color.ParseColor("#27AE60"));
            tvOfferText.SetTextColor(Android.Graphics.Color.Black);
           
            if (DTO.status == "ACTIVE")
            {
                btnMakeAnOffer.Visibility = DTO.user.id == presenter.userService.CurrentUser.id ? ViewStates.Gone : ViewStates.Visible;
                btnStatusCompleted.Visibility = ViewStates.Gone;
                rlComment.Visibility = ViewStates.Visible;
            }
            else
            {
                btnMakeAnOffer.Visibility = ViewStates.Gone;
                btnStatusCompleted.Visibility = ViewStates.Visible;
                rlComment.Visibility = ViewStates.Gone;
            }
            GetComments(true);

        }

        bool notSameUser;
        ItemDTO DTO;
        List<string> listOfImages;
        async Task GetItem() {
            UserDialogs.Instance.ShowLoading();
            var response = await presenter.GetItem(Intent.GetLongExtra("Id", 0));
            if (response.Success)
            {
                DTO = response.Data;
                adapter.barterOwner = presenter.userService.CurrentUser.id == response.Data.user.id;
                GetComments();

                tvItemName.Text = response.Data.Item;
                tvLocation.Text = response.Data.location.city + " " + response.Data.location.province;
                tvLocationType.Text = response.Data.locationType == MobileNetStandardLibrary.Model.LocationType.STRICT ? "STRICTLY WITHIN" : "OPEN LOCATION";
                tvLocationType.SetTextColor(response.Data.locationType == MobileNetStandardLibrary.Model.LocationType.STRICT ? Android.Graphics.Color.Red : Android.Graphics.Color.Blue);
                tvReason.Text = response.Data.reason;
                tvPreferences.Text = response.Data.preferences;
                tvName.Text = response.Data.user.firstName + " " + response.Data.user.lastName;
                tvPostedDate.Text = CommonAndroidHelper.ConvertDateTimeToTextLong(response.Data.datePosted.GetValueOrDefault().ToLocalTime());
                if (response.Data.deliveryType == MobileNetStandardLibrary.Model.DeliveryOption.COURIER)
                {
                    tvDeliveryOption.Text = $"{response.Data.deliveryType.ToString()} via {response.Data.courier} at {response.Data.location.location} {response.Data.location.city} {response.Data.location.province}";
                }
                else {
                    tvDeliveryOption.Text = $"{response.Data.deliveryType.ToString()} at {response.Data.location.location} {response.Data.location.city}, {response.Data.location.province}";
                }
                tvContactNumber.Text = $"{response.Data.contactNumber}";
                //Picasso.Get().Load(URLPath.BaseURL + response.Data.photos[0]).Placeholder(Resource.Mipmap.product_placeholder).MemoryPolicy(MemoryPolicy.NoCache).Into(ivImage);
                Picasso.Get().Load(URLPath.BaseURL + response.Data.user.imageUrl).Placeholder(Resource.Mipmap.user_placeholder).MemoryPolicy(MemoryPolicy.NoCache).Into(ivProfilePic);

                if (response.Data.user.id == presenter.userService.CurrentUser.id) {
                    btnBarterComplete.Visibility = ViewStates.Visible;
                    notSameUser = true;
                }
                listOfImages = new List<string>();

                listOfImages.AddRange(response.Data.photos);
                ivImage.Adapter = new ImagePagerAdapter(this, listOfImages);
                if (listOfImages.Count > 1) {
                    ivRight.Visibility = ViewStates.Visible;
                }

                if (notSameUser)
                {
                    ivSearch.Visibility = ViewStates.Visible;
                    if (DTO.status == "ACTIVE")
                    {
                        btnBarterComplete.Visibility = ViewStates.Visible;
                        btnStatusCompleted.Visibility = ViewStates.Gone;
                        rlComment.Visibility = ViewStates.Visible;
                        btnMakeAnOffer.Visibility = ViewStates.Gone;
                    }
                    else if (DTO.status == ItemStatus.COMPLETED.ToString())
                    {
                        btnStatusCompleted.Visibility = ViewStates.Visible;
                        btnStatusCompleted.Text = "Barter Completed";
                        btnBarterComplete.Visibility = ViewStates.Gone;
                        rlComment.Visibility = ViewStates.Gone;
                        btnMakeAnOffer.Visibility = ViewStates.Gone;
                    }
                    else if (DTO.status == ItemStatus.CANCELLED.ToString())
                    {
                        btnStatusCompleted.Visibility = ViewStates.Visible;
                        btnStatusCompleted.Text = "Deleted";
                        btnStatusCompleted.SetTextColor(Android.Graphics.Color.Red);
                        btnBarterComplete.Visibility = ViewStates.Gone;
                        rlComment.Visibility = ViewStates.Gone;
                        btnMakeAnOffer.Visibility = ViewStates.Gone;
                    }
                }
                else
                {
                    ivSearch.Visibility = ViewStates.Gone;
                    btnBarterComplete.Visibility = ViewStates.Gone;
                    if (DTO.status == "ACTIVE")
                    {
                        rlComment.Visibility = ViewStates.Visible;
                        btnMakeAnOffer.Visibility = ViewStates.Visible;
                    }
                    else if (DTO.status == ItemStatus.COMPLETED.ToString())
                    {
                        btnStatusCompleted.Visibility = ViewStates.Visible;
                        btnStatusCompleted.Text = "Barter Completed";
                        rlComment.Visibility = ViewStates.Gone;
                        btnMakeAnOffer.Visibility = ViewStates.Gone;
                    }
                    else if (DTO.status == ItemStatus.CANCELLED.ToString())
                    {
                        btnStatusCompleted.Visibility = ViewStates.Visible;
                        btnStatusCompleted.Text = "Deleted";
                        btnStatusCompleted.SetTextColor(Android.Graphics.Color.Red);
                        rlComment.Visibility = ViewStates.Gone;
                        btnMakeAnOffer.Visibility = ViewStates.Gone;
                    }
                }

                tvCommentText.Text = $"{response.Data.commentCount} Comments";
                tvOfferText.Text = $"{response.Data.offerCount} Offers";
                totalComments = response.Data.commentCount;
                totalOffers = response.Data.offerCount;

                try
                {
                    if (Intent.HasExtra("CommentId"))
                    {
                        Intent intent = new Intent(this, typeof(CommentsSecondActivity));
                        intent.PutExtra("BarterId", DTO.id.ToString());
                        intent.PutExtra("Id", Intent.GetStringExtra("CommentId"));
                        intent.PutExtra("IsComment", true);
                        StartActivity(intent);
                    }

                    if (Intent.HasExtra("OfferId"))
                    {
                        Intent intent = new Intent(this, typeof(CommentsSecondActivity));
                        intent.PutExtra("BarterId", DTO.id.ToString());
                        intent.PutExtra("Id", Intent.GetStringExtra("OfferId"));
                        intent.PutExtra("IsComment", false);
                        StartActivity(intent);
                    }
                }
                catch (Exception ex)
                {

                }
            }
            UserDialogs.Instance.HideLoading();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        public void OnShow(IDialogInterface dialog)
        {
            BottomSheetDialog d = (BottomSheetDialog)dialog;
            var bottomSheet = d.FindViewById(Resource.Id.design_bottom_sheet);
            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.From(bottomSheet);
            bottomSheetBehavior.State = BottomSheetBehavior.StateExpanded;

        }

        public class AdListener : Android.Gms.Ads.AdListener
        {
            AppCompatActivity that;

            public AdListener(AppCompatActivity t)
            {
                that = t;
            }

            public override void OnAdFailedToLoad(int p0)
            {
                base.OnAdFailedToLoad(p0);
            }

            public override void OnAdLoaded()
            {
                base.OnAdLoaded();
            }

            public override void OnAdOpened()
            {
                base.OnAdOpened();
            }

            public override void OnAdClosed()
            {
                base.OnAdClosed();
            }
        }
    }
}
