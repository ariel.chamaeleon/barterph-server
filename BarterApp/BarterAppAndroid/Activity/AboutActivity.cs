﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AppStandardLibrary;

namespace BarterAppAndroid.Activity
{
    [Activity(Label = "AboutActivity", Theme = "@style/AppTheme.NoActionBar", ScreenOrientation = ScreenOrientation.Portrait)]
    public class AboutActivity : BaseActivity
    {
        TextView tvVersion;
        TextView tvPrivacyPolicy;
        TextView tvTermsAndConditions;
        TextView tvDescription;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.about_activity);
            tvVersion = FindViewById<TextView>(Resource.Id.tvVersion);
            tvDescription = FindViewById<TextView>(Resource.Id.tvDescription);

            var vNum = Application.Context.ApplicationContext.PackageManager.GetPackageInfo(Application.Context.ApplicationContext.PackageName, 0).VersionName;
            
            tvVersion.Text = $"Beta Version {vNum}\n\nLast update date: Aug 8, 2020";

            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            var actionBar = SupportActionBar;
            actionBar.SetHomeButtonEnabled(true);
            actionBar.SetDisplayHomeAsUpEnabled(true);
            actionBar.Title = "Profile";
            var ivCheck = toolbar.FindViewById<ImageView>(Resource.Id.ivCheck);

            ivCheck.Visibility = ViewStates.Gone;
            // Create your application here

            tvPrivacyPolicy = FindViewById<TextView>(Resource.Id.tvPrivacyPolicy);
            tvTermsAndConditions = FindViewById<TextView>(Resource.Id.tvTerms);

            tvPrivacyPolicy.Click += TvPrivacyPolicy_Click;
            tvTermsAndConditions.Click += TvTermsAndConditions_Click;

            if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
            {
                tvDescription.JustificationMode = Android.Text.JustificationMode.InterWord;
            }
        }

        private void TvTermsAndConditions_Click(object sender, EventArgs e)
        {
            var uri = Android.Net.Uri.Parse(URLPath.TermsAndConditions);
            var intent = new Intent(Intent.ActionView, uri);
            StartActivity(intent);
        }

        private void TvPrivacyPolicy_Click(object sender, EventArgs e)
        {
            var uri = Android.Net.Uri.Parse(URLPath.PrivacyPolicy);
            var intent = new Intent(Intent.ActionView, uri);
            StartActivity(intent);
        }
    }
}
