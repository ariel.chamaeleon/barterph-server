﻿using System;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.Gms.Common;
using Android.Net;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using AppStandardLibrary;
using AppStandardLibrary.Model;
using AppStandardLibrary.Repository;
using AppStandardLibrary.Services.Interface;
using BarterApp.Droid;
using BarterAppAndroid.Activity;
using MobileNetStandardLibrary.Controller;
using Unity;
using Unity.Lifetime;
using Xamarin.Essentials;
using Xamarin.Facebook;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Android.Preferences;
using Android.Gms.Ads;

namespace BarterAppAndroid
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        private const string FACEBOOKID = "311047940066108";
        public static bool IsAppActive;

        static readonly string TAG = "MainActivity";

        internal static readonly string CHANNEL_ID = "my_notification_channel";
        internal static readonly int NOTIFICATION_ID = 100;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            MobileAds.Initialize(ApplicationContext, "ca-app-pub-4384472926389482~6817257508");
            FacebookSdk.AutoInitEnabled = true;
            FacebookSdk.FullyInitialize();
            FacebookSdk.ApplicationId = FACEBOOKID;
            FacebookSdk.SdkInitialize(Application.Context);
            AppCenter.Start("968dd136-1b44-4d67-9714-4e49f6b1f38c",
                   typeof(Analytics), typeof(Crashes));

            var vNum = Application.Context.ApplicationContext.PackageManager.GetPackageInfo(Application.Context.ApplicationContext.PackageName, 0).VersionName;
            MobileNetStandardLibrary.Model.Device.DeviceId = Android.Provider.Settings.Secure.GetString(ContentResolver, Android.Provider.Settings.Secure.AndroidId);
            MobileNetStandardLibrary.Model.Device.DeviceName = Xamarin.Essentials.DeviceInfo.Name;
            MobileNetStandardLibrary.Model.Device.AppVersion = vNum;

            Resolver.Container = new UnityContainer();
            Resolver.Container.RegisterType<IUser, AppStandardLibrary.Services.Implementation.User>(new ContainerControlledLifetimeManager());
            Resolver.Container.RegisterType<IFileHelper, FileHelper>(new ContainerControlledLifetimeManager());
            Resolver.Container.RegisterType<IAccountDatabase, AccountDatabase>(new ContainerControlledLifetimeManager());
            Resolver.Container.RegisterType<IAccountService, AccountService>(new ContainerControlledLifetimeManager());
            Resolver.Container.RegisterType<IRestService, RestService>(new ContainerControlledLifetimeManager());

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.SetVmPolicy(builder.Build());
            UserDialogs.Init(this);
            IsPlayServicesAvailable();
            CreateNotificationChannel();
            DismissSplash();
        }

        public bool IsPlayServicesAvailable()
        {
            int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if (resultCode != ConnectionResult.Success)
            {
                if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
                {
                    //msgText.Text = GoogleApiAvailability.Instance.GetErrorString(resultCode);
                }
                else
                {
                    Finish();
                }
                return false;
            }
            else
            {
                //msgText.Text = "Google Play Services is available.";
                return true;
            }
        }

        void CreateNotificationChannel()
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
                // Notification channels are new in API 26 (and not a part of the
                // support library). There is no need to create a notification
                // channel on older versions of Android.
                return;
            }

            var channel = new NotificationChannel(CHANNEL_ID,
                                                  "FCM Notifications",
                                                  NotificationImportance.Default)
            {

                Description = "Firebase Cloud Messages appear in this channel"
            };

            var notificationManager = (NotificationManager)GetSystemService(Android.Content.Context.NotificationService);
            notificationManager.CreateNotificationChannel(channel);
        }

        public bool IsOnline()
        {
            var cm = (ConnectivityManager)GetSystemService(ConnectivityService);
            return cm.ActiveNetworkInfo == null ? false : cm.ActiveNetworkInfo.IsConnected;
        }

        async Task DismissSplash()
        {
            if (DeviceInfo.Idiom == DeviceIdiom.Tablet)
            {
                UserDialogs.Instance.Alert("Sorry your device is not yet supported.");
                return;
            }

            if (IsOnline())
            {
                await Task.Delay(2000);
                var controller = Resolver.Container.Resolve(typeof(BaseViewPresenter)) as BaseViewPresenter;
                await controller.accountService.CheckIfUserExist();

                //var controller = Resolver.Container.Resolve(typeof(BaseViewController)) as BaseViewController;
                if (controller.userService.CurrentUser == null || string.IsNullOrEmpty(controller.userService.CurrentUser.token))
                {
                    Intent intent = new Intent(this, typeof(LandingActivity));
                    intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.ClearTask | ActivityFlags.NewTask);
                    StartActivity(intent);
                }
                else
                {

                    ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(ApplicationContext);
                    var token = prefs.GetString("RegistrationToken", "");
                    if (!string.IsNullOrEmpty(token))
                    {
                        controller.accountService.UpdateFCMToken(token);
                    }

                    if (Intent.HasExtra("barterId"))
                    {
                        Intent intent = new Intent(this, typeof(DashboardActivity));
                        intent.PutExtra("barterId", Intent.GetStringExtra("barterId"));
                        intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.ClearTask | ActivityFlags.NewTask);
                        StartActivity(intent);
                    }
                    else
                    {
                        Intent intent = new Intent(this, typeof(DashboardActivity));
                        intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.ClearTask | ActivityFlags.NewTask);
                        StartActivity(intent);
                    }
                }

            }
            else
            {
                await UserDialogs.Instance.AlertAsync(GetText(Resource.String.PleaseCheckInternet));
                await Task.Delay(2000);
                DismissSplash();
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View)sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                .SetAction("Action", (Android.Views.View.IOnClickListener)null).Show();
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public class Resolver
        {
            public static UnityContainer Container { get; set; }
        }
    }
}
