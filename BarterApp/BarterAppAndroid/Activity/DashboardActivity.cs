﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Gms.Ads;
using Android.Graphics;
using Android.Media;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Text;
using Android.Text.Style;
using Android.Views;
using Android.Widget;
using AppStandardLibrary;
using BarterApp.Droid;
using BarterAppAndroid.Controls;
using MobileNetStandardLibrary.Commands;
using MobileNetStandardLibrary.Model;
using Square.Picasso;
using static Android.Views.View;

namespace BarterAppAndroid.Activity
{
    [Activity(Label = "DashboardActivity", Theme = "@style/AppTheme.NoActionBar", ScreenOrientation = ScreenOrientation.Portrait, WindowSoftInputMode = SoftInput.StateHidden)]
    public class DashboardActivity : BaseActivity, NavigationView.IOnNavigationItemSelectedListener, IOnScrollChangeListener
    {
        public static DashboardActivity Dashboard { get; set; }

        DrawerLayout drawerLayout;
        NavigationView navigationView;

        RelativeLayout rlProfile;
        Android.Support.V7.App.ActionBarDrawerToggle mDrawerToggle;

        private LinearLayout llRecentPost;
        private LinearLayout llMostViewedPost;
        private LinearLayout llPhotos;
        private TextView tvPhotos;
        private HorizontalScrollView hScrollView;
        private HorizontalScrollView hScrollView2;

        private RadioGroup rgDeliveryOption;

        private TextView tvUserName;
        private ImageView ivProfilePic;

        public readonly int PickImageId = 102;
        public readonly int CameraId = 101;
        public readonly int ExternalStorageId = 1001;
        static readonly int REQUEST_CAMERA = 2;
        static readonly int REQUEST_GALLERY = 1;

        public static List<string> Photos;
        public static List<string> LocalPhotos;
        public static int UploadCount;
        public static int InvalidPhotos;

        public string LoadingText { get; set; }

        private Android.Net.Uri mHighQualityImageUri = null;

        private EditText etItemForBarter;
        private RadioGroup rgItemLocation;
        private EditText etItemLocation;
        private EditText etDeliveryOptions;
        private RadioGroup rgDeliveryOptions;
        private EditText etReasonForBarter;
        private EditText etPreference;
        private EditText etContactNumber;
        private LinearLayout llDeliveryOptions;
        private Button btnPost;
        private RadioButton rbMeetup;
        private RadioButton rbCourier;

        private RadioButton rbOpenLocation;
        private RadioButton rbStrictLocation;

        private TextView tvMostViewed;
        private TextView tvRecentPost;

        private ScrollView scrollView;

        private SwipeRefreshLayout swipeRefreshLayout;

        private AdView mAdView;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.DashboardActivity);

            mAdView = FindViewById<AdView>(Resource.Id.adView);
            var adRequest = new AdRequest.Builder().Build();
            mAdView.LoadAd(adRequest);

            swipeRefreshLayout = FindViewById<SwipeRefreshLayout>(Resource.Id.swipeRefreshLayout);
            swipeRefreshLayout.Refresh += SwipeRefreshLayout_Refresh;
            tvRecentPost = FindViewById<TextView>(Resource.Id.tvRecentPost);
            tvMostViewed = FindViewById<TextView>(Resource.Id.tvMostViewed);
            tvMostViewed.Visibility = ViewStates.Gone;
            tvRecentPost.Visibility = ViewStates.Gone;
            etItemForBarter = FindViewById<EditText>(Resource.Id.etItemForBarter);
            etItemLocation = FindViewById<EditText>(Resource.Id.etItemLocation);
            rgItemLocation = FindViewById<RadioGroup>(Resource.Id.rgLocationOfItem);
            etDeliveryOptions = FindViewById<EditText>(Resource.Id.etDeliveryOption);
            rgDeliveryOption = FindViewById<RadioGroup>(Resource.Id.rgDeliveryOption);
            etReasonForBarter = FindViewById<EditText>(Resource.Id.etReason);
            etPreference = FindViewById<EditText>(Resource.Id.etPeference);
            etContactNumber = FindViewById<EditText>(Resource.Id.etContactNumber);
            llDeliveryOptions = FindViewById<LinearLayout>(Resource.Id.llDeliveryOptions);
            rbMeetup = FindViewById<RadioButton>(Resource.Id.rbMeetup);
            rbCourier = FindViewById<RadioButton>(Resource.Id.rbCourier);
            rbOpenLocation = FindViewById<RadioButton>(Resource.Id.rbOpenLocation);
            rbStrictLocation = FindViewById<RadioButton>(Resource.Id.rbStrictlyWithinMyPreferredLocation);
            scrollView = FindViewById<ScrollView>(Resource.Id.scrollView);
            hScrollView = FindViewById<HorizontalScrollView>(Resource.Id.hscrollView);
            hScrollView2 = FindViewById<HorizontalScrollView>(Resource.Id.hscrollView2);

            rbOpenLocation.Checked = true;
            rbMeetup.Checked = true;

            btnPost = FindViewById<Button>(Resource.Id.btnPost);
            btnPost.Click += BtnPost_Click;

            llRecentPost = FindViewById<LinearLayout>(Resource.Id.llMostRecent);
            llMostViewedPost = FindViewById<LinearLayout>(Resource.Id.llMostViewed);
            llPhotos = FindViewById<LinearLayout>(Resource.Id.llPhotos);
            tvPhotos = FindViewById<TextView>(Resource.Id.tvPhotos);
            tvPhotos.Click += TvPhotos_Click;

            rgDeliveryOption.CheckedChange += RgDeliveryOption_CheckedChange;
            rgItemLocation.CheckedChange += RgItemLocation_CheckedChange;

            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            var actionBar = SupportActionBar;
            actionBar.SetHomeButtonEnabled(true);
            actionBar.Title = "Home";
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            mDrawerToggle = new Android.Support.V7.App.ActionBarDrawerToggle(this, drawerLayout, toolbar, Resource.String.app_name, Resource.String.app_name);
            drawerLayout.AddDrawerListener(mDrawerToggle);
            mDrawerToggle.DrawerIndicatorEnabled = true;
            mDrawerToggle.SyncState();

            var ivSearch = toolbar.FindViewById<ImageView>(Resource.Id.ivCheck);
            ivSearch.SetImageResource(Resource.Mipmap.search);
            ivSearch.Visibility = ViewStates.Visible;
            ivSearch.Click += IvSearch_Click;

            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            navigationView.SetNavigationItemSelectedListener(this);

            var menuNav = navigationView.Menu;
            var element = menuNav.FindItem(Resource.Id.nav_notifications);
            var before = element.TitleFormatted.ToString();

            var counter = "5";
            var s = before + "   " + counter + " ";
            SpannableString sColored = new SpannableString(s);

            //sColored.SetSpan(new B(Color.Red), s.Length - 3, s.Length, 0);
            sColored.SetSpan(new ForegroundColorSpan(Color.Red), s.Length - 3, s.Length, 0);
            sColored.SetSpan(new ForegroundColorSpan(Color.Red), s.Length - 3, s.Length, 0);
            //element.SetTitle(sColored);

            rlProfile = navigationView.GetHeaderView(0).FindViewById<RelativeLayout>(Resource.Id.rlProfile);
            rlProfile.Click += RlProfile_Click;
            ivProfilePic = navigationView.GetHeaderView(0).FindViewById<ImageView>(Resource.Id.ivProfilePic);
            tvUserName = navigationView.GetHeaderView(0).FindViewById<TextView>(Resource.Id.tvUserName);

            if (!string.IsNullOrEmpty(presenter.userService.CurrentUser.imageUrl))
                Picasso.Get().Load(URLPath.BaseURL + presenter.userService.CurrentUser.imageUrl).Placeholder(Resource.Mipmap.user_placeholder).Into(ivProfilePic);

            tvUserName.Text = presenter.userService.CurrentUser.firstName + " " + presenter.userService.CurrentUser.lastName;
            UserDialogs.Instance.ShowLoading();
            GenerateRecentPost();
            GenerateMostViewedPost();
            GetCityList();

            etItemLocation.Enabled = true;
            etItemLocation.Hint = "Address kung saan makikipag-meetup";
            etItemLocation.Focusable = false;
            etItemLocation.Clickable = true;
            etItemLocation.Click += EtDeliveryOptions_Click;

            etDeliveryOptions.Visibility = ViewStates.Gone;
            etDeliveryOptions.Text = "";
            etDeliveryOptions.Hint = "ex. LBC, 2go, JRS, etc";
            Dashboard = this;

            if (Intent.HasExtra("barterId"))
            {
                Intent intent = new Intent(this, typeof(NotificationActivity));
                StartActivity(intent);
            }
            scrollView.SetOnScrollChangeListener(this);

            hScrollView.ScrollChange += HScrollView_ScrollChange;
            hScrollView2.ScrollChange += HScrollView2_ScrollChange;
        }

        private async void HScrollView2_ScrollChange(object sender, ScrollChangeEventArgs e)
        {
            swipeRefreshLayout.Enabled = false;
            await Task.Delay(2000);
            swipeRefreshLayout.Enabled = true;
        }

        private async void HScrollView_ScrollChange(object sender, ScrollChangeEventArgs e)
        {
            swipeRefreshLayout.Enabled = false;
            await Task.Delay(2000);
            swipeRefreshLayout.Enabled = true;
        }

        private async void SwipeRefreshLayout_Refresh(object sender, EventArgs e)
        {
            swipeRefreshLayout.Refreshing = true;
            await GenerateMostViewedPost();
            await GenerateRecentPost();
            swipeRefreshLayout.Refreshing = false;
        }

        public void UpdateNameAndPhoto()
        {
            if (!string.IsNullOrEmpty(presenter.userService.CurrentUser.imageUrl))
                Picasso.Get().Load(URLPath.BaseURL + presenter.userService.CurrentUser.imageUrl).Placeholder(Resource.Mipmap.user_placeholder).Into(ivProfilePic);
            tvUserName.Text = presenter.userService.CurrentUser.firstName + " " + presenter.userService.CurrentUser.lastName;
        }

        public override void OnBackPressed()
        {
            if (drawerLayout.IsShown)
            {
                drawerLayout.CloseDrawers();
            }
            else
            {
                base.OnBackPressed();
            }
        }

        Dialog dialog;

        public void UpdateCount(int badgeCount)
        {

        }

        private void EtDeliveryOptions_Click(object sender, EventArgs e)
        {
            ShowDialog();
        }

        private void RgItemLocation_CheckedChange(object sender, RadioGroup.CheckedChangeEventArgs e)
        {
            if (e.CheckedId == Resource.Id.rbStrictlyWithinMyPreferredLocation)
            {
                etItemLocation.Text = string.Empty;
                etItemLocation.Hint = rbCourier.Checked ? "Address ng padadalhan base sa Home Location ko" : "Address ng meetup base sa Home Location ko";
            }
            else
            {
                etItemLocation.Text = string.Empty;
                etItemLocation.Hint = rbCourier.Checked ? "Address ng padadalhan ng barter" : "Address kung saan makikipag-meetup";

            }
        }

        private void RgDeliveryOption_CheckedChange(object sender, RadioGroup.CheckedChangeEventArgs e)
        {
            if (e.CheckedId == Resource.Id.rbCourier)
            {
                etDeliveryOptions.Visibility = ViewStates.Visible;
                etItemLocation.Hint = rbOpenLocation.Checked ? "Address ng padadalhan ng barter" : "Address ng padadalhan base sa Home Location ko";

            }
            else
            {
                etDeliveryOptions.Visibility = ViewStates.Gone;
                etItemLocation.Hint = rbOpenLocation.Checked ? "Address kung saan makikipag-meetup" : "Address ng meetup base sa Home Location ko";
            }
        }


        GenericEditText etProvince;
        GenericEditText etCity;
        GenericEditText etOpenLocation;
        Button btnUseThisAddress;
        Dialog addressDialog;

        async void ShowDialog()
        {
            if (addressDialog == null)
            {
                addressDialog = new Dialog(this);
                addressDialog.SetContentView(Resource.Layout.addressDialog);
                addressDialog.SetTitle("Set Address");
                etCity = addressDialog.FindViewById<GenericEditText>(Resource.Id.etCity);
                etProvince = addressDialog.FindViewById<GenericEditText>(Resource.Id.etProvince);
                etOpenLocation = addressDialog.FindViewById<GenericEditText>(Resource.Id.etOpenLocation);
                btnUseThisAddress = addressDialog.FindViewById<Button>(Resource.Id.btnUseAddress);

                etCity.Field.Focusable = false;
                etCity.Field.Clickable = true;
                etCity.Field.Click += Field_Click;
                etProvince.Field.Focusable = false;
                etProvince.Field.Clickable = true;
                etProvince.Field.Click += Field_Click1;

                etOpenLocation.Field.Hint = etOpenLocation.floatingLabel.Text = "Specific Meetup Location";
                etProvince.Field.Hint = etProvince.floatingLabel.Text = "Province";
                etCity.Field.Hint = etCity.floatingLabel.Text = "City/ Municipality";

                btnUseThisAddress.Click += BtnUseThisAddress_Click;
            }

            if (rbStrictLocation.Checked)
            {
                etOpenLocation.Field.Text = string.Empty;
                etProvince.Field.Text = presenter.userService.CurrentUser.province;
                etCity.Field.Text = presenter.userService.CurrentUser.city;
                etProvince.Field.Enabled = false;
                etCity.Field.Enabled = false;
                etProvince.arrowDown.Visibility = ViewStates.Gone;
                etCity.arrowDown.Visibility = ViewStates.Gone;
            }
            else
            {
                etOpenLocation.Field.Text = string.Empty;
                etProvince.Field.Text = string.Empty;
                etCity.Field.Text = string.Empty;
                etProvince.Field.Enabled = true;
                etCity.Field.Enabled = true;
                etProvince.arrowDown.Visibility = ViewStates.Visible;
                etCity.arrowDown.Visibility = ViewStates.Visible;
            }

            addressDialog.Show();
        }

        string openLocationText;
        string cityText;
        string provinceText;

        private void BtnUseThisAddress_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(etOpenLocation.Field.Text.Trim()))
            {
                UserDialogs.Instance.Alert("Sepecific meetup is required");
                return;
            }

            if (string.IsNullOrEmpty(etCity.Field.Text.Trim()))
            {
                UserDialogs.Instance.Alert("City is required");
                return;
            }

            if (string.IsNullOrEmpty(etProvince.Field.Text.Trim()))
            {
                UserDialogs.Instance.Alert("Province is required");
                return;
            }

            addressDialog.Hide();
            etItemLocation.Text = etOpenLocation.Field.Text + " " + etCity.Field.Text + ", " + etProvince.Field.Text;

            openLocationText = etOpenLocation.Field.Text;
            cityText = etCity.Field.Text;
            provinceText = etProvince.Field.Text;
        }

        private async void Field_Click1(object sender, EventArgs e)
        {
            var countryPicker = await UserDialogs.Instance.ActionSheetAsync("Choose your Province", GetText(Resource.String.Cancel), null, null, listOfProvince.ToArray());
            if (countryPicker == "Cancel")
                return;
            etProvince.Field.Text = countryPicker;
            etCity.Field.Text = "";
        }

        private async void Field_Click(object sender, EventArgs e)
        {
            listOfCity = new List<string>();
            String[] rl = this.Resources.GetStringArray(Resource.Array.ZipCodes);
            for (int i = 0; i < rl.Length; i++)
            {
                String[] g = rl[i].Split(",");
                if (etProvince.Field.Text != "")
                {
                    if (g[1] == etProvince.Field.Text)
                    {
                        if (!listOfCity.Contains(g[2]))
                            listOfCity.Add(g[2]);
                    }

                }
                else
                {
                    if (!listOfCity.Contains(g[2]))
                        listOfCity.Add(g[2]);
                }
            }

            listOfCity.Sort();
            var countryPicker = await UserDialogs.Instance.ActionSheetAsync("Choose your City", GetText(Resource.String.Cancel), null, null, listOfCity.ToArray());
            if (countryPicker == "Cancel")
                return;
            etCity.Field.Text = countryPicker;
            for (int i = 0; i < rl.Length; i++)
            {
                String[] g = rl[i].Split(",");
                if (countryPicker == g[2])
                {
                    //etProvince.Field.Text = g[1];

                }
            }
        }

        public List<string> listOfCity;
        public List<string> listOfProvince;

        private void GetCityList()
        {
            listOfCity = new List<string>();
            listOfProvince = new List<string>();
            String[] rl = this.Resources.GetStringArray(Resource.Array.ZipCodes);
            for (int i = 0; i < rl.Length; i++)
            {
                String[] g = rl[i].Split(",");
                if (!listOfCity.Contains(g[2]))
                    listOfCity.Add(g[2]);

                if (!listOfProvince.Contains(g[1]))
                    listOfProvince.Add(g[1]);
            }
            listOfCity.Sort();
        }


        private async void BtnPost_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(etItemForBarter.Text.Trim()))
            {
                UserDialogs.Instance.Alert("Item for barter is required");
                return;
            }

            if (string.IsNullOrEmpty(openLocationText))
            {
                UserDialogs.Instance.Alert("Specific meetup location is required");
                return;
            }

            if (string.IsNullOrEmpty(cityText))
            {
                UserDialogs.Instance.Alert("City is required");
                return;
            }

            if (string.IsNullOrEmpty(provinceText))
            {
                UserDialogs.Instance.Alert("Province is required");
                return;
            }

            if (string.IsNullOrEmpty(etContactNumber.Text.Trim()))
            {
                UserDialogs.Instance.Alert("Contact number is required");
                return;
            }

            if (string.IsNullOrEmpty(etPreference.Text.Trim()))
            {
                UserDialogs.Instance.Alert("Preference field is required");
                return;
            }

            if (string.IsNullOrEmpty(etReasonForBarter.Text.Trim()))
            {
                UserDialogs.Instance.Alert("Reason for barter is required");
                return;
            }

            if (Photos?.Count == 0)
            {
                UserDialogs.Instance.Alert("Atleast one photo is required");
                return;
            }

            UserDialogs.Instance.ShowLoading();
            var response = await presenter.PostItem(new MobileNetStandardLibrary.Commands.CreateBartersCommand
            {
                item = etItemForBarter.Text.Trim(),
                locationType = rbOpenLocation.Checked ? LocationType.OPEN.ToString() : LocationType.STRICT.ToString(),
                deliveryType = rbCourier.Checked ? DeliveryOption.COURIER.ToString() : DeliveryOption.MEETUP.ToString(),
                deliveryCity = cityText,
                deliveryLocation = openLocationText,
                deliveryProvince = provinceText,
                courier = etDeliveryOptions.Text.Trim(),
                location = openLocationText,
                city = cityText,
                province = provinceText,
                contactNumber = etContactNumber.Text.Trim(),
                preferences = etPreference.Text.Trim(),
                reason = etReasonForBarter.Text.Trim(),
                photos = Photos
            });

            UserDialogs.Instance.HideLoading();
            if (response.Success)
            {
                GenerateMostViewedPost();
                GenerateRecentPost();
                scrollView.ScrollTo(0, 0);
                await UserDialogs.Instance.AlertAsync("Barter is posted successfully!");

                etItemForBarter.Text = string.Empty;
                etOpenLocation.Field.Text = string.Empty;
                etPreference.Text = string.Empty;
                Photos?.Clear();
                LocalPhotos?.Clear();
                llPhotos.RemoveAllViews();
                etCity.Field.Text = string.Empty;
                etProvince.Field.Text = string.Empty;
                etReasonForBarter.Text = string.Empty;
                etDeliveryOptions.Text = string.Empty;
                etItemLocation.Text = string.Empty;
                etContactNumber.Text = string.Empty;
                Intent intent = new Intent(this, typeof(BarterItemDetailActivity));
                intent.PutExtra("Id", response.Data.id);
                StartActivity(intent);

            }
        }

        public bool OnNavigationItemSelected(IMenuItem menuItem)
        {
            if (menuItem.ItemId == Resource.Id.nav_logout)
            {
                Logout();
            }

            if (menuItem.ItemId == Resource.Id.nav_settings)
            {
                Intent intent = new Intent(this, typeof(UserSettingsActivity));
                StartActivity(intent);
            }


            if (menuItem.ItemId == Resource.Id.nav_about)
            {
                Intent intent = new Intent(this, typeof(AboutActivity));
                StartActivity(intent);
            }

            if (menuItem.ItemId == Resource.Id.nav_my_list)
            {
                Intent intent = new Intent(this, typeof(SearchActivity));
                intent.PutExtra("MyList", true);
                StartActivity(intent);
            }

            if (menuItem.ItemId == Resource.Id.nav_notifications)
            {
                Intent intent = new Intent(this, typeof(NotificationActivity));
                StartActivity(intent);
            }

            if (menuItem.ItemId == Resource.Id.nav_support)
            {
                Intent intent = new Intent(this, typeof(SupportActivity));
                StartActivity(intent);
            }

            if (menuItem.ItemId == Resource.Id.nav_items)
            {
                Intent intent = new Intent(this, typeof(SearchActivity));
                StartActivity(intent);
            }
            return true;
        }

        async Task Logout()
        {
            if (await UserDialogs.Instance.ConfirmAsync("Are you sure you want to logout?"))
            {
                await presenter.database.DeleteUser(presenter.userService.CurrentUser);
                presenter.userService.CurrentUser = null;
                Intent intent = new Intent(this, typeof(LandingActivity));
                intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.ClearTask | ActivityFlags.NewTask);
                StartActivity(intent);
            }
        }

        protected override void OnPostCreate(Bundle savedInstanceState)
        {
            base.OnPostCreate(savedInstanceState);
            mDrawerToggle.SyncState();
        }

        private void TvPhotos_Click(object sender, EventArgs e)
        {
            AllowMultiplePhotos = true;
            ShowCameraPicker();
        }
      
        protected override async void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent intent)
        {
            try
            {
                if (resultCode == Result.Ok)
                {
                    if (requestCode == 101)
                    {
                        if ((resultCode == Result.Ok))
                        {

                            Bitmap bitmap = LoadAndResizeBitmap(outputDir.AbsolutePath);
                            if (bitmap != null)
                            {
                                byte[] bitmapData;
                                var stream = new MemoryStream();
                                bitmap.Compress(Bitmap.CompressFormat.Jpeg, 90, stream);
                                bitmapData = stream.ToArray();

                                UserDialogs.Instance.ShowLoading("Uploading...");
                                var response = await presenter.UploadPhoto(bitmapData);


                                if (response.Success)
                                {
                                    if (Photos == null)
                                        Photos = new List<string>();
                                    Photos.Add(response.Data);

                                    ImageView image = new ImageView(this);
                                    var paramsImage = new LinearLayout.LayoutParams(150, 150);
                                    paramsImage.LeftMargin = 20;
                                    image.LayoutParameters = paramsImage;
                                    image.SetScaleType(scaleType: ImageView.ScaleType.CenterCrop);
                                    image.SetImageBitmap(bitmap);
                                    image.Tag = outputDir.AbsolutePath;
                                    image.Click += Image_Click;
                                    llPhotos.AddView(image);

                                    if (LocalPhotos == null)
                                        LocalPhotos = new List<string>();
                                    LocalPhotos.Add(outputDir.AbsolutePath);
                                }
                                UserDialogs.Instance.HideLoading();

                                GC.Collect();
                            }
                        }
                    }
                    if (requestCode == 102)
                    {
                        if (intent.Data == null)
                        {
                            if (intent.ClipData != null)
                            {
                                if (Photos == null)
                                    Photos = new List<string>();

                                if (LocalPhotos == null)
                                    LocalPhotos = new List<string>();

                                if (intent.ClipData.ItemCount + Photos.Count > PhotosAllowed)
                                {
                                    await Task.Delay(1000);
                                    UserDialogs.Instance.Alert(GetText(Resource.String.SorryYouHaveExceededMaxPhoto));
                                    return;
                                }
                                LoadingText = $"{GetText(Resource.String.UploadingPhotos)} ({1} out of {intent.ClipData.ItemCount}).";
                                var progress = UserDialogs.Instance.Progress(LoadingText, null, null, true);

                                for (int y = 0; y < intent.ClipData.ItemCount; y++)
                                {
                                    int counts = intent.ClipData.ItemCount > (PhotosAllowed - Photos.Count) ? PhotosAllowed - Photos.Count : intent.ClipData.ItemCount;
                                    LoadingText = $"{GetText(Resource.String.UploadingPhotos)} ({y + 1} out of {intent.ClipData.ItemCount}).";
                                    progress.Title = LoadingText;
                                    double percentage = ((y + 1.0) / counts);
                                    percentage = percentage * 100d;
                                    progress.PercentComplete = (int)percentage;
                                    ClipData.Item item = intent.ClipData.GetItemAt(y);
                                    var uri = item.Uri;
                                    if (uri == null)
                                    {
                                        InvalidPhotos++;
                                        continue;
                                    }
                                    ExifInterface exif = null;
                                    try
                                    {
                                        exif = new ExifInterface(GetPathToImage(uri));
                                    }
                                    catch (System.Exception ex)
                                    {
                                        //Crashes.TrackError(ex);
                                    }

                                    int orientation = exif != null ? exif.GetAttributeInt(ExifInterface.TagOrientation, 0) : 0;
                                    Bitmap bitmap = null;

                                    try
                                    {
                                        bitmap = MediaStore.Images.Media.GetBitmap(this.ContentResolver, uri);
                                    }
                                    catch (Exception ex)
                                    {
                                        //Crashes.TrackError(ex);
                                    }

                                    if (bitmap == null)
                                    {
                                        InvalidPhotos++;
                                        continue;
                                    }

                                    var path = Save(MediaHelper._dir.AbsolutePath, bitmap);

                                    MediaHelper.bitmap = path.LoadAndResizeBitmap(750, 750);

                                    MediaHelper.bitmap = RotateBitmap(MediaHelper.bitmap, orientation);

                                    if (MediaHelper.bitmap != null)
                                    {
                                        byte[] bitmapData;
                                        var stream = new MemoryStream();
                                        MediaHelper.bitmap.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
                                        bitmapData = stream.ToArray();
                                        //UserDialogs.Instance.ShowLoading("Uploading Photo...");


                                        var response = await presenter.UploadPhoto(bitmapData);
                                        if (response.Success)
                                        {
                                            if (Photos == null)
                                                Photos = new List<string>();
                                            Photos.Add(response.Data);

                                            ImageView image = new ImageView(this);
                                            var paramsImage = new LinearLayout.LayoutParams(150, 150);
                                            paramsImage.LeftMargin = 20;
                                            image.LayoutParameters = paramsImage;
                                            image.SetScaleType(scaleType: ImageView.ScaleType.CenterCrop);
                                            image.SetImageBitmap(MediaHelper.bitmap);
                                            image.Tag = path;
                                            image.Click += Image_Click;
                                            llPhotos.AddView(image);

                                            if (LocalPhotos == null)
                                                LocalPhotos = new List<string>();
                                            LocalPhotos.Add(path);
                                        }
                                        MediaHelper.bitmap.Dispose();
                                        MediaHelper.bitmap = null;
                                    }
                                    // Dispose of the Java side bitmap.
                                    GC.Collect();
                                }
                                progress.Hide();

                                if (InvalidPhotos > 0)
                                {
                                    UserDialogs.Instance.Alert($"We detected {InvalidPhotos} invalid/ private photo. Please use other photo as replacement.");
                                    InvalidPhotos = 0;
                                }
                                InvalidPhotos = 0;
                                //UserDialogs.Instance.Progress(LoadingText, null, null, true);
                            }
                        }
                        else
                        {
                            ExifInterface exif = null;
                            try
                            {
                                exif = new ExifInterface(GetPathToImage(intent.Data));

                            }
                            catch (System.Exception ex)
                            {
                                //Crashes.TrackError(ex);
                            }

                            int orientation = exif != null ? exif.GetAttributeInt(ExifInterface.TagOrientation, 0) : 0;
                            Bitmap bitmap = MediaStore.Images.Media.GetBitmap(this.ContentResolver, intent.Data);

                            var path = Save(MediaHelper._dir.AbsolutePath, bitmap);

                            MediaHelper.bitmap = path.LoadAndResizeBitmap(750, 750);

                            MediaHelper.bitmap = RotateBitmap(MediaHelper.bitmap, orientation);

                            if (MediaHelper.bitmap != null)
                            {
                                byte[] bitmapData;
                                var stream = new MemoryStream();
                                MediaHelper.bitmap.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
                                bitmapData = stream.ToArray();
                                UserDialogs.Instance.ShowLoading("Uploading Photo...");

                                var response = await presenter.UploadPhoto(bitmapData);
                                UserDialogs.Instance.HideLoading();
                                if (response.Success)
                                {
                                    if (Photos == null)
                                        Photos = new List<string>();
                                    Photos.Add(response.Data);

                                    ImageView image = new ImageView(this);
                                    var paramsImage = new LinearLayout.LayoutParams(150, 150);
                                    paramsImage.LeftMargin = 20;
                                    image.LayoutParameters = paramsImage;
                                    image.SetScaleType(scaleType: ImageView.ScaleType.CenterCrop);
                                    image.SetImageBitmap(bitmap);
                                    image.Tag = path;
                                    image.Click += Image_Click;
                                    llPhotos.AddView(image);


                                    if (LocalPhotos == null)
                                        LocalPhotos = new List<string>();
                                    LocalPhotos.Add(path);
                                }
                                MediaHelper.bitmap = null;
                            }
                            // Dispose of the Java side bitmap.
                            GC.Collect();
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            base.OnActivityResult(requestCode, resultCode, intent);
        }


        private async void Image_Click(object sender, EventArgs e)
        {
            var response = await UserDialogs.Instance.ConfirmAsync(GetText(Resource.String.DoYouWantToRemoveThisPhoto), "", GetText(Resource.String.Yes), GetText(Resource.String.Cancel));
            if (response)
            {
                var imageView = sender as ImageView;
                llPhotos.RemoveView(imageView);
                var index = LocalPhotos.IndexOf(imageView.Tag.ToString());
                Photos.RemoveAt(index);
                LocalPhotos.RemoveAt(index);
            }
        }

        private void IvSearch_Click(object sender, System.EventArgs e)
        {
            Intent intent = new Intent(this, typeof(SearchActivity));
            StartActivity(intent);
        }

        public async Task GenerateRecentPost()
        {
            try
            {
                /**/
                tvRecentPost.Visibility = ViewStates.Gone;
                var response = await presenter.GetItems(new MobileNetStandardLibrary.Commands.SearchCommand
                {
                    page = 1,
                    pageSize = 30,
                    sort = 1,
                    status = "ACTIVE,COMPLETED"
                });

                UserDialogs.Instance.HideLoading();
                if (response.Success)
                {
                    llRecentPost.RemoveAllViews();
                    if (response.Data.list != null)
                    {

                        foreach (var data in response.Data.list)
                        {
                            LayoutInflater inflater = (LayoutInflater)GetSystemService(Context.LayoutInflaterService);
                            var itemCard = inflater.Inflate(Resource.Layout.ItemWhiteCardForDashboard, null);
                            var profilePic = itemCard.FindViewById<ImageView>(Resource.Id.ivProfilePic);
                            var ivImage = itemCard.FindViewById<ImageView>(Resource.Id.ivImage);
                            var ivGrayScale = itemCard.FindViewById<ImageView>(Resource.Id.ivGrayScale);
                            var tvItemName = itemCard.FindViewById<TextView>(Resource.Id.tvItemName);
                            var tvLocation = itemCard.FindViewById<TextView>(Resource.Id.tvLocation);
                            var tvDate = itemCard.FindViewById<TextView>(Resource.Id.tvDateCreated);
                            var tvOffers = itemCard.FindViewById<TextView>(Resource.Id.tvOffers);
                            tvOffers.Text = data.offerCount == 1 ? $"{data.offerCount} offer" : $"{data.offerCount} offers";
                            Picasso.Get().Load(URLPath.BaseURL + data.photos[0]).Placeholder(Resource.Mipmap.product_placeholder).MemoryPolicy(MemoryPolicy.NoCache).Into(ivImage);
                            Picasso.Get().Load(URLPath.BaseURL + data.user.imageUrl).Placeholder(Resource.Mipmap.user_placeholder).MemoryPolicy(MemoryPolicy.NoCache).Into(profilePic);
                            tvItemName.Text = data.Item;
                            tvLocation.Text = data.location.city + ", " + data.location.province;
                            tvDate.Text = data.lastUpdateDate.GetValueOrDefault().ToLocalTime().ToString("MMM dd, yyyy");

                            var btnStatus = itemCard.FindViewById<Button>(Resource.Id.btnStatus);
                            if (data.status == ItemStatus.COMPLETED.ToString())
                            {
                                btnStatus.Visibility = ViewStates.Visible;
                                btnStatus.Text = "Completed";
                                btnStatus.SetTextColor(Android.Graphics.Color.ParseColor("#27AE60"));
                                ivGrayScale.Visibility = ViewStates.Gone;
                            }
                            else if (data.status == ItemStatus.CANCELLED.ToString())
                            {
                                btnStatus.Visibility = ViewStates.Visible;
                                btnStatus.Text = "Deleted";
                                btnStatus.SetTextColor(Android.Graphics.Color.Red);
                                ivGrayScale.Visibility = ViewStates.Visible;
                            }
                            else
                            {
                                btnStatus.Visibility = ViewStates.Gone;
                                ivGrayScale.Visibility = ViewStates.Gone;
                            }

                            itemCard.Tag = data.id;
                            itemCard.Click += ItemCard_Click;
                            llRecentPost.AddView(itemCard, 0);

                            tvRecentPost.Visibility = ViewStates.Visible;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }


       


        public async Task GenerateMostViewedPost()
        {
            try
            {
                /**/
                tvMostViewed.Visibility = ViewStates.Gone;
                var response = await presenter.GetItems(new MobileNetStandardLibrary.Commands.SearchCommand
                {
                    page = 1,
                    pageSize = 30,
                    sort = 3,
                    status = "ACTIVE,COMPLETED"
                });
                UserDialogs.Instance.HideLoading();
                if (response.Success)
                {
                    llMostViewedPost.RemoveAllViews();
                    if (response.Data.list != null)
                    {
                        foreach (var data in response.Data.list)
                        {
                            LayoutInflater inflater = (LayoutInflater)GetSystemService(Context.LayoutInflaterService);
                            var itemCard = inflater.Inflate(Resource.Layout.ItemWhiteCardForDashboard, null);
                            var profilePic = itemCard.FindViewById<ImageView>(Resource.Id.ivProfilePic);
                            var ivImage = itemCard.FindViewById<ImageView>(Resource.Id.ivImage);
                            var ivGrayScale = itemCard.FindViewById<ImageView>(Resource.Id.ivGrayScale);
                            var tvItemName = itemCard.FindViewById<TextView>(Resource.Id.tvItemName);
                            var tvLocation = itemCard.FindViewById<TextView>(Resource.Id.tvLocation);
                            var tvDate = itemCard.FindViewById<TextView>(Resource.Id.tvDateCreated);
                            var tvOffers = itemCard.FindViewById<TextView>(Resource.Id.tvOffers);
                            tvOffers.Text = data.offerCount == 1 ? $"{data.offerCount} offer" : $"{data.offerCount} offers";
                            Picasso.Get().Load(URLPath.BaseURL + data.photos[0]).Placeholder(Resource.Mipmap.product_placeholder).MemoryPolicy(MemoryPolicy.NoCache).Into(ivImage);
                            Picasso.Get().Load(URLPath.BaseURL + data.user.imageUrl).Placeholder(Resource.Mipmap.user_placeholder).MemoryPolicy(MemoryPolicy.NoCache).Into(profilePic);
                            tvItemName.Text = data.Item;
                            tvLocation.Text = data.location.city + ", " + data.location.province;
                            tvDate.Text = data.lastUpdateDate.GetValueOrDefault().ToLocalTime().ToString("MMM dd, yyyy");
                            itemCard.Tag = data.id;
                            itemCard.Click += ItemCard_Click;

                            var btnStatus = itemCard.FindViewById<Button>(Resource.Id.btnStatus);
                            if (data.status == ItemStatus.COMPLETED.ToString())
                            {
                                btnStatus.Visibility = ViewStates.Visible;
                                btnStatus.Text = "Completed";
                                btnStatus.SetTextColor(Android.Graphics.Color.ParseColor("#27AE60"));
                                ivGrayScale.Visibility = ViewStates.Gone;
                            }
                            else if (data.status == ItemStatus.CANCELLED.ToString())
                            {
                                btnStatus.Visibility = ViewStates.Visible;
                                btnStatus.Text = "Deleted";
                                btnStatus.SetTextColor(Android.Graphics.Color.Red);
                                ivGrayScale.Visibility = ViewStates.Visible;
                            }
                            else
                            {
                                btnStatus.Visibility = ViewStates.Gone;
                                ivGrayScale.Visibility = ViewStates.Gone;
                            }

                            llMostViewedPost.AddView(itemCard, 0);
                            tvMostViewed.Visibility = ViewStates.Visible;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void ItemCard_Click(object sender, EventArgs e)
        {
            try
            {
                var senderView = sender as View;
                var id = (long)senderView.Tag;
                Intent intent = new Intent(this, typeof(BarterItemDetailActivity));
                intent.PutExtra("Id", id);
                StartActivity(intent);
            }
            catch (Exception ex)
            {

            }
        }

        private void RlProfile_Click(object sender, System.EventArgs e)
        {
            Intent intent = new Intent(this, typeof(ProfileActivity));
            StartActivity(intent);
        }

        public void OnScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY)
        {
            swipeRefreshLayout.Enabled = scrollY == 0;
        }

        protected override void OnPause()
        {
            if (mAdView != null)
            {
                mAdView.Pause();
            }
            base.OnPause();
        }

        protected override void OnResume()
        {
            base.OnResume();
            if (mAdView != null)
            {
                mAdView.Resume();
            }
        }

        protected override void OnDestroy()
        {
            if (mAdView != null)
            {
                mAdView.Destroy();
            }
            base.OnDestroy();
        }

      
    }
}
