﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace BarterAppAndroid.Activity
{
    [Activity(Label = "ChangePasswordActivity", Theme = "@style/AppTheme.NoActionBar", ScreenOrientation = ScreenOrientation.Portrait)]
    public class ChangePasswordActivity : BaseActivity
    {
        EditText etPassword;
        EditText etConfirmPassword;
        TextView tvPassword;
        TextView tvConfirmPassword;
        Button btnSetPassword;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ChangePasswordDialog);
            // Create your application here
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            var actionBar = SupportActionBar;
            actionBar.SetHomeButtonEnabled(true);
            actionBar.SetDisplayHomeAsUpEnabled(true);
            actionBar.Title = "Change Password";

            etPassword = FindViewById<EditText>(Resource.Id.etPassword);
            tvPassword = FindViewById<TextView>(Resource.Id.tvNewPassword);
            tvConfirmPassword = FindViewById<TextView>(Resource.Id.tvConfirmPassword);

            if (Intent.HasExtra("ChangePassword"))
            {
                tvPassword.Text = "Current Password";
                tvConfirmPassword.Text = "New Password";
            }

            etConfirmPassword = FindViewById<EditText>(Resource.Id.etConfirmPasswordPassword);
            btnSetPassword = FindViewById<Button>(Resource.Id.btnSetPassword);
            btnSetPassword.Click += BtnSetPassword_Click;
        }

        private async void BtnSetPassword_Click(object sender, EventArgs e)
        {
            if (Intent.HasExtra("ChangePassword"))
            {
                UserDialogs.Instance.ShowLoading();
                var response = await presenter.ChangePassword(etPassword.Text, etConfirmPassword.Text);
                UserDialogs.Instance.HideLoading();
                if (response.Success)
                {
                    if (Intent.HasExtra("MobileNumber"))
                    {
                        Intent intent = new Intent(this, typeof(DashboardActivity));
                        intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.ClearTask | ActivityFlags.NewTask);
                        StartActivity(intent);
                    }
                    else
                    {
                        await UserDialogs.Instance.AlertAsync("Password Changed!");
                        Finish();
                    }
                }
                else
                {
                    if (response.ErrorMessage != null)
                        UserDialogs.Instance.Alert(response.ErrorMessage);
                }
            }
            else
            {
                if (etPassword.Text.Trim() != etConfirmPassword.Text.Trim())
                {
                    if (!string.IsNullOrWhiteSpace(etPassword.Text))
                    {
                        UserDialogs.Instance.Alert("Password did not match!");
                    }
                    return;
                }
                UserDialogs.Instance.ShowLoading();
                var response = await presenter.ResetPasswordFinish(new MobileNetStandardLibrary.Commands.ChangePasswordCommand
                {
                    key = Intent.GetStringExtra("otp"),
                    mobileNo = Intent.HasExtra("MobileNumber") ? Intent.GetStringExtra("MobileNumber") : presenter.userService.CurrentUser.mobileNo,
                    newPassword = etPassword.Text
                });
                UserDialogs.Instance.HideLoading();
                if (response.Success)
                {
                    if (Intent.HasExtra("MobileNumber"))
                    {
                        Intent intent = new Intent(this, typeof(DashboardActivity));
                        intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.ClearTask | ActivityFlags.NewTask);
                        StartActivity(intent);
                    }
                    else
                    {
                        Finish();
                    }
                }
                else
                {
                    if (response.ErrorMessage != null)
                        UserDialogs.Instance.Alert(response.ErrorMessage);
                }
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}
