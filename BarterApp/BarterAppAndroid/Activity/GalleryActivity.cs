﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.View;
using Android.Views;
using Android.Widget;
using BarterAppAndroid.Adapter;

namespace BarterAppAndroid.Activity
{
    [Activity(Label = "GalleryActivity", Theme = "@style/AppTheme.NoActionBar", ScreenOrientation = ScreenOrientation.Portrait)]
    public class GalleryActivity : BaseActivity
    {
        public static List<string> ListOfImages = new List<string>();
        public static int ListOfImagesIndex;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.GalleryActivity);
            this.Window.AddFlags(WindowManagerFlags.Fullscreen); //to show
            this.Window.ClearFlags(WindowManagerFlags.Fullscreen);

            var closeButton = FindViewById<TextView>(Resource.Id.closeButton);
            closeButton.Click += (object sender, EventArgs e) =>
            {
                Finish();
            };

            var tvPagerCounter = FindViewById<TextView>(Resource.Id.tvPagerCounter);
            ViewPager viewPager = FindViewById<ViewPager>(Resource.Id.itemImageViewPager);
            viewPager.Adapter = new GalleryPagerAdapter(this, ListOfImages);
            tvPagerCounter.Text = $"1/{ListOfImages.Count}";
            viewPager.PageSelected += (object sender, ViewPager.PageSelectedEventArgs e) =>
            {
                tvPagerCounter.Text = $"{e.Position + 1}/{ListOfImages.Count}";
            };
            viewPager.SetCurrentItem(ListOfImagesIndex, false);
        }
    }
}

