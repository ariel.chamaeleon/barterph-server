﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Widget;
using BarterAppAndroid.Adapter;
using MobileNetStandardLibrary.Commands;
using MobileNetStandardLibrary.DTO;

namespace BarterAppAndroid.Activity
{
    [Activity(Label = "NotificationActivity", Theme = "@style/AppTheme.NoActionBar", ScreenOrientation = ScreenOrientation.Portrait)]
    public class NotificationActivity : BaseActivity
    {
        private ListView lvList;
        private NotificationAdapter adapter;
        private List<NotificationDTO> listOfNotifications;
        private TextView tvEmptyText;

        public SwipeRefreshLayout swipeRefreshLayout;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.NotificationLayout);
            // Create your application here
            swipeRefreshLayout = FindViewById<SwipeRefreshLayout>(Resource.Id.swipeRefreshLayout);
            swipeRefreshLayout.Refresh += SwipeRefreshLayout_Refresh;
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            lvList = FindViewById<ListView>(Resource.Id.lvList);
            listOfNotifications = new List<NotificationDTO>();
            tvEmptyText = FindViewById<TextView>(Resource.Id.tvEmptyText);
            tvEmptyText.Visibility = ViewStates.Gone;
            adapter = new NotificationAdapter(this, listOfNotifications);
            lvList.Adapter = adapter;
            SetSupportActionBar(toolbar);
            var actionBar = SupportActionBar;
            actionBar.SetHomeButtonEnabled(true);
            actionBar.SetDisplayHomeAsUpEnabled(true);
            actionBar.Title = "Notifications";
            GetNotifications();
            lvList.Scroll += LvList_Scroll;
            lvList.ItemClick += LvList_ItemClick;
        }

        private void LvList_Scroll(object sender, AbsListView.ScrollEventArgs e)
        {
            if (lvList.GetChildAt(0) != null)
            {
                swipeRefreshLayout.Enabled = e.FirstVisibleItem == 0 && lvList.GetChildAt(0).Top == 0;
            }
        }

        private async void SwipeRefreshLayout_Refresh(object sender, EventArgs e)
        {
            swipeRefreshLayout.Refreshing = true;

            await GetNotifications();
            swipeRefreshLayout.Refreshing = false;
        }

        private void LvList_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            if (listOfNotifications[e.Position].comment != null)
            {
                Intent intent = new Intent(this, typeof(BarterItemDetailActivity));
                intent.PutExtra("Id", (long)listOfNotifications[e.Position].barter.id);
                intent.PutExtra("CommentId", listOfNotifications[e.Position].comment.id.ToString());
                StartActivity(intent);
            }
            else if (listOfNotifications[e.Position].offer != null)
            {
                Intent intent = new Intent(this, typeof(BarterItemDetailActivity));
                intent.PutExtra("Id", (long)listOfNotifications[e.Position].barter.id);
                intent.PutExtra("OfferId", listOfNotifications[e.Position].offer.id.ToString());
                StartActivity(intent);
            }
            else if (listOfNotifications[e.Position].type == "BARTER_REMINDER")
            {
                if (listOfNotifications[e.Position].barter.status == ItemStatus.ACTIVE.ToString())
                {
                    ShowReminderDialogs(listOfNotifications[e.Position].barter.id, e.Position);
                }
                else
                {
                    Intent intent = new Intent(this, typeof(BarterItemDetailActivity));
                    intent.PutExtra("Id", listOfNotifications[e.Position].barter.id);
                    StartActivity(intent);
                }
            }
            else
            {
                Intent intent = new Intent(this, typeof(BarterItemDetailActivity));
                intent.PutExtra("Id", listOfNotifications[e.Position].barter.id);
                StartActivity(intent);
            }
        }

        public async Task ShowReminderDialogs(long barterId, int position)
        {
            var selection = await UserDialogs.Instance.ActionSheetAsync("What do you want to do?", "Cancel", null, null, new string[] { "Show Item Details", "Repost", "Remind me again", "Barter Complete" });
            if (selection == "Show Item Details")
            {
                Intent intent = new Intent(this, typeof(BarterItemDetailActivity));
                intent.PutExtra("Id", barterId);
                StartActivity(intent);
            }
            else if (selection == "Repost")
            {
                var response = await presenter.RepostBarter(barterId);
                if (response.Success)
                {
                    if (DashboardActivity.Dashboard != null)
                    {
                        DashboardActivity.Dashboard.GenerateRecentPost();
                        DashboardActivity.Dashboard.GenerateMostViewedPost();
                    }
                    UserDialogs.Instance.Alert("You have successfully re-posted your barter item");
                }
                else
                {
                    if (!string.IsNullOrEmpty(response?.ErrorMessage))
                    {
                        UserDialogs.Instance.Alert(response?.ErrorMessage);
                    }
                }
            }
            else if (selection == "Remind me again")
            {
                BottomSheetDialog dialog = new BottomSheetDialog(this);
                dialog.SetContentView(Resource.Layout.remind_me_again_bottom);

                var rbTomorrow = dialog.FindViewById<RadioButton>(Resource.Id.rbTomorrow);
                var rbTwoDays = dialog.FindViewById<RadioButton>(Resource.Id.rbTwoDays);
                var rbNextWeek = dialog.FindViewById<RadioButton>(Resource.Id.rbNextWeek);
                var rbNextMonth = dialog.FindViewById<RadioButton>(Resource.Id.rbNextMonth);
                var btnRemindMeAgain = dialog.FindViewById<Button>(Resource.Id.btnRemindMeAgain);

                rbTomorrow.CheckedChange += (s, e) =>
                {
                    if (e.IsChecked)
                    {
                        rbTwoDays.Checked = !e.IsChecked;
                        rbNextWeek.Checked = !e.IsChecked;
                        rbNextMonth.Checked = !e.IsChecked;
                    }
                };
                rbTwoDays.CheckedChange += (s, e) =>
                {
                    if (e.IsChecked)
                    {
                        rbTomorrow.Checked = !e.IsChecked;
                        rbNextWeek.Checked = !e.IsChecked;
                        rbNextMonth.Checked = !e.IsChecked;
                    }
                };
                rbNextWeek.CheckedChange += (s, e) =>
                {
                    if (e.IsChecked)
                    {
                        rbTomorrow.Checked = !e.IsChecked;
                        rbTwoDays.Checked = !e.IsChecked;
                        rbNextMonth.Checked = !e.IsChecked;
                    }
                };
                rbNextMonth.CheckedChange += (s, e) =>
                {
                    if (e.IsChecked)
                    {
                        rbTomorrow.Checked = !e.IsChecked;
                        rbTwoDays.Checked = !e.IsChecked;
                        rbNextWeek.Checked = !e.IsChecked;
                    }
                };

                btnRemindMeAgain.Click += async (s, e) =>
                {
                    var days = rbTomorrow.Checked ? 1 : rbTwoDays.Checked ? 2 : rbNextWeek.Checked ? 7 : rbNextMonth.Checked ? 30 : 0;
                    if (days > 0)
                    {
                        UserDialogs.Instance.ShowLoading();
                        var response = await presenter.UpdateReminderSettings(barterId, days);
                        UserDialogs.Instance.HideLoading();
                        if (response.Success)
                        {
                            UserDialogs.Instance.Alert("Reminder has been set.");
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(response?.ErrorMessage))
                            {
                                UserDialogs.Instance.Alert(response?.ErrorMessage);
                            }
                        }
                    }
                    dialog.Dismiss();
                };

                dialog.Show();
            }
            else if (selection == "Barter Complete")
            {
                UserDialogs.Instance.ShowLoading();
                var resp = await UserDialogs.Instance.ConfirmAsync("Set this barter as completed?", "Confirmation", "Yes", "Cancel");
                if (resp)
                {
                    var response = await presenter.BarterComplete(barterId);
                    UserDialogs.Instance.HideLoading();
                    if (response.Success)
                    {
                        listOfNotifications[position].barter.status = ItemStatus.COMPLETED.ToString();
                        UserDialogs.Instance.Alert("Barter Completed!");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(response?.ErrorMessage))
                        {
                            UserDialogs.Instance.Alert(response?.ErrorMessage);
                        }
                    }
                }
            }
        }

        public async Task GetNotifications()
        {

            var response = await presenter.GetNotifications(new MobileNetStandardLibrary.Commands.NotificationCommand
            {
                receiverId = presenter.userService.CurrentUser.id,
                page = 1,
                pageSize = 1000,
                sort = 2,
            });

            if (response.Success)
            {
                listOfNotifications.Clear();
                listOfNotifications.AddRange(response.Data.list);
                adapter.NotifyDataSetChanged();
            }

            if (listOfNotifications.Count > 0)
            {
                tvEmptyText.Visibility = ViewStates.Gone;
            }
            else
            {
                tvEmptyText.Visibility = ViewStates.Visible;
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}
