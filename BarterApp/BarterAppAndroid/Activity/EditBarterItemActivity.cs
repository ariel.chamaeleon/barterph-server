﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.Media;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using AppStandardLibrary;
using BarterApp.Droid;
using BarterAppAndroid.Controls;
using MobileNetStandardLibrary.DTO;
using MobileNetStandardLibrary.Model;
using Square.Picasso;

namespace BarterAppAndroid.Activity
{
    [Activity(Label = "EditBarterItemActivity", Theme = "@style/AppTheme.NoActionBar", ScreenOrientation = ScreenOrientation.Portrait, WindowSoftInputMode = SoftInput.StateHidden)]
    public class EditBarterItemActivity : BaseActivity, NavigationView.IOnNavigationItemSelectedListener
    {
        public readonly int PickImageId = 102;
        public readonly int CameraId = 101;
        public readonly int ExternalStorageId = 1001;
        static readonly int REQUEST_CAMERA = 2;
        static readonly int REQUEST_GALLERY = 1;

        public static List<string> Photos;
        public static List<string> LocalPhotos;
        public static int UploadCount;
        public static int InvalidPhotos;
        public string LoadingText { get; set; }
        private Android.Net.Uri mHighQualityImageUri = null;

        public static ItemDTO DTO { get; set; }

        private LinearLayout llPhotos;
        private TextView tvPhotos;
        private RadioGroup rgDeliveryOption;
        private TextView tvUserName;
        private ImageView ivProfilePic;
        private EditText etItemForBarter;
        private RadioGroup rgItemLocation;
        private EditText etItemLocation;
        private EditText etDeliveryOptions;
        private RadioGroup rgDeliveryOptions;
        private EditText etReasonForBarter;
        private EditText etPreference;
        private EditText etContactNumber;
        private LinearLayout llDeliveryOptions;
        private Button btnPost;
        private RadioButton rbMeetup;
        private RadioButton rbCourier;
        private RadioButton rbOpenLocation;
        private RadioButton rbStrictLocation;
        private TextView tvMostViewed;
        private TextView tvRecentPost;
        private ScrollView scrollView;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.EditBarterPost);
            etItemForBarter = FindViewById<EditText>(Resource.Id.etItemForBarter);
            etItemLocation = FindViewById<EditText>(Resource.Id.etItemLocation);
            rgItemLocation = FindViewById<RadioGroup>(Resource.Id.rgLocationOfItem);
            etDeliveryOptions = FindViewById<EditText>(Resource.Id.etDeliveryOption);
            rgDeliveryOption = FindViewById<RadioGroup>(Resource.Id.rgDeliveryOption);
            etReasonForBarter = FindViewById<EditText>(Resource.Id.etReason);
            etPreference = FindViewById<EditText>(Resource.Id.etPeference);
            etContactNumber = FindViewById<EditText>(Resource.Id.etContactNumber);
            llDeliveryOptions = FindViewById<LinearLayout>(Resource.Id.llDeliveryOptions);
            rbMeetup = FindViewById<RadioButton>(Resource.Id.rbMeetup);
            rbCourier = FindViewById<RadioButton>(Resource.Id.rbCourier);
            rbOpenLocation = FindViewById<RadioButton>(Resource.Id.rbOpenLocation);
            rbStrictLocation = FindViewById<RadioButton>(Resource.Id.rbStrictlyWithinMyPreferredLocation);
            scrollView = FindViewById<ScrollView>(Resource.Id.scrollView);

            etItemForBarter.Text = DTO.Item;
            etItemLocation.Text = DTO.location.location + " " + DTO.location.city + ", " + DTO.location.province;
            etReasonForBarter.Text = DTO.reason;
            etPreference.Text = DTO.preferences;
            etContactNumber.Text = DTO.contactNumber;
            rbMeetup.Checked = DTO.deliveryType == DeliveryOption.COURIER ? false : true;
            rbCourier.Checked = DTO.deliveryType == DeliveryOption.COURIER ? true : false;
            etDeliveryOptions.Visibility = rbCourier.Checked ? ViewStates.Visible : ViewStates.Gone;
            rbOpenLocation.Checked = DTO.locationType == LocationType.OPEN ? true : false;
            rbStrictLocation.Checked = DTO.locationType == LocationType.OPEN ? false : true;
            openLocationText = DTO.location.location;
            cityText = DTO.location.city;
            provinceText = DTO.location.province;

            btnPost = FindViewById<Button>(Resource.Id.btnPost);
            btnPost.Click += BtnPost_Click;

            llPhotos = FindViewById<LinearLayout>(Resource.Id.llPhotos);
            tvPhotos = FindViewById<TextView>(Resource.Id.tvPhotos);
            tvPhotos.Click += TvPhotos_Click;
            Photos = new List<string>();
            LocalPhotos = new List<string>();
            foreach (var photo in DTO.photos)
            {
                ImageView image = new ImageView(this);
                var paramsImage = new LinearLayout.LayoutParams(150, 150);
                paramsImage.LeftMargin = 20;
                image.LayoutParameters = paramsImage;
                image.SetScaleType(scaleType: ImageView.ScaleType.CenterCrop);
                image.Tag = photo.Replace("photo/barter/", "");
                Picasso.Get().Load(URLPath.BaseURL + photo).Into(image);
                image.Click += Image_Click;
                llPhotos.AddView(image);

                if (LocalPhotos == null)
                    LocalPhotos = new List<string>();
                LocalPhotos.Add(photo.Replace("photo/barter/", ""));

                if (Photos == null)
                    Photos = new List<string>();
                Photos.Add(photo.Replace("photo/barter/", ""));
            }

            rgDeliveryOption.CheckedChange += RgDeliveryOption_CheckedChange;
            rgItemLocation.CheckedChange += RgItemLocation_CheckedChange;

            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            var actionBar = SupportActionBar;
            actionBar.SetHomeButtonEnabled(true);
            actionBar.SetDisplayHomeAsUpEnabled(true);
            actionBar.Title = "Edit Post";

            var ivSearch = toolbar.FindViewById<ImageView>(Resource.Id.ivCheck);
            ivSearch.SetImageResource(Resource.Mipmap.check);
            ivSearch.Visibility = ViewStates.Visible;
            ivSearch.Click += BtnPost_Click;

            CreateDirectoryForPictures();
            GetCityList();

            etItemLocation.Enabled = true;
            etItemLocation.Hint = "Address kung saan makikipag-meetup";
            etItemLocation.Focusable = false;
            etItemLocation.Clickable = true;
            etItemLocation.Click += EtDeliveryOptions_Click;

            if (rbCourier.Checked)
            {
                etDeliveryOptions.Text = DTO.courier;
                etDeliveryOptions.Hint = "ex. LBC, 2go, JRS, etc";
            }
            else
            {
                etDeliveryOptions.Visibility = ViewStates.Gone;
                etDeliveryOptions.Text = "";
                etDeliveryOptions.Hint = "ex. LBC, 2go, JRS, etc";
            }

        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        public void UpdateNameAndPhoto()
        {
            if (!string.IsNullOrEmpty(presenter.userService.CurrentUser.imageUrl))
                Picasso.Get().Load(URLPath.BaseURL + presenter.userService.CurrentUser.imageUrl).Placeholder(Resource.Mipmap.user_placeholder).Into(ivProfilePic);
            tvUserName.Text = presenter.userService.CurrentUser.firstName + " " + presenter.userService.CurrentUser.lastName;
        }

        Dialog dialog;


        private void EtDeliveryOptions_Click(object sender, EventArgs e)
        {
            ShowDialog();
        }

        private void RgItemLocation_CheckedChange(object sender, RadioGroup.CheckedChangeEventArgs e)
        {
            if (e.CheckedId == Resource.Id.rbStrictlyWithinMyPreferredLocation)
            {
                etItemLocation.Text = string.Empty;
                etItemLocation.Hint = rbCourier.Checked ? "Address ng padadalhan base sa Home Location ko" : "Address ng meetup base sa Home Location ko";
            }
            else
            {
                etItemLocation.Text = string.Empty;
                etItemLocation.Hint = rbCourier.Checked ? "Address ng padadalhan ng barter" : "Address kung saan makikipag-meetup";

            }
        }

        private void RgDeliveryOption_CheckedChange(object sender, RadioGroup.CheckedChangeEventArgs e)
        {
            if (e.CheckedId == Resource.Id.rbCourier)
            {
                etDeliveryOptions.Visibility = ViewStates.Visible;
                etItemLocation.Hint = rbOpenLocation.Checked ? "Address ng padadalhan ng barter" : "Address ng padadalhan base sa Home Location ko";

            }
            else
            {
                etDeliveryOptions.Visibility = ViewStates.Gone;
                etItemLocation.Hint = rbOpenLocation.Checked ? "Address kung saan makikipag-meetup" : "Address ng meetup base sa Home Location ko";
            }
        }


        GenericEditText etProvince;
        GenericEditText etCity;
        GenericEditText etOpenLocation;
        Button btnUseThisAddress;
        Dialog addressDialog;

        async void ShowDialog()
        {
            if (addressDialog == null)
            {
                addressDialog = new Dialog(this);
                addressDialog.SetContentView(Resource.Layout.addressDialog);
                addressDialog.SetTitle("Set Address");
                etCity = addressDialog.FindViewById<GenericEditText>(Resource.Id.etCity);
                etProvince = addressDialog.FindViewById<GenericEditText>(Resource.Id.etProvince);
                etOpenLocation = addressDialog.FindViewById<GenericEditText>(Resource.Id.etOpenLocation);
                btnUseThisAddress = addressDialog.FindViewById<Button>(Resource.Id.btnUseAddress);

                etCity.Field.Focusable = false;
                etCity.Field.Clickable = true;
                etCity.Field.Click += Field_Click;
                etProvince.Field.Focusable = false;
                etProvince.Field.Clickable = true;
                etProvince.Field.Click += Field_Click1;

                etOpenLocation.Field.Hint = etOpenLocation.floatingLabel.Text = "Specific Meetup Location";
                etProvince.Field.Hint = etProvince.floatingLabel.Text = "Province";
                etCity.Field.Hint = etCity.floatingLabel.Text = "City/ Municipality";

                etOpenLocation.Field.Text = DTO.location.location;
                etProvince.Field.Text = DTO.location.province;
                etCity.Field.Text = DTO.location.city;

                btnUseThisAddress.Click += BtnUseThisAddress_Click;
            }

            if (rbStrictLocation.Checked)
            {
                etOpenLocation.Field.Text = string.Empty;
                etProvince.Field.Text = presenter.userService.CurrentUser.province;
                etCity.Field.Text = presenter.userService.CurrentUser.city;
                etProvince.Field.Enabled = false;
                etCity.Field.Enabled = false;
                etProvince.arrowDown.Visibility = ViewStates.Gone;
                etCity.arrowDown.Visibility = ViewStates.Gone;
            }
            else
            {
                etOpenLocation.Field.Text = DTO.location.location;
                etProvince.Field.Text = DTO.location.province;
                etCity.Field.Text = DTO.location.city;
                etProvince.Field.Enabled = true;
                etCity.Field.Enabled = true;
                etProvince.arrowDown.Visibility = ViewStates.Visible;
                etCity.arrowDown.Visibility = ViewStates.Visible;
            }

            addressDialog.Show();
        }

        string openLocationText;
        string cityText;
        string provinceText;

        private void BtnUseThisAddress_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(etOpenLocation.Field.Text.Trim()))
            {
                UserDialogs.Instance.Alert("Sepecific meetup is required");
                return;
            }

            if (string.IsNullOrEmpty(etCity.Field.Text.Trim()))
            {
                UserDialogs.Instance.Alert("City is required");
                return;
            }

            if (string.IsNullOrEmpty(etProvince.Field.Text.Trim()))
            {
                UserDialogs.Instance.Alert("Province is required");
                return;
            }

            addressDialog.Hide();
            etItemLocation.Text = etOpenLocation.Field.Text + " " + etCity.Field.Text + ", " + etProvince.Field.Text;

            openLocationText = etOpenLocation.Field.Text;
            cityText = etCity.Field.Text;
            provinceText = etProvince.Field.Text;
        }

        private async void Field_Click1(object sender, EventArgs e)
        {
            var countryPicker = await UserDialogs.Instance.ActionSheetAsync("Choose your Province", GetText(Resource.String.Cancel), null, null, listOfProvince.ToArray());
            if (countryPicker == "Cancel")
                return;
            etProvince.Field.Text = countryPicker;
            etCity.Field.Text = "";
        }

        private async void Field_Click(object sender, EventArgs e)
        {
            listOfCity = new List<string>();
            String[] rl = this.Resources.GetStringArray(Resource.Array.ZipCodes);
            for (int i = 0; i < rl.Length; i++)
            {
                String[] g = rl[i].Split(",");
                if (etProvince.Field.Text != "")
                {
                    if (g[1] == etProvince.Field.Text)
                    {
                        if (!listOfCity.Contains(g[2]))
                            listOfCity.Add(g[2]);
                    }

                }
                else
                {
                    if (!listOfCity.Contains(g[2]))
                        listOfCity.Add(g[2]);
                }
            }

            listOfCity.Sort();
            var countryPicker = await UserDialogs.Instance.ActionSheetAsync("Choose your City", GetText(Resource.String.Cancel), null, null, listOfCity.ToArray());
            if (countryPicker == "Cancel")
                return;
            etCity.Field.Text = countryPicker;
            for (int i = 0; i < rl.Length; i++)
            {
                String[] g = rl[i].Split(",");
                if (countryPicker == g[2])
                {
                    etProvince.Field.Text = g[1];

                }
            }
        }

        public List<string> listOfCity;
        public List<string> listOfProvince;

        private void GetCityList()
        {
            listOfCity = new List<string>();
            listOfProvince = new List<string>();
            String[] rl = this.Resources.GetStringArray(Resource.Array.ZipCodes);
            for (int i = 0; i < rl.Length; i++)
            {
                String[] g = rl[i].Split(",");
                if (!listOfCity.Contains(g[2]))
                    listOfCity.Add(g[2]);

                if (!listOfProvince.Contains(g[1]))
                    listOfProvince.Add(g[1]);
            }
            listOfCity.Sort();
        }


        private async void BtnPost_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(etItemForBarter.Text.Trim()))
            {
                UserDialogs.Instance.Alert("Item for barter is required");
                return;
            }

            if (string.IsNullOrEmpty(openLocationText))
            {
                UserDialogs.Instance.Alert("Specific meetup location is required");
                return;
            }

            if (string.IsNullOrEmpty(cityText))
            {
                UserDialogs.Instance.Alert("City is required");
                return;
            }

            if (string.IsNullOrEmpty(provinceText))
            {
                UserDialogs.Instance.Alert("Province is required");
                return;
            }

            if (string.IsNullOrEmpty(etContactNumber.Text.Trim()))
            {
                UserDialogs.Instance.Alert("Contact number is required");
                return;
            }

            if (string.IsNullOrEmpty(etPreference.Text.Trim()))
            {
                UserDialogs.Instance.Alert("Preference field is required");
                return;
            }

            if (string.IsNullOrEmpty(etReasonForBarter.Text.Trim()))
            {
                UserDialogs.Instance.Alert("Reason for barter is required");
                return;
            }

            if (Photos?.Count == 0)
            {
                UserDialogs.Instance.Alert("Atleast one photo is required");
                return;
            }

            UserDialogs.Instance.ShowLoading();
            var response = await presenter.EditBarter(new MobileNetStandardLibrary.Commands.CreateBartersCommand
            {
                id = DTO.id,
                item = etItemForBarter.Text.Trim(),
                locationType = rbOpenLocation.Checked ? LocationType.OPEN.ToString() : LocationType.STRICT.ToString(),
                deliveryType = rbCourier.Checked ? DeliveryOption.COURIER.ToString() : DeliveryOption.MEETUP.ToString(),
                deliveryCity = cityText,
                deliveryLocation = openLocationText,
                deliveryProvince = provinceText,
                courier = etDeliveryOptions.Text.Trim(),
                location = openLocationText,
                city = cityText,
                province = provinceText,
                contactNumber = etContactNumber.Text.Trim(),
                preferences = etPreference.Text.Trim(),
                reason = etReasonForBarter.Text.Trim(),
                photos = Photos
            });

            UserDialogs.Instance.HideLoading();
            if (response.Success)
            {
                SetResult(Result.Ok);
                Finish();
            }
        }

        public bool OnNavigationItemSelected(IMenuItem menuItem)
        {
            if (menuItem.ItemId == Resource.Id.nav_logout)
            {
                Logout();
            }

            if (menuItem.ItemId == Resource.Id.nav_settings)
            {
                Intent intent = new Intent(this, typeof(UserSettingsActivity));
                StartActivity(intent);
            }


            if (menuItem.ItemId == Resource.Id.nav_about)
            {
                Intent intent = new Intent(this, typeof(AboutActivity));
                StartActivity(intent);
            }

            if (menuItem.ItemId == Resource.Id.nav_my_list)
            {
                Intent intent = new Intent(this, typeof(SearchActivity));
                intent.PutExtra("MyList", true);
                StartActivity(intent);
            }

            if (menuItem.ItemId == Resource.Id.nav_notifications)
            {
                Intent intent = new Intent(this, typeof(NotificationActivity));
                StartActivity(intent);
            }

            if (menuItem.ItemId == Resource.Id.nav_support)
            {
                Intent intent = new Intent(this, typeof(SupportActivity));
                StartActivity(intent);
            }

            if (menuItem.ItemId == Resource.Id.nav_items)
            {
                Intent intent = new Intent(this, typeof(SearchActivity));
                StartActivity(intent);
            }
            return true;
        }

        async Task Logout()
        {
            if (await UserDialogs.Instance.ConfirmAsync("Are you sure you want to logout?"))
            {
                await presenter.database.DeleteUser(presenter.userService.CurrentUser);
                presenter.userService.CurrentUser = null;
                Intent intent = new Intent(this, typeof(LandingActivity));
                intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.ClearTask | ActivityFlags.NewTask);
                StartActivity(intent);
            }
        }

        private void TvPhotos_Click(object sender, EventArgs e)
        {
            ShowCameraPicker();
        }

        public async Task ShowCameraPicker()
        {
            var response = await UserDialogs.Instance.ActionSheetAsync(GetText(Resource.String.ChooseOne), GetText(Resource.String.Cancel), "", null, new string[2] { GetText(Resource.String.TakeAPhoto), GetText(Resource.String.SelectFromGallery) });
            if (response == GetText(Resource.String.TakeAPhoto))
            {
                if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.Camera) == Permission.Granted)
                {
                    if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) == Permission.Granted &&
                        ContextCompat.CheckSelfPermission(this, Manifest.Permission.WriteExternalStorage) == Permission.Granted)
                    {
                        mHighQualityImageUri = GenerateTimeStampPhotoFileUri();
                        Intent intent = new Intent(MediaStore.ActionImageCapture);
                        intent.PutExtra(MediaStore.ExtraOutput, mHighQualityImageUri);
                        StartActivityForResult(intent, 101);
                    }
                    else
                    {
                        ActivityCompat.RequestPermissions(this, new String[] { Manifest.Permission.Camera, Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage }, REQUEST_CAMERA);
                    }
                }
                else
                {
                    ActivityCompat.RequestPermissions(this, new String[] { Manifest.Permission.Camera, Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage }, REQUEST_CAMERA);
                }
            }
            else if (response == GetText(Resource.String.SelectFromGallery))
            {
                if (ActivityCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) != (int)Permission.Granted)
                {
                    RequestGalleryPermission();
                    return;
                }

                var imageIntent = new Intent();
                imageIntent.SetType("image/*");
                imageIntent.PutExtra(Intent.ExtraAllowMultiple, true);
                imageIntent.SetAction(Intent.ActionGetContent);
                StartActivityForResult(
                    Intent.CreateChooser(imageIntent, "Select photo"), 102);
            }
        }

        private string GetPathToImage(Android.Net.Uri uri)
        {
            try
            {
                /**/
                string doc_id = "";
                using (var c1 = this.BaseContext.ContentResolver.Query(uri, null, null, null, null))
                {
                    c1.MoveToFirst();
                    String document_id = c1.GetString(0);
                    doc_id = document_id.Substring(document_id.LastIndexOf(":") + 1);
                }

                string path = null;

                // The projection contains the columns we want to return in our query.
                string selection = Android.Provider.MediaStore.Images.Media.InterfaceConsts.Id + " =? ";

                using (var cursor = ContentResolver.Query(Android.Provider.MediaStore.Images.Media.ExternalContentUri, null, selection, new string[] { doc_id }, null))
                {
                    if (cursor == null) return path;
                    var columnIndex = cursor.GetColumnIndexOrThrow(Android.Provider.MediaStore.Images.Media.InterfaceConsts.Data);
                    cursor.MoveToFirst();
                    path = cursor.GetString(columnIndex);
                }
                return path;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            try
            {
                if (requestCode == REQUEST_CAMERA)
                {
                    if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.Camera) == Permission.Granted)
                    {
                        if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) == Permission.Granted &&
                            ContextCompat.CheckSelfPermission(this, Manifest.Permission.WriteExternalStorage) == Permission.Granted)
                        {
                            mHighQualityImageUri = GenerateTimeStampPhotoFileUri();
                            Intent intent = new Intent(MediaStore.ActionImageCapture);
                            intent.PutExtra(MediaStore.ExtraOutput, mHighQualityImageUri);
                            StartActivityForResult(intent, 101);
                        }
                        else
                        {
                            ActivityCompat.RequestPermissions(this, new String[] { Manifest.Permission.Camera, Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage }, CameraId);
                        }
                    }
                    else
                    {
                        ActivityCompat.RequestPermissions(this, new String[] { Manifest.Permission.Camera, Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage }, CameraId);
                    }
                }
                else if (requestCode == REQUEST_GALLERY)
                {
                    if (ActivityCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) == (int)Permission.Granted)
                    {
                        var imageIntent = new Intent();
                        imageIntent.PutExtra(Intent.ExtraAllowMultiple, true);
                        imageIntent.SetType("image/*");
                        imageIntent.SetAction(Intent.ActionGetContent);
                        StartActivityForResult(
                            Intent.CreateChooser(imageIntent, "Select photo"), 102);
                    }
                }
                else
                {
                    base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
                }

            }
            catch (System.Exception ex)
            {
                //Crashes.TrackError(ex);
            }
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public static int CalculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
        {
            int height = options.OutHeight;
            int width = options.OutWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth)
            {
                var heightRatio = (int)Math.Round(height / (double)reqHeight);
                var widthRatio = (int)Math.Round(width / (double)reqWidth);
                inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
            }
            return inSampleSize;
        }

        private void CreateDirectoryForPictures()
        {
            MediaHelper._dir = new Java.IO.File(
                System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "BarterAppPhoto");
            if (!MediaHelper._dir.Exists())
            {
                MediaHelper._dir.Mkdirs();
            }
            MediaHelper._dir.SetWritable(true);
            MediaHelper._dir.SetReadable(true);
        }


        private string Save(string directory, Bitmap bitmap)
        {
            Java.IO.File myPath = new Java.IO.File(directory, Guid.NewGuid() + ".jpg");

            using (var os = new FileStream(myPath.AbsolutePath, FileMode.Create))
            {
                bitmap.Compress(Bitmap.CompressFormat.Jpeg, 100, os);
            }

            return myPath.AbsolutePath;
        }


        protected override async void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent intent)
        {
            try
            {
                if (resultCode == Result.Ok)
                {
                    if (requestCode == 101)
                    {
                        if ((resultCode == Result.Ok))
                        {

                            Bitmap bitmap = LoadAndResizeBitmap(outputDir.AbsolutePath);
                            if (bitmap != null)
                            {
                                byte[] bitmapData;
                                var stream = new MemoryStream();
                                bitmap.Compress(Bitmap.CompressFormat.Jpeg, 90, stream);
                                bitmapData = stream.ToArray();

                                UserDialogs.Instance.ShowLoading("Uploading...");
                                var response = await presenter.UploadPhoto(bitmapData);


                                if (response.Success)
                                {
                                    if (Photos == null)
                                        Photos = new List<string>();
                                    Photos.Add(response.Data);

                                    ImageView image = new ImageView(this);
                                    var paramsImage = new LinearLayout.LayoutParams(150, 150);
                                    paramsImage.LeftMargin = 20;
                                    image.LayoutParameters = paramsImage;
                                    image.SetScaleType(scaleType: ImageView.ScaleType.CenterCrop);
                                    image.SetImageBitmap(bitmap);
                                    image.Tag = outputDir.AbsolutePath;
                                    image.Click += Image_Click;
                                    llPhotos.AddView(image);

                                    if (LocalPhotos == null)
                                        LocalPhotos = new List<string>();
                                    LocalPhotos.Add(outputDir.AbsolutePath);
                                }
                                UserDialogs.Instance.HideLoading();

                                GC.Collect();
                            }
                        }
                    }
                    if (requestCode == 102)
                    {
                        if (intent.Data == null)
                        {
                            if (intent.ClipData != null)
                            {
                                if (Photos == null)
                                    Photos = new List<string>();

                                if (LocalPhotos == null)
                                    LocalPhotos = new List<string>();

                                if (intent.ClipData.ItemCount + Photos.Count > 15)
                                {
                                    await Task.Delay(1000);
                                    UserDialogs.Instance.Alert(GetText(Resource.String.SorryYouHaveExceededMaxPhoto));
                                    return;
                                }
                                LoadingText = $"{GetText(Resource.String.UploadingPhotos)} ({1} out of {intent.ClipData.ItemCount}).";
                                var progress = UserDialogs.Instance.Progress(LoadingText, null, null, true);

                                for (int y = 0; y < intent.ClipData.ItemCount; y++)
                                {
                                    int counts = intent.ClipData.ItemCount > (15 - Photos.Count) ? 15 - Photos.Count : intent.ClipData.ItemCount;
                                    LoadingText = $"{GetText(Resource.String.UploadingPhotos)} ({y + 1} out of {intent.ClipData.ItemCount}).";
                                    progress.Title = LoadingText;
                                    double percentage = ((y + 1.0) / counts);
                                    percentage = percentage * 100d;
                                    progress.PercentComplete = (int)percentage;
                                    ClipData.Item item = intent.ClipData.GetItemAt(y);
                                    var uri = item.Uri;
                                    if (uri == null)
                                    {
                                        InvalidPhotos++;
                                        continue;
                                    }
                                    ExifInterface exif = null;
                                    try
                                    {
                                        exif = new ExifInterface(GetPathToImage(uri));
                                    }
                                    catch (System.Exception ex)
                                    {
                                        //Crashes.TrackError(ex);
                                    }

                                    int orientation = exif != null ? exif.GetAttributeInt(ExifInterface.TagOrientation, 0) : 0;
                                    Bitmap bitmap = null;

                                    try
                                    {
                                        bitmap = MediaStore.Images.Media.GetBitmap(this.ContentResolver, uri);
                                    }
                                    catch (Exception ex)
                                    {
                                        //Crashes.TrackError(ex);
                                    }

                                    if (bitmap == null)
                                    {
                                        InvalidPhotos++;
                                        continue;
                                    }

                                    var path = Save(MediaHelper._dir.AbsolutePath, bitmap);

                                    MediaHelper.bitmap = path.LoadAndResizeBitmap(750, 750);

                                    MediaHelper.bitmap = RotateBitmap(MediaHelper.bitmap, orientation);

                                    if (MediaHelper.bitmap != null)
                                    {
                                        byte[] bitmapData;
                                        var stream = new MemoryStream();
                                        MediaHelper.bitmap.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
                                        bitmapData = stream.ToArray();
                                        //UserDialogs.Instance.ShowLoading("Uploading Photo...");


                                        var response = await presenter.UploadPhoto(bitmapData);
                                        if (response.Success)
                                        {
                                            if (Photos == null)
                                                Photos = new List<string>();
                                            Photos.Add(response.Data);

                                            ImageView image = new ImageView(this);
                                            var paramsImage = new LinearLayout.LayoutParams(150, 150);
                                            paramsImage.LeftMargin = 20;
                                            image.LayoutParameters = paramsImage;
                                            image.SetScaleType(scaleType: ImageView.ScaleType.CenterCrop);
                                            image.SetImageBitmap(MediaHelper.bitmap);
                                            image.Tag = path;
                                            image.Click += Image_Click;
                                            llPhotos.AddView(image);

                                            if (LocalPhotos == null)
                                                LocalPhotos = new List<string>();
                                            LocalPhotos.Add(path);
                                        }
                                        MediaHelper.bitmap.Dispose();
                                        MediaHelper.bitmap = null;
                                    }
                                    // Dispose of the Java side bitmap.
                                    GC.Collect();
                                }
                                progress.Hide();

                                if (InvalidPhotos > 0)
                                {
                                    UserDialogs.Instance.Alert($"We detected {InvalidPhotos} invalid/ private photo. Please use other photo as replacement.");
                                    InvalidPhotos = 0;
                                }
                                InvalidPhotos = 0;
                                //UserDialogs.Instance.Progress(LoadingText, null, null, true);
                            }
                        }
                        else
                        {
                            ExifInterface exif = null;
                            try
                            {
                                exif = new ExifInterface(GetPathToImage(intent.Data));

                            }
                            catch (System.Exception ex)
                            {
                                //Crashes.TrackError(ex);
                            }

                            int orientation = exif != null ? exif.GetAttributeInt(ExifInterface.TagOrientation, 0) : 0;
                            Bitmap bitmap = MediaStore.Images.Media.GetBitmap(this.ContentResolver, intent.Data);

                            var path = Save(MediaHelper._dir.AbsolutePath, bitmap);

                            MediaHelper.bitmap = path.LoadAndResizeBitmap(750, 750);

                            MediaHelper.bitmap = RotateBitmap(MediaHelper.bitmap, orientation);

                            if (MediaHelper.bitmap != null)
                            {
                                byte[] bitmapData;
                                var stream = new MemoryStream();
                                MediaHelper.bitmap.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
                                bitmapData = stream.ToArray();
                                UserDialogs.Instance.ShowLoading("Uploading Photo...");

                                var response = await presenter.UploadPhoto(bitmapData);
                                UserDialogs.Instance.HideLoading();
                                if (response.Success)
                                {
                                    if (Photos == null)
                                        Photos = new List<string>();
                                    Photos.Add(response.Data);

                                    ImageView image = new ImageView(this);
                                    var paramsImage = new LinearLayout.LayoutParams(150, 150);
                                    paramsImage.LeftMargin = 20;
                                    image.LayoutParameters = paramsImage;
                                    image.SetScaleType(scaleType: ImageView.ScaleType.CenterCrop);
                                    image.SetImageBitmap(bitmap);
                                    image.Tag = path;
                                    image.Click += Image_Click;
                                    llPhotos.AddView(image);


                                    if (LocalPhotos == null)
                                        LocalPhotos = new List<string>();
                                    LocalPhotos.Add(path);
                                }
                                MediaHelper.bitmap = null;
                            }
                            // Dispose of the Java side bitmap.
                            GC.Collect();
                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            base.OnActivityResult(requestCode, resultCode, intent);
        }


        private async void Image_Click(object sender, EventArgs e)
        {
            var response = await UserDialogs.Instance.ConfirmAsync(GetText(Resource.String.DoYouWantToRemoveThisPhoto), "", GetText(Resource.String.Yes), GetText(Resource.String.Cancel));
            if (response)
            {
                var imageView = sender as ImageView;
                llPhotos.RemoveView(imageView);
                var index = LocalPhotos.IndexOf(imageView.Tag.ToString());
                Photos.RemoveAt(index);
                LocalPhotos.RemoveAt(index);
            }
        }


        void RequestGalleryPermission()
        {
            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.RequestPermissions(this, new String[] { Manifest.Permission.WriteExternalStorage, Manifest.Permission.ReadExternalStorage }, REQUEST_GALLERY);
        }
        Java.IO.File outputDir;
        private Android.Net.Uri GenerateTimeStampPhotoFileUri()
        {
            Android.Net.Uri photoFileUri = null;
            var sdCardPath = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDocuments).AbsolutePath;
            var filePath = System.IO.Path.Combine(sdCardPath, DateTime.Now.ToString("mmddhhss") + ".png");

            outputDir = new Java.IO.File(filePath);
            photoFileUri = Android.Net.Uri.FromFile(outputDir);

            return photoFileUri;
        }

        private void IvSearch_Click(object sender, System.EventArgs e)
        {

        }


        public Bitmap RotateBitmap(Bitmap bitmap, int orientation)
        {

            Matrix matrix = new Matrix();
            Android.Media.Orientation orient = (Android.Media.Orientation)orientation;

            switch (orient)
            {
                case Android.Media.Orientation.Normal:
                    return bitmap;
                case Android.Media.Orientation.FlipHorizontal:
                    matrix.SetScale(-1, 1);
                    break;
                case Android.Media.Orientation.Rotate180:
                    matrix.SetRotate(180);
                    break;
                case Android.Media.Orientation.FlipVertical:
                    matrix.SetRotate(180);
                    matrix.PostScale(-1, 1);
                    break;
                case Android.Media.Orientation.Transpose:
                    matrix.SetRotate(90);
                    matrix.PostScale(-1, 1);
                    break;
                case Android.Media.Orientation.Rotate90:
                    matrix.SetRotate(90);
                    break;
                case Android.Media.Orientation.Transverse:
                    matrix.SetRotate(-90);
                    matrix.PostScale(-1, 1);
                    break;
                case Android.Media.Orientation.Rotate270:
                    matrix.SetRotate(-90);
                    break;
                default:
                    return bitmap;
            }
            try
            {
                Bitmap bmRotated = Bitmap.CreateBitmap(bitmap, 0, 0, bitmap.Width, bitmap.Height, matrix, true);
                bitmap.Recycle();
                return bmRotated;
            }
            catch (Java.Lang.OutOfMemoryError e)
            {
                return null;
            }
            catch (System.Exception ex)
            {
                //Crashes.TrackError(ex);
                return null;
            }
        }
    }
}
