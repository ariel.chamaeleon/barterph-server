﻿using System;
using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using Unity;
using Android.Database;
using Android.Provider;
using Android.Util;
using Android.Graphics;
using Android.Media;
using Android.Content;
using MobileNetStandardLibrary.Controller;
using static BarterAppAndroid.MainActivity;
using Android;
using Android.Support.V4.App;
using Acr.UserDialogs;
using BarterApp.Droid;
using System.Threading.Tasks;
using Android.Support.V4.Content;
using Android.Content.PM;
using System.IO;
using Android.Runtime;

namespace BarterAppAndroid
{
    [Activity(Label = "BaseActivity", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public abstract class BaseActivity : AppCompatActivity
    {
        public BaseViewPresenter presenter;

        public readonly int PickImageId = 102;
        public readonly int CameraId = 101;
        public readonly int ExternalStorageId = 1001;
        static readonly int REQUEST_CAMERA = 2;
        static readonly int REQUEST_GALLERY = 1;

        public readonly int PhotosAllowed = 10;

        private Android.Net.Uri mHighQualityImageUri = null;
        public bool AllowMultiplePhotos { get; set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            presenter = Resolver.Container.Resolve(typeof(BaseViewPresenter)) as BaseViewPresenter;
            MainActivity.IsAppActive = true;
            CreateDirectoryForPictures();
        }

        protected override void OnPause()
        {
            base.OnPause();
            MainActivity.IsAppActive = false;
        }

        protected override void OnResume()
        {
            base.OnResume();
            MainActivity.IsAppActive = true;
        }

        public async Task ShowCameraPicker()
        {
            var response = await UserDialogs.Instance.ActionSheetAsync(GetText(Resource.String.ChooseOne), GetText(Resource.String.Cancel), "", null, new string[2] { GetText(Resource.String.TakeAPhoto), GetText(Resource.String.SelectFromGallery) });
            if (response == GetText(Resource.String.TakeAPhoto))
            {
                if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.Camera) == Permission.Granted)
                {
                    if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) == Permission.Granted &&
                        ContextCompat.CheckSelfPermission(this, Manifest.Permission.WriteExternalStorage) == Permission.Granted)
                    {
                        mHighQualityImageUri = GenerateTimeStampPhotoFileUri();
                        Intent intent = new Intent(MediaStore.ActionImageCapture);
                        intent.PutExtra(MediaStore.ExtraOutput, mHighQualityImageUri);
                        StartActivityForResult(intent, 101);
                    }
                    else
                    {
                        ActivityCompat.RequestPermissions(this, new String[] { Manifest.Permission.Camera, Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage }, REQUEST_CAMERA);
                    }
                }
                else
                {
                    ActivityCompat.RequestPermissions(this, new String[] { Manifest.Permission.Camera, Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage }, REQUEST_CAMERA);
                }
            }
            else if (response == GetText(Resource.String.SelectFromGallery))
            {
                if (ActivityCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) != (int)Permission.Granted)
                {
                    RequestGalleryPermission();
                    return;
                }

                var imageIntent = new Intent();
                imageIntent.SetType("image/*");
                if (AllowMultiplePhotos) {
                    imageIntent.PutExtra(Intent.ExtraAllowMultiple, true);
                }
                imageIntent.SetAction(Intent.ActionGetContent);
                StartActivityForResult(
                    Intent.CreateChooser(imageIntent, "Select photo"), 102);
            }
        }

        public string GetPathToImage(Android.Net.Uri uri)
        {
            try
            {
                /**/
                string doc_id = "";
                using (var c1 = this.BaseContext.ContentResolver.Query(uri, null, null, null, null))
                {
                    c1.MoveToFirst();
                    String document_id = c1.GetString(0);
                    doc_id = document_id.Substring(document_id.LastIndexOf(":") + 1);
                }

                string path = null;

                // The projection contains the columns we want to return in our query.
                string selection = Android.Provider.MediaStore.Images.Media.InterfaceConsts.Id + " =? ";

                using (var cursor = ContentResolver.Query(Android.Provider.MediaStore.Images.Media.ExternalContentUri, null, selection, new string[] { doc_id }, null))
                {
                    if (cursor == null) return path;
                    var columnIndex = cursor.GetColumnIndexOrThrow(Android.Provider.MediaStore.Images.Media.InterfaceConsts.Data);
                    cursor.MoveToFirst();
                    path = cursor.GetString(columnIndex);
                }
                return path;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            try
            {
                if (requestCode == REQUEST_CAMERA)
                {
                    if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.Camera) == Permission.Granted)
                    {
                        if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) == Permission.Granted &&
                            ContextCompat.CheckSelfPermission(this, Manifest.Permission.WriteExternalStorage) == Permission.Granted)
                        {
                            mHighQualityImageUri = GenerateTimeStampPhotoFileUri();
                            Intent intent = new Intent(MediaStore.ActionImageCapture);
                            intent.PutExtra(MediaStore.ExtraOutput, mHighQualityImageUri);
                            StartActivityForResult(intent, 101);
                        }
                        else
                        {
                            ActivityCompat.RequestPermissions(this, new String[] { Manifest.Permission.Camera, Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage }, CameraId);
                        }
                    }
                    else
                    {
                        ActivityCompat.RequestPermissions(this, new String[] { Manifest.Permission.Camera, Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage }, CameraId);
                    }
                }
                else if (requestCode == REQUEST_GALLERY)
                {
                    if (ActivityCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) == (int)Permission.Granted)
                    {
                        var imageIntent = new Intent();
                        if (AllowMultiplePhotos)
                        {
                            imageIntent.PutExtra(Intent.ExtraAllowMultiple, true);
                        }
                        imageIntent.SetType("image/*");
                        imageIntent.SetAction(Intent.ActionGetContent);
                        StartActivityForResult(
                            Intent.CreateChooser(imageIntent, "Select photo"), 102);
                    }
                }
                else
                {
                    base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
                }

            }
            catch (System.Exception ex)
            {
                //Crashes.TrackError(ex);
            }
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        private void CreateDirectoryForPictures()
        {
            MediaHelper._dir = new Java.IO.File(
                System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "BarterAppPhoto");
            if (!MediaHelper._dir.Exists())
            {
                MediaHelper._dir.Mkdirs();
            }
            MediaHelper._dir.SetWritable(true);
            MediaHelper._dir.SetReadable(true);
        }


        public string Save(string directory, Bitmap bitmap)
        {
            Java.IO.File myPath = new Java.IO.File(directory, Guid.NewGuid() + ".jpg");

            using (var os = new FileStream(myPath.AbsolutePath, FileMode.Create))
            {
                bitmap.Compress(Bitmap.CompressFormat.Jpeg, 100, os);
            }

            return myPath.AbsolutePath;
        }

        public Bitmap RotateBitmap(Bitmap bitmap, int orientation)
        {

            Matrix matrix = new Matrix();
            Android.Media.Orientation orient = (Android.Media.Orientation)orientation;

            switch (orient)
            {
                case Android.Media.Orientation.Normal:
                    return bitmap;
                case Android.Media.Orientation.FlipHorizontal:
                    matrix.SetScale(-1, 1);
                    break;
                case Android.Media.Orientation.Rotate180:
                    matrix.SetRotate(180);
                    break;
                case Android.Media.Orientation.FlipVertical:
                    matrix.SetRotate(180);
                    matrix.PostScale(-1, 1);
                    break;
                case Android.Media.Orientation.Transpose:
                    matrix.SetRotate(90);
                    matrix.PostScale(-1, 1);
                    break;
                case Android.Media.Orientation.Rotate90:
                    matrix.SetRotate(90);
                    break;
                case Android.Media.Orientation.Transverse:
                    matrix.SetRotate(-90);
                    matrix.PostScale(-1, 1);
                    break;
                case Android.Media.Orientation.Rotate270:
                    matrix.SetRotate(-90);
                    break;
                default:
                    return bitmap;
            }
            try
            {
                Bitmap bmRotated = Bitmap.CreateBitmap(bitmap, 0, 0, bitmap.Width, bitmap.Height, matrix, true);
                bitmap.Recycle();
                return bmRotated;
            }
            catch (Java.Lang.OutOfMemoryError e)
            {
                return null;
            }
            catch (System.Exception ex)
            {
                //Crashes.TrackError(ex);
                return null;
            }
        }

        void RequestGalleryPermission()
        {
            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.RequestPermissions(this, new String[] { Manifest.Permission.WriteExternalStorage, Manifest.Permission.ReadExternalStorage }, REQUEST_GALLERY);
        }

        public Java.IO.File outputDir;

        public Android.Net.Uri GenerateTimeStampPhotoFileUri()
        {
            Android.Net.Uri photoFileUri = null;
            var sdCardPath = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDocuments).AbsolutePath;
            var filePath = System.IO.Path.Combine(sdCardPath, DateTime.Now.ToString("mmddhhss") + ".png");

            outputDir = new Java.IO.File(filePath);
            photoFileUri = Android.Net.Uri.FromFile(outputDir);

            return photoFileUri;
        }


        public static int CalculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
        {
            int height = options.OutHeight;
            int width = options.OutWidth;
            int inSampleSize = 2;

            if (height > reqHeight || width > reqWidth)
            {
                var heightRatio = (int)Math.Round(height / (double)reqHeight);
                var widthRatio = (int)Math.Round(width / (double)reqWidth);
                inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
            }
            return inSampleSize;
        }

        public Bitmap LoadAndResizeBitmap(string filePath)
        {
            BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
            //BitmapFactory.DecodeFile(filePath, options);

            int REQUIRED_SIZE = 750;
            //int width_tmp = options.OutWidth, height_tmp = options.OutHeight;
            //var memory = GC.GetTotalMemory(true);
            //int scale = 3;
            //while (true)
            //{
            //    if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
            //        break;
            //    width_tmp /= 2;
            //    height_tmp /= 2;
            //    scale++;
            //}

            options.InSampleSize = CalculateInSampleSize(options, REQUIRED_SIZE, REQUIRED_SIZE);
            options.InJustDecodeBounds = false;
            Bitmap resizedBitmap = BitmapFactory.DecodeFile(filePath, options);

            ExifInterface exif = null;
            try
            {
                exif = new ExifInterface(filePath);
                string orientation = exif.GetAttribute(ExifInterface.TagOrientation);

                Matrix matrix = new Matrix();
                switch (orientation)
                {
                    case "1": // landscape
                        break;
                    case "3":
                        matrix.PreRotate(180);
                        resizedBitmap = Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                        matrix.Dispose();
                        matrix = null;
                        break;
                    case "4":
                        matrix.PreRotate(180);
                        resizedBitmap = Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                        matrix.Dispose();
                        matrix = null;
                        break;
                    case "5":
                        matrix.PreRotate(90);
                        resizedBitmap = Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                        matrix.Dispose();
                        matrix = null;
                        break;
                    case "6": // portrait
                        matrix.PreRotate(90);
                        resizedBitmap = Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                        matrix.Dispose();
                        matrix = null;
                        break;
                    case "7":
                        matrix.PreRotate(-90);
                        resizedBitmap = Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                        matrix.Dispose();
                        matrix = null;
                        break;
                    case "8":
                        matrix.PreRotate(-90);
                        resizedBitmap = Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                        matrix.Dispose();
                        matrix = null;
                        break;
                }

                return resizedBitmap;
            }

            catch (Java.IO.IOException ex)
            {
                Console.WriteLine("An exception was thrown when reading exif from media file...:" + ex.Message);
                return null;
            }
        }

        public static string GetRealPathFromURI(BaseActivity act, Android.Net.Uri contentURI)
        {
            try
            {
                ICursor cursor = act.ContentResolver.Query(contentURI, null, null, null, null);
                cursor.MoveToFirst();
                string documentId = cursor.GetString(0);
                documentId = documentId.Split(':')[1];
                cursor.Close();

                cursor = act.ContentResolver.Query(
                MediaStore.Images.Media.ExternalContentUri,
                null, MediaStore.Images.Media.InterfaceConsts.Id + " = ? ", new[] { documentId }, null);
                cursor.MoveToFirst();
                string path = cursor.GetString(cursor.GetColumnIndex(MediaStore.Images.Media.InterfaceConsts.Data));
                cursor.Close();


                return path;
            }
            catch (Exception ex)
            {
                //Crashes.TrackError(ex);
                return "";
            }
        }
    }
}
