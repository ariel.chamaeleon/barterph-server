﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.Media;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using AppStandardLibrary;
using BarterApp.Droid;
using BarterAppAndroid.Controls;
using Square.Picasso;

namespace BarterAppAndroid.Activity
{
    [Activity(Label = "ProfileActivity", Theme = "@style/AppTheme.NoActionBar", ScreenOrientation = ScreenOrientation.Portrait)]
    public class ProfileActivity : BaseActivity
    {
        private GenericEditText etFirstName;
        private GenericEditText etLastName;
        private GenericEditText etCity;

        private GenericEditText etProvince;
        private GenericEditText etBarangay;
        private GenericEditText etHouseNumber;

        private ImageView ivProfilePic;

        public readonly int PickImageId = 102;
        public readonly int CameraId = 101;
        public readonly int ExternalStorageId = 1001;
        static readonly int REQUEST_CAMERA = 2;
        static readonly int REQUEST_GALLERY = 1;

        public static List<string> Photos;
        public static List<string> LocalPhotos;
        public static int UploadCount;
        public static int InvalidPhotos;

        public string LoadingText { get; set; }

        private Android.Net.Uri mHighQualityImageUri = null;

        private Button btnSave;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.profile_activity);
            btnSave = FindViewById<Button>(Resource.Id.btnSave);
            btnSave.Click += IvCheck_Click;
            etProvince = FindViewById<GenericEditText>(Resource.Id.etProvince);
            etBarangay = FindViewById<GenericEditText>(Resource.Id.etBarangay);
            etHouseNumber = FindViewById<GenericEditText>(Resource.Id.etHouseNumber);
            etFirstName = FindViewById<GenericEditText>(Resource.Id.etFirstName);
            etLastName = FindViewById<GenericEditText>(Resource.Id.etLastName);
            ivProfilePic = FindViewById<ImageView>(Resource.Id.ivProfilePic);
            etCity = FindViewById<GenericEditText>(Resource.Id.etCity);
            etProvince.arrowDown.Visibility = ViewStates.Visible;
            etCity.arrowDown.Visibility = ViewStates.Visible;
            etCity.Field.Focusable = false;
            etCity.Field.Clickable = true;
            etCity.Field.Click += Field_Click;

            etProvince.Field.Focusable = false;
            etProvince.Field.Clickable = true;
            etProvince.Field.Click += Field_Click1; ;

            etFirstName.floatingLabel.Text = "First Name *";
            etLastName.floatingLabel.Text = "Last Name *";
            etFirstName.Field.Hint = "First Name *";
            etLastName.Field.Hint = "Last Name *";

            etHouseNumber.Field.Hint = etHouseNumber.floatingLabel.Text = "House #, bldg and street name (optional)";
            etProvince.Field.Hint = etProvince.floatingLabel.Text = "Province *";
            etCity.Field.Hint = etCity.floatingLabel.Text = "City/ Municipality *";
            etBarangay.Field.Hint = etBarangay.floatingLabel.Text = "Barangay (optional)";

            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            var actionBar = SupportActionBar;
            actionBar.SetHomeButtonEnabled(true);
            actionBar.SetDisplayHomeAsUpEnabled(true);
            actionBar.Title = "Profile";
            var ivCheck = toolbar.FindViewById<ImageView>(Resource.Id.ivCheck);

            ivCheck.Visibility = ViewStates.Gone;
            ivCheck.Click += IvCheck_Click;
            ivProfilePic.Click += IvProfilePic_Click;

            GetCityList();
            InitUser();
       
        }


        //public async Task ShowCameraPicker()
        //{
        //    var response = await UserDialogs.Instance.ActionSheetAsync(GetText(Resource.String.ChooseOne), GetText(Resource.String.Cancel), "", null, new string[2] { GetText(Resource.String.TakeAPhoto), GetText(Resource.String.SelectFromGallery) });
        //    if (response == GetText(Resource.String.TakeAPhoto))
        //    {
        //        if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.Camera) == Permission.Granted)
        //        {
        //            if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) == Permission.Granted &&
        //                ContextCompat.CheckSelfPermission(this, Manifest.Permission.WriteExternalStorage) == Permission.Granted)
        //            {
        //                mHighQualityImageUri = GenerateTimeStampPhotoFileUri();
        //                Intent intent = new Intent(MediaStore.ActionImageCapture);
        //                intent.PutExtra(MediaStore.ExtraOutput, mHighQualityImageUri);
        //                StartActivityForResult(intent, 101);
        //            }
        //            else
        //            {
        //                ActivityCompat.RequestPermissions(this, new String[] { Manifest.Permission.Camera, Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage }, REQUEST_CAMERA);
        //            }
        //        }
        //        else
        //        {
        //            ActivityCompat.RequestPermissions(this, new String[] { Manifest.Permission.Camera, Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage }, REQUEST_CAMERA);
        //        }
        //    }
        //    else if (response == GetText(Resource.String.SelectFromGallery))
        //    {
        //        if (ActivityCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) != (int)Permission.Granted)
        //        {
        //            RequestGalleryPermission();
        //            return;
        //        }

        //        var imageIntent = new Intent();
        //        imageIntent.SetType("image/*");
        //        imageIntent.SetAction(Intent.ActionGetContent);
        //        StartActivityForResult(
        //            Intent.CreateChooser(imageIntent, "Select photo"), 102);
        //    }
        //}

        //private string GetPathToImage(Android.Net.Uri uri)
        //{
        //    try
        //    {
        //        /**/
        //        string doc_id = "";
        //        using (var c1 = this.BaseContext.ContentResolver.Query(uri, null, null, null, null))
        //        {
        //            c1.MoveToFirst();
        //            String document_id = c1.GetString(0);
        //            doc_id = document_id.Substring(document_id.LastIndexOf(":") + 1);
        //        }
        //        string path = null;
        //        // The projection contains the columns we want to return in our query.
        //        string selection = Android.Provider.MediaStore.Images.Media.InterfaceConsts.Id + " =? ";
        //        using (var cursor = ContentResolver.Query(Android.Provider.MediaStore.Images.Media.ExternalContentUri, null, selection, new string[] { doc_id }, null))
        //        {
        //            if (cursor == null) return path;
        //            var columnIndex = cursor.GetColumnIndexOrThrow(Android.Provider.MediaStore.Images.Media.InterfaceConsts.Data);
        //            cursor.MoveToFirst();
        //            path = cursor.GetString(columnIndex);
        //        }
        //        return path;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        //public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        //{
        //    try
        //    {
        //        if (requestCode == REQUEST_CAMERA)
        //        {
        //            if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.Camera) == Permission.Granted)
        //            {
        //                if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) == Permission.Granted &&
        //                    ContextCompat.CheckSelfPermission(this, Manifest.Permission.WriteExternalStorage) == Permission.Granted)
        //                {
        //                    mHighQualityImageUri = GenerateTimeStampPhotoFileUri();
        //                    Intent intent = new Intent(MediaStore.ActionImageCapture);
        //                    intent.PutExtra(MediaStore.ExtraOutput, mHighQualityImageUri);
        //                    StartActivityForResult(intent, 101);
        //                }
        //                else
        //                {
        //                    ActivityCompat.RequestPermissions(this, new String[] { Manifest.Permission.Camera, Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage }, CameraId);
        //                }
        //            }
        //            else
        //            {
        //                ActivityCompat.RequestPermissions(this, new String[] { Manifest.Permission.Camera, Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage }, CameraId);
        //            }
        //        }
        //        else if (requestCode == REQUEST_GALLERY)
        //        {
        //            if (ActivityCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) == (int)Permission.Granted)
        //            {
        //                var imageIntent = new Intent();
        //                imageIntent.SetType("image/*");
        //                imageIntent.SetAction(Intent.ActionGetContent);
        //                StartActivityForResult(
        //                    Intent.CreateChooser(imageIntent, "Select photo"), 102);
        //            }
        //        }
        //        else
        //        {
        //            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        //        }

        //    }
        //    catch (System.Exception ex)
        //    {
        //        //Crashes.TrackError(ex);
        //    }
        //    base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        //}

        //public static int CalculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
        //{
        //    int height = options.OutHeight;
        //    int width = options.OutWidth;
        //    int inSampleSize = 1;

        //    if (height > reqHeight || width > reqWidth)
        //    {
        //        var heightRatio = (int)Math.Round(height / (double)reqHeight);
        //        var widthRatio = (int)Math.Round(width / (double)reqWidth);
        //        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        //    }
        //    return inSampleSize;
        //}

        //private void CreateDirectoryForPictures()
        //{
        //    MediaHelper._dir = new Java.IO.File(
        //        System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "BarterAppPhoto");
        //    if (!MediaHelper._dir.Exists())
        //    {
        //        MediaHelper._dir.Mkdirs();
        //    }
        //    MediaHelper._dir.SetWritable(true);
        //    MediaHelper._dir.SetReadable(true);
        //}


        //private string Save(string directory, Bitmap bitmap)
        //{
        //    Java.IO.File myPath = new Java.IO.File(directory, Guid.NewGuid() + ".jpg");

        //    using (var os = new FileStream(myPath.AbsolutePath, FileMode.Create))
        //    {
        //        bitmap.Compress(Bitmap.CompressFormat.Jpeg, 100, os);
        //    }

        //    return myPath.AbsolutePath;
        //}


        protected override async void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent intent)
        {
            try
            {
                if (resultCode == Result.Ok)
                {
                    if (requestCode == 101)
                    {
                        if ((resultCode == Result.Ok))
                        {

                            Bitmap bitmap = LoadAndResizeBitmap(outputDir.AbsolutePath);
                            if (bitmap != null)
                            {
                                byte[] bitmapData;
                                var stream = new MemoryStream();
                                bitmap.Compress(Bitmap.CompressFormat.Jpeg, 90, stream);
                                bitmapData = stream.ToArray();

                                UserDialogs.Instance.ShowLoading("Uploading...");
                                var response = await presenter.UploadPhoto(bitmapData);
                                if (response.Success)
                                {
                                    if (response.Success)
                                    {
                                        UserDialogs.Instance.ShowLoading();
                                        await presenter.UpdateProfile(new MobileNetStandardLibrary.Commands.UpdateUserCommand
                                        {
                                            imageUrl = response.Data
                                        });
                                        if (!string.IsNullOrEmpty(presenter.userService.CurrentUser.imageUrl))
                                            Picasso.Get().Load(URLPath.BaseURL + presenter.userService.CurrentUser.imageUrl).Placeholder(Resource.Mipmap.user_placeholder).Into(ivProfilePic);
                                        UserDialogs.Instance.HideLoading();
                                        if (DashboardActivity.Dashboard != null)
                                        {
                                            DashboardActivity.Dashboard.UpdateNameAndPhoto();
                                        }
                                    }
                                }
                                UserDialogs.Instance.HideLoading();

                                GC.Collect();
                            }
                        }
                    }
                    if (requestCode == 102)
                    {
                        ExifInterface exif = null;
                        try
                        {
                            exif = new ExifInterface(GetPathToImage(intent.Data));

                        }
                        catch (System.Exception ex)
                        {
                            //Crashes.TrackError(ex);
                        }

                        int orientation = exif != null ? exif.GetAttributeInt(ExifInterface.TagOrientation, 0) : 0;
                        Bitmap bitmap = MediaStore.Images.Media.GetBitmap(this.ContentResolver, intent.Data);

                        var path = Save(MediaHelper._dir.AbsolutePath, bitmap);

                        MediaHelper.bitmap = path.LoadAndResizeBitmap(750, 750);

                        MediaHelper.bitmap = RotateBitmap(MediaHelper.bitmap, orientation);

                        if (MediaHelper.bitmap != null)
                        {
                            byte[] bitmapData;
                            var stream = new MemoryStream();
                            MediaHelper.bitmap.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
                            bitmapData = stream.ToArray();
                            UserDialogs.Instance.ShowLoading("Uploading Photo...");

                            var response = await presenter.UploadPhoto(bitmapData);
                            UserDialogs.Instance.HideLoading();
                            if (response.Success)
                            {
                                UserDialogs.Instance.ShowLoading();
                                await presenter.UpdateProfile(new MobileNetStandardLibrary.Commands.UpdateUserCommand
                                {
                                    imageUrl = response.Data
                                });
                                if (!string.IsNullOrEmpty(presenter.userService.CurrentUser.imageUrl))
                                    Picasso.Get().Load(URLPath.BaseURL + presenter.userService.CurrentUser.imageUrl).Placeholder(Resource.Mipmap.user_placeholder).Into(ivProfilePic);
                                UserDialogs.Instance.HideLoading();
                                if (DashboardActivity.Dashboard != null)
                                {
                                    DashboardActivity.Dashboard.UpdateNameAndPhoto();
                                }
                            }
                            MediaHelper.bitmap = null;
                        }
                        // Dispose of the Java side bitmap.
                        GC.Collect();
                    }
                }
            }
            catch (Exception ex)
            {

            }
            base.OnActivityResult(requestCode, resultCode, intent);
        }

        public Bitmap RotateBitmap(Bitmap bitmap, int orientation)
        {

            Matrix matrix = new Matrix();
            Android.Media.Orientation orient = (Android.Media.Orientation)orientation;

            switch (orient)
            {
                case Android.Media.Orientation.Normal:
                    return bitmap;
                case Android.Media.Orientation.FlipHorizontal:
                    matrix.SetScale(-1, 1);
                    break;
                case Android.Media.Orientation.Rotate180:
                    matrix.SetRotate(180);
                    break;
                case Android.Media.Orientation.FlipVertical:
                    matrix.SetRotate(180);
                    matrix.PostScale(-1, 1);
                    break;
                case Android.Media.Orientation.Transpose:
                    matrix.SetRotate(90);
                    matrix.PostScale(-1, 1);
                    break;
                case Android.Media.Orientation.Rotate90:
                    matrix.SetRotate(90);
                    break;
                case Android.Media.Orientation.Transverse:
                    matrix.SetRotate(-90);
                    matrix.PostScale(-1, 1);
                    break;
                case Android.Media.Orientation.Rotate270:
                    matrix.SetRotate(-90);
                    break;
                default:
                    return bitmap;
            }
            try
            {
                Bitmap bmRotated = Bitmap.CreateBitmap(bitmap, 0, 0, bitmap.Width, bitmap.Height, matrix, true);
                bitmap.Recycle();
                return bmRotated;
            }
            catch (Java.Lang.OutOfMemoryError e)
            {
                return null;
            }
            catch (System.Exception ex)
            {
                //Crashes.TrackError(ex);
                return null;
            }
        }

        private void IvProfilePic_Click(object sender, EventArgs e)
        {
            AllowMultiplePhotos = false;
            ShowCameraPicker();
        }

        private async void Field_Click1(object sender, EventArgs e)
        {
            var countryPicker = await UserDialogs.Instance.ActionSheetAsync("Choose your Province", GetText(Resource.String.Cancel), null, null, listOfProvince.ToArray());
            if (countryPicker == "Cancel")
                return;
            etProvince.Field.Text = countryPicker;
        }

        void InitUser()
        {
            etFirstName.Field.Text = presenter.userService.CurrentUser.firstName;
            etLastName.Field.Text = presenter.userService.CurrentUser.lastName;
            etCity.Field.Text = presenter.userService.CurrentUser.city;
            etProvince.Field.Text = presenter.userService.CurrentUser.province;
            etBarangay.Field.Text = presenter.userService.CurrentUser.barangay;
            etHouseNumber.Field.Text = presenter.userService.CurrentUser.location;

            if (!string.IsNullOrEmpty(presenter.userService.CurrentUser.imageUrl))
                Picasso.Get().Load(URLPath.BaseURL + presenter.userService.CurrentUser.imageUrl).Placeholder(Resource.Mipmap.user_placeholder).Into(ivProfilePic);
        }

        private async void Field_Click(object sender, EventArgs e)
        {
            listOfCity = new List<string>();
            String[] rl = this.Resources.GetStringArray(Resource.Array.ZipCodes);
            for (int i = 0; i < rl.Length; i++)
            {
                String[] g = rl[i].Split(",");
                if (etProvince.Field.Text != "")
                {
                    if (g[1] == etProvince.Field.Text)
                    {
                        if (!listOfCity.Contains(g[2]))
                            listOfCity.Add(g[2]);
                    }

                }
                else
                {
                    if (!listOfCity.Contains(g[2]))
                        listOfCity.Add(g[2]);
                }
            }

            listOfCity.Sort();
            var countryPicker = await UserDialogs.Instance.ActionSheetAsync("Choose your City", GetText(Resource.String.Cancel), null, null, listOfCity.ToArray());
            if (countryPicker == "Cancel")
                return;
            etCity.Field.Text = countryPicker;
            for (int i = 0; i < rl.Length; i++)
            {
                String[] g = rl[i].Split(",");
                if (countryPicker == g[2])
                {
                    etProvince.Field.Text = g[1];

                }
            }
        }

        public List<string> listOfCity;
        public List<string> listOfProvince;

        private void GetCityList()
        {
            listOfCity = new List<string>();
            listOfProvince = new List<string>();
            String[] rl = this.Resources.GetStringArray(Resource.Array.ZipCodes);
            for (int i = 0; i < rl.Length; i++)
            {
                String[] g = rl[i].Split(",");
                if (!listOfCity.Contains(g[2]))
                    listOfCity.Add(g[2]);

                if (!listOfProvince.Contains(g[1]))
                    listOfProvince.Add(g[1]);
            }
            listOfCity.Sort();
        }


        private async void IvCheck_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(etFirstName.Field.Text))
            {
                UserDialogs.Instance.Alert("First name is required.");
                return;
            }
            else if (string.IsNullOrWhiteSpace(etLastName.Field.Text))
            {
                UserDialogs.Instance.Alert("Last name is required.");
                return;
            }
            else if (string.IsNullOrWhiteSpace(etCity.Field.Text))
            {
                UserDialogs.Instance.Alert("City is required.");
                return;
            }
            else if (string.IsNullOrWhiteSpace(etProvince.Field.Text))
            {
                UserDialogs.Instance.Alert("Province is required.");
                return;
            }

            UserDialogs.Instance.ShowLoading();
            var response = await presenter.UpdateProfile(new MobileNetStandardLibrary.Commands.UpdateUserCommand
            {
                firstName = etFirstName.Field.Text.Trim(),
                lastName = etLastName.Field.Text.Trim(),
                location = etHouseNumber.Field.Text.Trim(),
                city = etCity.Field.Text.Trim(),
                barangay = etBarangay.Field.Text.Trim(),
                province = etProvince.Field.Text.Trim()
            });
            UserDialogs.Instance.HideLoading();
            if (response.Success)
            {
                if (Intent.HasExtra("FromRegistration"))
                {
                    Intent intent = new Intent(this, typeof(DashboardActivity));
                    intent.SetFlags(ActivityFlags.ClearTop | ActivityFlags.ClearTask | ActivityFlags.NewTask);
                    StartActivity(intent);
                }
                else
                {
                    UserDialogs.Instance.Alert("Profile Saved!");
                    if (DashboardActivity.Dashboard != null)
                    {
                        DashboardActivity.Dashboard.UpdateNameAndPhoto();
                    }
                }
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}

