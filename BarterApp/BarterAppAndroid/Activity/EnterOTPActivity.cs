﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Preferences;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Views.Animations;
using Android.Views.InputMethods;
using Android.Widget;
using Unity;
using Xamarin.Essentials;
using static Android.Resource;

namespace BarterAppAndroid.Activity
{
    [Activity(Label = "EnterOTPActivity", Theme = "@style/AppTheme.NoActionBar", ScreenOrientation = ScreenOrientation.Portrait)]
    public class EnterOTPActivity : BaseActivity
    {
        private TextView tvMessage;
        private TextView tvTimer;
        private EditText tfInvi;
        private TextView tf1;
        private TextView tf2;
        private TextView tf3;
        private TextView tf4;
        private TextView tf5;
        private TextView tf6;
        private TextView tvClose;
        private LinearLayout llPins;
        private Button btnResend;

        public string confirmText { get; set; }
        public string firstText { get; set; }

        public bool SuccessPin;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.EnterOTPActivity);
            confirmText = "00000000000";

            RequestOTP();
            tvMessage = FindViewById<TextView>(Resource.Id.tvMessage);
            tvTimer = FindViewById<TextView>(Resource.Id.tvTimer);
            tfInvi = FindViewById<EditText>(Resource.Id.etInvi);
            tvMessage.Text = string.Format(GetText(Resource.String.EnterOTPWeSent), "+" + Intent.GetStringExtra("user"));
            tvClose = FindViewById<TextView>(Resource.Id.tvClose);
            tvClose.Click += TvClose_Click;
            tvTimer.Text = "02:00";
            // Create your application here
            btnResend = FindViewById<Button>(Resource.Id.btnResend);
            btnResend.Enabled = false;
            llPins = FindViewById<LinearLayout>(Resource.Id.llPins);
            tf1 = FindViewById<TextView>(Resource.Id.tv1);
            tf2 = FindViewById<TextView>(Resource.Id.tv2);
            tf3 = FindViewById<TextView>(Resource.Id.tv3);
            tf4 = FindViewById<TextView>(Resource.Id.tv4);
            tf5 = FindViewById<TextView>(Resource.Id.tv5);
            tf6 = FindViewById<TextView>(Resource.Id.tv6);

            FocusText();
            TickTime();
            tfInvi.TextChanged += TfInvi_TextChanged;

            tf1.Click += Tf1_Click;
            tf2.Click += Tf1_Click;
            tf3.Click += Tf1_Click;
            tf4.Click += Tf1_Click;
            tf5.Click += Tf1_Click;
            tf6.Click += Tf1_Click;
            btnResend.Click += BtnResend_Click;
            confirmText = Intent.GetStringExtra("otp");
        }

        private async void TfInvi_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            Android.Views.Animations.Animation shake = AnimationUtils.LoadAnimation(ApplicationContext, Resource.Animation.shake_anim);

            if (tfInvi.Text.Length > 6)
                return;

            tf1.Text = tfInvi.Text.Length >= 1 ? tfInvi.Text.Substring(0, 1) : "";
            tf2.Text = tfInvi.Text.Length >= 2 ? tfInvi.Text.Substring(1, 1) : "";
            tf3.Text = tfInvi.Text.Length >= 3 ? tfInvi.Text.Substring(2, 1) : "";
            tf4.Text = tfInvi.Text.Length >= 4 ? tfInvi.Text.Substring(3, 1) : "";
            tf5.Text = tfInvi.Text.Length >= 5 ? tfInvi.Text.Substring(4, 1) : "";
            tf6.Text = tfInvi.Text.Length >= 6 ? tfInvi.Text.Substring(5, 1) : "";

            //if (string.IsNullOrEmpty(confirmText))
            //{
            //    confirmText = "123456";
            //}

            if (tfInvi.Text.Length == 6)
            {
                if (Intent.HasExtra("ResetPassword"))
                {
                    if (tfInvi.Text == confirmText)
                    {
                        Finish();
                        Intent intent = new Intent(this, typeof(ChangePasswordActivity));
                        intent.PutExtra("MobileNumber", Intent.GetStringExtra("MobileNumber"));
                        intent.PutExtra("otp", confirmText);
                        StartActivity(intent);
                    }
                }
                else
                {
                    UserDialogs.Instance.ShowLoading();
                    var response = await presenter.ActivateUser(new MobileNetStandardLibrary.Commands.ActivateAccountCommand
                    {
                        key = tfInvi.Text.Trim(),
                        mobileNo = Intent.GetStringExtra("user").Replace("+", "")
                    });
                    UserDialogs.Instance.HideLoading();
                    if (response.Success)
                    {
                        ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(ApplicationContext);
                        var token = prefs.GetString("RegistrationToken", "");
                        if (!string.IsNullOrEmpty(token))
                        {
                            presenter.accountService.UpdateFCMToken(token);
                        }
                        Intent intent = new Intent(this, typeof(ProfileActivity));
                        intent.PutExtra("FromRegistration", true);
                        StartActivity(intent);
                        return;
                    }
                    else
                    {
                        llPins.StartAnimation(shake);
                        tvMessage.Text = GetText(Resource.String.WrongCombinationPleaseTryAgain);
                        tfInvi.Text = string.Empty;
                    }
                }
                return;
            }
            else
            {
                await Task.Delay(500);
                if (tfInvi.Text.Length >= 6)
                {
                    llPins.StartAnimation(shake);
                    tvMessage.Text = GetText(Resource.String.WrongCombinationPleaseTryAgain);
                    tfInvi.Text = string.Empty;
                }
            }
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (requestCode == 8923)
            {
                SetResult(Result.Ok);
                Finish();
            }
        }

        private void TvClose_Click(object sender, EventArgs e)
        {
            Finish();
        }

        async Task RequestOTP()
        {
            //var response = await controller.RequestOTP(Intent.GetStringExtra("user"), Intent.GetBooleanExtra("IsMobile", false));

            //if (response.Success)
            //{
            //    confirmText = response.Data;
            //}
            confirmText = Intent.GetStringExtra("OTPValue");
            //confirmText = "123456";
        }

        async void BtnResend_Click(object sender, EventArgs e)
        {
            UserDialogs.Instance.ShowLoading();
            tvMessage.Text = string.Format(GetText(Resource.String.EnterOTPWeSent), Intent.GetStringExtra("user"));
            if (Intent.HasExtra("ResetPassword"))
            {
                var mobileNo = presenter.userService.CurrentUser != null ? presenter.userService.CurrentUser.mobileNo : Intent.GetStringExtra("MobileNumber");
                var response = await presenter.ResetPasswordInit(mobileNo);
                if (response.Success)
                {
                    //confirmText = response.Data.Pin;
                    confirmText = "123456";
                    tvTimer.Text = "02:00";
                    btnResend.Enabled = false;
                    seconds = 120;
                    TickTime();
                }
            }
            else
            {
                var response = await presenter.RegisterUser(new AppStandardLibrary.LoginRegistrationCommand { mobileNo = Intent.GetStringExtra("user"), password = Intent.GetStringExtra("password") });
                if (response.Success)
                {
                    confirmText = "123456";
                    tvTimer.Text = "02:00";
                    btnResend.Enabled = false;
                    seconds = 120;
                    TickTime();
                }
            }

            tfInvi.Text = string.Empty;
            tfInvi.TextChanged += TfInvi_TextChanged;
            UserDialogs.Instance.HideLoading();
            FocusText();
        }


        void Tf1_Click(object sender, EventArgs e)
        {
            FocusText();
        }

        async Task FocusText()
        {
            await Task.Delay(500);

            tfInvi.RequestFocus();

            InputMethodManager imm = (InputMethodManager)GetSystemService(Context.InputMethodService);
            imm.ShowSoftInput(tfInvi, ShowFlags.Implicit);
        }

        long seconds = 120;

        async Task TickTime()
        {
            while (seconds != 0)
            {
                await Task.Delay(1000);
                seconds--;
                tvTimer.Text = TimeSpan.FromSeconds(seconds).ToString(@"mm\:ss");
            }

            if (seconds == 0)
            {
                confirmText = "1234567890";
                tvMessage.Text = GetText(Resource.String.OopsLooksLikeYourOTPExpired);
                tvTimer.Text = string.Empty;
                btnResend.Enabled = true;
                tf1.Text = string.Empty;
                tf2.Text = string.Empty;
                tf3.Text = string.Empty;
                tf4.Text = string.Empty;
                tf5.Text = string.Empty;
                tf6.Text = string.Empty;
                tfInvi.Text = string.Empty;
                tfInvi.TextChanged -= TfInvi_TextChanged;

            }
        }
    }
}
