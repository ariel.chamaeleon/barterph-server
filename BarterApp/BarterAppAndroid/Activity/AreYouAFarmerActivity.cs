﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace BarterAppAndroid.Activity
{
    [Activity(Label = "AreYouAFarmerActivity", Theme = "@style/AppTheme.NoActionBar", ScreenOrientation = ScreenOrientation.Portrait)]
    public class AreYouAFarmerActivity : BaseActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.are_you_a_farmer_activity);

            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            var actionBar = SupportActionBar;
            actionBar.SetHomeButtonEnabled(true);
            actionBar.SetDisplayHomeAsUpEnabled(true);
            actionBar.Title = "Onboarding Questions";
            var ivCheck = toolbar.FindViewById<ImageView>(Resource.Id.ivCheck);
            ivCheck.Visibility = ViewStates.Visible;
            ivCheck.Click += IvCheck_Click;
        }

        private void IvCheck_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(DashboardActivity));
            StartActivity(intent);
        }
    }
}
