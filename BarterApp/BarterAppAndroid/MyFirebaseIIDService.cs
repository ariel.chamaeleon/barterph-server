﻿using System;
using Android.App;
using Firebase.Iid;
using Android.Util;
using MobileNetStandardLibrary.Controller;
using static BarterAppAndroid.MainActivity;
using Android.Content;
using Android.Preferences;
using Unity;

namespace FCMClient
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    public class MyFirebaseIIDService : FirebaseInstanceIdService
    {
        const string TAG = "MyFirebaseIIDService";
        public override void OnTokenRefresh()
        {
            var refreshedToken = FirebaseInstanceId.Instance.Token;
            Log.Debug(TAG, "Refreshed token: " + refreshedToken);
            SendRegistrationToServer(refreshedToken);
        }
        async void SendRegistrationToServer(string token)
        {
            var controller = Resolver.Container.Resolve(typeof(BaseViewPresenter)) as BaseViewPresenter;
            if (controller != null)
            {
                controller.accountService.UpdateFCMToken(token);
            }
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(ApplicationContext);
            ISharedPreferencesEditor editor = prefs.Edit();
            editor.PutString("RegistrationToken", token);
            editor.Apply();
        }
    }
}