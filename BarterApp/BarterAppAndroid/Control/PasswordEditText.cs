﻿using System;
using Android.Content;
using Android.Runtime;
using Android.Support.V4.Content.Res;
using Android.Util;
using Android.Views;
using Android.Widget;
using BarterAppAndroid.Droid;

namespace BarterAppAndroid.Controls
{
    public class PasswordEditText : RelativeLayout
    {
        public PasswordEditText(Context context) :
           base(context)
        {
            Initialize(context);
        }

        public PasswordEditText(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            Initialize(context);
        }

        public PasswordEditText(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            Initialize(context);
        }
        RelativeLayout.LayoutParams paramsLine;
        public TextView line { get; set; }
        public EditText Field { get; set; }
        public bool IsConfirmPassword { get; set; }
        public string FromPassword { get; set; }
        public PasswordEditText FromPasswordField { get; set; }
        public TextView floatingLabel { get; set; }
        public bool RemoveValidation { get; set; }

        bool isPasswordShown;

        public void ShowPasswordDontMatch(bool show) {
            if (show)
            {
                line.Text = Context.GetText(Resource.String.PasswordDidntMatch);
                line.SetTextColor(Android.Graphics.Color.Red);
                paramsLine.Height = CommonAndroidHelper.ConvertDPtoPX(CommonAndroidHelper.ConvertDPtoPX(11, Context), Context);
                line.SetPadding(10, 0, 0, 0);
                paramsLine.AddRule(LayoutRules.AlignParentBottom);
                paramsLine.AddRule(LayoutRules.Below, Field.Id);
                line.LayoutParameters = paramsLine;
                line.Invalidate();
            }
            else {
                paramsLine.Height = CommonAndroidHelper.ConvertDPtoPX(1, Context);
                line.LayoutParameters = paramsLine;
                line.Invalidate();
            }
        }

        void Initialize(Context context)
        {

            var floatingLabelParams = new RelativeLayout.LayoutParams(LayoutParams.WrapContent, LayoutParams.MatchParent);
            floatingLabelParams.LeftMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);
            floatingLabelParams.RightMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);
            floatingLabelParams.BottomMargin = CommonAndroidHelper.ConvertDPtoPX(2, Context);

            floatingLabel = new TextView(Context);
            floatingLabel.TextSize = CommonAndroidHelper.ConvertDPtoPX(4, Context);
            floatingLabel.Gravity = GravityFlags.CenterVertical;
            floatingLabel.Text = "Password";
            floatingLabel.SetTextColor(Android.Graphics.Color.Black);
            floatingLabel.LayoutParameters = floatingLabelParams;
            floatingLabel.Id = View.GenerateViewId();
            floatingLabel.Visibility = ViewStates.Invisible;
            this.AddView(floatingLabel);

            var relativeLayout = new RelativeLayout(Context);
            relativeLayout.SetBackgroundResource(Resource.Drawable.field_style);
            var relativeLayoutParams = new RelativeLayout.LayoutParams(LayoutParams.WrapContent, LayoutParams.MatchParent);
            relativeLayoutParams.AddRule(LayoutRules.Below, floatingLabel.Id);
            relativeLayout.LayoutParameters = relativeLayoutParams;
            this.AddView(relativeLayout);

            var imageView = new ImageView(Context);
            var imageViewParams = new RelativeLayout.LayoutParams(CommonAndroidHelper.ConvertDPtoPX(30, Context), CommonAndroidHelper.ConvertDPtoPX(30, Context));
            imageViewParams.AddRule(LayoutRules.AlignParentTop);
            imageViewParams.AddRule(LayoutRules.AlignParentRight);
            imageViewParams.TopMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);
            imageViewParams.RightMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);
            imageView.LayoutParameters = imageViewParams;
            imageView.SetImageResource(Resource.Mipmap.visibility_off);
            imageView.Visibility = ViewStates.Invisible;
            imageView.Click += (sender, e) => {

                if (!isPasswordShown) 
                {
                    Field.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextVariationNormal;
                    imageView.SetImageResource(Resource.Mipmap.visibility);
                }
                else
                {
                    Field.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextVariationPassword;
                    imageView.SetImageResource(Resource.Mipmap.visibility_off);
                }
                isPasswordShown = !isPasswordShown;
                Field.SetSelection(Field.Text.Length);
            };

            Field = new EditText(Context);

            Field.SetFilters(new global::Android.Text.IInputFilter[] { new Android.Text.InputFilterLengthFilter(40) });
            var etParams = new RelativeLayout.LayoutParams(LayoutParams.MatchParent, LayoutParams.MatchParent);
            etParams.LeftMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);
            etParams.RightMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);
     
            Field.LayoutParameters = etParams;
            Field.Hint = "Password";
            Field.SetHintTextColor(Android.Graphics.Color.LightGray);
            Field.SetTextColor(Android.Graphics.Color.Black);
            Field.SetBackgroundColor(Android.Graphics.Color.Transparent);
            Field.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextVariationPassword;
            Field.Id = View.GenerateViewId();
            Field.FocusChange += (sender, e) =>
            {
                if (RemoveValidation)
                {
                    return;
                }

                paramsLine.Height = CommonAndroidHelper.ConvertDPtoPX(e.HasFocus ? 3 : 1, Context);
                line.LayoutParameters = paramsLine;
                line.Invalidate();

                if (!e.HasFocus && Field.Text.Length > 0)
                {
                    if (!IsConfirmPassword)
                    {
                        if (Field.Text.Length < 8 || !CommonAndroidHelper.CheckPasswordIfAlphaNumeric(Field.Text))
                        {
                            line.Text = "Invalid Password";
                            paramsLine.Height = CommonAndroidHelper.ConvertDPtoPX(CommonAndroidHelper.ConvertDPtoPX(11, Context), Context);
                            line.SetPadding(10, 0, 0, 0);
                            paramsLine.AddRule(LayoutRules.AlignParentBottom);
                            line.SetTextColor(Android.Graphics.Color.Red);
                            paramsLine.AddRule(LayoutRules.Below, Field.Id);
                            line.LayoutParameters = paramsLine;
                            line.Invalidate();
                        }
                        else
                        {
                            paramsLine.Height = CommonAndroidHelper.ConvertDPtoPX(1, Context);
                            line.LayoutParameters = paramsLine;
                            line.Invalidate();
                        }


                    }
                    else
                    {
                        paramsLine.Height = CommonAndroidHelper.ConvertDPtoPX(1, Context);
                        line.LayoutParameters = paramsLine;
                        line.Invalidate();
                   
                    }
                }
            };

            Field.TextChanged += (sender, e) => {
                floatingLabel.Visibility = (Field.Text.Length > 0) ? ViewStates.Visible : ViewStates.Invisible;
                imageView.Visibility = (Field.Text.Length > 0) ? ViewStates.Visible : ViewStates.Invisible;
                if (!IsConfirmPassword)
                {
                    if (Field.Text.Length < 8 || !CommonAndroidHelper.CheckPasswordIfAlphaNumeric(Field.Text))
                    {
                        if (FromPassword != null)
                        {
                            line.Text = "Invalid Password";
                            paramsLine.Height = CommonAndroidHelper.ConvertDPtoPX(CommonAndroidHelper.ConvertDPtoPX(11, Context), Context);
                            line.SetPadding(10, 0, 0, 0);
                            paramsLine.AddRule(LayoutRules.AlignParentBottom);
                            line.SetTextColor(Android.Graphics.Color.Gray);
                            paramsLine.AddRule(LayoutRules.Below, Field.Id);
                            line.LayoutParameters = paramsLine;
                            line.Invalidate();
                        }
                    }
                    else
                    {
                        if (FromPassword != null)
                        {
                            if (Field.Text != FromPassword)
                            {
                                if (Field.Text != FromPassword && FromPasswordField.Field.Text.Length >= 8 && CommonAndroidHelper.CheckPasswordIfAlphaNumeric(Field.Text))
                                {
                                    FromPasswordField.ShowPasswordDontMatch(true);
                                }
                                else
                                {
                                    FromPasswordField.ShowPasswordDontMatch(false);
                                }
                                paramsLine.Height = CommonAndroidHelper.ConvertDPtoPX(1, Context);
                                line.LayoutParameters = paramsLine;
                                line.Invalidate();
                            }
                            else
                            {
                                paramsLine.Height = CommonAndroidHelper.ConvertDPtoPX(1, Context);
                                line.LayoutParameters = paramsLine;
                                line.Invalidate();
                                FromPasswordField.ShowPasswordDontMatch(false);
                            }
                        }
                    }
                  
                }
                if (IsConfirmPassword)
                {
                    if (Field.Text.Length >= 8)
                    {
                        if (Field.Text != FromPassword)
                        {
                            line.Text = Context.GetText(Resource.String.PasswordDidntMatch);
                            line.SetTextColor(Android.Graphics.Color.Red);
                            paramsLine.Height = CommonAndroidHelper.ConvertDPtoPX(CommonAndroidHelper.ConvertDPtoPX(11, Context), Context);
                            line.SetPadding(10, 0, 0, 0);
                            paramsLine.AddRule(LayoutRules.AlignParentBottom);
                            paramsLine.AddRule(LayoutRules.Below, Field.Id);
                            line.LayoutParameters = paramsLine;
                            line.Invalidate();
                        }
                        else
                        {
                            paramsLine.Height = CommonAndroidHelper.ConvertDPtoPX(1, Context);
                            line.LayoutParameters = paramsLine;
                            line.Invalidate();
                        }
                    }
                }

            };

            line = new TextView(Context);
            line.TextSize = CommonAndroidHelper.ConvertDPtoPX(4, Context);
            line.Gravity = GravityFlags.CenterVertical;
            //line.SetBackgroundColor(Android.Graphics.Color.White);
            line.Text = string.Empty;
            
            paramsLine = new RelativeLayout.LayoutParams(LayoutParams.MatchParent, CommonAndroidHelper.ConvertDPtoPX(1, Context));
            paramsLine.AddRule(LayoutRules.AlignParentBottom);
            paramsLine.LeftMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);
            paramsLine.RightMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);
            paramsLine.TopMargin = CommonAndroidHelper.ConvertDPtoPX(-15, Context);
            line.LayoutParameters = paramsLine;

            relativeLayout.AddView(Field);
            relativeLayout.AddView(line);
            relativeLayout.AddView(imageView);
        }
    }
}
