﻿using System;
using Android.Content;
using Android.Runtime;
using Android.Support.V4.Content.Res;
using Android.Util;
using Android.Views;
using Android.Widget;
using BarterAppAndroid.Droid;

namespace BarterAppAndroid.Controls
{
    public class GenericEditText : RelativeLayout
    {
        public GenericEditText(Context context) :
           base(context)
        {
            Initialize(context);
        }

        public GenericEditText(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            Initialize(context);
        }

        public GenericEditText(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            Initialize(context);
        }

        RelativeLayout.LayoutParams paramsLine;
        public TextView line { get; set; }
        public EditText Field { get; set; }
        public bool IsConfirmPassword { get; set; }
        public string FromPassword { get; set; }
        public TextView floatingLabel { get; set; }
        RelativeLayout relativeLayout;
        public ImageView arrowDown { get; set; }

        public void SetFocus(bool hasFocus) {
            paramsLine.Height = CommonAndroidHelper.ConvertDPtoPX(hasFocus ? 3 : 1, Context);
            line.LayoutParameters = paramsLine;
            line.Invalidate();
        }

        public void RenewField(Context context) {

            relativeLayout.RemoveView(Field);
            Field = new EditText(Context);
            Field.SetFilters(new global::Android.Text.IInputFilter[] { new Android.Text.InputFilterLengthFilter(1000) });
            var etParams = new RelativeLayout.LayoutParams(LayoutParams.MatchParent, LayoutParams.MatchParent);
            etParams.LeftMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);
            etParams.RightMargin = CommonAndroidHelper.ConvertDPtoPX(5, Context);
            Field.SetPadding(Field.TotalPaddingLeft, Field.TotalPaddingTop, 55, Field.TotalPaddingBottom);

            Field.LayoutParameters = etParams;
            Field.SetHintTextColor(Android.Graphics.Color.Gray);
            Field.SetTextColor(Android.Graphics.Color.ParseColor("#3d9dfb"));
            Field.SetBackgroundColor(Android.Graphics.Color.Transparent);
            Field.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextFlagCapSentences;
            Field.Id = View.GenerateViewId();
            Field.FocusChange += (sender, e) =>
            {
                paramsLine.Height = CommonAndroidHelper.ConvertDPtoPX(e.HasFocus ? 3 : 1, Context);
                line.LayoutParameters = paramsLine;
                line.Invalidate();

            };

            Field.TextChanged += (sender, e) => {
                floatingLabel.Visibility = (Field.Text.Length > 0) ? ViewStates.Visible : ViewStates.Invisible;
            };
            relativeLayout.AddView(Field);
        }

        void Initialize(Context context)
        {
            var floatingLabelParams = new RelativeLayout.LayoutParams(LayoutParams.WrapContent, LayoutParams.MatchParent);
            floatingLabelParams.LeftMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);
            floatingLabelParams.RightMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);
            floatingLabelParams.BottomMargin = CommonAndroidHelper.ConvertDPtoPX(2, Context);

            floatingLabel = new TextView(Context);
            floatingLabel.TextSize = CommonAndroidHelper.ConvertDPtoPX(5, Context);
            floatingLabel.Gravity = GravityFlags.CenterVertical;
            floatingLabel.SetTextColor(Android.Graphics.Color.Black);
            floatingLabel.LayoutParameters = floatingLabelParams;
            floatingLabel.Id = View.GenerateViewId();
            floatingLabel.Visibility = ViewStates.Invisible;
            this.AddView(floatingLabel);

            relativeLayout = new RelativeLayout(Context);
            relativeLayout.SetBackgroundResource(BarterAppAndroid.Resource.Drawable.field_style);
            var relativeLayoutParams = new RelativeLayout.LayoutParams(LayoutParams.WrapContent, LayoutParams.MatchParent);
            relativeLayoutParams.AddRule(LayoutRules.Below, floatingLabel.Id);
            relativeLayout.LayoutParameters = relativeLayoutParams;
            this.AddView(relativeLayout);


            Field = new EditText(Context);
            Field.SetFilters(new global::Android.Text.IInputFilter[] { new Android.Text.InputFilterLengthFilter(1000) });
            var etParams = new RelativeLayout.LayoutParams(LayoutParams.MatchParent, LayoutParams.MatchParent);
            etParams.LeftMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);
            etParams.RightMargin = CommonAndroidHelper.ConvertDPtoPX(5, Context);
            Field.SetPadding(Field.TotalPaddingLeft, Field.TotalPaddingTop, 55, Field.TotalPaddingBottom);

            Field.LayoutParameters = etParams;
            Field.SetHintTextColor(Android.Graphics.Color.Gray);
            Field.SetTextColor(Android.Graphics.Color.Black);
            Field.SetBackgroundColor(Android.Graphics.Color.Transparent);
            Field.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextFlagCapSentences;
            Field.Id = View.GenerateViewId();
            Field.FocusChange += (sender, e) =>
            {
                paramsLine.Height = CommonAndroidHelper.ConvertDPtoPX(e.HasFocus ? 3 : 1, Context);
                line.LayoutParameters = paramsLine;
                line.Invalidate();

            };

            Field.TextChanged += (sender, e) => {
                floatingLabel.Visibility = (Field.Text.Length > 0) ? ViewStates.Visible : ViewStates.Invisible;
            };

            arrowDown = new ImageView(Context);
            arrowDown.Visibility = ViewStates.Gone;
            RelativeLayout.LayoutParams arrowParams = new RelativeLayout.LayoutParams(40, 40);
            arrowParams.AddRule(LayoutRules.AlignParentRight);
            arrowParams.AddRule(LayoutRules.CenterVertical);
            arrowDown.SetColorFilter(Android.Graphics.Color.ParseColor("#a9a9a9"));
            arrowParams.RightMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);
            arrowDown.LayoutParameters = arrowParams;
            arrowDown.SetImageResource(Resource.Mipmap.arrow_down);

            line = new TextView(Context);
            line.TextSize = CommonAndroidHelper.ConvertDPtoPX(4, Context);
            line.Gravity = GravityFlags.CenterVertical;
            line.SetBackgroundColor(Android.Graphics.Color.ParseColor("#3d9dfb"));
            line.Text = string.Empty;
            line.SetTextColor(Android.Graphics.Color.Red);
            paramsLine = new RelativeLayout.LayoutParams(LayoutParams.MatchParent, CommonAndroidHelper.ConvertDPtoPX(1, Context));
            paramsLine.AddRule(LayoutRules.AlignParentBottom);
            line.LayoutParameters = paramsLine;

            relativeLayout.AddView(Field);
            relativeLayout.AddView(arrowDown);
            //relativeLayout.AddView(line);
        }
    }
}

