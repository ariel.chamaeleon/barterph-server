﻿using System;
using System;
using Android.Content;
using Android.Runtime;
using Android.Support.V4.Content.Res;
using Android.Telephony;
using Android.Text;
using Android.Util;
using Android.Views;
using Android.Widget;
using BarterAppAndroid.Droid;

namespace BarterAppAndroid.Controls
{
    public class MobileNumberText : RelativeLayout
    {
        public MobileNumberText(Context context) :
           base(context)
        {
            Initialize(context);
        }

        public MobileNumberText(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            Initialize(context);
        }

        public MobileNumberText(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            Initialize(context);
        }
        RelativeLayout.LayoutParams paramsLine;
        TextView line;
        public TextView FloatingLabel { get; set; }
        public EditText Field { get; set; }
        public EditText CountrySim { get; set; }


        void Initialize(Context context)
        {
            //this.SetBackgroundResource(Resource.Drawable.email_field_bg);

            var floatingLabelParams = new RelativeLayout.LayoutParams(LayoutParams.WrapContent, LayoutParams.MatchParent);
            floatingLabelParams.LeftMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);
            floatingLabelParams.RightMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);
            floatingLabelParams.BottomMargin = CommonAndroidHelper.ConvertDPtoPX(2, Context);

            FloatingLabel = new TextView(Context);
            FloatingLabel.TextSize = CommonAndroidHelper.ConvertDPtoPX(5, Context);
            FloatingLabel.Gravity = GravityFlags.CenterVertical;
            FloatingLabel.Text = "Mobile Number";
            FloatingLabel.SetTextColor(Android.Graphics.Color.Black);
            FloatingLabel.LayoutParameters = floatingLabelParams;
            FloatingLabel.Id = View.GenerateViewId();
            FloatingLabel.Visibility = ViewStates.Invisible;
            this.AddView(FloatingLabel);

            var relativeLayout = new RelativeLayout(Context);
            relativeLayout.SetBackgroundResource(Resource.Drawable.field_style);
            var relativeLayoutParams = new RelativeLayout.LayoutParams(LayoutParams.WrapContent, LayoutParams.MatchParent);
            relativeLayoutParams.AddRule(LayoutRules.Below, FloatingLabel.Id);
            relativeLayout.LayoutParameters = relativeLayoutParams;
            this.AddView(relativeLayout);

            //TelephonyManager mTelephonyMgr;
            //mTelephonyMgr = (TelephonyManager)Context.GetSystemService(Context.TelephonyService);
            //var simIso = mTelephonyMgr.SimCountryIso.ToUpper();
            var CountryZipCode = string.Empty;
            //String[] rl = this.Resources.GetStringArray(Resource.Array.CountryCodes);
            //for (int i = 0; i < rl.Length; i++)
            //{
            //    String[] g = rl[i].Split(",");
            //    if (g[1].Trim().Equals(simIso.Trim()))
            //    {
            //        CountryZipCode = g[0];
            //        break;
            //    }
            //}
            CountryZipCode = "63";
            Field = new EditText(Context);
            var etParams = new RelativeLayout.LayoutParams(LayoutParams.MatchParent, LayoutParams.MatchParent);
            etParams.LeftMargin = CommonAndroidHelper.ConvertDPtoPX(25 * CountryZipCode.Length, Context);
            etParams.RightMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);

            var etMobileParams = new RelativeLayout.LayoutParams(CommonAndroidHelper.ConvertDPtoPX(25 * CountryZipCode.Length, Context), LayoutParams.MatchParent);
            etMobileParams.LeftMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);
            etMobileParams.RightMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);
        

            CountrySim = new EditText(Context);
            CountrySim.LayoutParameters = etMobileParams;
            CountrySim.SetTextColor(Android.Graphics.Color.Black);
            CountrySim.SetBackgroundColor(Android.Graphics.Color.Transparent);
            CountrySim.InputType = Android.Text.InputTypes.ClassPhone | Android.Text.InputTypes.TextVariationPhonetic;
            CountrySim.Text = $"+{CountryZipCode}";
            CountrySim.Enabled = false;
            CountrySim.Id = View.GenerateViewId();

            Field.SetFilters(new IInputFilter[] { new InputFilterLengthFilter(10) });
            Field.LayoutParameters = etParams;
            Field.Hint = "Mobile Number";
            Field.SetHintTextColor(Android.Graphics.Color.LightGray);
            Field.SetBackgroundColor(Android.Graphics.Color.Transparent);
            Field.InputType = Android.Text.InputTypes.ClassPhone | Android.Text.InputTypes.TextVariationPhonetic;
            Field.Id = View.GenerateViewId();
            Field.FocusChange += (sender, e) =>
            {
                paramsLine.Height = CommonAndroidHelper.ConvertDPtoPX(e.HasFocus ? 3 : 1, Context);
                line.LayoutParameters = paramsLine;
                line.Invalidate();

                if (!e.HasFocus && Field.Text.Length > 0)
                {
                    if (Field.Text.Length < 7)
                    {
                        line.Text = "Invalid Mobile Number";
                        paramsLine.Height = CommonAndroidHelper.ConvertDPtoPX(CommonAndroidHelper.ConvertDPtoPX(11, Context), Context);
                        line.SetPadding(10, 0, 0, 0);
                        paramsLine.AddRule(LayoutRules.AlignParentBottom);
                        paramsLine.AddRule(LayoutRules.Below, Field.Id);
                        line.LayoutParameters = paramsLine;
                        line.Invalidate();
                    }
                    else
                    {
                        paramsLine.Height = CommonAndroidHelper.ConvertDPtoPX(1, Context);
                        line.LayoutParameters = paramsLine;
                        line.Invalidate();
                    }
                }
            };

            Field.TextChanged += (sender, e) =>
            {
                FloatingLabel.Visibility = (Field.Text.Length > 0) ? ViewStates.Visible : ViewStates.Invisible;
            };

            line = new TextView(Context);
            line.TextSize = CommonAndroidHelper.ConvertDPtoPX(4, Context);
            line.Gravity = GravityFlags.CenterVertical;
            line.Text = string.Empty;
            line.SetTextColor(Android.Graphics.Color.Red);
            paramsLine = new RelativeLayout.LayoutParams(LayoutParams.MatchParent, CommonAndroidHelper.ConvertDPtoPX(1, Context));
            paramsLine.AddRule(LayoutRules.AlignParentBottom);
            paramsLine.LeftMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);
            paramsLine.RightMargin = CommonAndroidHelper.ConvertDPtoPX(10, Context);
            paramsLine.TopMargin = CommonAndroidHelper.ConvertDPtoPX(-15, Context);
            line.LayoutParameters = paramsLine;

            relativeLayout.AddView(Field);
            relativeLayout.AddView(line);
            relativeLayout.AddView(CountrySim);
        }
    }
}

