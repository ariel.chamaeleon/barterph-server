<p>Thank you for downloading the Barter PH app, the first official barter mobile application in the Philippines!</p>

<p>Barter PH app aims to provide everyone a free service platform for effective trading of goods between two parties without using any money in the transaction.</p>

<p>We want to help you find the best barter item and fulfill your needs without spending money!</p>

<p>This beta version contains only the lightweight features that we believe can help you make an easy, secure and free barter service. Should you have any suggestions, concerns or technical issues with our Barter PH app, we would love to hear it from you! Connect with us in Facebook.</p>

<p>We sincerely hope you would fully enjoy our Barter PH app, and give us a good rating in the PlayStore.</p>

<p>Your good rating will keep us inspired in maintaining this app with optimized features. Please rate us soon in the Playstore. :)</p>

<p>Happy and safe bartering!</p>

Your Barter PH app family