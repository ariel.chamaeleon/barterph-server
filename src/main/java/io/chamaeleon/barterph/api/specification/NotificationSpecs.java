package io.chamaeleon.barterph.api.specification;

import io.chamaeleon.barterph.api.domain.*;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

public class NotificationSpecs {

  public static Specification<Notification> isReceiverEqualTo(final Long receiverId){
    return (Specification<Notification>) (root, cq, cb) -> {
      if(receiverId == null) return null;
      return cb.equal(root.get(Notification_.RECEIVER).get(User_.ID), receiverId);
    };
  }

  public static Specification<Notification> isSenderEqualTo(final Long senderId){
    return (Specification<Notification>) (root, cq, cb) -> {
      if(senderId == null) return null;
      return cb.equal(root.get(Notification_.SENDER).get(User_.ID), senderId);
    };
  }

  public static Sort buildSortObject(Integer sort){
    Sort sortObject = Sort.by(Sort.Order.asc(Notification_.ID));
    if(sort == null){
    } else if(sort == 1){
      sortObject = Sort.by(Sort.Order.asc(Notification_.DATE_POSTED));
    } else if(sort == 2) {
      sortObject = Sort.by(Sort.Order.desc(Notification_.DATE_POSTED));
    }
    return sortObject;
  }
}
