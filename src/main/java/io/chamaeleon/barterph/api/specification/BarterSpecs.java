package io.chamaeleon.barterph.api.specification;

import io.chamaeleon.barterph.api.domain.*;
import io.chamaeleon.barterph.api.util.BarterStatus;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.time.*;
import java.util.*;

public class BarterSpecs {

  public static Specification<Barter> isStatusEqualTo(final BarterStatus[] status) {
    return (Specification<Barter>) (root, cq, cb) -> {
      if(status == null) return null;
      return root.get(Barter_.STATUS).in(status);
    };
  }

  public static Specification<Barter> isUserEqualTo(final Long userId){
    return (Specification<Barter>) (root, cq, cb) -> {
      if(userId == null) return null;
      return cb.equal(root.get(Barter_.USER).get(User_.ID), userId);
    };
  }

  public static Specification<Barter> searchKey(final String key){
    return (Specification<Barter>) (root, cq, cb) -> {
      if(StringUtils.isBlank(key)) return null;
      return cb.or(
          cb.like(root.get(Barter_.ITEM), "%"+key+"%"),
          cb.like(root.get(Barter_.LOCATION).get(Location_.CITY), "%"+key+"%"),
          cb.like(root.get(Barter_.LOCATION).get(Location_.PROVINCE), "%"+key+"%")
        );
    };
  }

  public static Specification<Barter> reminderDateToday(){
    return (Specification<Barter>) (root, cq, cb) -> cb.and(
        cb.greaterThanOrEqualTo(root.get(Barter_.NEXT_REMIND_DATE),  LocalDate.now().atTime(0, 0,0)),
        cb.lessThanOrEqualTo(root.get(Barter_.NEXT_REMIND_DATE),LocalDate.now().atTime(23, 59,59))
    );
  }

  public static Specification<Barter> orderBy(final Integer sort){
    return (Specification<Barter>) (root, cq, cb) -> {

      List<Order> orders = new ArrayList<>();
      if(Objects.equals(sort,1)){
        orders.add(cb.asc(cb.coalesce(root.get(Barter_.DATE_REPOSTED), root.get(Barter_.DATE_POSTED))));
      } else if(Objects.equals(sort,2)){
        orders.add(cb.desc(cb.coalesce(root.get(Barter_.DATE_REPOSTED), root.get(Barter_.DATE_POSTED))));
      } else if(Objects.equals(sort,3)){
        orders.add(cb.asc(root.get(Barter_.VIEW_COUNT)));
      } else if(Objects.equals(sort,4)){
        orders.add(cb.desc(root.get(Barter_.VIEW_COUNT)));
      }

      cq.orderBy(orders);

      Specification<Barter> emptySpec = Specification.where(null);
      return emptySpec.toPredicate(root, cq, cb);
    };
  }

  public static Specification<Barter> withComments(final Boolean withComments){
    return (Specification<Barter>) (root, cq, cb) -> {
      if(withComments == null) return null;
      Subquery<Long> subquery = cq.subquery(Long.class);
      Root<BarterComment> subroot = subquery.from(BarterComment.class);
      Predicate isBarterIdEqual = cb.equal(subroot.get(BarterComment_.BARTER).get(Barter_.ID), root.get("id"));
      Expression ex = subquery.select(cb.count(subroot.get(BarterComment_.ID))).where(isBarterIdEqual);
      if(withComments){
        return cb.gt(ex, 0);
      } else {
        return cb.equal(ex, 0);
      }
    };
  }

  public static Specification<Barter> withOffers(final Boolean withOffers){
    return (Specification<Barter>) (root, cq, cb) -> {
      if(withOffers == null) return null;
      Subquery<Long> subquery = cq.subquery(Long.class);
      Root<BarterOffer> subroot = subquery.from(BarterOffer.class);
      Predicate isBarterIdEqual = cb.equal(subroot.get(BarterOffer_.BARTER).get(Barter_.ID), root.get("id"));
      Expression ex = subquery.select(cb.count(subroot.get(BarterOffer_.ID))).where(isBarterIdEqual);
      if(withOffers){
        return cb.gt(ex, 0);
      } else {
        return cb.equal(ex, 0);
      }
    };
  }
}
