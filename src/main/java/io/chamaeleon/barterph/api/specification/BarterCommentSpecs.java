package io.chamaeleon.barterph.api.specification;

import io.chamaeleon.barterph.api.domain.*;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

public class BarterCommentSpecs {

  public static Specification<BarterComment> isUserEqualTo(final Long userId){
    return (Specification<BarterComment>) (root, cq, cb) -> {
      if(userId == null) return null;
      return cb.equal(root.get(BarterComment_.OWNER).get(User_.ID), userId);
    };
  }

  public static Specification<BarterComment> isParentCommentEqualTo(final Long parentCommentId){
    return (Specification<BarterComment>) (root, cq, cb) -> {
      if(parentCommentId == null) {
        return cb.isNull(root.get(BarterComment_.PARENT_COMMENT).get(BarterComment_.ID));
      }
      return cb.equal(root.get(BarterComment_.PARENT_COMMENT).get(BarterComment_.ID), parentCommentId);
    };
  }

  public static Specification<BarterComment> isOfferEqualTo(final Long offerId){
    return (Specification<BarterComment>) (root, cq, cb) -> {
      if(offerId == null) {
        return cb.isNull(root.get(BarterComment_.OFFER).get(BarterOffer_.ID));
      }
      return cb.equal(root.get(BarterComment_.OFFER).get(BarterOffer_.ID), offerId);
    };
  }

  public static Specification<BarterComment> isBarterEqualTo(final Long barterId){
    return (Specification<BarterComment>) (root, cq, cb) -> {
      if(barterId == null) return null;
      return cb.equal(root.get(BarterComment_.BARTER).get(Barter_.ID), barterId);
    };
  }

  public static Specification<BarterComment> isNotDeleted(){
    return (Specification<BarterComment>) (root, cq, cb) -> cb.equal(root.get(BarterComment_.IS_DELETED), false);
  }

  public static Sort buildSortObject(Integer sort){
    Sort sortObject = Sort.by(Sort.Order.asc(BarterComment_.ID));
    if(sort == null){
    } else if(sort == 1){
      sortObject = Sort.by(Sort.Order.asc(BarterComment_.CREATED_DATE));
    } else if(sort == 2) {
      sortObject = Sort.by(Sort.Order.desc(BarterComment_.CREATED_DATE));
    }
    return sortObject;
  }
}
