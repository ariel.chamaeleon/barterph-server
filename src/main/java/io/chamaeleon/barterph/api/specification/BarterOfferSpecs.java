package io.chamaeleon.barterph.api.specification;

import io.chamaeleon.barterph.api.domain.*;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

public class BarterOfferSpecs {

  public static Specification<BarterOffer> isUserEqualTo(final Long userId){
    return (Specification<BarterOffer>) (root, cq, cb) -> {
      if(userId == null) return null;
      return cb.equal(root.get(BarterOffer_.OWNER).get(User_.ID), userId);
    };
  }

  public static Specification<BarterOffer> isBarterEqualTo(final Long barterId){
    return (Specification<BarterOffer>) (root, cq, cb) -> {
      if(barterId == null) return null;
      return cb.equal(root.get(BarterOffer_.BARTER).get(Barter_.ID), barterId);
    };
  }

  public static Sort buildSortObject(Integer sort){
    Sort sortObject = Sort.by(Sort.Order.asc(BarterComment_.ID));
    if(sort == null){
    } else if(sort == 1){
      sortObject = Sort.by(Sort.Order.asc(BarterComment_.CREATED_DATE));
    } else if(sort == 2) {
      sortObject = Sort.by(Sort.Order.desc(BarterComment_.CREATED_DATE));
    }
    return sortObject;
  }
}
