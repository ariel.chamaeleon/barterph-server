package io.chamaeleon.barterph.api.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.chamaeleon.barterph.api.util.SocialMediaPlatform;
import lombok.Data;

import java.time.LocalDate;
import java.util.Map;

/**
 * @author Ariel
 * @since 11/28/2019
 */
@Data
public class SocialUserDTO {
  @JsonProperty("id")
  private String id;

  @JsonProperty("first_name")
  private String firstName;

  @JsonProperty("last_name")
  private String lastName;

  @JsonProperty("email")
  private String email;

  @JsonProperty("birthday")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
  private LocalDate birthday;

  @JsonProperty("short_name")
  private String alias;

  private String city;
  private String country;
  private String state;
  private String zip;
  private String imageUrl;
  private SocialMediaPlatform platform;

  @SuppressWarnings("unchecked")
  @JsonProperty("location")
  private void unpackLocation(Map<String, Object> location) {
		if (location == null) {
			return;
		}
    Map<String, String> inner = (Map<String, String>) location.get("location");
    this.city = inner.get("city");
    this.country = inner.get("country");
    this.state = inner.get("state");
    this.zip = inner.get("zip");
  }
}
