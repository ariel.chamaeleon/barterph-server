package io.chamaeleon.barterph.api.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.chamaeleon.barterph.api.domain.Location;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LocationDTO {

  private Long id;

  @Size(max=256)
  private String location;

  private Long longitude;

  private Long latitude;

  private String country;

  @NotBlank
  private String province;

  @NotBlank
  private String city;

  private String houseNo;

  private String street;

  private String barangay;

  public LocationDTO(Location location){
    this.id = location.getId();
    this.location = location.getLocation();
    this.city = location.getCity();
    this.country = location.getCountry();
    this.latitude = location.getLatitude();
    this.longitude = location.getLongitude();
    this.province = location.getProvince();
    this.houseNo = location.getHouseNo();
    this.street = location.getStreet();
    this.barangay = location.getBarangay();
  }
}
