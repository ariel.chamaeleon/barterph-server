package io.chamaeleon.barterph.api.service.mapper;

import io.chamaeleon.barterph.api.config.Constants;
import io.chamaeleon.barterph.api.domain.Location;
import io.chamaeleon.barterph.api.service.dto.LocationDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class LocationMapper {

  public Location toLocation(LocationDTO dto){
    if(dto == null) return null;
    return Location.builder()
      .location(dto.getLocation())
      .city(dto.getCity())
      .country(StringUtils.isNoneBlank(dto.getCountry()) ? dto.getCountry() : Constants.DEFAULT_COUNTRY)
      .latitude(dto.getLatitude())
      .longitude(dto.getLongitude())
      .id(dto.getId())
      .province(dto.getProvince())
      .barangay(dto.getBarangay())
      .houseNo(dto.getHouseNo())
      .street(dto.getStreet())
      .build();
  }

  public Location toLocation(Location location, LocationDTO dto){
    if(dto == null) return null;
    if(location == null){
      return toLocation(dto);
    } else {
      return location.toBuilder()
        .city(dto.getCity())
        .country(StringUtils.isNoneBlank(dto.getCountry()) ? dto.getCountry() : Constants.DEFAULT_COUNTRY)
        .latitude(dto.getLatitude())
        .location(dto.getLocation())
        .longitude(dto.getLongitude())
        .province(dto.getProvince())
        .barangay(dto.getBarangay())
        .houseNo(dto.getHouseNo())
        .street(dto.getStreet())
        .build();
    }
  }
}
