package io.chamaeleon.barterph.api.service.mapper;

import io.chamaeleon.barterph.api.config.Constants;
import io.chamaeleon.barterph.api.domain.Barter;
import io.chamaeleon.barterph.api.domain.BarterPhoto;
import io.chamaeleon.barterph.api.repository.BarterCommentRepository;
import io.chamaeleon.barterph.api.repository.BarterOfferRepository;
import io.chamaeleon.barterph.api.service.dto.BarterDTO;
import io.chamaeleon.barterph.api.service.dto.LocationDTO;
import lombok.Data;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Service
public class BarterMapper {

  private final LocationMapper locationMapper;

  private final UserMapper userMapper;

  private final BarterCommentRepository commentRepository;

  private final BarterOfferRepository offerRepository;

  private final CategoryMapper categoryMapper;

  public BarterDTO toBarterDTO(Barter barter){
    if(barter == null) return null;
    return BarterDTO.builder()
      .photos(photosToString(barter.getBarterPhotos()))
      .contactNumber(barter.getContactNumber())
      .datePosted(barter.getDatePosted())
      .item(barter.getItem())
      .description(barter.getDescription())
      .id(barter.getId())
      .lastUpdateDate(barter.getLastUpdateDate())
      .location(new LocationDTO(barter.getLocation()))
      .locationType(barter.getLocationType())
      .deliveryType(barter.getDeliveryType())
      .courier(barter.getCourier())
      .preferences(barter.getPreferences())
      .reason(barter.getReason())
      .status(barter.getStatus())
      .user(userMapper.toUserDTOMinimal(barter.getUser()))
      .viewCount(barter.getViewCount())
      .commentCount(commentRepository.countByBarterIdAndParentCommentIdAndOfferIdAndIsDeleted(barter.getId(), null, null, false))
      .offerCount(offerRepository.countByBarterId(barter.getId()))
      .dateReposted(barter.getDateReposted())
      .category(categoryMapper.toCategoryDTO(barter.getCategory()))
      .build();
  }

  public Barter toBarter(BarterDTO dto){
    if(dto == null) return null;
    return Barter.builder()
      .barterPhotos(photosFromString(dto.getPhotos()))
      .contactNumber(dto.getContactNumber())
      .datePosted(dto.getDatePosted())
      .item(dto.getItem())
      .description(dto.getDescription())
      .id(dto.getId())
      .lastUpdateDate(dto.getLastUpdateDate())
      .location(locationMapper.toLocation(dto.getLocation()))
      .locationType(dto.getLocationType())
      .deliveryType(dto.getDeliveryType())
      .courier(dto.getCourier())
      .preferences(dto.getPreferences())
      .reason(dto.getReason())
      .status(dto.getStatus())
      .category(categoryMapper.toCategory(dto.getCategory()))
      .build();
  }

  public List<BarterPhoto> photosFromString(List<String> strPhotos){
    if(CollectionUtils.isEmpty(strPhotos)) return Collections.EMPTY_LIST;
    return strPhotos.stream().map(s -> BarterPhoto.builder()
      .photoUrl(s.startsWith(Constants.BARTER_PHOTOS_URL) ? s : Constants.BARTER_PHOTOS_URL + s)
      .build()).collect(Collectors.toList());
  }

  public List<String>  photosToString(List<BarterPhoto> barterPhotos){
    if(CollectionUtils.isEmpty(barterPhotos)) return Collections.EMPTY_LIST;
    return barterPhotos.stream().map(b -> b.getPhotoUrl()).collect(Collectors.toList());
  }
}
