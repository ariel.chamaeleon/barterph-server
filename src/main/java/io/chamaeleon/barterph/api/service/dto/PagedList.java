package io.chamaeleon.barterph.api.service.dto;

import lombok.Data;

import java.util.List;

@Data
public class PagedList<T> {
  private List<T> list;
  private int page;
  private long pageSize;
  private long totalPages;
  private long totalCount;

  public void setPageSize(long pageSize) {
    this.pageSize = pageSize;
    calculateTotalPages();
  }

  public void setTotalCount(long totalCount) {
    this.totalCount = totalCount;
    calculateTotalPages();
  }

  private void calculateTotalPages(){
    if(this.pageSize > 0)
      this.totalPages = (int) Math.ceil(totalCount /(double) this.pageSize);
    else
      this.totalPages = 0;
  }
}
