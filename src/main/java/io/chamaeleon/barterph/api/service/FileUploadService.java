package io.chamaeleon.barterph.api.service;

import com.google.api.client.util.Value;
import io.chamaeleon.barterph.api.util.RandomUtil;
import io.chamaeleon.barterph.api.util.SpringUtil;
import io.chamaeleon.barterph.api.web.rest.errors.*;
import lombok.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.imageio.*;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.*;
import java.util.*;

@Service
public class FileUploadService {

  private static final Logger log = LoggerFactory.getLogger(FileUploadService.class);

  @Autowired
  private SpringUtil springUtil;

  @Getter
  @Value("${file-upload.base-dir:${user.home}}")
  private String baseDir;

  @Getter
  private String waterMark;

  @Getter
  @Value("${file-upload.str-watermark: Barter PH App}")
  private String strWaterMark;

  public String userPhotoDir;
  private String barterPhotoDir;
  private String offerPhotoDir;
  private String tempDir;

  @PostConstruct
  public void postConstruct() {
    this.userPhotoDir = baseDir + File.separator + "photos/user";
    this.barterPhotoDir = baseDir + File.separator + "photos/barter";
    this.offerPhotoDir = baseDir + File.separator + "photos/offer";
    this.tempDir = baseDir + File.separator + "temp";
    createDirIfNotExists(this.userPhotoDir);
    createDirIfNotExists(this.barterPhotoDir);
    createDirIfNotExists(this.offerPhotoDir);
    createDirIfNotExists(this.tempDir);
  }

  public void uploadFileToTemp(MultipartFile file) {
    throwIfEmptyFile(file);
    try {
      Path copyLocation = Paths.get(this.tempDir);
      file.transferTo(copyLocation);
    } catch (IOException e) {
      throw new ApiException(
        new ApiError(ApiErrorCode.FILE_UPLOAD_ERROR,
          "Could not store file " + file.getOriginalFilename()+ ". Please try again!"),
        e);
    }
  }

  public String uploadImageToTemp(MultipartFile file){
    throwIfEmptyFile(file);

    BufferedImage imageFile = fileToImage(file);
    BufferedImage compressedImage = compressPhoto(7, imageFile);

    FileName fileName = generateFileName(file.getOriginalFilename());
    if(compressedImage != null){
      try {
        BufferedImage watermarked = addTextWatermark(compressedImage, fileName.ext);
        String fullPath = this.tempDir + File.separator + fileName.fullName();
        ImageIO.write(watermarked, fileName.getExt(), new File(fullPath));
      } catch (IOException e) {
        throw new ApiException( new ApiError(ApiErrorCode.FILE_UPLOAD_ERROR,
            "Could not store file " + file.getOriginalFilename()+ ". Please try again!"), e);
      }
    }
    return fileName.fullName();
  }

  public void transferFromTempToUser(String fileName){
    transferFromTemp(fileName, this.userPhotoDir);
  }

  public void transferFromTempToBarter(String fileName){
    transferFromTemp(fileName, this.barterPhotoDir);
  }

  public void transferFromTempToOffer(String fileName){
    transferFromTemp(fileName, this.offerPhotoDir);
  }

  public FileName generateFileName(String oldName){
    String ext = Optional.ofNullable(oldName)
      .map(StringUtils::cleanPath)
      .map(n -> n.substring(n.lastIndexOf('.') +1, n.length()))
      .orElse(null);

    return FileName.builder()
      .baseName(UUID.randomUUID().toString() +"-" + RandomUtil.generateRandomNumericString(6))
      .ext(ext)
      .build();
  }

  public InputStreamResource readFile(String dir, FileName fileName){
    try {
      String useDir = null;
      if(Objects.equals(dir, "user")){
        useDir = this.userPhotoDir;
      } else if(Objects.equals(dir, "barter")){
        useDir = this.barterPhotoDir;
      } else if(Objects.equals(dir, "offer")){
        useDir = this.offerPhotoDir;
      }

      File file = new File(useDir+ File.separator +fileName.fullName());
      FileInputStream fis = new FileInputStream(file);
      return new InputStreamResource(fis);
    } catch (FileNotFoundException e) {
      throw new ApiException( new ApiError(ApiErrorCode.FILE_NOT_FOUND,
        "Could not read file " + fileName.fullName()+ ". Please try again!"), e);
    }
  }

  //*********** PRIVATES *************//

  private void createDirIfNotExists(String dirName){
    Path path = Paths.get(dirName);
    if(Files.notExists(path)){
      try {
        Files.createDirectories(path);
      } catch (IOException e) {
        log.error("Cannot create "+dirName, e);
      }
    }
  }

  private void throwIfEmptyFile(MultipartFile file){
    if(file == null){
      throw new ApiException(springUtil.buildApiError(ApiErrorCode.EMPTY_FILE));
    }
  }

  private BufferedImage fileToImage(MultipartFile file){
    BufferedImage imageFile = null;
    try (InputStream is = file.getInputStream()){
      imageFile = ImageIO.read(is);
      imageFile.toString();
    } catch (Exception e) {
      throw new ApiException(springUtil.buildApiError(ApiErrorCode.INVALID_PHOTO), e);
    }
    return imageFile;
  }

  private BufferedImage resizeImage(BufferedImage img, Dimension d) throws IOException{
    Image tmp = img.getScaledInstance((int) d.getWidth(), (int) d.getHeight(), Image.SCALE_SMOOTH);
    BufferedImage outputImage = new BufferedImage((int) d.getWidth(), (int) d.getHeight(), img.getType());

    // scales the input image to the output image
    Graphics2D g2d = outputImage.createGraphics();
    g2d.drawImage(tmp, 0, 0, null);
    g2d.dispose();

    return outputImage;
  }

  private Dimension getScaledDimension(Dimension imgSize, Dimension boundary) {
    int original_width = imgSize.width;
    int original_height = imgSize.height;
    int bound_width = boundary.width;
    int bound_height = boundary.height;
    int new_width = original_width;
    int new_height = original_height;

    // first check if we need to scale width
    if (original_width > bound_width) {
      //scale width to fit
      new_width = bound_width;
      //scale height to maintain aspect ratio
      new_height = (new_width * original_height) / original_width;
    }

    // then check if we need to scale even with the new height
    if (new_height > bound_height) {
      //scale height to fit instead
      new_height = bound_height;
      //scale width to maintain aspect ratio
      new_width = (new_height * original_width) / original_height;
    }

    return new Dimension(new_width, new_height);
  }

  private void writeImage(BufferedImage image, FileName fileName, String dir) throws IOException{
    ImageIO.write(image, fileName.getExt(), new File(dir+ File.separator +fileName.fullName()));
  }

  private void writeImageWithResize(File srcFile, FileName fileName, String dir, Dimension currentSize, Dimension targetSize) throws IOException{
    BufferedImage image = null;
    if(targetSize != null){
      Dimension scaledSize  = getScaledDimension(currentSize, targetSize);
      image		= resizeImage(ImageIO.read(srcFile), scaledSize);
    }

    if(image != null){
      ImageIO.write(image, fileName.getExt(), new File(dir+ File.separator +fileName.fullName()));
    }
  }

  private BufferedImage compressPhoto(int compression, BufferedImage image){
    BufferedImage compressed = null;
    Iterator<ImageWriter> i = ImageIO.getImageWritersByFormatName("jpeg");
    ImageWriter writer = i.next();

    ImageWriteParam param = writer.getDefaultWriteParam();
    param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
    param.setCompressionQuality(0.1f * compression);

    try {
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      ImageOutputStream ios = ImageIO.createImageOutputStream(out);
      writer.setOutput(ios);
      writer.write(null, new IIOImage(image, null, null), param);
      writer.dispose();
      out.flush();
      byte[] byteArray = out.toByteArray();
      out.close();

      compressed = ImageIO.read(new ByteArrayInputStream(byteArray));
    } catch (IOException e) {
      throw new ApiException(springUtil.buildApiError(ApiErrorCode.FILE_UPLOAD_ERROR), e);
    }
    return compressed;
  }

  private void transferFromTemp(String fileName, String dir){
    try {
      File srcFile = new File(tempDir + File.separator + fileName);
      BufferedImage srcImage = ImageIO.read(srcFile);
      writeImage(srcImage, new FileName(fileName, null, false), dir) ;

      Dimension currSize = new Dimension(srcImage.getWidth(), srcImage.getHeight());
      FileName miniFileName = new FileName(fileName, "-mini", false);
      writeImageWithResize(srcFile, miniFileName, dir, currSize, new Dimension(320, 320));

      FileName thumbFileName = new FileName(fileName, "-thumb", false);
      writeImageWithResize(srcFile, thumbFileName, dir, currSize, new Dimension(96, 96));
    } catch (IOException e) {
      throw new ApiException( new ApiError(ApiErrorCode.FILE_UPLOAD_ERROR,
        "Could not store file " + fileName+ ". Please try again!"), e);
    }
  }

  private BufferedImage addTextWatermark(BufferedImage imageFile, String type) throws IOException {
    final String txt = "Barter PH App";
    // initializes necessary graphic properties
    Graphics2D w = (Graphics2D) imageFile.getGraphics();
    AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f);
    w.setComposite(alphaChannel);
    w.setColor(Color.BLACK);

    int fontSize = ((imageFile.getWidth() + imageFile.getHeight()) / 2) / txt.length();
    w.setFont(new Font("Arial", Font.BOLD, fontSize));
    FontMetrics fontMetrics = w.getFontMetrics();
    Rectangle2D rect = fontMetrics.getStringBounds(txt, w);

    // calculates the coordinate where the String is painted
    int posX = imageFile.getWidth() / 2 - ((int) rect.getWidth()) / 2;
    int posY = imageFile.getHeight() / 2 - ((int) rect.getHeight()) / 2;

    // paints the textual watermark
    w.drawString(txt, posX, posY);
    w.dispose();
    return imageFile;
  }

  @Data
  @Builder
  @AllArgsConstructor
  @NoArgsConstructor(access = AccessLevel.PROTECTED)
  public static class FileName{
    private String baseName;
    private String ext;

    public String fullName(){
      return this.baseName +'.'+ this.ext.toLowerCase();
    }
    public FileName(String strName, String baseNameAppend, boolean placeholder){
      Optional.ofNullable(strName)
        .map(StringUtils::cleanPath)
        .ifPresent(n -> {
          this.baseName = n.substring(0, n.lastIndexOf('.'));
          this.ext = n.substring(n.lastIndexOf('.') +1, n.length());
          if(Objects.nonNull(baseNameAppend)){
            this.baseName += baseNameAppend;
          }
        });
    }
  }
}
