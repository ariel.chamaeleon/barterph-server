package io.chamaeleon.barterph.api.service;

import io.chamaeleon.barterph.api.domain.*;
import io.chamaeleon.barterph.api.repository.BarterRepository;
import io.chamaeleon.barterph.api.repository.BarterViewRepository;
import io.chamaeleon.barterph.api.util.SpringUtil;
import io.chamaeleon.barterph.api.web.rest.errors.ApiErrorCode;
import io.chamaeleon.barterph.api.web.rest.errors.ApiException;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
@Transactional
public class BarterViewService {

  private static final Logger log = LoggerFactory.getLogger(BarterService.class);

  private BarterViewRepository barterViewRepository;
  private BarterRepository barterRepository;
  private SpringUtil springUtil;
  private UserService userService;

  public BarterView addNewView(Long barterId, Long viewerId){
    Barter barter = barterRepository.findById(barterId).orElse(null);
    User viewer = userService.findUserById(viewerId);
    return addNewView(barter, viewer);
  }

  public BarterView addNewView(Barter barter, User viewer){
    if(Objects.isNull(barter)){
      throw new ApiException(springUtil.buildApiError(ApiErrorCode.BARTER_NOT_FOUND));
    }

    barter.setViewCount(barter.getViewCount() + 1);
    barterRepository.save(barter);

    BarterView view = BarterView.builder()
      .barter(barter)
      .viewer(viewer)
      .dateViewed(LocalDateTime.now())
      .build();
    return barterViewRepository.save(view);
  }
}
