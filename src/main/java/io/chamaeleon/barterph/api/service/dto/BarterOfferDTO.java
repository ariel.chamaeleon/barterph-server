package io.chamaeleon.barterph.api.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder(toBuilder = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BarterOfferDTO {

  private Long id;

  private UserDTO user;

  private BarterDTO barter;

  private String item;

  @Deprecated
  private String note;

  private String description;

  private List<BarterCommentDTO> replies;

  private long repliesCount;

  private LocalDateTime datePosted;

  private List<String> photos;
}
