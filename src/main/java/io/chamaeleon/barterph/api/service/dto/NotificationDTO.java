package io.chamaeleon.barterph.api.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.chamaeleon.barterph.api.util.NotificationType;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NotificationDTO {
  private String title;
  private String subTitle;
  private Long id;
  private NotificationType type;
  private LocalDateTime datePosted;
  private UserDTO receiver;
  private UserDTO sender;
  private BarterDTO barter;
  @JsonProperty("comment")
  private BarterCommentDTO barterComment;
  @JsonProperty("offer")
  private BarterOfferDTO barterOffer;
  private boolean isRead;
}
