package io.chamaeleon.barterph.api.service;

public class MobileNoAlreadyUsedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public MobileNoAlreadyUsedException() {
        super("Mobile number already used!");
    }

}
