package io.chamaeleon.barterph.api.service.mapper;

import io.chamaeleon.barterph.api.domain.BarterComment;
import io.chamaeleon.barterph.api.repository.BarterCommentRepository;
import io.chamaeleon.barterph.api.service.dto.BarterCommentDTO;
import io.chamaeleon.barterph.api.service.dto.BarterDTO;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@Data
public class BarterCommentMapper {

  private final BarterMapper barterMapper;

  private final UserMapper userMapper;

  private final BarterCommentRepository commentRepository;

  public BarterCommentDTO toBarterCommentDTO(BarterComment comment){
    return BarterCommentDTO.builder()
      .comment(comment.getComment())
      .user(userMapper.toUserDTOMinimal(comment.getOwner()))
      .id(comment.getId())
      .datePosted(comment.getCreatedDate())
      .repliesCount(commentRepository.countByBarterIdAndParentCommentIdAndOfferIdAndIsDeleted(comment.getBarter().getId(), comment.getId(), null, false))
      .barter(BarterDTO.builder()
        .id(comment.getBarter().getId())
        .build())
      .build();
  }

  public BarterCommentDTO toBarterCommentDTOWithReplies(BarterComment comment){
    return BarterCommentDTO.builder()
      .comment(comment.getComment())
      .user(userMapper.toUserDTOMinimal(comment.getOwner()))
      .id(comment.getId())
      .repliesCount(comment.getReplies().size())
      .datePosted(comment.getCreatedDate())
      .barter(BarterDTO.builder()
        .id(comment.getBarter().getId())
        .build())
      .replies(comment.getReplies().stream()
        .map(r -> toBarterCommentDTO(r))
        .collect(Collectors.toList())
      )
      .build();
  }
}
