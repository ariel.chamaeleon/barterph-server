package io.chamaeleon.barterph.api.service.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CategoryDTO {
  private Long id;
  private String name;
  private String profession;
}
