package io.chamaeleon.barterph.api.service.mapper;

import io.chamaeleon.barterph.api.domain.Authority;
import io.chamaeleon.barterph.api.domain.User;
import io.chamaeleon.barterph.api.service.dto.UserDTO;

import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Mapper for the entity {@link User} and its DTO called {@link UserDTO}.
 *
 * Normal mappers are generated using MapStruct, this one is hand-coded as MapStruct
 * support is still in beta, and requires a manual step with an IDE.
 */
@Service
public class UserMapper {

    public List<UserDTO> usersToUserDTOs(List<User> users) {
        return users.stream()
            .filter(Objects::nonNull)
            .map(this::userToUserDTO)
            .collect(Collectors.toList());
    }

    public UserDTO userToUserDTO(User user) {
        return new UserDTO(user);
    }

    public List<User> userDTOsToUsers(List<UserDTO> userDTOs) {
        return userDTOs.stream()
            .filter(Objects::nonNull)
            .map(this::userDTOToUser)
            .collect(Collectors.toList());
    }

    public User userDTOToUser(UserDTO userDTO) {
        if (userDTO == null) {
            return null;
        } else {
            User user = User.builder()
              .activated(userDTO.isActivated())
              .authorities(this.authoritiesFromStrings(userDTO.getAuthorities()))
              .id(userDTO.getId())
              .email(userDTO.getEmail())
              .mobileNo(userDTO.getMobileNo())
              .firstName(userDTO.getFirstName())
              .lastName(userDTO.getLastName())
              .middleName(userDTO.getMiddleName())
              .fbId(userDTO.getFbId())
              .googleId(userDTO.getGoogleId())
              .build();
            return user;
        }
    }


    private Set<Authority> authoritiesFromStrings(Set<String> authoritiesAsString) {
        Set<Authority> authorities = new HashSet<>();

        if (authoritiesAsString != null) {
            authorities = authoritiesAsString.stream().map(string -> {
                Authority auth = Authority.builder()
                  .name(string)
                  .build();
                return auth;
            }).collect(Collectors.toSet());
        }

        return authorities;
    }

    public UserDTO toUserDTOMinimal(User user){
        return UserDTO.builder()
          .id(user.getId())
          .firstName(user.getFirstName())
          .lastName(user.getLastName())
          .middleName(user.getMiddleName())
          .email(user.getEmail())
          .mobileNo(user.getMobileNo())
          .imageUrl(user.getImageUrl())
          .build();
    }
}
