package io.chamaeleon.barterph.api.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.chamaeleon.barterph.api.service.dto.SocialUserDTO;
import io.chamaeleon.barterph.api.util.SocialMediaPlatform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Service;

@Service
public class FacebookService {

  private static final Logger log = LoggerFactory.getLogger(FacebookService.class);

  private final ObjectMapper objectMapper;
  public FacebookService(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  public SocialUserDTO getFBUserProfile(String accessToken) {
    SocialUserDTO user = null;
    try {
      log.info("FB Access Token: "+accessToken);
      Facebook facebook = new FacebookTemplate(accessToken);
      String[] fields = {"id", "first_name", "last_name", "email", "location{location{city,country,zip,state}}", "birthday", "short_name"};
      String strUserProfile = facebook.fetchObject("me", String.class, fields);
      log.info("FB Response: "+strUserProfile);
      if(strUserProfile != null){
        user = objectMapper.readerFor(SocialUserDTO.class).readValue(strUserProfile);
        user.setImageUrl(new StringBuilder("http://graph.facebook.com/")
          .append(user.getId()).append("/picture?width=100&height=100").toString());
        log.info("FB User: "+user);
        user.setPlatform(SocialMediaPlatform.FB);
      }
    } catch (Exception e) {
      log.error("Error getting facebook data", e);
      user = null;
    }
    return user;
  }
}
