package io.chamaeleon.barterph.api.service.dto;

import lombok.Data;

@Data
public class PageRequestDTO {
  private Integer page;
  private Integer pageSize;
  private Integer sort;
}
