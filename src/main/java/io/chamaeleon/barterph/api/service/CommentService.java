package io.chamaeleon.barterph.api.service;

import io.chamaeleon.barterph.api.domain.*;
import io.chamaeleon.barterph.api.repository.BarterCommentRepository;
import io.chamaeleon.barterph.api.repository.BarterOfferRepository;
import io.chamaeleon.barterph.api.service.dto.BarterCommentDTO;
import io.chamaeleon.barterph.api.service.dto.PagedList;
import io.chamaeleon.barterph.api.service.mapper.BarterCommentMapper;
import io.chamaeleon.barterph.api.specification.BarterCommentSpecs;
import io.chamaeleon.barterph.api.util.SpringUtil;
import io.chamaeleon.barterph.api.web.rest.errors.ApiErrorCode;
import io.chamaeleon.barterph.api.web.rest.errors.ApiException;
import io.chamaeleon.barterph.api.web.rest.vm.AddCommentVM;
import io.chamaeleon.barterph.api.web.rest.vm.SearchCommentVM;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
@Data
public class CommentService {

  private static final Logger log = LoggerFactory.getLogger(CommentService.class);

  private final BarterCommentRepository barterCommentRepository;

  private final BarterCommentMapper barterCommentMapper;

  private final UserService userService;

  private final BarterService barterService;

  private final NotificationService notificationService;

  private final SpringUtil springUtil;

  private final BarterOfferRepository barterOfferRepository;

  public BarterComment saveComment(BarterComment comment){
    return barterCommentRepository.save(comment);
  }

  public BarterCommentDTO saveComment(AddCommentVM vm){
    Barter barter = barterService.findOneById(vm.getBarterId())
      .orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.BARTER_NOT_FOUND)));

    User user = userService.getCurrentUser()
      .orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.ACCOUNT_NOT_FOUND)));

    BarterComment parentComment  = null;
    if(vm.getParentCommentId() != null){
      parentComment = barterCommentRepository.findById(vm.getParentCommentId())
        .orElse(null);
    }

    BarterOffer offer = null;
    if(vm.getOfferId() != null){
      offer = barterOfferRepository.findById(vm.getOfferId())
        .orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.OFFER_NOT_FOUND)));

      if(user.getId() != offer.getOwner().getId()
          && user.getId() != barter.getUser().getId()){
        throw new ApiException(springUtil.buildApiError(ApiErrorCode.USER_CANNOT_REPLY_TO_OFFER));
      }
    }

    BarterComment barterComment = BarterComment.builder()
      .barter(barter)
      .comment(vm.getComment())
      .owner(user)
      .parentComment(parentComment)
      .offer(offer)
      .build();

    barterComment = barterCommentRepository.save(barterComment);
    notificationService.buildAndSendCommentNotification(barterComment);
    return barterCommentMapper.toBarterCommentDTO(barterComment);
  }

  public PagedList<BarterCommentDTO> getComments(SearchCommentVM vm){
    Specification<BarterComment> specs = Specification
      .where(BarterCommentSpecs.isBarterEqualTo(vm.getBarterId()))
      .and(BarterCommentSpecs.isParentCommentEqualTo(vm.getParentCommentId()))
      .and(BarterCommentSpecs.isUserEqualTo(vm.getUserId()))
      .and(BarterCommentSpecs.isOfferEqualTo(vm.getOfferId()))
      .and(BarterCommentSpecs.isNotDeleted());

    Sort sort = BarterCommentSpecs.buildSortObject(vm.getSort());
    PagedList<BarterCommentDTO> pagedList = new PagedList<>();

    List<BarterComment> list;
    long totalCount = 0;
    if(vm.getPageSize() != null){
      Pageable pageable = PageRequest.of(vm.getPage() - 1, vm.getPageSize(), sort);
      Page paged =  barterCommentRepository.findAll(specs, pageable);
      list = paged.getContent();
      totalCount = paged.getTotalElements();
    } else {
      list = barterCommentRepository.findAll(specs, sort);
      totalCount = list.size();
    }

    List<BarterCommentDTO> dtos = list.stream()
      .map(b -> barterCommentMapper.toBarterCommentDTO(b))
      .collect(Collectors.toList());
    pagedList.setList(dtos);
    pagedList.setPage(vm.getPage() == null ? 1 : vm.getPage());
    pagedList.setPageSize(vm.getPageSize() == null ? totalCount : vm.getPageSize());
    pagedList.setTotalCount(totalCount);
    return pagedList;
  }

  public Optional<BarterComment> getCommentById(Long id){
    return barterCommentRepository.findById(id);
  }

  public long countByBarterIdAndParentCommentId(long barterId, Long parentCommentId){
    return barterCommentRepository.countByBarterIdAndParentCommentIdAndOfferIdAndIsDeleted(barterId, parentCommentId, null, false);
  }

  public void deleteComment(long id){
    User user = userService.getCurrentUser()
      .orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.ACCOUNT_NOT_FOUND)));

    BarterComment comment = barterCommentRepository.findById(id)
      .filter(c -> !c.isDeleted())
      .filter(c -> Objects.equals(user.getId(), c.getOwner().getId()))
      .orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.COMMENT_NOT_FOUND)));

    comment.setDeleted(true);
    comment.setDateDeleted(LocalDateTime.now());
    barterCommentRepository.save(comment);
  }

  public BarterCommentDTO updateComment(long id, String commentStr){
    User user = userService.getCurrentUser()
      .orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.ACCOUNT_NOT_FOUND)));

    BarterComment comment = barterCommentRepository.findById(id)
      .filter(c -> !c.isDeleted())
      .filter(c -> Objects.equals(user.getId(), c.getOwner().getId()))
      .orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.COMMENT_NOT_FOUND)));

    comment.setComment(commentStr);
    comment.setLastUpdateDate(LocalDateTime.now());
    comment = barterCommentRepository.save(comment);
    return barterCommentMapper.toBarterCommentDTO(comment);
  }
}
