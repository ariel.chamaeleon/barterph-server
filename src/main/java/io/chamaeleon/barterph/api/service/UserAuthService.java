package io.chamaeleon.barterph.api.service;

import io.chamaeleon.barterph.api.domain.User;
import io.chamaeleon.barterph.api.domain.UserAuth;
import io.chamaeleon.barterph.api.repository.UserAuthRepository;
import io.chamaeleon.barterph.api.util.SpringUtil;
import io.chamaeleon.barterph.api.web.rest.errors.ApiErrorCode;
import io.chamaeleon.barterph.api.web.rest.errors.ApiException;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@AllArgsConstructor
public class UserAuthService {
  private static final Logger log = LoggerFactory.getLogger(UserAuthService.class);

  private final SpringUtil springUtil;

  private final UserAuthRepository userAuthRepository;

  private final UserService userService;

  public UserAuth save(UserAuth userAuth){
    return userAuthRepository.save(userAuth);
  }

  public UserAuth createUserAuth(String deviceId, String token){
    User user = userService.getCurrentUser()
      .orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.ACCOUNT_NOT_FOUND)));

    UserAuth userAuth = userAuthRepository.findOneByDeviceIdAndUserId(deviceId, user.getId())
      .orElseGet(() -> UserAuth.builder()
        .user(user)
        .deviceId(deviceId)
        .build());

    userAuth = userAuth.toBuilder().fcmToken(token).build();
    return userAuthRepository.save(userAuth);
  }
}
