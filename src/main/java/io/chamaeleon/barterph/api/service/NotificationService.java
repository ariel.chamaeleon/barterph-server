package io.chamaeleon.barterph.api.service;

import com.google.firebase.messaging.*;
import io.chamaeleon.barterph.api.domain.Notification;
import io.chamaeleon.barterph.api.domain.*;
import io.chamaeleon.barterph.api.repository.NotificationRepository;
import io.chamaeleon.barterph.api.repository.UserAuthRepository;
import io.chamaeleon.barterph.api.service.dto.NotificationDTO;
import io.chamaeleon.barterph.api.service.dto.PagedList;
import io.chamaeleon.barterph.api.service.mapper.NotificationMapper;
import io.chamaeleon.barterph.api.specification.NotificationSpecs;
import io.chamaeleon.barterph.api.util.NotificationType;
import io.chamaeleon.barterph.api.util.SpringUtil;
import io.chamaeleon.barterph.api.web.rest.vm.SearchNotificationVM;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class NotificationService {

  private static final Logger log = LoggerFactory.getLogger(NotificationService.class);

  private final NotificationRepository notificationRepository;

  private final UserAuthRepository userAuthRepository;

  private final SpringUtil springUtil;

  private final NotificationMapper notificationMapper;

  public void buildAndSendCommentNotification(BarterComment comment){
    if(comment.getOffer() != null){
      Notification notification = createOfferReplyNotification(comment);
      sendPushNotification(notification);
    } else {
      Notification notification = createCommentNotification(comment);
      sendPushNotification(notification);

      if(comment.getParentComment() != null){
        notification = createCommentReplyNotification(comment);
        sendPushNotification(notification);
      }
    }
  }

  public void buildAndSendOfferNotification(BarterOffer offer){
    if(offer != null){
      Notification notification = createOfferNotification(offer);
      sendPushNotification(notification);
    }
  }

  public Notification createCommentNotification(BarterComment comment){
    Notification notification = Notification.builder()
      .barter(comment.getBarter())
      .barterComment(comment)
      .receiver(comment.getBarter().getUser())
      .sender(comment.getOwner())
      .isRead(false)
      .datePosted(LocalDateTime.now())
      .type(NotificationType.BARTER_COMMENT)
      .build();

    if(notification.getReceiver().getId() == notification.getSender().getId()){
      return null;
    }
    return notificationRepository.save(notification);
  }

  public Notification createCommentReplyNotification(BarterComment comment){
    Notification notification = Notification.builder()
      .barter(comment.getBarter())
      .barterComment(comment)
      .receiver(comment.getParentComment().getOwner())
      .sender(comment.getOwner())
      .isRead(false)
      .datePosted(LocalDateTime.now())
      .type(NotificationType.BARTER_COMMENT_REPLY)
      .build();
    if(notification.getReceiver().getId() == notification.getSender().getId()){
      return null;
    }
    return notificationRepository.save(notification);
  }

  public Notification createOfferNotification(BarterOffer offer){
    Notification notification = Notification.builder()
      .barter(offer.getBarter())
      .barterOffer(offer)
      .receiver(offer.getBarter().getUser())
      .sender(offer.getOwner())
      .isRead(false)
      .datePosted(LocalDateTime.now())
      .type(NotificationType.BARTER_OFFER)
      .build();

    if(notification.getReceiver().getId() == notification.getSender().getId()){
      return null;
    }
    return notificationRepository.save(notification);
  }


  public Notification createOfferReplyNotification(BarterComment comment){
    Notification notification = Notification.builder()
      .barter(comment.getBarter())
      .barterOffer(comment.getOffer())
      .receiver(comment.getOffer().getOwner())
      .sender(comment.getOwner())
      .isRead(false)
      .datePosted(LocalDateTime.now())
      .type(NotificationType.BARTER_OFFER_REPLY)
      .build();
    if(notification.getReceiver().getId() == notification.getSender().getId()){
      return null;
    }
    return notificationRepository.save(notification);
  }

  public PagedList<NotificationDTO> getNotifications(SearchNotificationVM vm){
    Specification<Notification> specs = Specification
      .where(NotificationSpecs.isReceiverEqualTo(vm.getReceiverId()))
      .and(NotificationSpecs.isSenderEqualTo(vm.getSenderId()));

    Sort sort = NotificationSpecs.buildSortObject(vm.getSort());
    PagedList<NotificationDTO> pagedList = new PagedList<>();

    List<Notification> list;
    long totalCount = 0;
    if(vm.getPageSize() != null){
      Pageable pageable = PageRequest.of(vm.getPage() - 1, vm.getPageSize(), sort);
      Page paged =  notificationRepository.findAll(specs, pageable);
      list = paged.getContent();
      totalCount = paged.getTotalElements();
    } else {
      list = notificationRepository.findAll(specs, sort);
      totalCount = list.size();
    }

    List<NotificationDTO> dtos = list.stream()
      .map(b -> notificationMapper.toNotificationDTO(b))
      .collect(Collectors.toList());
    pagedList.setList(dtos);
    pagedList.setPage(vm.getPage() == null ? 1 : vm.getPage());
    pagedList.setPageSize(vm.getPageSize() == null ? totalCount : vm.getPageSize());
    pagedList.setTotalCount(totalCount);
    return pagedList;
  }

  public long countUnreadNotifications(long userId){
    return notificationRepository.countAllByReceiverIdAndIsRead(userId, false);
  }

  public void sendPushNotification(Notification notification){
    System.out.println("sendPushNotification: "+notification);
    if(notification == null) return;

    User receiver = notification.getReceiver();
    long notificationCount = countUnreadNotifications(receiver.getId());
    userAuthRepository.findAllByUserId(receiver.getId()).stream()
      .map(UserAuth::getFcmToken)
      .collect(Collectors.toList())
      .forEach(token -> {
        System.out.println("TOKEN "+token);
        try {
          Message message = buildFirebaseMessage(notification, notificationCount, token);
          System.out.println(message.toString());
          FirebaseMessaging.getInstance().send(message);
        } catch (FirebaseMessagingException e) {
          log.error("Firebase error", e);
        }
      });
  }

  public Notification createBarterReminderNotification(Barter barter){
    Notification notification = Notification.builder()
      .barter(barter)
      .receiver(barter.getUser())
      .sender(null)
      .isRead(false)
      .datePosted(LocalDateTime.now())
      .type(NotificationType.BARTER_REMINDER)
      .build();
    return notificationRepository.save(notification);
  }

  private Message buildFirebaseMessage(Notification notification, long notificationCount, String token){
    String[] result = notificationMapper.getTitleAndSubtitle(notification);
    com.google.firebase.messaging.Notification gNotification = com.google.firebase.messaging.Notification.builder()
      .setTitle(result[0])
      .setBody(result[1])
      .build();

    Message.Builder builder =  Message.builder()
      .setNotification(gNotification)
      .putData("category", notification.getType().name())
      .putData("sound", "default")
      .putData("badge", ""+(notificationCount));

    Barter barter = notification.getBarter();
    if(barter != null){
      builder.putData("barterId", ""+barter.getId());
      builder.putData("barterItem", barter.getItem());
    }

    BarterComment comment = notification.getBarterComment();
    if(comment != null){
      builder.putData("barterCommentId", ""+comment.getId());

      BarterComment parentComment = comment.getParentComment();
      if(parentComment != null){
        builder.putData("parentCommentId", ""+parentComment.getId());
      }
    }

    BarterOffer offer = notification.getBarterOffer();
    if(offer != null){
      builder.putData("offerId", ""+offer.getId());
    }

    return builder.setToken(token).build();
  }
}
