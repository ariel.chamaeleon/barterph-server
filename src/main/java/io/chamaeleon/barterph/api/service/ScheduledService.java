package io.chamaeleon.barterph.api.service;

import io.chamaeleon.barterph.api.domain.Barter;
import io.chamaeleon.barterph.api.domain.Notification;
import io.chamaeleon.barterph.api.repository.BarterOfferRepository;
import io.chamaeleon.barterph.api.repository.BarterRepository;
import io.chamaeleon.barterph.api.specification.BarterSpecs;
import io.chamaeleon.barterph.api.util.BarterStatus;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.*;
import java.util.List;

@Data
@Service
public class ScheduledService {

  private static final Logger log = LoggerFactory.getLogger(ScheduledService.class);

  private final BarterRepository barterRepository;

  private final NotificationService notificationService;

  private final BarterOfferRepository barterOfferRepository;

  @Transactional
  @Scheduled(cron="0 0 8 * * *")
  public void sendBarterReminder(){
    System.out.println(LocalDate.now().atTime(0,0,01));
    System.out.println(LocalDate.now().atTime(23, 59,59));
    List<Barter> barters = barterRepository.findAll(BarterSpecs.reminderDateToday());
    log.debug("Size: "+barters.size());
    barters.forEach(b -> {
      if(b.getStatus() == BarterStatus.ACTIVE
        && barterOfferRepository.countByBarterId(b.getId()) > 0){
        log.debug("Sending reminders for "+b);
        Notification notification = notificationService.createBarterReminderNotification(b);
        notificationService.sendPushNotification(notification);
        b.setLastRemindDate(LocalDateTime.now());
        barterRepository.save(b);
      }
    });
  }
}
