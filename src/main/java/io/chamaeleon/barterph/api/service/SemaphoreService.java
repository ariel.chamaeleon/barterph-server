package io.chamaeleon.barterph.api.service;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.lang.Collections;
import lombok.Data;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class SemaphoreService {

  @Value("${semaphore.key}")
  private String apiKey;

  @Value("${semaphore.uri}")
  private String apiUri;

  @Value("${semaphore.sender}")
  private String senderName;


  public String sendMessage(String to, String from, String message) {
    if(true) return "";
    String responseId = "";
    if(to.startsWith("+")){
      to = to.substring(1, to.length());
    }

    HttpPost post = new HttpPost(apiUri+"messages");
    List<NameValuePair> urlParameters = new ArrayList<>();
    urlParameters.add(new BasicNameValuePair("apikey", apiKey));
    urlParameters.add(new BasicNameValuePair("number", to));
    urlParameters.add(new BasicNameValuePair("message", message));
    urlParameters.add(new BasicNameValuePair("sendername", from));

    try{
      post.setEntity(new UrlEncodedFormEntity(urlParameters));
      try (
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = httpClient.execute(post)) {

        String strResponse = EntityUtils.toString(response.getEntity());
        ObjectMapper mapper = new ObjectMapper();
        List<SemaphoreMessage> semaphoreMessages = mapper.readValue(
          strResponse,
          mapper.getTypeFactory().constructCollectionType(List.class, SemaphoreMessage.class)
        );

        if (!Collections.isEmpty(semaphoreMessages)) {
          responseId = semaphoreMessages.get(0).getMessageId();
        }
      }
    } catch (Exception e) {
      throw new RuntimeException("Sorry, message cannot be sent this time. Please try again later." ,e);
    }

    return responseId;
  }

  public String sendMessage(String to, String message) {
    return sendMessage(to, this.senderName, message);
  }

  @Data
  static class SemaphoreMessage{
    @JsonProperty("message_id")
    private String messageId;

    @JsonProperty("user_id")
    private String userId;
    private String user;

    @JsonProperty("account_id")
    private String accountId;
    private String account;
    private String recipient;
    private String message;

    @JsonProperty("sender_name")
    private String senderName;
    private String network;
    private String status;
    private String type;
    private String source;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("updated_at")
    private String updatedAt;

  }
}
