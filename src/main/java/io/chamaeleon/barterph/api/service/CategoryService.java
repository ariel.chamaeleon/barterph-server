package io.chamaeleon.barterph.api.service;

import io.chamaeleon.barterph.api.repository.CategoryRepository;
import io.chamaeleon.barterph.api.service.dto.CategoryDTO;
import io.chamaeleon.barterph.api.service.mapper.CategoryMapper;
import lombok.Data;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@Data
public class CategoryService {
  private CategoryMapper categoryMapper;
  private CategoryRepository categoryRepository;

  public List<CategoryDTO> getAllCategories() {
    return categoryRepository.findAll()
      .stream()
      .map(categoryMapper::toCategoryDTO)
      .collect(Collectors.toList());
  }

  public List<CategoryDTO> getAllByProfession(String profession) {
    return categoryRepository.findAllByProfession(profession).stream()
      .map(categoryMapper::toCategoryDTO)
      .collect(Collectors.toList());
  }
}
