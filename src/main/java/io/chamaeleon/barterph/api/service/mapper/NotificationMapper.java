package io.chamaeleon.barterph.api.service.mapper;

import com.google.common.base.MoreObjects;
import io.chamaeleon.barterph.api.domain.Barter;
import io.chamaeleon.barterph.api.domain.Notification;
import io.chamaeleon.barterph.api.service.dto.*;
import io.chamaeleon.barterph.api.util.SpringUtil;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;


@Data
@Service
public class NotificationMapper {

  private final SpringUtil springUtil;

  private final UserMapper userMapper;

  public String[] getTitleAndSubtitle(Notification notification){
    String[] result = new String[2];
    switch (notification.getType()){
      case BARTER_COMMENT:
        result[0] = springUtil.getMessage("notification.BARTER_COMMENT.title", new Object[]{notification.getSender().getDisplayName()});
        result[1] = springUtil.getMessage("notification.BARTER_COMMENT.subtitle", new Object[]{notification.getSender().getDisplayName(), notification.getBarter().getItem()});
        break;
      case BARTER_OFFER:
        result[0] = springUtil.getMessage("notification.BARTER_OFFER.title", new Object[]{notification.getSender().getDisplayName()});
        result[1] = springUtil.getMessage("notification.BARTER_OFFER.subtitle", new Object[]{notification.getSender().getDisplayName(), notification.getBarter().getItem()});
        break;
      case BARTER_OFFER_REPLY:
        if(Objects.equals(notification.getReceiver().getId(), notification.getBarter().getUser().getId())){
          result[0] = springUtil.getMessage("notification.BARTER_OFFER_REPLY.barter-owner.title", new Object[]{notification.getSender().getDisplayName()});
          result[1] = springUtil.getMessage("notification.BARTER_OFFER_REPLY.barter-owner.subtitle", new Object[]{notification.getSender().getDisplayName(), notification.getBarter().getItem()});
        } else {
          result[0] = springUtil.getMessage("notification.BARTER_OFFER_REPLY.offer-owner.title", new Object[]{notification.getSender().getDisplayName()});
          result[1] = springUtil.getMessage("notification.BARTER_OFFER_REPLY.offer-owner.subtitle", new Object[]{notification.getSender().getDisplayName(), notification.getBarter().getItem()});
        }
        break;
      case BARTER_COMMENT_REPLY:
        result[0] = springUtil.getMessage("notification.BARTER_COMMENT_REPLY.title", new Object[]{notification.getSender().getDisplayName()});
        result[1] = springUtil.getMessage("notification.BARTER_COMMENT_REPLY.subtitle", new Object[]{notification.getSender().getDisplayName(), notification.getBarter().getItem()});
        break;
      case BARTER_REMINDER:
        Barter barter = notification.getBarter();
        long daysDiff = ChronoUnit.DAYS.between(MoreObjects.firstNonNull(barter.getDateReposted(), barter.getDatePosted()), LocalDateTime.now());
        result[0] = springUtil.getMessage("notification.BARTER_REMINDER.title", new Object[]{barter.getItem(), daysDiff});
        result[1] = springUtil.getMessage("notification.BARTER_REMINDER.subtitle", new Object[]{barter.getUser().getDisplayName(), daysDiff, barter.getItem()});
        break;
      default:
        break;
    }
    return result;
  }

  public NotificationDTO toNotificationDTO(Notification notification){
    String[] titleAndSubTitle = getTitleAndSubtitle(notification);
    return NotificationDTO.builder()
      .barter(BarterDTO.builder()
        .id(notification.getBarter().getId())
        .item(notification.getBarter().getItem())
        .status(notification.getBarter().getStatus())
        .build()
      ).datePosted(notification.getDatePosted())
      .barterComment(notification.getBarterComment() != null ? BarterCommentDTO.builder()
        .id(notification.getBarterComment().getId())
        .comment(notification.getBarterComment().getComment())
        .build() : null)
      .barterOffer(notification.getBarterOffer() != null ? BarterOfferDTO.builder()
        .id(notification.getBarterOffer().getId())
        .description(notification.getBarterOffer().getDescription())
        .build() : null)
      .isRead(notification.isRead())
      .id(notification.getId())
      .receiver(userMapper.toUserDTOMinimal(notification.getReceiver()))
      .sender(notification.getSender() != null ? userMapper.toUserDTOMinimal(notification.getSender()) : null)
      .title(titleAndSubTitle[0])
      .subTitle(titleAndSubTitle[1])
      .type(notification.getType())
      .build();
  }
}
