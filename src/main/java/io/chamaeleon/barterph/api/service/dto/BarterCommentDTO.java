package io.chamaeleon.barterph.api.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder(toBuilder = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BarterCommentDTO {

  private Long id;

  private UserDTO user;

  private BarterDTO barter;

  private String comment;

  private List<BarterCommentDTO> replies;

  private long repliesCount;

  private LocalDateTime datePosted;

  private Long commentCount;

  private Long offerCount;
}
