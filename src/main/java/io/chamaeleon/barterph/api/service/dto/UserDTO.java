package io.chamaeleon.barterph.api.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.chamaeleon.barterph.api.domain.Authority;
import io.chamaeleon.barterph.api.domain.User;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A DTO representing a user, with his authorities.
 */
@Data
@Builder
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDTO {

    private Long id;

    @Size(max = 50)
    private String firstName;

    @Size(max = 50)
    private String lastName;

    @Size(max = 50)
    private String middleName;

    @Email
    @Size(min = 5, max = 254)
    private String email;

    @Size(max = 20)
    private String mobileNo;

    @Size(max = 256)
    private String imageUrl;

    @Size(max=50)
    private String fbId;

    @Size(max=50)
    private String googleId;

    @Builder.Default
    private boolean activated = true;

    @JsonIgnore
    private String createdBy;

    private LocalDateTime createdDate;

    @JsonIgnore
    private String lastModifiedBy;

    private LocalDateTime lastModifiedDate;

    @JsonIgnore
    private Set<String> authorities;

    private LocationDTO location;

    private Long unreadNotifsCount;

    private String profession;

    public UserDTO() {
        // Empty constructor needed for Jackson.
    }

    public UserDTO(User user) {
        this.id = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.middleName = user.getMiddleName();
        this.email = user.getEmail();
        this.mobileNo = user.getMobileNo();
        this.activated = user.isActivated();
        this.imageUrl = user.getImageUrl();
        this.fbId = user.getFbId();
        this.googleId = user.getGoogleId();
        this.createdBy = user.getCreatedBy();
        this.createdDate = user.getCreatedDate();
        this.lastModifiedBy = user.getLastModifiedBy();
        this.lastModifiedDate = user.getLastModifiedDate();
        this.authorities = user.getAuthorities().stream()
            .map(Authority::getName)
            .collect(Collectors.toSet());
        this.location = Optional.ofNullable(user.getLocation())
            .map(LocationDTO::new).orElse(null);
        this.profession = user.getProfession();
    }
}
