package io.chamaeleon.barterph.api.service;

import io.chamaeleon.barterph.api.config.Constants;
import io.chamaeleon.barterph.api.domain.*;
import io.chamaeleon.barterph.api.repository.BarterOfferRepository;
import io.chamaeleon.barterph.api.service.dto.BarterOfferDTO;
import io.chamaeleon.barterph.api.service.dto.PagedList;
import io.chamaeleon.barterph.api.service.mapper.BarterOfferMapper;
import io.chamaeleon.barterph.api.specification.BarterOfferSpecs;
import io.chamaeleon.barterph.api.util.SpringUtil;
import io.chamaeleon.barterph.api.web.rest.errors.ApiErrorCode;
import io.chamaeleon.barterph.api.web.rest.errors.ApiException;
import io.chamaeleon.barterph.api.web.rest.vm.AddOfferVM;
import io.chamaeleon.barterph.api.web.rest.vm.SearchOfferVM;
import lombok.Data;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@Data
public class OfferService {
  private static final Logger log = LoggerFactory.getLogger(OfferService.class);

  private final BarterOfferRepository barterOfferRepository;

  private final BarterOfferMapper barterOfferMapper;

  private final UserService userService;

  private final BarterService barterService;

  private final NotificationService notificationService;

  private final SpringUtil springUtil;

  private final FileUploadService fileUploadService;

  public BarterOfferDTO saveBarterOffer(AddOfferVM vm){
    Barter barter = barterService.findOneById(vm.getBarterId())
      .orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.BARTER_NOT_FOUND)));

    User user = userService.getCurrentUser()
      .orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.ACCOUNT_NOT_FOUND)));

    BarterOffer offer = BarterOffer.builder()
      .barter(barter)
      .item(ObjectUtils.firstNonNull(vm.getItem(), vm.getNote()))
      .description(vm.getDescription())
      .owner(user)
      .photos(barterOfferMapper.photosFromString(vm.getPhotos()))
      .build();

    BarterOffer finalOffer = offer;
    finalOffer.getPhotos().forEach(photo -> {
      photo.setOffer(finalOffer);
      uploadBarterPhotos(photo.getPhotoUrl());
    });

    offer = barterOfferRepository.save(offer);
    notificationService.buildAndSendOfferNotification(offer);
    return barterOfferMapper.toBarterOfferDTO(offer);
  }

  public PagedList<BarterOfferDTO> getOffers(SearchOfferVM vm){
    Specification<BarterOffer> specs = Specification
      .where(BarterOfferSpecs.isBarterEqualTo(vm.getBarterId()))
      .and(BarterOfferSpecs.isUserEqualTo(vm.getUserId()));

    Sort sort = BarterOfferSpecs.buildSortObject(vm.getSort());
    PagedList<BarterOfferDTO> pagedList = new PagedList<>();

    List<BarterOffer> list;
    long totalCount = 0;
    if(vm.getPageSize() != null){
      Pageable pageable = PageRequest.of(vm.getPage() - 1, vm.getPageSize(), sort);
      Page paged =  barterOfferRepository.findAll(specs, pageable);
      list = paged.getContent();
      totalCount = paged.getTotalElements();
    } else {
      list = barterOfferRepository.findAll(specs, sort);
      totalCount = list.size();
    }

    List<BarterOfferDTO> dtos = list.stream()
      .map(b -> barterOfferMapper.toBarterOfferDTO(b))
      .collect(Collectors.toList());
    pagedList.setList(dtos);
    pagedList.setPage(vm.getPage() == null ? 1 : vm.getPage());
    pagedList.setPageSize(vm.getPageSize() == null ? totalCount : vm.getPageSize());
    pagedList.setTotalCount(totalCount);
    return pagedList;
  }

  public Optional<BarterOffer> getOfferById(Long id){
    return barterOfferRepository.findById(id);
  }

  public long countOfferByBarterId(long barterId){
    return barterOfferRepository.countByBarterId(barterId);
  }

  private void uploadBarterPhotos(String photoUrl){
    photoUrl = photoUrl.replaceAll(Constants.OFFER_PHOTOS_URL, "");
    fileUploadService.transferFromTempToOffer(photoUrl);
  }
}
