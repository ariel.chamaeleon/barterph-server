package io.chamaeleon.barterph.api.service;

import io.chamaeleon.barterph.api.config.Constants;
import io.chamaeleon.barterph.api.domain.*;
import io.chamaeleon.barterph.api.repository.*;
import io.chamaeleon.barterph.api.security.AuthoritiesConstants;
import io.chamaeleon.barterph.api.security.SecurityUtils;
import io.chamaeleon.barterph.api.service.dto.SocialUserDTO;
import io.chamaeleon.barterph.api.service.dto.UserDTO;
import io.chamaeleon.barterph.api.service.mapper.LocationMapper;
import io.chamaeleon.barterph.api.util.*;
import io.chamaeleon.barterph.api.web.rest.errors.ApiErrorCode;
import io.chamaeleon.barterph.api.web.rest.errors.ApiException;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service class for managing users.
 */
@Service
@Transactional
@AllArgsConstructor
public class UserService {

  private static final Logger log = LoggerFactory.getLogger(UserService.class);

  private final UserRepository userRepository;

  private final PasswordEncoder passwordEncoder;

  private final AuthorityRepository authorityRepository;

  private final CacheManager cacheManager;

  private final SpringUtil springUtil;

  private final LocationMapper locationMapper;

  private final LocationRepository locationRepository;

  private final FileUploadService fileUploadService;

  public Optional<User> activateRegistration(String mobileNo, String key) {
    log.debug("Activating user for activation key {}", key);
    return userRepository.findOneByMobileNoAndActivationKey(mobileNo, key)
      .map(user -> {
        // activate given user for the registration key.
        user.setActivated(true);
        user.setActivationKey(null);
        user.setActivationDate(LocalDateTime.now());
        this.clearUserCaches(user);
        log.debug("Activated user: {}", user);
        return user;
      });
  }

  public Optional<User> completePasswordReset(String newPassword, String key, String mobileNo) {
    log.debug("Reset user password for reset key {}", key);
    return userRepository.findOneByMobileNoAndActivationKey(mobileNo, key)
      .filter(User::isActivated)
      .map(user -> {
        user.setPassword(passwordEncoder.encode(newPassword));
        user.setActivationKey(null);
        this.clearUserCaches(user);
        return user;
      });
  }

  public Optional<User> requestPasswordReset(String mobileNo) {
    User user = userRepository.findOneByMobileNo(mobileNo).orElse(null);
    if (!user.isActivated()) {
      throw new ApiException(springUtil.buildApiError(ApiErrorCode.ACCOUNT_NOT_ACTIVATED));
    }
    if (Objects.nonNull(user.getFbId()) || Objects.nonNull(user.getGoogleId())) {
      throw new ApiException(springUtil.buildApiError(ApiErrorCode.INVALID_FOR_FB_AND_GOOGLE));
    }
    return Optional.of(user).map(u -> {
      u.setActivationKey(RandomUtil.generateResetKey());
      this.clearUserCaches(u);
      return u;
    });
  }

  public User loginByMobilNo(String mobileNo, String password) {
    return userRepository.findOneByMobileNo(mobileNo)
      .filter(u -> passwordEncoder.matches(password, u.getPassword()))
      .orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.AUTH_ACCOUNT_NOT_FOUND)));
  }

  public Optional<User> findUserByMobileNo(String mobileNo) {
    return userRepository.findOneByMobileNo(mobileNo);
  }

  public User findUserById(Long id) {
    return userRepository.findOneById(id).orElse(null);
  }

  public User findUserByFacebookId(String fbId) {
    return userRepository.findOneByFbId(fbId).orElse(null);
  }

  public User findUserByGoogleId(String googleId) {
    return userRepository.findOneByGoogleId(googleId).orElse(null);
  }

  public User registerUser(String mobileNo, String password) {
    Optional<User> user = userRepository.findOneByMobileNo(mobileNo);
    user.filter(User::isActivated)
      .ifPresent(u -> {
        throw new ApiException(springUtil.buildApiError(ApiErrorCode.DUPLICATE_MOBILE_NUMBER));
      });

    if(user.isPresent()){
      return user.get();
    }

    Set<Authority> authorities = new HashSet<>();
    authorityRepository.findById(AuthoritiesConstants.USER).ifPresent(authorities::add);

    User newUser = User.builder()
      .mobileNo(mobileNo)
      .password(passwordEncoder.encode(password))
      .activated(false)
      .activationKey(RandomUtil.generateActivationKey())
      .authorities(authorities)
      .build();

    userRepository.save(newUser);
    this.clearUserCaches(newUser);
    log.debug("Created Information for User: {}", newUser);
    return newUser;
  }

  public User registerUser(SocialUserDTO socialUser) {
    Set<Authority> authorities = new HashSet<>();
    authorityRepository.findById(AuthoritiesConstants.USER).ifPresent(authorities::add);

    if(socialUser.getEmail() != null){
      userRepository.findOneByEmail(socialUser.getEmail())
        .ifPresent((u) -> {
          throw new ApiException(springUtil.buildApiError(ApiErrorCode.DUPLICATE_EMAIL));
        });
    }


    User newUser = User.builder()
      .firstName(socialUser.getFirstName())
      .lastName(socialUser.getLastName())
      .email(socialUser.getEmail())
      .imageUrl(socialUser.getImageUrl())
      .activated(true)
      .activationDate(LocalDateTime.now())
      .authorities(authorities)
      .fbId(socialUser.getPlatform() == SocialMediaPlatform.FB ? socialUser.getId() : null)
      .googleId(socialUser.getPlatform() == SocialMediaPlatform.GOOGLE ? socialUser.getId() : null)
      .password(passwordEncoder.encode(RandomUtil.generatePassword()))
      .build();

    newUser = userRepository.save(newUser);
    this.clearUserCaches(newUser);
    log.debug("Created Information for User: {}", newUser);
    return newUser;
  }

  private boolean removeNonActivatedUser(User existingUser) {
    if (existingUser.isActivated()) {
      return false;
    }
    userRepository.delete(existingUser);
    userRepository.flush();
    this.clearUserCaches(existingUser);
    return true;
  }

    /*public User createUser(UserDTO userDTO) {
        User user = new User();
        user.setLogin(userDTO.getLogin().toLowerCase());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        if (userDTO.getEmail() != null) {
            user.setEmail(userDTO.getEmail().toLowerCase());
        }
        user.setImageUrl(userDTO.getImageUrl());
        if (userDTO.getLangKey() == null) {
            user.setLangKey(Constants.DEFAULT_LANGUAGE); // default language
        } else {
            user.setLangKey(userDTO.getLangKey());
        }
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(Instant.now());
        user.setActivated(true);
        if (userDTO.getAuthorities() != null) {
            Set<Authority> authorities = userDTO.getAuthorities().stream()
                .map(authorityRepository::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
            user.setAuthorities(authorities);
        }
        userRepository.save(user);
        this.clearUserCaches(user);
        log.debug("Created Information for User: {}", user);
        return user;
    }*/

  /**
   * Update all information for a specific user, and return the modified user.
   *
   * @param userDTO user to update.
   * @return updated user.
   */
   /* public Optional<UserDTO> updateUser(UserDTO userDTO) {
        return Optional.of(userRepository
            .findById(userDTO.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(user -> {
                this.clearUserCaches(user);
                user.setLogin(userDTO.getLogin().toLowerCase());
                user.setFirstName(userDTO.getFirstName());
                user.setLastName(userDTO.getLastName());
                if (userDTO.getEmail() != null) {
                    user.setEmail(userDTO.getEmail().toLowerCase());
                }
                user.setImageUrl(userDTO.getImageUrl());
                user.setActivated(userDTO.isActivated());
                user.setLangKey(userDTO.getLangKey());
                Set<Authority> managedAuthorities = user.getAuthorities();
                managedAuthorities.clear();
                userDTO.getAuthorities().stream()
                    .map(authorityRepository::findById)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .forEach(managedAuthorities::add);
                this.clearUserCaches(user);
                log.debug("Changed Information for User: {}", user);
                return user;
            })
            .map(UserDTO::new);
    }*/

    /*public void deleteUser(String login) {
        userRepository.findOneByLogin(login).ifPresent(user -> {
            userRepository.delete(user);
            this.clearUserCaches(user);
            log.debug("Deleted User: {}", user);
        });
    }*/

  /**
   * Update basic information (first name, last name, email) for the current user.
   *
   * @param firstName first name of user.
   * @param lastName  last name of user.
   * @param email     email id of user.
   * @param imageUrl  image URL of user.
   */
  public UserDTO updateLoggedInUser(UserDTO userDTO) {
    return SecurityUtils.getCurrentUserLoginId()
      .flatMap(userRepository::findOneById)
      .map(user -> {
        if(Objects.nonNull(userDTO.getFirstName())){
          user.setFirstName(userDTO.getFirstName());
        }

        if(Objects.nonNull(userDTO.getLastName())){
          user.setLastName(userDTO.getLastName());
        }

        if(Objects.nonNull(userDTO.getEmail())){
          userRepository.findOneByEmail(userDTO.getEmail())
            .filter(u -> u.getId() != user.getId())
            .ifPresent(u -> {
              throw new ApiException(springUtil.buildApiError(ApiErrorCode.DUPLICATE_EMAIL));
            });

          user.setEmail(userDTO.getEmail());
        }

        if(Objects.nonNull(userDTO.getLocation())){
          Location location = locationMapper.toLocation(user.getLocation(), userDTO.getLocation());
          locationRepository.save(location);
          user.setLocation(location);
        }

        if(Objects.nonNull(userDTO.getImageUrl())){
          String fileName = userDTO.getImageUrl();
          fileName = fileName.replaceAll(Constants.USER_PHOTOS_URL, "");
          fileUploadService.transferFromTempToUser(fileName);
          String imgUrl = Constants.USER_PHOTOS_URL+fileName;
          updateLoggedInUserImgUrl(imgUrl);
        }

        if(Objects.nonNull(userDTO.getProfession())){
          user.setProfession(userDTO.getProfession());
        }

        this.clearUserCaches(user);
        log.debug("Changed Information for User: {}", user);
        return user;
      })
      .map(UserDTO::new).orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.ACCOUNT_NOT_FOUND)));
  }

  public UserDTO updateLoggedInUserImgUrl(String imgURL) {
    return SecurityUtils.getCurrentUserLoginId()
      .flatMap(userRepository::findOneById)
      .map(user -> {
        user.setImageUrl(imgURL);
        this.clearUserCaches(user);
        log.debug("Changed Image URL for User: {}", user);
        return user;
      })
      .map(UserDTO::new).orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.ACCOUNT_NOT_FOUND)));
  }

  @Transactional
  public void changePassword(String currentClearTextPassword, String newPassword) {
    SecurityUtils.getCurrentUserLoginId()
      .flatMap(userRepository::findOneById)
      .ifPresent(user -> {
        String currentEncryptedPassword = user.getPassword();
        if (!passwordEncoder.matches(currentClearTextPassword, currentEncryptedPassword)) {
          throw new ApiException(springUtil.buildApiError(ApiErrorCode.INCORRECT_PASSWORD));
        }

        String encryptedPassword = passwordEncoder.encode(newPassword);
        user.setPassword(encryptedPassword);
        this.clearUserCaches(user);
        log.debug("Changed password for User: {}", user);
      });
  }

  /*@Transactional(readOnly = true)
  public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
    return userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(UserDTO::new);
  }*/
  /*@Transactional(readOnly = true)
  public Optional<User> getUserWithAuthoritiesByLogin(String login) {
    return userRepository.findOneWithAuthoritiesByLogin(login);
  }*/

  @Transactional(readOnly = true)
  public Optional<User> getCurrentUserWithAuthorities() {
    return SecurityUtils.getCurrentUserLoginId().flatMap(id -> userRepository.findOneWithAuthoritiesById(id));
  }

  @Transactional(readOnly = true)
  public Optional<User> getCurrentUser() {
    return SecurityUtils.getCurrentUserLoginId().flatMap(id -> userRepository.findOneById(id));
  }

  /**
   * Not activated users should be automatically deleted after 3 days.
   * <p>
   * This is scheduled to get fired everyday, at 01:00 (am).
   */
  @Scheduled(cron = "0 0 1 * * ?")
  public void removeNotActivatedUsers() {
    userRepository
      .findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(LocalDateTime.now()
        .minus(3, ChronoUnit.DAYS))
      .forEach(user -> {
        log.debug("Deleting not activated user {}", user.getEmail());
        userRepository.delete(user);
        this.clearUserCaches(user);
      });
  }

  /**
   * Gets a list of all the authorities.
   *
   * @return a list of all the authorities.
   */
  @Transactional(readOnly = true)
  public List<String> getAuthorities() {
    return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
  }


  private void clearUserCaches(User user) {
    if (user.getEmail() != null) {
      Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE)).evict(user.getEmail());
    }
  }
}
