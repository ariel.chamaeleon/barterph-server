/**
 * MapStruct mappers for mapping domain objects and Data Transfer Objects.
 */
package io.chamaeleon.barterph.api.service.mapper;
