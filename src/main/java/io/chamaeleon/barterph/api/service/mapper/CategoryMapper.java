package io.chamaeleon.barterph.api.service.mapper;

import io.chamaeleon.barterph.api.domain.Category;
import io.chamaeleon.barterph.api.service.dto.CategoryDTO;
import org.springframework.stereotype.Service;

@Service
public class CategoryMapper {

  public Category toCategory(CategoryDTO dto){
    if(dto == null) return null;
    return Category.builder()
      .id(dto.getId())
      .name(dto.getName())
      .profession(dto.getProfession())
      .build();
  }

  public CategoryDTO toCategoryDTO(Category category){
    if(category == null) return null;
    return CategoryDTO.builder()
      .id(category.getId())
      .name(category.getName())
      .profession(category.getProfession())
      .build();
  }
}
