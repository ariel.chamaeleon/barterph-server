package io.chamaeleon.barterph.api.service.mapper;

import io.chamaeleon.barterph.api.config.Constants;
import io.chamaeleon.barterph.api.domain.BarterOffer;
import io.chamaeleon.barterph.api.domain.OfferPhoto;
import io.chamaeleon.barterph.api.service.dto.BarterDTO;
import io.chamaeleon.barterph.api.service.dto.BarterOfferDTO;
import lombok.Data;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Data
public class BarterOfferMapper {
  private final BarterMapper barterMapper;

  private final UserMapper userMapper;

  private final BarterCommentMapper barterCommentMapper;

  public BarterOfferDTO toBarterOfferDTO(BarterOffer offer){
    return BarterOfferDTO.builder()
      .description(offer.getDescription())
      .item(offer.getItem())
      .note(offer.getItem())
      .user(userMapper.toUserDTOMinimal(offer.getOwner()))
      .id(offer.getId())
      .datePosted(offer.getCreatedDate())
      .photos(photosToString(offer.getPhotos()))
      .barter(BarterDTO.builder().id(offer.getBarter().getId()).build())
      .repliesCount(CollectionUtils.isEmpty(offer.getReplies()) ? 0 : offer.getReplies().size())
      .build();
  }

  public BarterOfferDTO toBarterOfferDTOWithReplies(BarterOffer offer){
    return BarterOfferDTO.builder()
      .item(offer.getItem())
      .note(offer.getItem())
      .description(offer.getDescription())
      .user(userMapper.toUserDTOMinimal(offer.getOwner()))
      .id(offer.getId())
      .datePosted(offer.getCreatedDate())
      .photos(photosToString(offer.getPhotos()))
      .repliesCount(offer.getReplies().size())
      .barter(BarterDTO.builder().id(offer.getBarter().getId()).build())
      .replies(offer.getReplies().stream()
        .map(r -> barterCommentMapper.toBarterCommentDTO(r))
        .collect(Collectors.toList())
      )
      .build();
  }

  public List<OfferPhoto> photosFromString(List<String> strPhotos){
    if(CollectionUtils.isEmpty(strPhotos)) return Collections.EMPTY_LIST;
    return strPhotos.stream().map(s -> OfferPhoto.builder()
      .photoUrl(s.startsWith(Constants.OFFER_PHOTOS_URL) ? s : Constants.OFFER_PHOTOS_URL + s)
      .build()).collect(Collectors.toList());
  }

  public List<String>  photosToString(List<OfferPhoto> offerPhotos){
    if(CollectionUtils.isEmpty(offerPhotos)) return Collections.EMPTY_LIST;
    return offerPhotos.stream().map(b -> b.getPhotoUrl()).collect(Collectors.toList());
  }
}
