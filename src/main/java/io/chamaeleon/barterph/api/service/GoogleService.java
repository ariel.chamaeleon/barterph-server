package io.chamaeleon.barterph.api.service;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import io.chamaeleon.barterph.api.service.dto.SocialUserDTO;
import io.chamaeleon.barterph.api.util.SocialMediaPlatform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.SQLOutput;
import java.util.Collections;

@Service
public class GoogleService {
  private static final Logger log = LoggerFactory.getLogger(GoogleService.class);

  @Value("${spring.social.google.appId}")
  private String googleAppId;

  public SocialUserDTO getGoogleUserProfile(String idTokenStr) {
    if(idTokenStr == null) return null;
    SocialUserDTO user = null;
    log.info("ID Token: "+idTokenStr);
    log.info("Gooogle App Id: "+googleAppId);
    try {
      HttpTransport transport = GoogleNetHttpTransport.newTrustedTransport();
      JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
      GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
        .setAudience(Collections.singletonList(googleAppId))
        .build();

      System.out.println(verifier);

      GoogleIdToken idToken = verifier.verify(idTokenStr);
      if(idToken != null){
        GoogleIdToken.Payload payload = idToken.getPayload();

        // Print user identifier
        String userId = payload.getSubject();
        log.info("Google User ID: " + userId);

        user = new SocialUserDTO();
        user.setFirstName((String) payload.get("given_name"));
        user.setLastName((String) payload.get("family_name"));
        user.setAlias((String) payload.get("given_name"));
        user.setEmail(payload.getEmail());
        user.setImageUrl((String) payload.get("picture"));
        user.setId(userId);
        user.setPlatform(SocialMediaPlatform.GOOGLE);
      } else {
        log.error("Invalid ID Token: "+idTokenStr);
      }
    } catch (Exception e) {
      log.error("Error validating google access: ", e);
    }
    return user;
  }
}
