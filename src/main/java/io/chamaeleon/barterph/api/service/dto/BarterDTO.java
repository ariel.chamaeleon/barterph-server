package io.chamaeleon.barterph.api.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.chamaeleon.barterph.api.util.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BarterDTO {

  private Long id;

  private UserDTO user;

  private String item;

  private String description;

  private BarterLocationType locationType;

  private LocationDTO location;

  private DeliveryType deliveryType;

  private String reason;

  private String courier;

  private String preferences;

  private String contactNumber;

  private String contactNumber2;

  private LocalDateTime datePosted;

  private BarterStatus status;

  private LocalDateTime lastUpdateDate;

  private String tradedFor;

  private UserDTO tradedTo;

  private Long viewCount;

  private Long commentCount;

  private Long offerCount;

  private LocalDateTime dateReposted;

  private CategoryDTO category;

  @JsonProperty("photos")
  private List<String> photos;

  @JsonProperty("tags")
  private List<String> tags;
}
