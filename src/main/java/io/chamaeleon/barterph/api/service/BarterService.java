package io.chamaeleon.barterph.api.service;

import io.chamaeleon.barterph.api.config.Constants;
import io.chamaeleon.barterph.api.domain.*;
import io.chamaeleon.barterph.api.repository.*;
import io.chamaeleon.barterph.api.service.dto.BarterDTO;
import io.chamaeleon.barterph.api.service.dto.PagedList;
import io.chamaeleon.barterph.api.service.mapper.*;
import io.chamaeleon.barterph.api.specification.BarterSpecs;
import io.chamaeleon.barterph.api.util.BarterStatus;
import io.chamaeleon.barterph.api.util.SpringUtil;
import io.chamaeleon.barterph.api.web.rest.errors.ApiErrorCode;
import io.chamaeleon.barterph.api.web.rest.errors.ApiException;
import io.chamaeleon.barterph.api.web.rest.vm.*;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class BarterService {

  private static final Logger log = LoggerFactory.getLogger(BarterService.class);

  private final UserService userService;

  private final UserRepository userRepository;

  private final BarterRepository barterRepository;

  private final LocationMapper locationMapper;

  private final BarterMapper barterMapper;

  private final SpringUtil springUtil;

  private final FileUploadService fileUploadService;

  private final CategoryMapper categoryMapper;

  private final CategoryRepository categoryRepository;

  public Optional<BarterDTO> createBarter(PostBarterVM input){
    Barter barter = Barter.builder()
      .barterPhotos(barterMapper.photosFromString(input.getPhotos()))
      .contactNumber(input.getContactNumber())
      .datePosted(LocalDateTime.now())
      .item(input.getItem())
      .description(input.getDescription())
      .lastUpdateDate(LocalDateTime.now())
      .location(locationMapper.toLocation(input.getLocation()))
      .locationType(input.getLocationType())
      .deliveryType(input.getDeliveryType())
      .courier(input.getCourier())
      .preferences(input.getPreferences())
      .reason(input.getReason())
      .user(userService.getCurrentUser().orElse(null))
      .status(BarterStatus.ACTIVE)
      .remindInDays(Constants.DEFAULT_REMIND_IN_DAYS)
      .nextRemindDate(LocalDateTime.now().plusDays(Constants.DEFAULT_REMIND_IN_DAYS))
      .category(categoryRepository.findById(input.getCategoryId()).orElse(null))
      .build();

    Barter finalBarter = barter;
    barter.getBarterPhotos().forEach(photo -> {
      photo.setBarter(finalBarter);
      uploadBarterPhotos(photo.getPhotoUrl());
    });

    barter = barterRepository.save(barter);
    return Optional.of(barterMapper.toBarterDTO(barter));
  }

  public void cancelBarter(long id){
    Barter barter = throwIfInvalidBarter(id);
    barter.setStatus(BarterStatus.CANCELLED);
    barter.setLastUpdateDate(LocalDateTime.now());
    barterRepository.save(barter);
  }

  public Optional<BarterDTO> closeBarter(CloseBarterVM vm){
    Barter barter = throwIfInvalidBarter(vm.getId());
    if(vm.getTradedTo() != null){
      User tradedTo = userRepository.findOneById(vm.getTradedTo())
        .orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.TRADE_PARTNER_NOT_FOUND)));

      barter.setTradedTo(tradedTo);
    }

    barter.setStatus(BarterStatus.COMPLETED);
    barter.setTradedFor(vm.getTradedFor());
    barter.setLastUpdateDate(LocalDateTime.now());
    return Optional.ofNullable(barterRepository.save(barter))
      .map(b -> barterMapper.toBarterDTO(b));
  }

  public Optional<BarterDTO> repostBarter(long barterId){
    Barter barter = throwIfInvalidBarter(barterId);
    barter.setDateReposted(LocalDateTime.now());
    return Optional.ofNullable(barterRepository.save(barter))
      .map(b -> barterMapper.toBarterDTO(b));
  }

  public Optional<BarterDTO> setBarterReminderSettings(long barterId, int noOfDays){
    Barter barter = throwIfInvalidBarter(barterId);
    barter.setLastUpdateDate(LocalDateTime.now());
    barter.setRemindInDays(noOfDays);
    barter.setNextRemindDate(LocalDateTime.now().plusDays(noOfDays));
    return Optional.ofNullable(barterRepository.save(barter))
      .map(b -> barterMapper.toBarterDTO(b));
  }

  public Optional<BarterDTO> updateBarter(UpdateBarterVM vm){
    Barter barter = throwIfInvalidBarter(vm.getId());
    barter.setLastUpdateDate(LocalDateTime.now());
    if(vm.getItem() != null){
      barter.setItem(vm.getItem());
    }

    if(vm.getDescription() != null){
      barter.setDescription(vm.getDescription());
    }

    if(vm.getLocationType() != null){
      barter.setLocationType(vm.getLocationType());
    }

    if(vm.getLocation() != null){
      barter.setLocation(locationMapper.toLocation(vm.getLocation()));
    }

    if(vm.getDeliveryType() != null){
      barter.setDeliveryType(vm.getDeliveryType());
    }

    if(vm.getCourier() != null){
      barter.setCourier(vm.getCourier());
    }

    if(vm.getReason() != null){
      barter.setReason(vm.getReason());
    }

    if(vm.getPreferences() != null){
      barter.setPreferences(vm.getPreferences());
    }

    if(!CollectionUtils.isEmpty(vm.getPhotos())){
      barter.getBarterPhotos().clear();
      barter.getBarterPhotos().addAll(barterMapper.photosFromString(vm.getPhotos()));
      Barter finalBarter = barter;
      finalBarter.getBarterPhotos().forEach(photo -> {
        photo.setBarter(finalBarter);
        uploadBarterPhotos(photo.getPhotoUrl());
      });
    }

    return Optional.ofNullable(barterRepository.save(barter))
      .map(b -> barterMapper.toBarterDTO(b));
  }

  public Optional<Barter> findOneById(Long id){
    return barterRepository.findOneWithBarterPhotosById(id);
  }

  public PagedList<BarterDTO> getBarters(SearchBarterVM vm){
    Specification<Barter> specs = Specification.<Barter>where((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(Barter_.ID), root.get(Barter_.ID)))
      .and(BarterSpecs.isUserEqualTo(vm.getUserId()))
      .and(BarterSpecs.isStatusEqualTo(vm.getStatus()))
      .and(BarterSpecs.searchKey(vm.getKey()))
      .and(BarterSpecs.withComments(vm.withCommentsToBoolean()))
      .and(BarterSpecs.withOffers(vm.withOffersToBoolean()))
      .and(BarterSpecs.orderBy(vm.getSort()));

    Sort sort = createGetBartersSortObject(vm.getSort());
    PagedList<BarterDTO> pagedList = new PagedList<>();
    if(vm.getPageSize() != null){
      Pageable pageable = PageRequest.of(vm.getPage() - 1, vm.getPageSize(), Sort.unsorted());
      Page paged =  barterRepository.findAll(specs, pageable);

      List<BarterDTO> barterDTOS = (List<BarterDTO>) paged.getContent().stream()
        .map(b -> barterMapper.toBarterDTO((Barter) b))
        .collect(Collectors.toList());
      pagedList.setList(barterDTOS);
      pagedList.setPage(vm.getPage() == null ? 1 : vm.getPage());
      pagedList.setPageSize(vm.getPageSize() == null ? paged.getTotalElements() : vm.getPageSize());
      pagedList.setTotalCount(paged.getTotalElements());
    } else {
      List<BarterDTO> barterDTOS = barterRepository.findAll(specs, Sort.unsorted()).stream()
        .map(b -> barterMapper.toBarterDTO(b))
        .collect(Collectors.toList());
      pagedList.setList(barterDTOS);
      pagedList.setPage(vm.getPage() == null ? 1 : vm.getPage());
      pagedList.setPageSize(vm.getPageSize() == null ? barterDTOS.size() : vm.getPageSize());
      pagedList.setTotalCount(barterDTOS.size());
    }
    return pagedList;
  }
  
  private Sort createGetBartersSortObject(Integer sort){
    Sort sortObject = Sort.by(Sort.Order.asc(Barter_.ID));
    if(sort == null){
    } else if(sort == 1){
      sortObject = Sort.by(Sort.Order.asc(Barter_.DATE_POSTED));
    } else if(sort == 2){
      sortObject = Sort.by(Sort.Order.desc(Barter_.DATE_POSTED));
    } else if(sort == 3){
      sortObject = Sort.by(Sort.Order.asc(Barter_.VIEW_COUNT));
    } else if(sort == 4){
      sortObject = Sort.by(Sort.Order.desc(Barter_.VIEW_COUNT));
    }
    return sortObject;
  }

  private void uploadBarterPhotos(String photoUrl){
    photoUrl = photoUrl.replaceAll(Constants.BARTER_PHOTOS_URL, "");
    fileUploadService.transferFromTempToBarter(photoUrl);
  }

  private Barter throwIfInvalidBarter(long barterId){
    User currentUser =  userService.getCurrentUser()
      .orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.ACCOUNT_NOT_FOUND)));

    return barterRepository.findById(barterId)
      .filter(b -> b.getUser().getId() == currentUser.getId())
      .filter(b -> b.getStatus() == BarterStatus.ACTIVE)
      .orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.BARTER_NOT_FOUND)));
  }
}
