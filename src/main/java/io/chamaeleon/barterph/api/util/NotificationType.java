package io.chamaeleon.barterph.api.util;

public enum NotificationType {
  BARTER_OFFER,
  BARTER_COMMENT,
  BARTER_OFFER_REPLY,
  BARTER_COMMENT_REPLY,
  BARTER_REMINDER;
}
