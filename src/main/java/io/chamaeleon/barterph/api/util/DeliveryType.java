package io.chamaeleon.barterph.api.util;

public enum DeliveryType {
  MEETUP,
  COURIER
}
