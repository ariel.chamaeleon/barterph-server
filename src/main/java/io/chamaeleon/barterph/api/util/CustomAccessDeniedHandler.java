package io.chamaeleon.barterph.api.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.chamaeleon.barterph.api.config.Constants;
import io.chamaeleon.barterph.api.util.SpringUtil;
import io.chamaeleon.barterph.api.web.rest.errors.ApiError;
import io.chamaeleon.barterph.api.web.rest.errors.ApiErrorCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Data
@Component
@AllArgsConstructor
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

  private final SpringUtil springUtil;

  private final ObjectMapper objectMapper;

  @Override
  public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {
    PrintWriter writer = response.getWriter();
    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    response.setHeader(Constants.HTTP_HEADER_CONTENT_TYPE, Constants.HTTP_CONTENT_TYPE_JSON);
    writer.println(objectMapper.writeValueAsString(springUtil.buildApiError(ApiErrorCode.ACCESS_DENIED)));
    writer.close();
  }
}
