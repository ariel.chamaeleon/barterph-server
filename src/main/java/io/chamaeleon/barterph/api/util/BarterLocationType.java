package io.chamaeleon.barterph.api.util;

public enum BarterLocationType {
  STRICT,
  OPEN
}
