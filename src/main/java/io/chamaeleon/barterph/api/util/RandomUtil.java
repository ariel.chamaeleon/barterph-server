package io.chamaeleon.barterph.api.util;

import org.apache.commons.lang3.RandomStringUtils;

import java.security.SecureRandom;

public class RandomUtil {
  private static final SecureRandom SECURE_RANDOM = new SecureRandom();

  private RandomUtil() {
  }

  public static String generateRandomAlphanumericString() {
    return RandomStringUtils.random(20, 0, 0, true, true, (char[])null, SECURE_RANDOM);
  }

  public static String generateRandomNumericString(int len) {
    return RandomStringUtils.random(len, 0, 0, false, true, (char[])null, SECURE_RANDOM);
  }

  public static String generatePassword() {
    return generateRandomAlphanumericString();
  }

  public static String generateActivationKey() {
    //return generateRandomNumericString(6);
    return "123456";
  }

  public static String generateResetKey() {
    //return generateRandomNumericString(6);
    return "123456";
  }

  static {
    SECURE_RANDOM.nextBytes(new byte[64]);
  }
}
