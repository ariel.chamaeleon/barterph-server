package io.chamaeleon.barterph.api.util;

import io.chamaeleon.barterph.api.web.rest.errors.ApiError;
import io.chamaeleon.barterph.api.web.rest.errors.ApiErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.*;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class SpringUtil implements ApplicationContextAware, MessageSourceAware{

	private static final Logger log = LoggerFactory.getLogger(SpringUtil.class);
	private MessageSource messageSource;
	private ApplicationContext appContext;
	
	public String getMessage(String code, Object[] params){
		if(params == null) params = new Object[]{};
		String message = null;
		try {
			message = messageSource.getMessage(code, params, LocaleContextHolder.getLocale());
		} catch (NoSuchMessageException e) {
			log.debug("Message not found ", e);
		}
		return message;
	}
	
	public ApiError buildApiError(ApiErrorCode apiErrorCode){
		return new ApiError(apiErrorCode, getMessage("error."+apiErrorCode.getCode(), null));
	}
	
	public ApiError buildApiError(ApiErrorCode apiErrorCode, Object[] params){
		return new ApiError(apiErrorCode, getMessage("error."+apiErrorCode.getCode(), params));
	}

	public String getProperty(String name){
		return appContext.getEnvironment().getProperty(name);
	}
	
	@Override
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		appContext = arg0;
	}

	@Override
	public void setMessageSource(MessageSource arg0) {
		messageSource = arg0;
	}
}
