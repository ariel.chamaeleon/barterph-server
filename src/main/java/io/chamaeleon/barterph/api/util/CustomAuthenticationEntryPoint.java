package io.chamaeleon.barterph.api.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.chamaeleon.barterph.api.config.Constants;
import io.chamaeleon.barterph.api.util.SpringUtil;
import io.chamaeleon.barterph.api.web.rest.errors.ApiErrorCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Data
@Component
@AllArgsConstructor
public class CustomAuthenticationEntryPoint extends BasicAuthenticationEntryPoint {

  private final SpringUtil springUtil;

  private final ObjectMapper objectMapper;

  @Override
  public void commence(
    HttpServletRequest request, HttpServletResponse response,
    AuthenticationException authEx) throws IOException {

    response.addHeader(Constants.HTTP_HEADER_WWW_AUTHENTICATE, Constants.HTTP_HEADER_BASIC_REALM + getRealmName());
    response.setHeader(Constants.HTTP_HEADER_CONTENT_TYPE, Constants.HTTP_CONTENT_TYPE_JSON);
    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    PrintWriter writer = response.getWriter();
    writer.println(objectMapper.writeValueAsString(springUtil.buildApiError(ApiErrorCode.UNAUTHORIZED)));
    writer.close();
  }

  @Override
  public void afterPropertiesSet() {
    setRealmName(Constants.REALM_NAME);
    super.afterPropertiesSet();
  }
}
