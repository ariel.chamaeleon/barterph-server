package io.chamaeleon.barterph.api.util;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

public class WebUtil {
  public static final String extractDeviceId(HttpServletRequest httpRequest){
    return Optional.ofNullable(httpRequest.getHeader("DeviceId")).orElse(null);
  }
}
