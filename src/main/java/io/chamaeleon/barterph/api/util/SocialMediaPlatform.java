package io.chamaeleon.barterph.api.util;

public enum SocialMediaPlatform {
  FB,
  GOOGLE
}
