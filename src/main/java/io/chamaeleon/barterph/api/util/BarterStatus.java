package io.chamaeleon.barterph.api.util;

public enum BarterStatus {
  ACTIVE,
  CANCELLED,
  COMPLETED
}
