package io.chamaeleon.barterph.api.security;

import io.chamaeleon.barterph.api.domain.User;
import io.chamaeleon.barterph.api.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Authenticate a user from the database.
 */
@Component(value = "userDetailsService")
public class DomainUserDetailsService implements UserDetailsService {

    private final Logger log = LoggerFactory.getLogger(DomainUserDetailsService.class);

    private final UserRepository userRepository;

    public DomainUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String mobileNo) {
        log.debug("Authenticating {}", mobileNo);
        System.out.println("IM HERE!!!");
        return userRepository.findOneWithAuthoritiesByMobileNo(mobileNo)
            .map(user -> createSpringSecurityUser(mobileNo, user))
            .orElseThrow(() -> new UsernameNotFoundException("User " + mobileNo + " was not found in the database"));

    }

    private org.springframework.security.core.userdetails.User createSpringSecurityUser(String mobileNo, User user) {
        if (!user.isActivated()) {
            throw new UserNotActivatedException("User " + mobileNo + " was not activated");
        }
        List<GrantedAuthority> grantedAuthorities = user.getAuthorities().stream()
            .map(authority -> new SimpleGrantedAuthority(authority.getName()))
            .collect(Collectors.toList());
        return new org.springframework.security.core.userdetails.User(user.getMobileNo(),
            user.getPassword(),
            grantedAuthorities);
    }
}
