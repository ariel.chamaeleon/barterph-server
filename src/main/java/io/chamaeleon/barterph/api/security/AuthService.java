package io.chamaeleon.barterph.api.security;

import io.chamaeleon.barterph.api.domain.Authority;
import io.chamaeleon.barterph.api.security.jwt.TokenProvider;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class AuthService {
  private final Logger log = LoggerFactory.getLogger(AuthService.class);

  private final TokenProvider tokenProvider;

  public String buildJWTToken(Authentication authentication, boolean rememberMe){
    SecurityContextHolder.getContext().setAuthentication(authentication);
    return tokenProvider.createToken(authentication, rememberMe);
  }

  public List<GrantedAuthority> convertAuthority(Collection<Authority> authorities){
    return authorities.stream()
      .map(authority -> new SimpleGrantedAuthority(authority.getName()))
      .collect(Collectors.toList());
  }
}
