package io.chamaeleon.barterph.api.web.rest;

import io.chamaeleon.barterph.api.config.Constants;
import io.chamaeleon.barterph.api.domain.User;
import io.chamaeleon.barterph.api.security.AuthService;
import io.chamaeleon.barterph.api.security.jwt.JWTFilter;
import io.chamaeleon.barterph.api.service.*;
import io.chamaeleon.barterph.api.service.dto.SocialUserDTO;
import io.chamaeleon.barterph.api.service.dto.UserDTO;
import io.chamaeleon.barterph.api.util.SpringUtil;
import io.chamaeleon.barterph.api.util.WebUtil;
import io.chamaeleon.barterph.api.web.rest.errors.EmailAlreadyUsedException;
import io.chamaeleon.barterph.api.web.rest.errors.InvalidPasswordException;
import io.chamaeleon.barterph.api.web.rest.errors.*;
import io.chamaeleon.barterph.api.web.rest.vm.*;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class AccountResource {

  private final Logger log = LoggerFactory.getLogger(AccountResource.class);

  private final UserService userService;

  private final SemaphoreService semaphoreService;

  private final FacebookService facebookService;

  private final GoogleService googleService;

  private final AuthService authService;

  private final SpringUtil springUtil;

  private final FileUploadService fileUploadService;

  private final UserAuthService userAuthService;

  private final NotificationService notificationService;

  private final MailService mailService;

  /**
   * {@code POST  /register} : register the user.
   *
   * @param registerVM the register View Model.
   */
  @PostMapping("/register")
  public void registerAccount(@Valid @ModelAttribute RegisterVM registerVM) {
    User user = userService.registerUser(registerVM.getMobileNo(), registerVM.getPassword());
    String activationKey = user.getActivationKey();
    semaphoreService.sendMessage(user.getFormattedMobileNo(), activationKey);
  }

  @PostMapping(value = "/authenticate")
  public ResponseEntity<UserWithTokenVM> authorize(@Valid LoginVM loginVM) {
    User user = userService.loginByMobilNo(loginVM.getMobileNo(), loginVM.getPassword());

    String jwt = buildAuthenticationResponse(user);
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
    return new ResponseEntity<UserWithTokenVM>(new UserWithTokenVM(user, jwt), httpHeaders, HttpStatus.OK);
  }

  @PostMapping("/fbauth")
  public ResponseEntity<UserWithTokenVM> loginOrRegisterByFb(@Valid @RequestParam("accessToken") String accessToken) {
    SocialUserDTO socialUserDTO = facebookService.getFBUserProfile(accessToken);
    if (socialUserDTO == null) {
      throw new ApiException(springUtil.buildApiError(ApiErrorCode.AUTH_FB_PROFILE_NOT_FOUND));
    }

    User user = userService.findUserByFacebookId(socialUserDTO.getId());
    if (user == null) {
      user = userService.registerUser(socialUserDTO);
      if(Objects.nonNull(user.getEmail())){
        mailService.sendWelcomeEmail(user);
      }
    }

    String jwt = buildAuthenticationResponse(user);
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
    return new ResponseEntity<UserWithTokenVM>(new UserWithTokenVM(user, jwt), httpHeaders, HttpStatus.OK);
  }

  @PostMapping("/googleauth")
  public ResponseEntity<UserWithTokenVM> loginOrRegisterByGoogle(@Valid @RequestParam("accessToken") String accessToken) {
    SocialUserDTO socialUserDTO = googleService.getGoogleUserProfile(accessToken);
    if (socialUserDTO == null) {
      throw new ApiException(springUtil.buildApiError(ApiErrorCode.AUTH_GOOGLE_PROFILE_NOT_FOUND));
    }

    User user = userService.findUserByGoogleId(socialUserDTO.getId());
    if (user == null) {
      user = userService.registerUser(socialUserDTO);
      if(Objects.nonNull(user.getEmail())){
        mailService.sendWelcomeEmail(user);
      }
    }

    String jwt = buildAuthenticationResponse(user);
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
    return new ResponseEntity<UserWithTokenVM>(new UserWithTokenVM(user, jwt), httpHeaders, HttpStatus.OK);
  }

  private String buildAuthenticationResponse(User user) {
    Authentication authentication = new UsernamePasswordAuthenticationToken(user.getId(), null,
      authService.convertAuthority(user.getAuthorities())
    );
    return authService.buildJWTToken(authentication, true);
  }

  /**
   * {@code POST  /activate} : activate the registered user.
   *
   * @param mobileNo the mobileNumber.
   * @param key      the activation key.
   *
   * @throws RuntimeException {@code 500 (Internal Server Error)} if the user couldn't be activated.
   */
  @PostMapping("/activate")
  public ResponseEntity<UserWithTokenVM> activateAccount(
    @RequestParam(value = "mobileNo") String mobileNo,
    @RequestParam(value = "key") String key
  ) {
    Optional<User> user = userService.activateRegistration(mobileNo, key);
    if (!user.isPresent()) {
      throw new ApiException(new ApiError(ApiErrorCode.ACCOUNT_NOT_FOUND, "No user was found for this activation key"));
    }

    String jwt = buildAuthenticationResponse(user.get());
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
    return new ResponseEntity<UserWithTokenVM>(new UserWithTokenVM(user.get(), jwt), httpHeaders, HttpStatus.OK);
  }

  /**
   * {@code GET  /authenticate} : check if the user is authenticated, and return its login.
   *
   * @param request the HTTP request.
   *
   * @return the login if the user is authenticated.
   */
  @GetMapping("/authenticate")
  public String isAuthenticated(HttpServletRequest request) {
    log.debug("REST request to check if the current user is authenticated");
    return request.getRemoteUser();
  }

  /**
   * {@code GET  /account} : get the current user.
   *
   * @return the current user.
   *
   * @throws RuntimeException {@code 500 (Internal Server Error)} if the user couldn't be returned.
   */
  @GetMapping("/account")
  public UserDTO getAccount() {
    UserDTO dto = userService.getCurrentUserWithAuthorities()
      .map(UserDTO::new)
      .orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.ACCOUNT_NOT_FOUND)));

    dto.setUnreadNotifsCount(notificationService.countUnreadNotifications(dto.getId()));
    return dto;
  }

  /**
   * {@code POST  /account} : update the current user information.
   *
   * @param userDTO the current user information.
   *
   * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is already used.
   * @throws RuntimeException          {@code 500 (Internal Server Error)} if the user login wasn't found.
   */
  @PostMapping("/account")
  public UserDTO saveAccount(@Valid @ModelAttribute UserDTO userDTO) {
    return userService.updateLoggedInUser(userDTO);
  }

  /**
   * {@code POST  /account/change-password} : chan  ges the current user's password.
   *
   * @param vm current and new password.
   * @throws InvalidPasswordException {@code 400 (Bad Request)} if the new password is incorrect.
   */
    @PostMapping(path = "/account/change-password")
    public void changePassword(@Valid @ModelAttribute PasswordChangeVM vm) {
      userService.getCurrentUser()
        .filter(u -> Objects.isNull(u.getFbId()) && Objects.isNull(u.getGoogleId()))
        .orElseThrow(() ->  new ApiException(springUtil.buildApiError(ApiErrorCode.INVALID_FOR_FB_AND_GOOGLE)));

        userService.changePassword(vm.getCurrentPassword(), vm.getNewPassword());
    }

  /**
   * {@code POST   /account/reset-password/init} : Send an email to reset the password of the user.
   *
   * @param mobileNo the mobileNo of the user.
   */
    @PostMapping(path = "/account/reset-password/init")
    public Map<String, String> requestPasswordReset(@RequestParam("mobileNo") String mobileNo) {
      User user = userService.requestPasswordReset(mobileNo).orElse(null);
      if(Objects.nonNull(user)){
        semaphoreService.sendMessage(user.getFormattedMobileNo(), user.getActivationKey());
      } else{
        throw new ApiException(springUtil.buildApiError(ApiErrorCode.ACCOUNT_NOT_FOUND));
      }
      Map<String, String> map = new HashMap<>();
      map.put("key", user.getActivationKey());
      return map;
    }

  /**
   * {@code POST   /account/reset-password/finish} : Finish to reset the password of the user.
   *
   * @param keyAndPassword the generated key and the new password.
   *
   * @throws InvalidPasswordException {@code 400 (Bad Request)} if the password is incorrect.
   * @throws RuntimeException         {@code 500 (Internal Server Error)} if the password could not be reset.
   */
    @PostMapping(path = "/account/reset-password/finish")
    public ResponseEntity<UserWithTokenVM> finishPasswordReset(@Valid @ModelAttribute KeyAndPasswordVM keyAndPassword) {
      Optional<User> user = userService.completePasswordReset(keyAndPassword.getNewPassword(), keyAndPassword.getKey(), keyAndPassword.getMobileNo());
      if (!user.isPresent()) {
        throw new ApiException(springUtil.buildApiError(ApiErrorCode.ACCOUNT_NOT_FOUND));
      }

      String jwt = buildAuthenticationResponse(user.get());
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
      return new ResponseEntity<UserWithTokenVM>(new UserWithTokenVM(user.get(), jwt), httpHeaders, HttpStatus.OK);
    }

  @PostMapping(path = "/account/change-photo")
  public Map<String, String> changeAccountPhoto(@RequestParam("fileName") String fileName) {
    Map<String, String> map = new HashMap<>();
    fileUploadService.transferFromTempToUser(fileName);
    String imgUrl = Constants.USER_PHOTOS_URL+fileName;
    userService.updateLoggedInUserImgUrl(imgUrl);
    map.put("url", imgUrl);
    return map;
  }

  @PostMapping(value = "/fcm-token")
  public void saveFcmToken(@RequestParam("token")  String token, HttpServletRequest httpRequest) {
      String deviceId = WebUtil.extractDeviceId(httpRequest);
      if(StringUtils.isBlank(deviceId)){
        throw new ApiException(springUtil.buildApiError(ApiErrorCode.ACCOUNT_NOT_FOUND));
      }
      userAuthService.createUserAuth(deviceId, token);
  }

}
