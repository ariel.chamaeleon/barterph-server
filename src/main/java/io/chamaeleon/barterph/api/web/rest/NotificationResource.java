package io.chamaeleon.barterph.api.web.rest;

import io.chamaeleon.barterph.api.service.NotificationService;
import io.chamaeleon.barterph.api.service.dto.NotificationDTO;
import io.chamaeleon.barterph.api.service.dto.PagedList;
import io.chamaeleon.barterph.api.web.rest.vm.SearchNotificationVM;
import lombok.Data;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@Data
public class NotificationResource {

  private final NotificationService notificationService;

  @GetMapping("/notifications")
  public PagedList<NotificationDTO> searchNotifications(@ModelAttribute SearchNotificationVM vm){
    return notificationService.getNotifications(vm);
  }
}
