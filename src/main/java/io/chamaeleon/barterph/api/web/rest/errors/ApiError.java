package io.chamaeleon.barterph.api.web.rest.errors;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.chamaeleon.barterph.api.config.Constants;
import lombok.Data;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import javax.validation.ConstraintViolation;
import java.time.ZonedDateTime;
import java.util.*;

@Data
@JsonPropertyOrder(value = {"timestamp", "errorCode", "message", "debugMessage", "subErrors"})
public class ApiError {
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern= Constants.JSON_DATETIME_FORMAT)
	private ZonedDateTime timestamp;
	private String message;
	
	@JsonInclude(Include.NON_NULL)
	private String debugMessage;
	
	@JsonUnwrapped
	private ApiErrorCode errorCode;
	
	@JsonInclude(Include.NON_EMPTY)
	private List<ApiSubError> subErrors;
	
	private ApiError(){
		timestamp = ZonedDateTime.now();
	}

	public ApiError(ApiErrorCode errorCode){
		this();
		this.errorCode = errorCode;
	}

	public ApiError(ApiErrorCode errorCode, String message){
		this(errorCode);
		this.message = message;
	}

	public ApiError(ApiErrorCode errorCode, Throwable ex){
		this(errorCode, "Unexpected error.");
		this.debugMessage = ex.getLocalizedMessage();
	}

	public ApiError(ApiErrorCode errorCode, String message, Exception ex){
		this(errorCode);
		this.message = message;
		this.debugMessage = ex.getLocalizedMessage();
	}

	private void addSubError(ApiSubError subError){
		if(subErrors == null){
			subErrors = new ArrayList<ApiSubError>();
		}
		subErrors.add(subError);
	}
	
	private void addValidationError(String object, String field, Object rejectedValue, String message){
		addSubError(new ApiValidationError(object, field, rejectedValue, message));
	}
	
	private void addValidationError(String object, String message){
		addSubError(new ApiValidationError(object, message));
	}
	
	private void addValidationError(FieldError fieldError){
		this.addValidationError(fieldError.getObjectName(), fieldError.getField(), fieldError.getRejectedValue(), fieldError.getDefaultMessage());
	}
	
	public void addValidationErrors(List<FieldError> fieldErrors){
		fieldErrors.forEach(this::addValidationError);
	}
	
	private void addValidationError(ObjectError objectError){
		this.addValidationError(objectError.getObjectName(), objectError.getDefaultMessage());
	}
	
	public void addGlobalErrors(List<ObjectError> globalErrors){
		globalErrors.forEach(this::addValidationError);
	}
	
	private void addValidationError(ConstraintViolation<?> cv){
		this.addValidationError(cv.getRootBeanClass().getSimpleName(), 
				((PathImpl) cv.getPropertyPath()).getLeafNode().asString(),
				cv.getInvalidValue(),
				cv.getMessage());
	}
	
	public void addValidationErrors(Set<ConstraintViolation<?>> constraintViolations){
		constraintViolations.forEach(this::addValidationError);
	}
}
