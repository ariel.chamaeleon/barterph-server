package io.chamaeleon.barterph.api.web.rest.vm;

import io.chamaeleon.barterph.api.config.Constants;
import io.chamaeleon.barterph.api.service.dto.UserDTO;
import lombok.Data;

import javax.validation.constraints.Size;

/**
 * View Model extending the UserDTO, which is meant to be used in the user management UI.
 */
@Data
public class ManagedUserVM extends UserDTO {

    @Size(min = Constants.PASSWORD_MIN_LENGTH, max = Constants.PASSWORD_MAX_LENGTH)
    private String password;

    public ManagedUserVM() {
        // Empty constructor needed for Jackson.
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ManagedUserVM{" + super.toString() + "} ";
    }
}
