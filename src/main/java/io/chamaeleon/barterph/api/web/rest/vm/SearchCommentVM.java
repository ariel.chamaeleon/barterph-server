package io.chamaeleon.barterph.api.web.rest.vm;

import io.chamaeleon.barterph.api.service.dto.PageRequestDTO;
import lombok.Data;

@Data
public class SearchCommentVM extends PageRequestDTO {
  private Long userId;

  private Long barterId;

  private Long parentCommentId;

  private Long offerId;

  private boolean includeReplies;
}
