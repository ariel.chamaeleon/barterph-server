package io.chamaeleon.barterph.api.web.rest;

import io.chamaeleon.barterph.api.service.FileUploadService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class PhotoResource {

  private final Logger log = LoggerFactory.getLogger(PhotoResource.class);

  private final FileUploadService fileUploadService;

  @PostMapping("/photo/upload")
  public Map<String, String> uploadPhoto(@RequestParam("file") MultipartFile file) {
    Map<String, String> map = new LinkedHashMap<>();
    String tempFileName = fileUploadService.uploadImageToTemp(file);
    map.put("file", tempFileName);
    return map;
  }

  @GetMapping("/photo/{dir}/{fileBaseName}.{fileExt}")
  public ResponseEntity<InputStreamResource> getPhoto(@PathVariable("dir") String dir, @PathVariable("fileBaseName") String fileBase, @PathVariable("fileExt") String fileExt, @RequestParam(value="size", required=false) String size) {
    String baseNameAppend = StringUtils.isNoneBlank(size) ? "-"+size : null;
    FileUploadService.FileName fileName = new FileUploadService.FileName(fileBase+'.'+fileExt, baseNameAppend, true);
    InputStreamResource inputStreamResource = fileUploadService.readFile(dir, fileName);
    return ResponseEntity.ok().contentType(fileExt.equalsIgnoreCase("png") ? MediaType.IMAGE_PNG : MediaType.IMAGE_JPEG).body(inputStreamResource);
  }
}
