package io.chamaeleon.barterph.api.web.rest;

import io.chamaeleon.barterph.api.service.CategoryService;
import io.chamaeleon.barterph.api.service.dto.CategoryDTO;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@Data
public class CategoryResource {

  private final Logger log = LoggerFactory.getLogger(CategoryResource.class);

  private final CategoryService categoryService;

  @GetMapping("/categories")
  public ResponseEntity<List<CategoryDTO>> getCategories(@RequestParam(value = "profession", required = false) String profession){
    if(StringUtils.isEmpty(profession)){
      return ResponseEntity.ok(categoryService.getAllCategories());
    } else {
      return ResponseEntity.ok(categoryService.getAllByProfession(profession));
    }
  }
}
