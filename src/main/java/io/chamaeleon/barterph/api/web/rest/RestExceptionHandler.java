package io.chamaeleon.barterph.api.web.rest;

import io.chamaeleon.barterph.api.util.SpringUtil;
import io.chamaeleon.barterph.api.web.rest.errors.*;
import lombok.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.*;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Arrays;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

  private SpringUtil springUtil;
  private final Logger log = LoggerFactory.getLogger(RestExceptionHandler.class);

  @Override
  protected ResponseEntity<Object> handleMissingServletRequestParameter(
    MissingServletRequestParameterException ex,
    HttpHeaders headers, HttpStatus status, WebRequest request
  ) {
    String error = ex.getParameterName() + " parameter is missing";
    return buildResponseEntity(new ApiError(ApiErrorCode.MISSING_PARAMETER, error, ex));
  }

  @Override
  protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
    HttpMediaTypeNotSupportedException ex,
    HttpHeaders headers, HttpStatus status, WebRequest request
  ) {
    StringBuilder sb = new StringBuilder();
    sb.append(ex.getContentType());
    sb.append(" media type is not supported. Supported media types are ");
    ex.getSupportedMediaTypes().forEach(t -> sb.append(t).append(", "));
    return buildResponseEntity(new ApiError(ApiErrorCode.UNSUPPORTED_MEDIA_TYPE, sb.substring(0, sb.length() - 2), ex));
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
    MethodArgumentNotValidException ex,
    HttpHeaders headers, HttpStatus status, WebRequest request
  ) {
    ApiError apiError = new ApiError(ApiErrorCode.VALIDATION_ERROR);
    apiError.setMessage("Validation error.");
    apiError.addValidationErrors(ex.getBindingResult().getFieldErrors());
    apiError.addGlobalErrors(ex.getBindingResult().getGlobalErrors());
    return buildResponseEntity(apiError);
  }

  @Override
  protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
    HttpRequestMethodNotSupportedException ex,
    HttpHeaders headers, HttpStatus status, WebRequest request
  ) {
    StringBuilder sb = new StringBuilder();
    sb.append(ex.getMethod());
    sb.append(" is not supported. Supported methods are ");
    Arrays.stream(ex.getSupportedMethods()).forEach(t -> sb.append(t).append(", "));
    return buildResponseEntity(new ApiError(ApiErrorCode.METHOD_NOT_ALLOWED, sb.substring(0, sb.length() - 2), ex));
  }

  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(
    HttpMessageNotReadableException ex,
    HttpHeaders headers, HttpStatus status, WebRequest request
  ) {
    String error = "Malformed request.";
    return buildResponseEntity(new ApiError(ApiErrorCode.INVALID_OR_MISSING_REQUEST, error, ex));
  }


  @Override
  protected ResponseEntity<Object> handleNoHandlerFoundException(
    NoHandlerFoundException ex, HttpHeaders headers,
    HttpStatus status, WebRequest request
  ) {
    String message = ex.getRequestURL() + " not found.";
    return buildResponseEntity(new ApiError(ApiErrorCode.RESOURCE_NOT_FOUND, message, ex));
  }

  /**
   * Handle {@link ApiException}
   */
  @ExceptionHandler(ApiException.class)
  protected ResponseEntity<Object> handleApiException(ApiException ex) {
    ApiError error = ex.error;
    if (error.getMessage() == null) {
      error.setMessage(springUtil.getMessage("error." + error.getErrorCode().getCode(), null));
    }
    log.error("", ex);
    return buildResponseEntity(ex.error);
  }

  @ExceptionHandler(Exception.class)
  protected ResponseEntity<Object> handleException(Exception ex) {
    return buildResponseEntity(new ApiError(ApiErrorCode.INTERNAL_ERROR, "Unexpected error occured.", ex));
  }

  @Override
  protected ResponseEntity<Object> handleBindException(
    BindException ex, HttpHeaders headers, HttpStatus status,
    WebRequest request
  ) {
    ApiError apiError = new ApiError(ApiErrorCode.VALIDATION_ERROR);
    apiError.setMessage("Validation error.");
    apiError.addValidationErrors(ex.getBindingResult().getFieldErrors());
    apiError.addGlobalErrors(ex.getBindingResult().getGlobalErrors());
    return buildResponseEntity(apiError);
  }


  @Override
  protected ResponseEntity<Object> handleExceptionInternal(
    Exception ex, Object body, HttpHeaders headers,
    HttpStatus status, WebRequest request
  ) {
    return buildResponseEntity(new ApiError(ApiErrorCode.INTERNAL_ERROR, "Unexpected error occured.", ex));
  }

  private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
    return new ResponseEntity<Object>(apiError, apiError.getErrorCode().getHttpStatus());
  }
}
