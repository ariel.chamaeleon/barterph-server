package io.chamaeleon.barterph.api.web.rest.errors;

public class ApiException extends RuntimeException{
	
	private static final long serialVersionUID = -7396754843951291712L;
	
	public ApiError error;

	public ApiException(ApiError error) {
		super(error.getMessage(), null, true, false);
		this.error = error;
	}
	
	public ApiException(ApiError error, Throwable e) {
		super(error.getMessage(), e, true, false);
		this.error = error;
	}
}
