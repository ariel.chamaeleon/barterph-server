package io.chamaeleon.barterph.api.web.rest;

import io.chamaeleon.barterph.api.service.NotificationService;
import io.chamaeleon.barterph.api.service.OfferService;
import io.chamaeleon.barterph.api.service.dto.BarterOfferDTO;
import io.chamaeleon.barterph.api.service.dto.PagedList;
import io.chamaeleon.barterph.api.service.mapper.BarterOfferMapper;
import io.chamaeleon.barterph.api.util.SpringUtil;
import io.chamaeleon.barterph.api.web.rest.errors.ApiErrorCode;
import io.chamaeleon.barterph.api.web.rest.errors.ApiException;
import io.chamaeleon.barterph.api.web.rest.vm.AddOfferVM;
import io.chamaeleon.barterph.api.web.rest.vm.SearchOfferVM;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
@Data
public class OfferResource {

  private final Logger log = LoggerFactory.getLogger(OfferResource.class);

  private final OfferService offerService;

  private final NotificationService notificationService;

  private final SpringUtil springUtil;

  private final BarterOfferMapper barterOfferMapper;

  @Transactional
  @PostMapping("/offers/add")
  public ResponseEntity<BarterOfferDTO> addBarterOffer(@Valid @ModelAttribute AddOfferVM vm){
    return ResponseEntity.ok(offerService.saveBarterOffer(vm));
  }

  @GetMapping("/offers")
  public PagedList<BarterOfferDTO> searchBarterOffers(@ModelAttribute SearchOfferVM vm){
    return offerService.getOffers(vm);
  }

  @GetMapping("/offers/{id}")
  public BarterOfferDTO getCommentById(@PathVariable("id") Long id){
    return offerService.getOfferById(id)
      .map(c -> barterOfferMapper.toBarterOfferDTOWithReplies(c))
      .orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.RESOURCE_NOT_FOUND)));
  }

}
