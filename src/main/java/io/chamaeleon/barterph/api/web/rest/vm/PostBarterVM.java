package io.chamaeleon.barterph.api.web.rest.vm;

import io.chamaeleon.barterph.api.service.dto.LocationDTO;
import io.chamaeleon.barterph.api.util.BarterLocationType;
import io.chamaeleon.barterph.api.util.DeliveryType;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.*;
import java.util.List;

@Data
public class PostBarterVM {
  @NotBlank
  private String item;

  private String description;

  @NotNull
  private BarterLocationType locationType;

  @NotNull
  private LocationDTO location;

  @NotNull
  private DeliveryType deliveryType;

  private String courier;

  @NotBlank
  private String reason;

  private String preferences;

  @NotBlank
  private String contactNumber;

  private List<String> photos;

  private Long categoryId;

  @AssertFalse(message = "must not be empty")
  boolean isCourierEmpty(){
    return deliveryType == DeliveryType.COURIER && StringUtils.isBlank(courier);
  }

  @AssertFalse(message = "must not be empty")
  boolean isPhotosEmpty(){
    return CollectionUtils.isEmpty(photos);
  }

  @AssertFalse(message = "max photos exceeded")
  boolean isPhotosExceedLimit(){
    return !isPhotosEmpty() && photos.size() > 15;
  }
}
