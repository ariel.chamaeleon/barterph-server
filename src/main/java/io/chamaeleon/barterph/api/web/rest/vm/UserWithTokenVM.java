package io.chamaeleon.barterph.api.web.rest.vm;

import io.chamaeleon.barterph.api.domain.User;
import io.chamaeleon.barterph.api.service.dto.UserDTO;
import lombok.Data;

@Data
public class UserWithTokenVM extends UserDTO {
  private String token;

  public UserWithTokenVM(User user, String token){
    super(user);
    this.token = token;
  }
}
