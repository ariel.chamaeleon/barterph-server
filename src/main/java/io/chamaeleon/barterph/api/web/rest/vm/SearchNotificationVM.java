package io.chamaeleon.barterph.api.web.rest.vm;

import io.chamaeleon.barterph.api.service.dto.PageRequestDTO;
import lombok.Data;

@Data
public class SearchNotificationVM extends PageRequestDTO {
  private Long receiverId;
  private Long senderId;
}
