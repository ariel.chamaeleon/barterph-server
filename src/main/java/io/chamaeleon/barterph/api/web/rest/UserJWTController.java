package io.chamaeleon.barterph.api.web.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.chamaeleon.barterph.api.security.AuthService;
import io.chamaeleon.barterph.api.security.jwt.JWTFilter;
import io.chamaeleon.barterph.api.web.rest.vm.LoginVM;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.*;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class UserJWTController {

    private final AuthService authService;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    /*@PostMapping(value = "/authenticate")
    public ResponseEntity<JWTToken> authorize(@Valid LoginVM loginVM) {

        UsernamePasswordAuthenticationToken authenticationToken =
            new UsernamePasswordAuthenticationToken(loginVM.getMobileNo(), loginVM.getPassword());

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);

        String jwt = authService.buildJWTToken(authentication, loginVM.getRememberMe() == null ? false : loginVM.getRememberMe());

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<JWTToken>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
    }

    *//**
     * Object to return as body in JWT Authentication.
     *//*
    @Data
    static class JWTToken {

        @JsonProperty("token")
        private String idToken;

        JWTToken(String idToken) {
            this.idToken = idToken;
        }
    }*/
}
