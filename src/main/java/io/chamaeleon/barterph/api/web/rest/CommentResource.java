package io.chamaeleon.barterph.api.web.rest;

import io.chamaeleon.barterph.api.service.CommentService;
import io.chamaeleon.barterph.api.service.NotificationService;
import io.chamaeleon.barterph.api.service.dto.BarterCommentDTO;
import io.chamaeleon.barterph.api.service.dto.PagedList;
import io.chamaeleon.barterph.api.service.mapper.BarterCommentMapper;
import io.chamaeleon.barterph.api.util.SpringUtil;
import io.chamaeleon.barterph.api.web.rest.errors.ApiErrorCode;
import io.chamaeleon.barterph.api.web.rest.errors.ApiException;
import io.chamaeleon.barterph.api.web.rest.vm.AddCommentVM;
import io.chamaeleon.barterph.api.web.rest.vm.SearchCommentVM;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
@Data
public class CommentResource {

  private final Logger log = LoggerFactory.getLogger(CommentResource.class);

  private final CommentService commentService;

  private final NotificationService notificationService;

  private final SpringUtil springUtil;

  private final BarterCommentMapper barterCommentMapper;

  @Transactional
  @PostMapping("/comments/add")
  public ResponseEntity<BarterCommentDTO> addBarterComment(@Valid @ModelAttribute AddCommentVM vm){
    return ResponseEntity.ok(commentService.saveComment(vm));
  }

  @GetMapping("/comments")
  public PagedList<BarterCommentDTO> searchBarterComments(@ModelAttribute SearchCommentVM vm){
    return commentService.getComments(vm);
  }

  @Transactional
  @DeleteMapping("/comments")
  public void deleteBarterComment(@RequestParam("id") Long id){
    commentService.deleteComment(id);
  }

  @Transactional
  @PostMapping("/comments/update")
  public ResponseEntity<BarterCommentDTO> updateBarterComment(@RequestParam("id") Long id, @RequestParam("comment") String commentStr){
    return ResponseEntity.ok(commentService.updateComment(id, commentStr));
  }

  @GetMapping("/comments/{id}")
  public BarterCommentDTO getCommentById(@PathVariable("id") Long id){
    return commentService.getCommentById(id)
        .map(c -> barterCommentMapper.toBarterCommentDTOWithReplies(c))
        .orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.RESOURCE_NOT_FOUND)));
  }
}
