package io.chamaeleon.barterph.api.web.rest.vm;

import io.chamaeleon.barterph.api.service.dto.PageRequestDTO;
import lombok.Data;

@Data
public class SearchOfferVM extends PageRequestDTO {
  private Long userId;

  private Long barterId;

  private boolean includeReplies;
}
