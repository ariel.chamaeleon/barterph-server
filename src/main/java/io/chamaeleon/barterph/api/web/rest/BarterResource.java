package io.chamaeleon.barterph.api.web.rest;

import io.chamaeleon.barterph.api.service.*;
import io.chamaeleon.barterph.api.service.dto.BarterDTO;
import io.chamaeleon.barterph.api.service.dto.PagedList;
import io.chamaeleon.barterph.api.service.mapper.BarterMapper;
import io.chamaeleon.barterph.api.util.SpringUtil;
import io.chamaeleon.barterph.api.web.rest.errors.ApiErrorCode;
import io.chamaeleon.barterph.api.web.rest.errors.ApiException;
import io.chamaeleon.barterph.api.web.rest.vm.*;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class BarterResource {

  private final Logger log = LoggerFactory.getLogger(BarterResource.class);

  private final BarterService barterService;

  private final FileUploadService fileUploadService;

  private final SpringUtil springUtil;

  private final BarterMapper barterMapper;

  private final BarterViewService barterViewService;

  private final UserService userService;

  private final OfferService offerService;

  private final CommentService commentService;

  @Transactional
  @PostMapping("/barters")
  public ResponseEntity<BarterDTO> createBarter(@Valid @ModelAttribute PostBarterVM input){
    BarterDTO newBarter = barterService.createBarter(input)
      .orElseThrow(() ->  new ApiException(springUtil.buildApiError(ApiErrorCode.OPERATION_FAILED)));
    return ResponseEntity.ok(newBarter);
  }

  @Transactional
  @PostMapping("/barters/cancel")
  public void cancelBarter(@RequestParam("id") Long id){
    barterService.cancelBarter(id);
  }

  @Transactional
  @PostMapping("/barters/close")
  public void closeBarter(@ModelAttribute @Valid CloseBarterVM vm){
    barterService.closeBarter(vm);
  }

  @Transactional
  @PostMapping("/barters/update")
  public void updateBarter(@ModelAttribute @Valid UpdateBarterVM vm){
    barterService.updateBarter(vm);
  }

  @Transactional
  @PostMapping("/barters/repost")
  public void repostBarter(@RequestParam Long id){
    barterService.repostBarter(id);
  }

  @Transactional
  @PostMapping("/barters/remind-settings")
  public void repostBarter(@RequestParam(value = "id") Long id, @RequestParam(value="remindInDays", defaultValue = "0") int noOfDays){
    barterService.setBarterReminderSettings(id, noOfDays);
  }

  @GetMapping("/barters/{id}")
  public BarterDTO getBarterById(@PathVariable("id") Long id){
    return barterService.findOneById(id)
      .map(b -> {
        barterViewService.addNewView(b, userService.getCurrentUser().orElse(null));
        BarterDTO dto = barterMapper.toBarterDTO(b);
        dto.setOfferCount(offerService.countOfferByBarterId(b.getId()));
        dto.setCommentCount(commentService.countByBarterIdAndParentCommentId(b.getId(), null));
        return dto;
      })
      .orElseThrow(() -> new ApiException(springUtil.buildApiError(ApiErrorCode.BARTER_NOT_FOUND)));
  }

  @GetMapping("/barters")
  public PagedList<BarterDTO> searchBarters(@ModelAttribute SearchBarterVM vm){
    return barterService.getBarters(vm);
  }
}
