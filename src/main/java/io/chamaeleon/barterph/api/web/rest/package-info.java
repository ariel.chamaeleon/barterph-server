/**
 * Spring MVC REST controllers.
 */
package io.chamaeleon.barterph.api.web.rest;
