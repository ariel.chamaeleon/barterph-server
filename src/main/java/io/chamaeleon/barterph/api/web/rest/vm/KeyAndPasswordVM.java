package io.chamaeleon.barterph.api.web.rest.vm;

import io.chamaeleon.barterph.api.config.Constants;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * View Model object for storing the user's key and password.
 */
@Data
public class KeyAndPasswordVM {
    @NotNull
    private String mobileNo;

    @NotNull
    private String key;

    @NotNull
    @Size(min = Constants.PASSWORD_MIN_LENGTH, max = Constants.PASSWORD_MAX_LENGTH)
    private String newPassword;
}
