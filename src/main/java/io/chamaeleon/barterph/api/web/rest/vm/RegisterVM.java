package io.chamaeleon.barterph.api.web.rest.vm;

import io.chamaeleon.barterph.api.config.Constants;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class RegisterVM {

  @NotNull
  @Size(max = 20)
  private String mobileNo;

  @NotNull
  @Size(min = Constants.PASSWORD_MIN_LENGTH, max = Constants.PASSWORD_MAX_LENGTH)
  private String password;
}
