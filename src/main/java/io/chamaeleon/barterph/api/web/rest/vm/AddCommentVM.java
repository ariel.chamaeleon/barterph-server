package io.chamaeleon.barterph.api.web.rest.vm;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AddCommentVM {
  @NotNull
  private Long barterId;

  @NotNull
  private String comment;

  private Long parentCommentId;

  private Long offerId;
}
