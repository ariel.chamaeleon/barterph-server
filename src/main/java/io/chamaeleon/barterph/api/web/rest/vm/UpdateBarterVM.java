package io.chamaeleon.barterph.api.web.rest.vm;

import io.chamaeleon.barterph.api.service.dto.LocationDTO;
import io.chamaeleon.barterph.api.util.BarterLocationType;
import io.chamaeleon.barterph.api.util.DeliveryType;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class UpdateBarterVM{

  @NotNull
  private Long id;

  private String item;

  private String description;

  private BarterLocationType locationType;

  private LocationDTO location;

  private DeliveryType deliveryType;

  private String courier;

  private String reason;

  private String preferences;

  private String contactNumber;

  private List<String> photos;
}
