package io.chamaeleon.barterph.api.web.rest.vm;

import io.chamaeleon.barterph.api.domain.User;
import lombok.Data;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

@Data
public class CloseBarterVM {

  @NotNull
  private Long id;

  private Long tradedTo;

  private String tradedFor;

}
