package io.chamaeleon.barterph.api.web.rest.vm;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class AddOfferVM {
  @NotNull
  private Long barterId;

  //@NotNull
  private String item;

  @Deprecated
  private String note;

  private String description;

  private List<String> photos;
}
