package io.chamaeleon.barterph.api.web.rest.vm;

import io.chamaeleon.barterph.api.service.dto.PageRequestDTO;
import io.chamaeleon.barterph.api.util.BarterStatus;
import lombok.Data;

import java.util.Objects;

@Data
public class SearchBarterVM extends PageRequestDTO {
  private String key;
  private Long userId;
  private BarterStatus[] status;
  private Integer withOffers;
  private Integer withComments;

  public Boolean withCommentsToBoolean(){
    if(Objects.equals(withComments, 0)){
      return false;
    }
    if(Objects.equals(withComments, 1)){
      return true;
    }
    return null;
  }

  public Boolean withOffersToBoolean(){
    if(Objects.equals(withOffers, 0)){
      return false;
    }
    if(Objects.equals(withOffers, 1)){
      return true;
    }
    return null;
  }
}
