package io.chamaeleon.barterph.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A user.
 */

@Entity
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Builder
@Getter
@Setter
@EqualsAndHashCode(exclude = {"barters", "authorities", "location"})
public class User extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @NotNull
    @Size(min = 60, max = 60)
    @Column(name = "password_hash", length = 60, nullable = false)
    private String password;

    @Size(max = 50)
    @Column(name = "first_name", length = 50)
    private String firstName;

    @Size(max = 50)
    @Column(name = "last_name", length = 50)
    private String lastName;

    @Size(max = 50)
    @Column(name = "middle_name", length = 50)
    private String middleName;

    @Email
    @Size(min = 5, max = 254)
    @Column(length = 254, unique = true)
    private String email;

    @Size(max = 20)
    @Column(name="mobile_no", length = 20, unique = true)
    private String mobileNo;

    @NotNull
    @Column(nullable = false)
    private boolean activated = false;

    @Column(name="activation_key", length = 6)
    private String activationKey;

    @Column(name="activation_date")
    private LocalDateTime activationDate;

    @Size(max = 256)
    @Column(name = "image_url", length = 256)
    private String imageUrl;

    @Size(max=50)
    @Column(name = "fb_id", length = 50)
    private String fbId;

    @Size(max=50)
    @Column(name = "google_id", length = 50)
    private String googleId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", referencedColumnName = "id")
    private Location location;

    @NotNull
    @Column(nullable = false)
    private boolean deleted;

    @Column(name="profession", length = 100)
    private String profession;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
        name = "user_authority",
        joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "authority_name", referencedColumnName = "name")})
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @BatchSize(size = 20)
    private Set<Authority> authorities = new HashSet<>();

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private Set<Barter> barters;

    public String getFormattedMobileNo(){
        return "+"+this.mobileNo;
    }

    public static User createEmptyInstance(){
        User user = User.builder().build();
        user.setCreatedDate(null);
        user.setLastModifiedDate(null);
        return user;
    }

    public String getDisplayName() { return firstName +" "+lastName; }
}
