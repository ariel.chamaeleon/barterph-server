package io.chamaeleon.barterph.api.domain;

import io.chamaeleon.barterph.api.util.NotificationType;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "notification")
public class Notification implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Enumerated(EnumType.STRING)
  @Column(name="type", columnDefinition = "VARCHAR(30) NOT NULL")
  private NotificationType type;

  @Column(name="date_posted")
  private LocalDateTime datePosted;

  @ManyToOne
  @JoinColumn(name="receiver_id", referencedColumnName = "id")
  private User receiver;

  @ManyToOne
  @JoinColumn(name="sender_id", referencedColumnName = "id")
  private User sender;

  @ManyToOne
  @JoinColumn(name="barter_id", referencedColumnName = "id")
  private Barter barter;

  @ManyToOne
  @JoinColumn(name="barter_comment_id", referencedColumnName = "id")
  private BarterComment barterComment;

  @ManyToOne
  @JoinColumn(name="barter_offer_id", referencedColumnName = "id")
  private BarterOffer barterOffer;

  @Column(name="is_read", columnDefinition = "BIT(1) NOT NULL DEFAULT 0")
  private boolean isRead;
}
