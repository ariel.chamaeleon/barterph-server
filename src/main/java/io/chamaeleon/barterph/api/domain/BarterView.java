package io.chamaeleon.barterph.api.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@Entity
@Table(name = "barter_view")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class BarterView implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "viewer_id", referencedColumnName = "id")
  private User viewer;

  @ManyToOne
  @JoinColumn(name = "barter_id", referencedColumnName = "id")
  private Barter barter;

  @Column(name="date_viewed")
  private LocalDateTime dateViewed;
}
