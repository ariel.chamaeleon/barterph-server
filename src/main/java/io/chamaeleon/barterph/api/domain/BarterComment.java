package io.chamaeleon.barterph.api.domain;

import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "barter_comment")
public class BarterComment extends AbstractAuditingEntity implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "owner_id", referencedColumnName = "id")
  private User owner;

  @ManyToOne
  @JoinColumn(name = "barter_id", referencedColumnName = "id")
  private Barter barter;

  @Column(name="description", columnDefinition = "TEXT")
  private String comment;

  @ToString.Exclude
  @ManyToOne
  @JoinColumn(name="parent_comment_id", referencedColumnName = "id")
  private BarterComment parentComment;

  @ToString.Exclude
  @OneToMany(mappedBy = "parentComment")
  @Where(clause = "is_deleted = false")
  private List<BarterComment> replies;

  @ToString.Exclude
  @ManyToOne
  @JoinColumn(name="offer_id", referencedColumnName = "id")
  private BarterOffer offer;

  @Column(name="is_deleted", columnDefinition = "BIT NOT NULL DEFAULT 0")
  private boolean isDeleted;

  @Column(name="date_deleted")
  private LocalDateTime dateDeleted;

  @Column(name="last_update_date")
  private LocalDateTime lastUpdateDate;
}
