package io.chamaeleon.barterph.api.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "user_auth")
public class UserAuth extends AbstractAuditingEntity implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @JoinColumn(referencedColumnName = "id", name="user_id")
  private User user;

  @Column(name="auth_token")
  private String authToken;

  @Column(name="fcm_token")
  private String fcmToken;

  @Column(name="device_id")
  private String deviceId;
}
