package io.chamaeleon.barterph.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import java.io.Serializable;
import java.time.LocalDateTime;


@Data
@EntityListeners(AuditingEntityListener.class)
public class UserActivity extends AbstractAuditingEntity implements Serializable {

  @CreatedBy
  @Column(name = "done_by", nullable = false, length = 50, updatable = false)
  @JsonIgnore
  private String doneBy;

  @CreatedDate
  @Column(name = "activity_date", updatable = false)
  @JsonIgnore
  private LocalDateTime date = LocalDateTime.now();

}
