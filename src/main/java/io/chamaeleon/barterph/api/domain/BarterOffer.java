package io.chamaeleon.barterph.api.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "barter_offer")
@ToString(of = {"id", "item"})
public class BarterOffer extends AbstractAuditingEntity implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "owner_id", referencedColumnName = "id")
  private User owner;

  @ManyToOne
  @JoinColumn(name = "barter_id", referencedColumnName = "id")
  private Barter barter;

  @Column(name="item", columnDefinition = "TEXT")
  private String item;

  @Column(name="description", columnDefinition = "TEXT")
  private String description;

  @OneToMany(mappedBy = "offer")
  private List<BarterComment> replies;

  @OneToMany(mappedBy = "offer", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  private List<OfferPhoto> photos;
}
