package io.chamaeleon.barterph.api.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name="location")
public class Location implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Size(max=256)
  @Column(name="location")
  private String location;

  @Column(name="house_no", length = 10)
  private String houseNo;

  @Column(name="street", length = 50)
  private String street;

  @Column(name="barangay", length = 50)
  private String barangay;

  @Column(name="longitude")
  private Long longitude;

  @Column(name="latitude")
  private Long latitude;

  @Column(name="country")
  private String country;

  @Column(name="province")
  private String province;

  @Column(name="city")
  private String city;
}
