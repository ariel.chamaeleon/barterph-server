package io.chamaeleon.barterph.api.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder(toBuilder = true)
@Entity
@Table(name = "offer_photo")
@AllArgsConstructor
@EqualsAndHashCode(of = {"id", "photoUrl"})
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class OfferPhoto implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "photo_url", length = 100, nullable = false)
  private String photoUrl;

  @ManyToOne
  @JoinColumn(name = "offer_id", referencedColumnName = "id")
  private BarterOffer offer;
}
