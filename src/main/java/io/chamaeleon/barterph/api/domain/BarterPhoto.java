package io.chamaeleon.barterph.api.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@Entity
@Table(name = "barter_photo")
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode(of = {"id", "photoUrl"})
public class BarterPhoto implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name="photo_url", length = 100, nullable = false)
  private String photoUrl;

  @ManyToOne
  @JoinColumn(name="barter_id", referencedColumnName = "id")
  private Barter barter;
}
