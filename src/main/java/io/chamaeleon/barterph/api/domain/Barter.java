package io.chamaeleon.barterph.api.domain;

import io.chamaeleon.barterph.api.util.*;
import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(exclude = {"user", "location", "deliveryLocation", "transferTo"})
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "barter")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Barter extends AbstractAuditingEntity implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id", referencedColumnName = "id")
  private User user;

  @Column(name="item", columnDefinition = "VARCHAR(100) NOT NULL")
  private String item;

  @Column(name="description", columnDefinition = "TEXT")
  private String description;

  @Enumerated(EnumType.STRING)
  @Column(name="location_type")
  private BarterLocationType locationType;

  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "location_id", referencedColumnName = "id")
  private Location location;

  @Enumerated(EnumType.STRING)
  @Column(name="delivery_type")
  private DeliveryType deliveryType;

  @Column(name="courier", length = 30)
  private String courier;

  @Column(name="reason", columnDefinition = "TEXT")
  private String reason;

  @Column(name="preferences", columnDefinition = "TEXT")
  private String preferences;

  @Column(name="contact", columnDefinition = "VARCHAR(20) NOT NULL")
  private String contactNumber;

  @Column(name="contact2", columnDefinition = "VARCHAR(20)")
  private String contactNumber2;

  @Column(name="date_posted")
  private LocalDateTime datePosted;

  @Enumerated(EnumType.STRING)
  @Column(name="status")
  private BarterStatus status;

  @Column(name="last_date_updated")
  private LocalDateTime lastUpdateDate;

  @Column(name="traded_for", columnDefinition = "TEXT")
  private String tradedFor;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name="traded_to_id", referencedColumnName = "id")
  private User tradedTo;

  @OneToMany(mappedBy = "barter", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
  private List<BarterPhoto> barterPhotos;

  @OneToMany(mappedBy = "barter", fetch = FetchType.LAZY,  cascade = CascadeType.ALL, orphanRemoval = true)
  private List<BarterTag> barterTags;

  @Column(name="view_count", columnDefinition = "BIGINT NOT NULL DEFAULT 0")
  private long viewCount;

  @Column(name="remind_in_days")
  private int remindInDays;

  @Column(name="last_remind_date")
  private LocalDateTime lastRemindDate;

  @Column(name="next_remind_date")
  private LocalDateTime nextRemindDate;

  @OneToMany(mappedBy = "barter")
  @Where(clause = "is_deleted = false")
  private List<BarterComment> comments;

  @Column(name="date_reposted")
  private LocalDateTime dateReposted;

  @JoinColumn(name="category_id", referencedColumnName = "id")
  private Category category;
}
