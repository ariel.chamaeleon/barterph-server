package io.chamaeleon.barterph.api.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@Builder
@Entity
@Table(name = "barter_tag")
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BarterTag implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Size(max = 30)
  @NotNull
  @Column(name="tag", nullable = false, length = 30)
  private String tag;

  @ManyToOne
  @JoinColumn(name="barter_id", referencedColumnName = "id")
  private Barter barter;
}
