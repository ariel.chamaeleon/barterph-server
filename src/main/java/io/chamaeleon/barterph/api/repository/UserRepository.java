package io.chamaeleon.barterph.api.repository;

import io.chamaeleon.barterph.api.domain.User;

import org.hibernate.annotations.Cache;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.time.Instant;

/**
 * Spring Data JPA repository for the {@link User} entity.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    String USERS_BY_EMAIL_CACHE = "usersByEmail";

    Optional<User> findOneByMobileNoAndActivationKey(String mobileNo, String activationKey);

    List<User> findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(LocalDateTime dateTime);

    Optional<User> findOneById(Long id);

    Optional<User> findOneByMobileNo(String mobileNo);

    Optional<User> findOneByEmail(String email);

    Optional<User> findOneByFbId(String fbId);

    Optional<User> findOneByGoogleId(String googleId);

    @EntityGraph(attributePaths = "authorities")
    Optional<User> findOneWithAuthoritiesByMobileNo(String mobileNo);

    @EntityGraph(attributePaths = "authorities")
    Optional<User> findOneWithAuthoritiesById(Long id);
}
