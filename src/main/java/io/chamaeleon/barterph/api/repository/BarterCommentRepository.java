package io.chamaeleon.barterph.api.repository;

import io.chamaeleon.barterph.api.domain.BarterComment;
import org.checkerframework.common.util.report.qual.ReportCreation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

@ReportCreation
public interface BarterCommentRepository extends JpaRepository<BarterComment, Long>, JpaSpecificationExecutor<BarterComment> {
  long countByBarterIdAndParentCommentIdAndOfferIdAndIsDeleted(long barterId, Long parentCommentId, Long offerId, boolean isDeleted);
}
