package io.chamaeleon.barterph.api.repository;

import io.chamaeleon.barterph.api.domain.UserAuth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserAuthRepository extends JpaRepository<UserAuth, Long> {
  Optional<UserAuth> findOneByDeviceIdAndUserId(String deviceId, long userId);

  List<UserAuth> findAllByUserId(long userId);
}
