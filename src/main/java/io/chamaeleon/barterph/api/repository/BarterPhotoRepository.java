package io.chamaeleon.barterph.api.repository;

import io.chamaeleon.barterph.api.domain.Barter;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BarterPhotoRepository extends JpaRepository<Barter, Long> {

}
