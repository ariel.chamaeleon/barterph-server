package io.chamaeleon.barterph.api.repository;

import io.chamaeleon.barterph.api.domain.BarterView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BarterViewRepository extends JpaRepository<BarterView, Long> {
}
