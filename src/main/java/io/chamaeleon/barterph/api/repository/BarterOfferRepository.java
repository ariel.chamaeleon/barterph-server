package io.chamaeleon.barterph.api.repository;

import io.chamaeleon.barterph.api.domain.BarterOffer;
import org.checkerframework.common.util.report.qual.ReportCreation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

@ReportCreation
public interface BarterOfferRepository extends JpaRepository<BarterOffer, Long>, JpaSpecificationExecutor<BarterOffer> {
  long countByBarterId(long barterId);
}
