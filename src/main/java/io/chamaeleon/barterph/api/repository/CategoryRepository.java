package io.chamaeleon.barterph.api.repository;

import io.chamaeleon.barterph.api.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
  public List<Category> findAllByProfession(String profession);
}
