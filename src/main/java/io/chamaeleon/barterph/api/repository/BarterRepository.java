package io.chamaeleon.barterph.api.repository;

import io.chamaeleon.barterph.api.domain.Barter;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface BarterRepository extends JpaRepository<Barter, Long>, JpaSpecificationExecutor<Barter> {

  @EntityGraph(attributePaths = "barterPhotos")
  Optional<Barter> findOneWithBarterPhotosById(Long id);

}
