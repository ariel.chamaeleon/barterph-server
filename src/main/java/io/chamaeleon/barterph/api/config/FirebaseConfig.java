package io.chamaeleon.barterph.api.config;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.io.InputStream;

@Configuration
public class FirebaseConfig {


  private static final Logger log = LoggerFactory.getLogger(FirebaseConfig.class);

  @Bean
  public DatabaseReference firebaseDatabase(){
    return FirebaseDatabase.getInstance().getReference();
  }

  @Value("${firebase.dbname}")
  private String databaseName;

  @Value("${firebase.config}")
  private String configFile;

  @PostConstruct
  public void init(){
    try {
      System.out.println(configFile);
      InputStream inputStream = new ClassPathResource(configFile).getInputStream();

      FirebaseOptions options = new FirebaseOptions.Builder()
        .setCredentials(GoogleCredentials.fromStream(inputStream))
        .setDatabaseUrl("https://"+databaseName+".firebaseio.com/")
        .build();

      FirebaseApp.initializeApp(options);
    } catch (IOException e) {
      log.error("Firebase initialize error", e);
    }
  }

  @PreDestroy
  public void destroy(){
    if(FirebaseApp.getInstance() != null)
      FirebaseApp.getInstance().delete();
  }

}
