package io.chamaeleon.barterph.api.config;

/**
 * Application constants.
 */
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^(?>[a-zA-Z0-9!$&*+=?^_`{|}~.-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*)|(?>[_.@A-Za-z0-9-]+)$";

    public static final String SYSTEM_ACCOUNT = "system";
    public static final String DEFAULT_LANGUAGE = "en";
    public static final String ANONYMOUS_USER = "anonymoususer";

    public static final String HTTP_CONTENT_TYPE_JSON = "application/json;charset=UTF-8";
    public static final String HTTP_HEADER_CONTENT_TYPE = "Content-Type";
    public static final String HTTP_HEADER_WWW_AUTHENTICATE = "WWW-Authenticate";
    public static final String HTTP_HEADER_BASIC_REALM = "Basic realm=";
    public static final String REALM_NAME = "Barter PH";

    public static final String JSON_DATE_FORMAT= "yyyy-MM-dd";
    public static final String JSON_DATETIME_FORMAT= "yyyy-MM-dd HH:mm:ss";
    public static final String BINDER_DATETIME_FORMAT = "MM/dd/yyyy HH:mm:ss";

    public static final int PASSWORD_MIN_LENGTH = 4;
    public static final int PASSWORD_MAX_LENGTH = 100;

    public static final String USER_PHOTOS_URL = "photo/user/";
    public static final String BARTER_PHOTOS_URL = "photo/barter/";
    public static final String OFFER_PHOTOS_URL = "photo/offer/";


    public static final String DEFAULT_COUNTRY = "Philippines";
    public static final int DEFAULT_REMIND_IN_DAYS = 7;
    private Constants() {
    }
}
